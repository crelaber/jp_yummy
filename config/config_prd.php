<?php

// 系统配置
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'sys_config.php';

// 数据库表
include dirname(__FILE__) . DIRECTORY_SEPARATOR . 'tables.php';

// 微信公众号AppId
define("APPID", "wx0404a4b543bf52d0");

// 微信公众号AppSecret
define("APPSECRET", "f62f597a8a8a241316a3b47ed25bdbc8");

// 微信公众号通讯AESKey
define('EncodingAESKey', 'QiurKEJpWouQDM7IEq1sPDqyT1LMuILflGlTJJ5nzI7');

// 微信公众号验证TOKEN
define("TOKEN", "o4g13z4aifekd0xxm2peuqspc5fwlcpn");

// <微信支付> 商户ID(partnerId)
define("PARTNER", "1269117801"); 

// <微信支付> 商户通加密串(partnerKey)
define("PARTNERKEY", "1234567890qwertyuiopasdfghjklzxc");

// <微信支付> CA证书 .pem文件
define('CERT_PATH',  dirname(__FILE__) . DIRECTORY_SEPARATOR . "apiclient_cert.pem");

// <微信支付> CA证书 .pem文件
define('CERT_KEY_PATH',  dirname(__FILE__) . DIRECTORY_SEPARATOR . "apiclient_key.pem");

// <微信支付> CA证书 .pem文件
define('CERT_ROOTCA',  dirname(__FILE__) . DIRECTORY_SEPARATOR . "rootca.pem");

$config->debug = true;

$config->db['host'] = '127.0.0.1';

$config->db['db'] = 'cheerslife';

$config->db['user'] = 'root';

$config->db['pass'] = '';

// 是否已经通过微信认证
$config->wechatVerifyed = true;

// 废弃，不删除
$config->useWechatAddr = true;

// 是否打开memcached缓存,需要php扩展memcached
$config->memcached['on'] = false;

// memcached Host
$config->memcached['host'] = '127.0.0.1';

// memcached Host
$config->memcached['port'] = 3306;

// memcached 过期时间 秒
$config->memcached['exps'] = 2;

// 是否打开Smarty缓存
$config->Smarty['cached'] = false;

// Smarty缓存时间间隔
$config->Smarty['cache_lifetime'] = 2;

// 系统根目录
$config->shoproot = '/';

// 系统根域名 /结尾
$config->domain = 'http://cheerslife.dev.com/';

// 不需要微信支付直接下单 测试用
$config->order_nopayment = FALSE;

// 微信支付回调
$config->order_wxpay_notify = $config->domain . "wxpay_notify.php";
// 微信支付回调 代付
$config->order_wxpay_notify_req = $config->domain . "wxpay_notify_req.php";

// sendCloud邮件群发 废弃
$config->mail['API_KEY'] = '0Qz3I5NPMsh6hGMF';

$config->mail['server'] = "smtp.exmail.qq.com";

$config->mail['account'] = "noreply@iwshop.cn";

$config->mail['formAddress'] = "noreply@noreply.iwshop.cn";

$config->mail['password'] = "A919161a";

$config->mail['port'] = 25;

// 订单收到提示模板消息
$config->messageTpl['new_order_notify'] = 'uk0iYAwnR1uP5_YnR55u0rVlL1OtdajRVH_P2BYNqrA';
$config->messageTpl['user_order_notify'] = 'ZGXzXSREvlf2lS9gBrV9o7BE3wR4NzQomNeTjVNoPoc';

// 代理审核通过模板消息
$config->messageTpl['company_reg_notify'] = '0EeaE15LDIg-LJc0JKFDYgOVfFZgK4hPwGfN9S13-qM';

// 订单发货模板消息
$config->messageTpl['order_exp_notify'] = 'ds5sXHkdNYW7dDqjuDiL5b47wmWGmK6KopXPuyjEt3U';

$config->messageTpl['cash_pay_notify']='ZGXzXSREvlf2lS9gBrV9o7BE3wR4NzQomNeTjVNoPoc';
//
$config->messageTpl['cash_work_notify']='Xwv8YNPlppmZnyY-cWsp_bQa-nLKXdgnMXu1kEbiOC4';
// shop discount notify template
$config->messageTpl['settlement_notify'] = 'CdeaLI8tk4HE1DQTbId-G4rdzcHTegeW5OG6ZQLRKBk';

$config->messageTpl['fans_buy_notify'] = 'M93qxdbidhrsbYz9W5C6CH26LrG3JyYLPUuAgdO0jks';
$config->messageTpl['new_fans_notify'] = 'VoucIoQDUniG52TbHcakf5CI7uyQ2fRCdnPxlWszHOg';
$config->messageTpl['fans_crash_notify'] = 'x7f2KpiI5JMf9XpWxfcq4H2Xv6HidRuEfZEIWr1S-cE';

$config->messageTpl['express_send'] = 'nLuuKVigmfBL-jNxoA13XJGIyqvazoy_4HQF8N6Ch4s';


//拼团成功
$config->messageTpl['pin_success'] = 'A0mbG3av4dJxl9pKGF6b7X8Jqb9wsyFa9buk0NvVEYo';
//拼团失败
$config->messageTpl['pin_fail'] = '_xE_AWGFpp0tmnbu7EyZ7aIToLVUHICyhtwBF9LhvOw';
$config->pt_id = 124;

$config->imagesSuffix50 = '_x50';

$config->imagesSuffix100 = '_x100';

$config->imagesSuffix500 = '_x500';

$config->cssversion = '1.6.3';

$config->shopName = 'CheersLife';

// email使用sendcloud
$config->sendCloudOn = true;

// upyun 目录
$config->upyunDir = '=';

// upyun bucket
$config->upyunBucket = '';

// upyun operator
$config->upyunOperator = '';

// upyun password
$config->upyunPassword = '';

// upyun 商品图片前缀
$config->imagesPrefix = 'http://iwshop.b0.upaiyun.com/' . $config->upyunDir;

// 商品图片目录
$config->productPicRoot = dirname(__FILE__) . "/../uploads/product_hpic/";

// 商品图片临时目录
$config->productPicRootTmp = dirname(__FILE__) . "/../uploads/product_hpic_tmp/";

// 商品外链目录
$config->productPicLink = $config->shoproot . "uploads/product_hpic/";

// 商品外链预览
$config->productPicLinkTmp = $config->shoproot . "uploads/product_hpic_tmp/";

// 图片云 商品图片目录
$config->cdnProductPicRoot = '/' . $config->upyunDir . '/product_hpic/';

// 优惠券图片临时目录
$config->couponPicRoot = dirname(__FILE__) . "/../uploads/coupon/";

// 优惠券外链预览
$config->couponPicLink = $config->shoproot . "uploads/coupon/";

$config->codePicLink = "/data/wwwroot/prod/yummy/uploads/code/";

// 是否启用upyun CDN
$config->usecdn = false;

// 商品Id前缀
$config->out_trade_no_prefix = 'ord-';

// redis缓存服务器配置
$config->redis_on = false;
// redis host
$config->redis_host = '6634c8456e5f4d9c.redis.rds.aliyuncs.com';
// redis 端口
$config->redis_port = 6379;
// redis auth
$config->auth = 'QiEzI20160823';

//redis数据库编号
$config->redis_index = 14;

$config->redis_exps = 15;


$config->activity_title = '儿童节活动';

//token接口地址
$config->token_api_url = 'http://sp.qiezilife.com';
//获取token的接口path
$config->generate_token_api_path = '/api/developer/authirization';
//验证token的接口path
$config->valid_token_api_path = '/api/developer/token/validate';

//体检套餐签名的秘钥
$config->health_package_sign_secret = 'bc3188970a3c55c5a1de';
//合法的套餐类型
$config->valid_health_package_type = [300,500,800,1000];
//体检套餐购买成功后，返回的中英首页的链接
$config->health_package_pay_success_url = 'http://wh.aviva-cofco.com.cn/WX/redirect/entry.do?urlId=zxfwrk';

//中英的回调地址
$config->zy_callback_api_domain = 'http://wh.aviva-cofco.com.cn';
//中英的回调api路径
$config->zy_callback_api_path = '/WX/interface/addWxActivityPrize.do?cust_sign=__UID__&act_name=__ACTION_NAME__&prize_num=__CARD_NO__&prize_pwd=__CARD_SECRET__&prop=__PROP__&timestamp=__TIME_STAMP__&valid_time=__CARD_VALID_TIME__&src=__SRC__&sign=__SIGN__';
//中英的回调参数加密秘钥
//$config->zy_callback_api_encode_secret = '89aa1b78518cc7176121248';
$config->zy_callback_api_encode_secret = 'd8f9135f30f5c46a41';
//中英体检卡的appid
$config->zy_appid = 'qz23083c03ada25c07';
//中英订单的体检卡中缀
$config->zy_order_midfix = 'ZYTJK';
//中英体检卡订单预定的openid，下单时候要用，废弃
$config->zy_order_pay_openid = 'oVsAe09ty8HiIgJv4q5RgmFwFGxg';
//中英体检卡微信支付订单的标题
$config->zy_order_title = '中英体检卡尊享套餐';
//体检卡的预约链接,短信服务中要使用
$config->zy_package_book_url = 'http://y.kktijian.com/';

//康康的appid
$config->kangkang_appid = '201706081349134592';
//康康的secret
$config->kangkang_appkey = 'oIUrXTP51Vgkgokr';
//康康的商户唯一识别码
$config->kangkang_onlycode = '239ff817-8196-40a2-a369-53b1c36fd77b';
//康康的下单接口地址
$config->kangkang_order_url = 'http://api.kktijian.com/openapi/OrederService.ashx';

// 套餐对应的图片地址
$config->health_package_item_url = $config->shoproot . "static/img/health_package/";

// 套餐项目url
$config->sms_server_url = 'http://gj.qiezilife.com/Sms/sendSms?phone=';
//套餐映射信息
$config->package_mappings = [
    300 => [
        //数据库中的套餐编号
        'packege_id' => 1,
        'kangkang_package_code' => 'TC201706051507400001',
        //排除的体检机构
//        'exclude_city' => ['济南','青岛']
        'exclude_hospital' =>[
            '爱康卓悦济南纬二路山东商会大厦分院',
            '爱康卓悦青岛香格里拉分院',
        ]
    ],
    500 => [
        //数据库中的套餐编号
        'packege_id' => 2,
        'kangkang_package_code' => 'TC201706051532220001',
//        'exclude_city' => ['济南','青岛']
        'exclude_hospital' =>[
            '爱康卓悦济南纬二路山东商会大厦分院',
            '爱康卓悦青岛香格里拉分院',
        ]
    ],
    800 => [
        //数据库中的套餐编号
        'packege_id' => 3,
        'kangkang_package_code' => 'TC201706051533330001',
    ],
    1000 => [
        //数据库中的套餐编号
        'packege_id' => 4,
        'kangkang_package_code' => 'TC201706051534320001',
    ],
    110 => [
        //数据库中的套餐编号
        'packege_id' => 5,
        'kangkang_package_code' => 'TC201706051507400001',
    ]
];

//短信的签名
$config->sms_owner_company = '中粮数字健康';
//创蓝发送短信接口URL, 如无必要，该参数可不用修改
$config->sms_api_send_url = 'http://vsms.253.com/msg/send/json';

//创蓝变量短信接口URL, 如无必要，该参数可不用修改
$config->sms_api_variable_url = 'http://vsms.253.com/msg/variable/json';

//创蓝短信余额查询接口URL, 如无必要，该参数可不用修改
$config->sms_api_balance_query_url = 'http://vsms.253.com/msg/balance/json';
//创蓝账号 替换成你自己的账号
$config->sms_api_account	= 'N1704222';
//创蓝密码 替换成你自己的密码
$config->sms_api_password	= 'm8x6b1YV4ye1be';
//用户生成体检卡套餐的测试链接地址
$config->stable_token = '0e01bdcd1d5aff19c04e3f6c149bcb00_IX4Xx4si5Hta5eaTtMJ4bFZZ0WXZCM4xHjWtXwPTn9Qj5G98s94LXg';

//水果打卡的加密秘钥，zy_group#zhongliang#环境变量值拼接的md5
$config->fruit_punch_url_encode_key='d4393a07e8b2ef2461ebdc9c2cf683ad';

//初级打卡的名称
$config->low_level_name = '水果营养知识挑战';
//高级打卡名称
$config->high_level_name = '体重管理计划';
//中英的发送模板消息的接口
$config->zy_tpl_msg_action = $config->zy_callback_api_domain.'/WX/interface/addMessageRecord.do?';
//中英发送模板消息对应的url加密key
$config->zy_tpl_msg_encode_key = '70c5d5ffe2c629974';

//中英的回调地址
$config->zy_share_url = $config->zy_callback_api_domain.'/WX/redirect/entry.do?urlId=dkjh&';
//茄子的分享url
$config->qiezi_share_url = '?/vHealthClock/share/';

//水果打卡的来源平台
$config->fruit_punch_user_come_from = ['qiezi','zy_group'];
//开放兑换的日期为每月的
$config->exchange_open_day = '15';
//开放的时间节点
$config->exchange_open_time = '12:30:00';
//挑战序号的基础数字区间
$config->challenge_index_base_num = "3000,3000";

//环境变量,正式环境更改为production
$config->environment = 'production';
//用于检测是否开启当日只能打卡一次
$config->check_daily_record = false;
//开启条件验证
$config->open_permission_validate = true;
//健康打卡相关的
$config->system_titles = [
    1 => [
        'level' => 1,
        'name' => '水果标兵',
    ],
    2 => [
        'level' => 2,
        'name' => '水果达人',
    ],
    3 => [
        'level' => 3,
        'name' => '控重标兵',
    ],
    4 => [
        'level' => 4,
        'name' => '控重勇士',
    ],
    5 => [
        'level' => 5,
        'name' => '控重模范',
    ],
    6 => [
        'level' => 6,
        'name' => '控重达人',
    ]
];

//分享的信息
$config->zy_health_clock_share_info = [
    'home' => [
        'title' => '健康打卡',
        'desc' => '中英人寿健康打卡等你来',
        'icon' => '/static/img/clock/zy_logo.jpg',
        'link' => 'page=home&come_from=%s',
    ],
    'lowLevel' => [
        'title' => '初级打卡',
        'desc' => '中英人寿水果打卡大挑战',
        'icon' => '/static/img/clock/zy_logo.jpg',
        'link' => 'page=lowLevel&come_from=%s',
    ],
    'highLevel' => [
        'title' => '高级打卡',
        'desc' => '中英人寿控重打卡',
        'icon' => '/static/img/clock/zy_logo.jpg',
        'link' => 'page=highLevel&come_from=%s',
    ],
];


//workman的地址
$config->workman_jsonrpc_dir = '/data/server/workerman-jsonrpc';
//礼品兑换接口
$config->zy_exchage_api_path = '/WX/interface/addWxActivityPrize.do?cust_sign=__UID__&rec_time=__REC_TIME__&act_name=__ACTION_NAME__&act_desc=__ACT_DESC__&prize_num=__PRIZE_NO__&prize_pwd=__PRIZE_PWD__&valid_time=__VALID_TIME__&src=__SRC__&prop=__PIRZE_TYPE__&timestamp=__TIMESTAMP__&sign=__SIGN__&prize_grade=__PRIZE_GRADE__&is_show=__IS_SHOW__';


// 商品图片临时目录
$config->zy_hch_prize_img_dir = dirname(__FILE__) . "/../uploads/zy_hch/prize_img/";

// 商品图片临时目录
$config->zy_hch_prize_img_tmp_dir = dirname(__FILE__) . "/../uploads/zy_hch/prize_img_tmp/";

// 商品外链目录
$config->zy_hch_prize_img_link = $config->shoproot . "uploads/zy_hch/prize_img/";
//中英商品的配送城市
$config->zy_hch_prize_express_city = '上海';

//验证是否为中英客户的请求接口
$config->zy_hch_validate_customer_api = 'https://customersso.aviva-cofco.com.cn/sso-customer/services/customer/search/openid';
$config->zy_hch_validate_customer_appid = 'CB4E1AB263392B616721E69E133FF7F0';
$config->zy_hch_validate_customer_token = '195871CFD60CFA816966DDC2D4552FAF';
//中英用户能够兑换奖品的次数
$config->zy_hch_user_can_exchange_times = 1;//中英用户能够兑换奖品的次数
//中英客户能够兑换奖品的次数
$config->zy_hch_customer_can_exchange_times = 2;//中英客户能够兑换奖品的次数
$config->zy_hch_user_center_path = '/WX/redirect/entry.do?urlId=grzx&syflag=pc';
$config->zy_hch_user_bind_path = '/WX/redirect/entry.do?urlId=sfbd';