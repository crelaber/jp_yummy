<?php



/**
 * 购物车
 */
class Cart extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }

  /**
  *  跳转订单页面
  */
  public function cart(){

        $this->show();
  }

public function index_order($data){

      $this->loadModel('Carts');
      $this->loadModel('User');
      $this->loadModel('Product');
      $this->loadModel('mUserAddress');
      $this->loadModel('JsSdk');
      $this->loadModel('Coupons');
      $this->loadModel('mOrder');
      $this->loadModel('Util');
      $this->loadModel('mSupplier');
      $this->loadModel('mYun');


       $openid = $this->getOpenId();
       $time = $data->time;
       $isTuan = $data->isTuan;
       $groupId = $data->groupId;

       $pid = $data->pid;
       $isbalance = $data->isbalance;
       $fromUid = $data->fromUid;
       if($isbalance == ''){
         $isbalance=1;
       }
       if($time){

       	 $time = urldecode($time);
       }
       if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
       $this->User->wechatAutoReg($openid);
       
       $uinfo = $this->User->getUserInfo($openid);
       $uid = $uinfo['uid'];
       $address = $this->mUserAddress->enableUserAddress($uinfo['uid']);

      $amount = 0;
      if($isTuan == 1 || $isTuan ==2 ){
          $product = $this->Product->getProductInfo($pid,false);
          $pinfo = $this->Product->getProductInfoWithSpec($product['product_id'],$product['spec_id']);

          $providerList = array();
          $productList = array();
          array_push($providerList, $product);
          array_push($productList, $product);

          foreach($providerList as $pk => $pv){
              $provider_id =  $pv['provider_id'];
              $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
              $providerList[$pk]['yun_cost'] = 0;
              foreach ($productList as   $key => $val) {
                  $pinfo =   $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
                  $specs = $this->Product->getProductSpecs($val['product_id']);
                  $pinfo['sale_prices'] = $specs[0]['sale_price'];
                  $productList[$key]['pinfo'] = $pinfo;
                  $productList[$key]['is_tuan'] = $isTuan;
              }
              $yun_no = $productList[0]['yun_no'];
              $yun = $this->mYun->get_detail_yun_sys($yun_no);
              $providerList[$pk]['yun'] = $yun;
              $providerList[$pk]['productList'] = $productList;


              if($address){
                  $providerList[$pk]['yun_cost'] = $this->Carts->calc_product_yun($productList,$address);
                  $yun  = $providerList[$pk]['yun_cost'];
              }else{
                  $providerList[$pk]['yun_cost'] = 0;
              }

          }
          if($isTuan == 2){
              $amount =$pinfo['sale_prices'];

          }else{
              $amount = $product['group_price'];
          }
      }else{
          $providerList = $this->Carts->getCartProductSupplier($uid);
          foreach($providerList as $pk => $pv){

              $provider_id =  $pv['provider_id'];
              $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
              $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
              $yun_no = $productList[0]['yun_no'];
              $yun = $this->mYun->get_detail_yun_sys($yun_no);
              $providerList[$pk]['yun'] = $yun;
              foreach ($productList as   $key => $val) {
                  $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
              }
              $providerList[$pk]['productList'] = $productList;

              if($address){
                  $providerList[$pk]['yun_cost'] = $this->Carts->calc_product_yun($productList,$address);
              }else{
                  $providerList[$pk]['yun_cost'] = 0;
              }
          }

          $amount = $this->Carts->calc_cart_amount($uid);
      }

      $totalamount = $amount;
      error_log("amount===".json_encode($amount));
      error_log("======user_address=====".$address);

    
       $signPackage = $this->JsSdk->GetSignPackage();
        // 收货地址接口Json包
        $addrsignPackage = array(
            "appId" => APPID,
            "scope" => "jsapi_address",
            "signType" => "sha1",
            "addrSign" => isset($addrsign) ? $addrsign : false,
            "timeStamp" => (string) $timestamp,
            "nonceStr" => (string) $nonceStr
        );
      

      $reduceAmount = 0;
      $orderCoupons = $this->Coupons->get_avaliable_coupons_for_order(time(),$uinfo['uid'],0);
      if($orderCoupons && $isTuan==""){
      
           $couponAmount = $this->mOrder->cal_reduce_amount_by_coupon_id($amount,$orderCoupons[0]['id'],$uid,false);
           error_log("===============couponAmount============".$couponAmount);
           if($couponAmount > 0){

             $orderCoupons[0]['coupon_value'] =  round($couponAmount,2);

             $reduceAmount = $reduceAmount+ $orderCoupons[0]['coupon_value'];
             $this->assign('orderCoupons', $orderCoupons[0]);
           }
      }
      
      $couponId ="";
      //获取用户优惠券开关
      $awardSettings = $this->Dao->select("value")->from('wshop_settings')->where("`key` = 'award_settings'")->getOne();
      $award = json_decode($awardSettings, true);
      $userCouponSwitch = $award['user_coupon_switch'];
      $userCoupons ='';
      if($userCouponSwitch == 1){ 
          $userCoupons = $this->Coupons->get_avaliable_coupons_for_order(time(),$uinfo['uid'],1);
          $weekarray=array("日","一","二","三","四","五","六");
     
       $date = $_COOKIE['deliver_date'];
       $todayDate = date("Y-m-d");



            $couponList =  $this->UserCoupon->getUserCouponListByState($uinfo['uid'],0);
            error_log("==========".json_encode($couponList));
            foreach($couponList as $key=>$val){
                if($val['discount_type'] == 1 && $data->couponId!= -1){
                    $couponId = $val['coupon_id'];
                }
            }

         $useCoupon = $this->Coupons->get_coupon_info($couponId);
         $couponAmount = $this->mOrder->cal_reduce_amount_by_coupon_id($amount,$couponId,$uid,false);
         if($couponAmount > 0){

            $reduceAmount = $reduceAmount+$couponAmount;
            $this->assign('coupon',$useCoupon);
         }

    }
      
    if($data && $couponId != -1 && $isTuan==""){
         if($couponId !='') $reduceAmount=0;
         $couponId = $data->couponId;
         $useCoupon = $this->Coupons->get_coupon_info($couponId);
         $couponAmount = $this->mOrder->cal_reduce_amount_by_coupon_id($amount,$couponId,$uid,false);
         if($couponAmount > 0){

            $reduceAmount = $reduceAmount+$couponAmount;
            $this->assign('coupon',$useCoupon);
         }

      }
  
     
      $amount = round($amount - $reduceAmount,2);
      if($amount <= 0){
        $amount = 0;
      }
      if($userCoupons && $isTuan ==""){
            $this->assign('userCoupons', $userCoupons);
      }

      $yun = 0;


     foreach($providerList as $pk => $pv){
        $yun +=  $pv['yun_cost'] ;

     }
      $pay_amount = $totalamount+$yun-$uinfo['balance']-$reduceAmount;
//      if($orderCoupons){
//          $pay_amount = $pay_amount - $orderCoupons[0]['coupon_value'];
//      }
      if($pay_amount < 0){
          $pay_amount = 0;
      }
      $this->Smarty->assign('signPackage', $signPackage);
      $this->assign('couponId', $couponId);
      $this->Smarty->assign('userInfo', (array) $uinfo);
      $this->assign('address', $address);
      $this->assign('amount',$amount+$yun);
      $this->assign('totalamount',$totalamount+$yun);

      $this->assign('pay_amount',$pay_amount);

      $this->assign('time',$time);
      $this->assign('yun',$yun);
      $this->assign('isbalance', $isbalance);
      $this->assign('isTuan', $isTuan);
      $this->assign('pid', $pid);
      $this->assign('groupId', $groupId);


      $this->assign('provider_list', $providerList);
      $this->assign('fromUid',$fromUid);
      $this->show('./order/order.tpl');
   }




 
 public function ajaxDelCart(){
     $this->loadModel('User');
     $this->loadModel('Carts');
     $openid = $this->getOpenId();
     $uinfo = $this->User->getUserInfo($openid);
     $uid = $uinfo['uid'];
     $this->Carts->del_cart($uid);
     $this->echoMsg(1,"删除成功");
  }
   
   
   /**
   *   购物车增加商品
   */
   public function add_product_to_cart(){
    
        $this->loadModel('Carts');
        $this->loadModel('User');
       $this->loadModel('mSupplier');
       $this->loadModel('mUserAddress');
       $this->loadModel('mYun');
        $this->loadModel('Product');
        $openid = $this->getOpenId();

        $uinfo = $this->User->getUserInfo($openid);

        $uid = $uinfo['uid'];
        error_log("openId=================".$openid."===============uid=".$uid);
        $cartData = $this->carDataRepack(json_decode($_POST['data'], true));
        $state = 1;
        $msg = "";
        foreach ($cartData as $data) {
            
            $count = $data['count'];
            $productId = $data['pid'];
            $spid = $data['spid'];
            $product = $this->Product->getById($productId);

            if($product['product_online'] != 0){
                $this->Carts->update_cart_product($uid,$productId,$count,$spid,$product['provider_id']);
            }else{
                $this->Carts->remove_product($uid,$productId,$spid);

            }



        }


       $productList = $this->Carts->get_cart_products($uid);
        $productArray = array();
        $cartArray = array();
        $total = 0;


        foreach($productList as $plist){

            $key = 'p'.$plist['product_id'].'m'.$plist['spec_id'];
            $value =(int) $plist['product_quantity'];
            $productArray[$key] = $value;
            $productSpecs =  $this->Product->getProductSpecs($plist['product_id']);
            if($productSpecs){
                $total=$total+$value*$productSpecs[0]['sale_price'];
            }

        }

        $topCats = $this->Product->getCatList(0);
        foreach($topCats as   $c_k => $c_v) {  

                foreach($productList as   $p_k => $p_v) {  

                  if($c_v['cat_id'] == $p_v['product_cat']){

                       $topCats[$c_k]['count'] = $topCats[$c_k]['count']+$p_v['product_quantity'];

                  }
                }
         }

       $totalYun = 0;
       $address = $this->mUserAddress->enableUserAddress($uid);
       if($address){
           $providerList = $this->Carts->getCartProductSupplier($uid);
           foreach($providerList as $pk => $pv){

               $provider_id =  $pv['provider_id'];
               $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
               $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
               $yun_no = $productList[0]['yun_no'];
               $yun = $this->mYun->get_detail_yun_sys($yun_no);
               $providerList[$pk]['yun'] = $yun;

               foreach ($productList as   $key => $val) {
                   $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
               }
               $providerList[$pk]['productList'] = $productList;
               $cost =  $this->Carts->calc_product_yun($productList,$address);
               $providerList[$pk]['yun_cost'] = $cost;

               $totalYun = $totalYun + doubleval($cost);
           }

           // error_log("=========providerList========".json_encode($providerList));

       }else{
           $totalYun = 0;
       }
        $cartArray['cartData'] = $productArray;
        $cartArray['total'] = $total;
        $cartArray['topCats'] = $topCats;
        $cartArray['totalYun'] = $totalYun;

       //$this->echoMsg($state,$msg);
        $this->echoJson($cartArray);

   } 
   
   
   /*
   *
   */
   
   public function removeProduct($data){
   
       $productId = $data->product_id;
       $specId = $data->spec_id;
       $this->loadModel('Carts');
       $this->loadModel('User');
       $openid = $this->getOpenId();
   
       error_log("productId===============".$productId);
       $uinfo = $this->User->getUserInfo($openid);
       $uid = $uinfo['uid'];
       $this->Carts->remove_product($uid,$productId,$specId);
   }
  
   /**
  * 检查和同步购物车
  */
  public function checkCart(){
  
     $this->loadModel('Carts');
     $this->loadModel('User');
      $this->loadModel('mSupplier');
      $this->loadModel('mYun');
      $this->loadModel('Product');

      $openid = $this->getOpenId();
     $uinfo = $this->User->getUserInfo($openid);
     $uid = $uinfo['uid'];

      $product = $this->Carts->get_cart_products($uid);
      $productList = array();
      foreach ($product as $key => $val) {

          if($val['product_cat'] == 119){
              $killTime = $val['kill_time'];
              $nowTime = strtotime('now');
              $kill  =  strtotime($killTime);
              if($nowTime < $kill){
                  array_push($productList, $val);

              }else{
                  $this->Carts->remove_product($uid,$val['product_id'],$val['spec_id']);

              }
          }else{
              array_push($productList, $val);

          }

      }

     $productArray = array();
     $cartArray = array();
     $cartArray['cartData'] = $productArray;
     $this->echoJson($cartArray);

  }


  public function  ajaxSyncCart(){
      $this->loadModel('Carts');
      $this->loadModel('User');
      $this->loadModel('mSupplier');
      $this->loadModel('mYun');
      $this->loadModel('mUserAddress');
      $this->loadModel('Product');
      $openid = $this->getOpenId();
      $uinfo = $this->User->getUserInfo($openid);
      $uid = $uinfo['uid'];

      $product = $this->Carts->get_cart_products($uid);
      $productList = array();
      foreach ($product as $key => $val) {

          if($val['product_cat'] == 119){
              $killTime = $val['kill_time'];
              $nowTime = strtotime('now');
              $kill  =  strtotime($killTime);
              if($nowTime < $kill){
                  array_push($productList, $val);

              }else{
                  $this->Carts->remove_product($uid,$val['product_id'],$val['spec_id']);

              }
          }else{
              array_push($productList, $val);

          }

      }

      $total = 0;
      $totalCount = 0;
      foreach($productList as $plist){

          $key = 'p'.$plist['product_id'].'m'.$plist['spec_id'];
          $value =(int) $plist['product_quantity'];
          $totalCount += $value;
          $productArray[$key] = $value;
          $productSpecs =  $this->Product->getProductSpecs($plist['product_id']);
          if($productSpecs){
              $total=$total+$value*$productSpecs[0]['sale_price'];
          }

      }

      $totalYun = 0;
      $address = $this->mUserAddress->enableUserAddress($uid);
      if($address){
          $providerList = $this->Carts->getCartProductSupplier($uid);
          foreach($providerList as $pk => $pv){

              $provider_id =  $pv['provider_id'];
              $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
              $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
              $yun_no = $productList[0]['yun_no'];
              $yun = $this->mYun->get_detail_yun_sys($yun_no);
              $providerList[$pk]['yun'] = $yun;

              foreach ($productList as   $key => $val) {
                  $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
              }
              $providerList[$pk]['productList'] = $productList;
              $cost =  $this->Carts->calc_product_yun($productList,$address);
              $providerList[$pk]['yun_cost'] = $cost;

              $totalYun = $totalYun + doubleval($cost);
          }

          // error_log("=========providerList========".json_encode($providerList));

      }else{
          $totalYun = 0;
      }

      $data = array();
      if($productList){
          $data['totalCount'] = count($productList);
      }else{
          $data['totalCount'] = 0;
      }
      $data['total'] = $total;
      $data['yun'] = $totalYun;
      $data['totalCount'] = $totalCount;
      $this->echoJson($data);
  }



 public function  ajaxDoCartProduct(){

     $this->loadModel('Carts');
     $this->loadModel('User');
     $this->loadModel('Product');
     $this->loadModel('mUserAddress');
     $this->loadModel('mYun');
     $this->loadModel('mSupplier');


     $openid = $this->getOpenId();
     $uinfo = $this->User->getUserInfo($openid);
     $uid = $uinfo['uid'];

     $pcount = $_POST['pcount'];
     $pid = $_POST['pid'];
     $spid = $_POST['spid'];
     $product = $this->Product->getById($pid);
     $code = 0;


     if($product['product_online'] == '0'){

         $this->Carts->remove_product($uid,$pid,$spid);
         $code = -1;
     }else{
         if($product['product_cat'] == '119'){
             $killTime = $product['kill_time'];
             $nowTime = strtotime('now');
             $kill  =  strtotime($killTime);
             if($nowTime > $kill){
                 $this->Carts->remove_product($uid,$pid,$spid);
                 $code = -2;
             }else{
                 if($pcount == 0){
                     $this->Carts->remove_product($uid,$pid,$spid);
                     //$this->echoMsg(1,"已成功移除");
                     $code = 1;
                 }else{
                     $this->Carts->update_cart_product($uid,$pid,$pcount,$spid,$product['provider_id']);
                     // $this->echoMsg(1,"已添加移除");
                     $code = 2;
                 }
             }

         }else{
             if($pcount == 0){
                 $this->Carts->remove_product($uid,$pid,$spid);
                 //$this->echoMsg(1,"已成功移除");
                 $code = 1;
             }else{
                 $this->Carts->update_cart_product($uid,$pid,$pcount,$spid,$product['provider_id']);
                 // $this->echoMsg(1,"已添加移除");
                 $code = 2;
             }
         }

     }

     $productList = $this->Carts->get_cart_products($uid);
     $total = 0;
     $totalCount = 0;
     foreach($productList as $plist){

         $key = 'p'.$plist['product_id'].'m'.$plist['spec_id'];
         $value =(int) $plist['product_quantity'];
         $totalCount+= $value;
         $productArray[$key] = $value;
         $productSpecs =  $this->Product->getProductSpecs($plist['product_id']);
         if($productSpecs){
             $total=$total+$value*$productSpecs[0]['sale_price'];
         }

     }

     $totalYun = 0;
     $address = $this->mUserAddress->enableUserAddress($uid);
     if($address){
         $providerList = $this->Carts->getCartProductSupplier($uid);
         foreach($providerList as $pk => $pv){

             $provider_id =  $pv['provider_id'];
             $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
             $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
             $yun_no = $productList[0]['yun_no'];
             $yun = $this->mYun->get_detail_yun_sys($yun_no);
             $providerList[$pk]['yun'] = $yun;

             foreach ($productList as   $key => $val) {
                 $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
             }
             $providerList[$pk]['productList'] = $productList;
             $cost =  $this->Carts->calc_product_yun($productList,$address);
             $providerList[$pk]['yun_cost'] = $cost;
             $totalYun = $totalYun + doubleval($cost);
         }

     }else{
         $totalYun = 0;
     }

     $data = array();
     $data['yun'] = $totalYun;
     $data['total'] = $total;
     $data['totalCount'] =$totalCount;

     $this->echoMsg($code,$data);



 }




  public function carDataRepack($cartData) {
        $matchs = array();
        $ret = array();
        foreach ($cartData as $key => $count) {
            preg_match("/p(\d+)m(\d+)/is", $key, $matchs);
            $ret[] = array('pid' => intval($matchs[1]), 'spid' => intval($matchs[2]), 'count' => intval($count));
        }
        return $ret;
    }

}
