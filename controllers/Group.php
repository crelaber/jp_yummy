<?php



/**
 * 拼团
 */
class Group extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }


  public function group_list($query){


      $this->loadModel('WechatSdk');
      $this->loadModel('Product');
      $this->loadModel('User');
      $this->loadModel('JsSdk');

      if(!Controller::inWechat() && !$this->debug){

          $this->show('./index/error.tpl');
          die(0);
      }
      global $config;
      $openId = $this->getOpenId();
      $this->User->wechatAutoReg($openId);

      $catId = $query->id;
      if($catId == ""){
          $catId = $config->pt_id;
      }
      $products = $this->Product->getGroupProduct($catId);
      foreach ($products as $key => $val) {
          $cat = $this->Product->getCatInfo($val['product_cat']);
          $productSpecs = $this->Product->getProductSpecs($val['product_id']);
          if ($productSpecs) {
              $products[$key]['pinfo'] = $productSpecs[0];
              // get stock of given SKU for target date
              //$stock_info = $this->Stock->get_product_instock_by_sku_and_date($productSpecs[0]['id'], $target_time, $selected_store);
              //error_log('stock info:'.json_encode($stock_info));
              //$products[$key]['pinfo']['instock'] = $stock_info['stock']; // stocks can be sold for target date
          }
          $time1 = explode(" ", $val['kill_time']);
          $dateTime = $time1[0];
          $times = $time1[1];
          $time2 = explode("/", $dateTime);
          $d = $time2[2];

          $time3 = explode(":", $times);
         $kill_time =  strtotime($val['kill_time']);


          $products[$key]['start'] = $d."号".$time3[0];
          $products[$key]['h'] = "00";
          $products[$key]['m'] = "00";
          $products[$key]['s'] = "00";

      }
      $signPackage = $this->JsSdk->GetSignPackage();
      $this->assign('signPackage', $signPackage);
      $this->assign('products', $products);
      $this->assign('catId', $catId);

      $this->show();
  }


    public function group_detail($query){
        $this->loadModel('WechatSdk');
        $this->loadModel('Product');
        $this->loadModel('User');
        $this->loadModel('mGroup');
        $this->loadModel('JsSdk');


        global $config;
        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        $openId = $this->getOpenId();
        $this->User->wechatAutoReg($openId);
        $id = $query->id;
        $uinfo = $this->User->getUserInfo($openId);

        $isSubscribed = $this->User->isSubscribed();

        // 产品图片
        $productInfo = $this->Product->getProductInfo($id);
        // 加入产品首图到图片列表
        array_unshift($productInfo['images'], array('image_path' => $productInfo['catimg']));

        $specs = $this->Product->getProductSpecs($id);
        $productInfo['pinfo'] = $specs[0];
        $group = $this->mGroup->getGroupDetail($uinfo['uid'],$id);
        if($group){
            $this->assign('group', 1);
        }else{
            $this->assign('group', 0);
        }
        $now = time();
        if($now < strtotime($productInfo['kill_time'])){
            $isEnd = 0;
        }else{
            $isEnd = 1;

        }
        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
        $this->assign("catId",$config->pt_id);
        $this->assign('productInfo', $productInfo);
        $this->assign('productid', $id);
        $this->assign('isSubscribed', $isSubscribed);
        $this->assign('specs', $specs);
        $this->assign('isEnd', $isEnd);
        $this->assign('images', $productInfo['images']);
        $this->assign('images_count', count($productInfo['images']));
        $this->show();
    }


    public function grouping($query){

        global $config;
        $this->loadModel('WechatSdk');
        $this->loadModel('User');
        $this->loadModel('mGroup');
        $this->loadModel('mOrder');
        $this->loadModel('Product');
        $this->loadModel('JsSdk');


        if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
        $openId = $this->getOpenId();
        $this->User->wechatAutoReg($openId);
        $uinfo = $this->User->getUserInfo($openId);
        $uid = $uinfo['uid'];
        $order_id = $query->order_id;
        $id = $query->id;
        $pid = $query->pid;
        $isSubscribed = 0;
        $result = 0;//0 正在进行中  -1 失败  1 成功
        $isJoin = 0;// 0 表示未参加  1表示已参加
        $isSubscribed = $this->User->isSubscribed();

        if($order_id && !$id){
            $orders = $this->mOrder->GetOrderDetail($order_id);
            $product = $orders['products'][0];
            $specs = $this->Product->getProductSpecs($product['product_id']);
            $group = $this->mGroup->getGroupDetailById($id);
            if($group){
                $isExist = $this->mGroup->getGroupJoinByGroupId($id,$uid);
                $adminInfo = $this->User->getUserInfo($group['uid']);
                $this->assign("adminUinfo",$adminInfo);

            }else{
                $group = $this->mGroup->getGroupDetail($uinfo['uid'],$product['product_id']);
                $isJoin = 1;
                $this->assign("adminUinfo",$uinfo);

            }
            $id = $group['id'];
            $groupStartTime = date('Y-m-d H:i:s', $group['add_time']);
            $endTime = date('Y/m/d H:i:s', $group['end_time']);
            $this->assign("startTime",$groupStartTime);
            $this->assign("endTime",$endTime);
            $this->assign("productInfo",$product);
            $this->assign("spec",$specs[0]);


        } else{
            $group = $this->mGroup->getGroupDetail($uinfo['uid'],$pid);
            if($id){
                $group = $this->mGroup->getGroupDetailById($id);
            }
            $id = $group['id'];
            if($uid == $group['uid']){
                $isJoin = 1;
            }else{
               $groupJoin =   $this->mGroup->getGroupJoinByGroupId($id,$uid);
               if($groupJoin){
                   $isJoin = 1;
               }else{
                   $isJoin = 0;
               }
            }
            $productId = $group['product_id'];
            $product = $this->Product->getProductInfo($productId);
            $specs = $this->Product->getProductSpecs($productId);
            $adminId = $group['uid'];
            $adminUinfo = $this->User->getUserInfo($adminId);
            $groupStartTime = date('Y-m-d H:i:s', $group['add_time']);
            $endTime = date('Y/m/d H:i:s', $group['end_time']);
            $this->assign("startTime",$groupStartTime);
            $this->assign("endTime",$endTime);
            $this->assign("adminUinfo",$adminUinfo);
            $this->assign("productInfo",$product);
            $this->assign("spec",$specs[0]);


        }


        $joinList =  $this->mGroup->getGroupJoinList($id);

        $joinCount = 0;
        $joinNum = 0;
        $totalNum = $group['num'];
        $defaultArray = array();
        if($joinList){
            $joinCount = count($joinList);
            $joinNum = $totalNum - $joinCount-1;
            foreach($joinList as $key => $val){
                $user =   $this->User->getUserInfo($val['uid']);
                $joinList[$key]['user'] = $user;
                $joinList[$key]['add_time'] = date('Y/m/d H:i:s', $val['add_time']);

            }
        }else{
            $joinNum = $totalNum -1;

        }

        $now = time();
        $groupEndTime = $group['end_time'];
        if($groupEndTime <= $now && $group['status'] == 0){
            $serArray = array('status'=>-1);
            $this->mGroup->updateGroup($id,$serArray);
            $group['status'] = -1;
            $this->mGroup->groupFailMsg($id);
        }
        if($isSubscribed == 0){

            $qr = $this->mGroup->get_qr_code_by_value($id,"group");
            if(!$qr){
                $time = time();
                $dir = $config->codePicLink;
                $dst =  $dir."attcode.png";
                $wx_qr_code = $dir.$product['product_code'].$product['product_id']."_wx.jpg";
                $product_code = $dir.$product['product_code'].$product['product_id']."_code.jpg";

                $id = $this->mGroup->add_qr_code($id,"group",$time,$product['product_code'].$product['product_id']."_code.jpg",$product['product_id']);



                        $token = WechatSdk::getServiceAccessToken();
                        $ticket = WechatSdk::getCQrcodeTicket($token,$id,WechatSdk::QR_SCENE);
                        $wx_url = WechatSdk::getCQrcodeImage($ticket);
                        $this->downloadFile($wx_url,$wx_qr_code);

                    $invitation =  imagecreatefrompng($dst);
                    $qrcodeImage = imagecreatefromjpeg($wx_qr_code);
                    imagecopyresized($invitation, $qrcodeImage, 275, 130, 0, 0,320,320, 430,430);
                    imagedestroy($qrcodeImage);
                    imagejpeg($invitation,$product_code);
                    imagedestroy($invitation);

                $img ="http://".$_SERVER['SERVER_NAME']."/uploads/code/".$product['product_code'].$product['product_id']."_code.jpg";
            }else{
                $img = "http://".$_SERVER['SERVER_NAME']."/uploads/code/".$qr['file_path'];
            }

            $this->assign("code",$img);

        }

        if($joinNum>0){
            for ($i = 0; $i < $joinNum; $i++) {
                array_push($defaultArray, "test");
            }
        }

        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
        $this->assign("result",$group['status']);
        $this->assign("joinList",$joinList);
        $this->assign("joinNum",$joinNum);
        $this->assign("isJoin",$isJoin);
        $this->assign("defaultArray",$defaultArray);
        $this->assign("pid",$group['product_id']);
        $this->assign("isSubscribed",$isSubscribed);
        $this->assign("id",$id);
        $this->assign("catId",$config->pt_id);

        $this->show();
    }


    public function  test(){
        global $config;
        $dir = $config->codePicLink;
        $wx_qr_code = $dir.'111111'."_wx.jpg";
        $dst =  $dir."attcode.png";
        $product_code = $dir.'111111'."_code.jpg";
        $invitation =  imagecreatefrompng($dst);

        $token = WechatSdk::getServiceAccessToken();
        $ticket = WechatSdk::getCQrcodeTicket($token,"ssss",WechatSdk::QR_SCENE);
        $wx_url = WechatSdk::getCQrcodeImage($ticket);
        $this->downloadFile($wx_url,$wx_qr_code);


        $invitation =  imagecreatefrompng($dst);

        $qrcodeImage = imagecreatefromjpeg($wx_qr_code);
        imagecopyresized($invitation, $qrcodeImage, 265, 130, 0, 0,300,300, 430,430);
        imagedestroy($qrcodeImage);
        imagejpeg($invitation,$product_code);
        imagedestroy($invitation);
        $img ="http://".$_SERVER['SERVER_NAME']."/uploads/code/".'111111'."_code.jpg";
        echo "img===".$img;

    }


    public function downloadFile($url,$filename){

        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

        $img = curl_exec ($ch);
        curl_close ($ch);

        $fp = fopen($filename,'w');
        fwrite($fp, $img);
        fclose($fp);
    }


    public function  crabGroupStatus(){

        global $config;
        $this->loadModel('mGroup');

        $list = $this->mGroup->getGroupingList();
        foreach($list as $key=>$val){

           $now = time();
           $endTime =  $val['end_time'];
           if($endTime <= $now){
               $serArray = array('status'=>-1);
               $this->mGroup->updateGroup($val['id'],$serArray);
               $this->mGroup->groupFailMsg($val['id']);
           }
        }

    }




}
