<?php

// 支付授权目录 112.124.44.172/wshop/
// 支付请求示例 index.php
// 支付回调URL http://112.124.44.172/wshop/?/Order/payment_callback
// 维权通知URL http://112.124.44.172/wshop/?/Service/safeguarding
// 告警通知URL http://112.124.44.172/wshop/?/Service/warning

/**
 * 健康打卡的管理类
 */
class HealthClockManage extends Controller {
	
	const TPL = './views/wdminpage/';
	
	const STATUS_NOT_DELIEVERY = 'not_delievery';
	const STATUS_DELIEVERING = 'delievering';
	const STATUS_DELIEVERED = 'delievered';
	const STATUS_REACHED = 'reached';
	const STATUS_NOT_REACHED = 'not_reached';
	const STATUS_CANCEL = 'cancel';

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('mHealthClock');
    }
    

    /**
     * 后台的订单列表接口
     */
    public function ajaxLoadPrizeList($Q){
        $pageSize = 10;

        if($Q->pageSize){
            $pageSize = $Q->pageSize;
        }

        //增加限制机制，防止每页取的数据过多
        if($pageSize>10){
            $pageSize = 10;
        }

        $page = $Q->page;
        if(!$page){
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize;
        //查询的条件
        $condition = '';
        $count_sql = 'select count(*) from ' .mHealthClock::TABLE_NAME_EXCHAGE_PRIZE;
        $list = $this->mHealthClock->getPrizeList('is_delete = 0','id desc',$offset,$pageSize);
        //获取所有的总数
        $count = $this->Db->getOne($count_sql);
        $data = array(
            'list' => $list,  //列表
            'total' => $count
        );
        $this->echoJson($data);
    }


    /**
     * 上传产品图片
     * ImageUpload
     */
    public function updatePrizeImg() {
        global $config;
        $this->loadModel('ImageUploader');
        $this->ImageUploader->dir = $config->zy_hch_prize_img_dir;
        $targetFileName = $this->ImageUploader->upload();
        $arr = array(
            "s" => $targetFileName !== false,
            "pic" => $config->zy_hch_prize_img_dir . $targetFileName,
            "img_name" => $targetFileName,
            "link" => $config->zy_hch_prize_img_link . $targetFileName
        );
        $this->echoJson($arr);
    }


    /**
     * 编辑商品信息
     * @param type $Query
     */
    public function editPrize($Query) {
        $mod = $Query->mod;
        if ($mod == 'edit') {
            $id = $this->pGet('id');
            $prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($id);
            $belongTime = $prizeInfo['belong_time'];
            $belongMonth = substr($belongTime,4);
            $prizeInfo['belong_month'] = $belongMonth;
            $this->assign('ed', true);
            $this->assign('prizeInfo', $prizeInfo);
        } else {
            $this->assign('ed', false);
            $id = false;
        }
        $this->assign('mod', $mod);
        $this->show(self::TPL . 'health_clock/iframe_alter_prize.tpl');
    }


    /**
     * 更新商品信息
     */
    public function savePrize(){
        $id = $this->mHealthClock->modifyPrize($_POST);
        echo $id ? $id : 0;
    }

    /**
     * 商品上下架
     */
    public function switchOnline() {
        if(!$prizeId = $this->pPost('prize_id')){
            $this->apiFail('prize_id 不能为空');
            die(0);
        }
        //奖品信息
        if(!$prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($prizeId)){
            $this->apiFail('奖品不存在');
            die(0);
        }

        $isOnline = $this->pPost('is_online');

        $availableOnlines = [0,1];
        if(!in_array($isOnline,$availableOnlines)){
            $this->apiFail('is_online 参数不正确');
            die(0);
        }
        if($result =  $this->mHealthClock->switchOnlinePrize($prizeId,$isOnline,$prizeInfo)){
            $this->apiSuccess();
        }else{
            $this->apiFail('系统错误');
        }

    }


    public function deletePrize(){
        $prizeId = $this->pPost('prize_id');
        if(!$prizeId = $this->pPost('prize_id')){
            $this->apiFail('prize_id 不能为空');
            die(0);
        }

        //奖品信息
        if(!$prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($prizeId)){
            $this->apiFail('奖品不存在');
            die(0);
        }

        //是否已经删除
        if($prizeInfo['is_delete']){
            $this->apiFail('该奖品已经被删除');
            die(0);
        }

        if($prizeInfo['is_online']){
            $this->apiFail('该商品已经上架，请先下架');
            die(0);
        }


        if($result =  $this->mHealthClock->deletePrize($prizeId,$prizeInfo)){
            $this->apiSuccess();
        }else{
            $this->apiFail('系统错误');
        }
    }



}
