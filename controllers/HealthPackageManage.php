<?php

// 支付授权目录 112.124.44.172/wshop/
// 支付请求示例 index.php
// 支付回调URL http://112.124.44.172/wshop/?/Order/payment_callback
// 维权通知URL http://112.124.44.172/wshop/?/Service/safeguarding
// 告警通知URL http://112.124.44.172/wshop/?/Service/warning

/**
 * 配送类
 */
class HealthPackageManage extends Controller {
	
	const TPL = './views/wdminpage/';
	
	const STATUS_NOT_DELIEVERY = 'not_delievery';
	const STATUS_DELIEVERING = 'delievering';
	const STATUS_DELIEVERED = 'delievered';
	const STATUS_REACHED = 'reached';
	const STATUS_NOT_REACHED = 'not_reached';
	const STATUS_CANCEL = 'cancel';

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('mHealthPackage');
    }
    

    /**
     * 后台的订单列表接口
     */
    public function ajax_load_hp_order_list($Q){
    	$status = $Q->status;
    	if(!$status){ //如果没有则默认选择未
    		$status = 'not_delievery';
    	}
    	$day = $Q->day;
    	if(!$day){ //如果没有则默认选择今天
    		$day = 0;
    	}
    	
    	switch($day){
    		case 366 : //所有
    			break;
    		case 7 :
    			//数据库中的配送时间格式为2015-12-01 14:00-15:00
    			$where[] = 'YEARWEEK(left(FROM_UNIXTIME(order_time,"%Y-%m-%d %H:%i"),10)) = YEARWEEK(now())';
    			break;
    		case 30 :
    			//数据库中的配送时间格式为2015-12-01 14:00-15:00
    			$where[] = 'left(FROM_UNIXTIME(order_time,"%Y-%m-%d %H:%i"),7) = date_format(now(),"%Y-%m")';
    			break;
    		default :
    			$where[] = 'TO_DAYS(left(FROM_UNIXTIME(order_time,"%Y-%m-%d %H:%i"),10))-TO_DAYS(now()) = ' .$day;
    			break;
    	}
    	
    	if($status !== 'all'){
    		$where[] = 'status="'.$status.'"';
    	}

    	$pageSize = 10;
    	
    	if($Q->pageSize){
    		$pageSize = $Q->pageSize;
    	}
    	
    	//增加限制机制，防止每页取的数据过多
    	if($pageSize>10){
    		$pageSize = 10;
    	}
    	
    	$page = $Q->page;
    	if(!$page){
    		$page = 1;
    	}
    	$offset = ($page - 1) * $pageSize;
    	
    	//查询的条件
    	$condition = '';
    	$count_sql = 'select count(*) from ' .TABLE_HP_ORDER;
    	if(count($where)>0){
    		$condition = implode(' and ',$where);
    		$count_sql = $count_sql.' where '.$condition;
    	}
    	$list = $this->mHealthPackage->get_hp_order_list($condition,'order_time desc',$offset,$pageSize);
    	//获取所有的总数
    	$count = $this->Db->getOne($count_sql);
    	
    	$data = array(
    			'day' => $day,  //日期
    			'status' => $status, //状态
    			'list' => $list,  //列表
    			'total' => $count
    	);
    	$this->echoJson($data);
    }

    /**
     * 后台的订单列表接口
     */
    public function ajax_load_hp_order_card_list($Q){
        $pageSize = 10;

        if($Q->pageSize){
            $pageSize = $Q->pageSize;
        }

        //增加限制机制，防止每页取的数据过多
        if($pageSize>10){
            $pageSize = 10;
        }

        $page = $Q->page;
        if(!$page){
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize;

        //查询的条件
        $condition = '';
        $count_sql = 'select count(*) from ' .TABLE_HP_ORDER_CARD;

        $list = $this->mHealthPackage->get_hp_order_card_list('','add_time desc',$offset,$pageSize);
        //获取所有的总数
        $count = $this->Db->getOne($count_sql);

        $data = array(
            'list' => $list,  //列表
            'total' => $count
        );
        $this->echoJson($data);
    }

    /**
     * 后台的套餐列表
     */
    public function ajax_load_hp_package_list($Q){
        $pageSize = 10;

        if($Q->pageSize){
            $pageSize = $Q->pageSize;
        }

        //增加限制机制，防止每页取的数据过多
        if($pageSize>10){
            $pageSize = 10;
        }

        $page = $Q->page;
        if(!$page){
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize;

        //查询的条件
        $condition = '';
        $count_sql = 'select count(*) from ' .TABLE_HP_PACKAGE_INFO;

        $list = $this->mHealthPackage->get_hp_package_list('','add_time desc',$offset,$pageSize);
        //获取所有的总数
        $count = $this->Db->getOne($count_sql);

        $data = array(
            'list' => $list,  //列表
            'total' => $count
        );
        $this->echoJson($data);
    }

    /**
     * 后台的套餐列表
     */
    public function ajax_load_hp_hospital_list($Q){
        $pageSize = 10;

        if($Q->pageSize){
            $pageSize = $Q->pageSize;
        }

        //增加限制机制，防止每页取的数据过多
        if($pageSize>10){
            $pageSize = 10;
        }

        $page = $Q->page;
        if(!$page){
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize;

        //查询的条件
        $condition = '';
        $count_sql = 'select count(*) from ' .TABLE_HP_HOSPITAL;

        $list = $this->mHealthPackage->get_hp_hospital_list('','id desc',$offset,$pageSize);
        //获取所有的总数
        $count = $this->Db->getOne($count_sql);

        $data = array(
            'list' => $list,  //列表
            'total' => $count
        );
        $this->echoJson($data);
    }
    /**
     * 后台的接口调用状态列表
     */
    public function ajax_load_hp_api_invoke_list($Q){
        $pageSize = 10;

        if($Q->pageSize){
            $pageSize = $Q->pageSize;
        }

        //增加限制机制，防止每页取的数据过多
        if($pageSize>10){
            $pageSize = 10;
        }

        $page = $Q->page;
        if(!$page){
            $page = 1;
        }
        $offset = ($page - 1) * $pageSize;

        //查询的条件
        $condition = '';
        $count_sql = 'select count(*) from ' .TABLE_HP_API_INVOKE_STATUS;

        $list = $this->mHealthPackage->get_hp_api_invoke_list('','add_time desc',$offset,$pageSize);
        //获取所有的总数
        $count = $this->Db->getOne($count_sql);

        $data = array(
            'list' => $list,  //列表
            'total' => $count
        );
        $this->echoJson($data);
    }
}
