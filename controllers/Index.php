<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Index extends Controller {

    /**
     * 
     * @param type $ControllerName
     * @param type $Action
     * @param type $QueryString
     */
    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }

    /**
     * 店铺首页
     * @param type $Q
     */
    public function index($Q) {

        global $config;
        $this->loadModel('User');
        $this->loadModel('WechatSdk');
        $this->loadModel('Product');
        $this->loadModel('Carts');
        $this->loadModel('Stores');
        $this->loadModel('Banners');
        $this->loadModel('mSupplier');
        $this->loadModel('mUserAddress');
        $this->loadModel('mYun');

        $openId = $this->getOpenId();
        
        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        
        //error_log("==============$openId==================".$openId);
        $this->loadModel('JsSdk');
        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
        $from_uid = $Q->from_uid;
        if($from_uid){
            $this->User->wechatAutoReg($openId,$from_uid);
        }else{
            $this->User->wechatAutoReg($openId);

        }
        // 微信注册

        if ($Q->rptk_success == 'normal') {
            error_log('rptk jump');
            //header("location:" . 'http://www.icheerslife.com/?/CrashPay/welcome_view');
        }
        
        $this->Smarty->cache_lifetime = 0;
        $openid = $openId;//$this->getOpenId();
        $uinfo = $this->User->getUserInfo($openid);
        $uid = $uinfo['uid'];
        $catId = $Q->catId;

        $is_activity = $Q->is_activity;
        $title = 'CheersLife全球直采';

        if (true OR !$this->isCached()) {
            // get user info
            $cat = $this->Product->getCatInfo($catId,false);
            if($is_activity){
                $topCats = $this->Product->getActivityCatList(0);
                $title = $config->activity_title;
            }else{
                $topCats = $this->Product->getCatList(0);
            }

            if($cat['is_open'] == 0){
                $catId = $topCats[0]['cat_id'];
            }
            if($catId == ""){
                if($topCats){
                    $catId = $topCats[0]['cat_id'];
                    $this->assign('catId', $topCats[0]['cat_id']);
                }
            }

            if($catId == 119){
                $catId =  $catId = $topCats[0]['cat_id'];
            }
            // get user selected region
            if (isset($_COOKIE['deliver_store'])) {
                $selected_store = $_COOKIE['deliver_store'];
            } else {
                // region is not set, either use last order's address or use empty address
                $this->loadModel('mOrder');
                $last_order = $this->mOrder->get_latest_order_by_uid($uid);
                if ($last_order) {
                    $selected_store = $this->Stores->get_store_by_address($last_order['address']);
                } else {
                    $selected_store = 0;
                }
                $_COOKIE['deliver_store'] = $selected_store;
                $this->sCookie('deliver_store', $selected_store, 3600*24*60);
            }
            $this->assign('selected_store_id', $selected_store);

            $stores = $this->Stores->getAllActivated();
            $this->assign('stores', $stores);
            $this->assign('is_activity', $is_activity);
            $this->assign('title', $title);

            //
            if ($Q->date) {
                $target_date = strtotime($Q->date);
                $_COOKIE['deliver_date'] = date('y-n-d', $target_date);
                $this->sCookie('deliver_date', date('Y-m-d', $target_date), 3600*1);
            }
            
            $weekday_name = array('U', '周一', '周二', '周三', '周四', '周五', '周六', '周日');
            $deliver_end_hour = 13;
            // get selected time
            if (isset($_COOKIE['deliver_date'])) {
                $selected_time = strtotime($_COOKIE['deliver_date']);
                if (date('y-n-d') == $_COOKIE['deliver_date']) {
                    // check if order window is closed for TODAY
                    $current_hour = idate('H');
                    if ($current_hour >= $deliver_end_hour) {
                        // today's deliver window is closed. set to next day
                        $selected_time = strtotime('+1 day');
                    }
                }
            } else {
                $selected_time = time()+3600*24*0;
                $current_hour = idate('H');
                if ($current_hour >= $deliver_end_hour) {
                    // today's deliver window is closed. set to next day
                    $selected_time = strtotime('+1 day');
                }
                $grand_open = strtotime('2016-01-27');
                if ($selected_time < $grand_open) {
                    $selected_time = $grand_open;
                }
            }
            // search for first deliverable date
            do {
                $thisDay = date('Ymd', $selected_time);
                if (in_array($thisDay, $config->holidays[date('Y', $selected_time)]))
                    $selected_time += 3600*24;
                else
                    $found = true;
            }while(!$found);
            // set to array
            $weekday = date('N', $selected_time);
            /*
            if (7 == $weekday) {
                $selected_time += 3600*24;
                $weekday++;
            }
             */
            $weekday_str = $weekday_name[$weekday];

            $selected_date_str = date('y-n-d', $selected_time);
            $today_str = date('y-n-d');
            $tommrrow_str = date('y-n-d', strtotime('+1 day'));
            if ($today_str == $selected_date_str) {
                $weekday_str = '今天';
            } else if ($tommrrow_str == $selected_date_str) {
                $weekday_str = '明天';
            }
            
            if (!isset($_COOKIE['deliver_date'])) {
                $this->sCookie('deliver_date', $selected_date_str, 3600*1);
            } else if ($_COOKIE['deliver_date'] != $selected_date_str) {
                $this->sCookie('deliver_date', $selected_date_str, 3600*1);
            }
            $this->assign('selected_day', $selected_date_str);
            $this->assign('selected_weekday', $weekday_str);
            
        // error_log("======cat===".json_encode($topCats));
            if ($uinfo['first_login'] == 1) {
                $this->assign('show_tip', true);
                // clean flag
                $data = array('first_login' => 0);
                $this->User->updateUserInfo($uinfo['client_id'], $data);
            } else {
                $this->assign('show_tip', false);
            }
            
            $productList = $this->Carts->get_cart_products($uid);
            $count = 0;
            $total = 0;
            if($productList){

                foreach($productList as   $k => $v) {
                    $count += $v['product_quantity'];
                    $productSpecs =  $this->Product->getProductSpecs($v['product_id']);
                    if($productSpecs){
                        $total=$total+$v['product_quantity']*$productSpecs[0]['sale_price'];
                    }
                }
                
                
                foreach($topCats as   $c_k => $c_v) {
                    
                    foreach($productList as   $p_k => $p_v) {
                        
                        if($c_v['cat_id'] == $p_v['product_cat']){
                            
                            $topCats[$c_k]['count'] = $topCats[$c_k]['count']+$p_v['product_quantity'];
                            
                        }
                    }
                } 
            }
         $this->assign('topcat', $topCats);
         $this->assign('count', $count);
         $this->assign('total', $total);
         $this->assign("uinfo",$uinfo);
   
        }
        $totalYun = 0;
        $address = $this->mUserAddress->enableUserAddress($uid);
        if($address){
            $providerList = $this->Carts->getCartProductSupplier($uid);
            foreach($providerList as $pk => $pv){

                $provider_id =  $pv['provider_id'];
                $providerList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
                $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
                $yun_no = $productList[0]['yun_no'];
                $yun = $this->mYun->get_detail_yun_sys($yun_no);
                $providerList[$pk]['yun'] = $yun;

                foreach ($productList as   $key => $val) {
                    $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
                }
                $providerList[$pk]['productList'] = $productList;
                $cost =  $this->Carts->calc_product_yun($productList,$address);
                $providerList[$pk]['yun_cost'] = $cost;

                $totalYun = $totalYun + doubleval($cost);
            }

            // error_log("=========providerList========".json_encode($providerList));

        }else{
            $totalYun = 0;
        }
        $banners = $this->Banners->getBanners();
        $this->assign("banners",$banners);
        $this->assign("yun",$totalYun);
        $this->assign("catId",$catId);

        $this->show();
    }
    
    
    public final function ajax_list_item($Query){
        $this->loadModel('Product');
        $openid = $this->getOpenId();
        $this->loadModel('Carts');
        $this->loadModel('User');
        $this->loadModel('Stock');
        $this->loadModel('mSupplier');
        $this->loadModel('Brand');

        $uinfo = $this->User->getUserInfo($openid);
        $uid = $uinfo['uid'];
        
        // FIXME: $target_time should be set based on which day user select
        if (isset($_COOKIE['deliver_date'])) {
            $date = $_COOKIE['deliver_date'];
            $target_time = strtotime($date);
        } else {
            $target_time = time();
        }
        
        $selected_store = $_COOKIE['deliver_store'];

        $this->Smarty->cache_lifetime = 0;
        $isFirst = 0;
        $endTime = "";
        $count = 0;
        $totalMoney = 0;
        $cat_id = "";
        $isHaveMs = 0;
        if (isset($Query->id)) {
            $this->cacheId = intval($Query->id);
            $this->sCookie('catId', $this->cacheId, 3600*24*60);
            $topCats = $this->Product->getCatList(0);
            $cat_id = $this->cacheId;
            if($this->cacheId == $topCats[0]['cat_id']){
                $isFirst = 1;
                $productsList = $this->Product->getNewEst(119);

                $cat_id = $topCats[0]['cat_id'];
                foreach ($productsList as $key => $val) {
                    $isHaveMs = 1;
                    $cat = $this->Product->getCatInfo($val['product_cat']);
                    $productSpecs = $this->Product->getProductSpecs($val['product_id']);
                    if($productSpecs){
                        $productsList[$key]['pinfo'] = $productSpecs[0];
                        // get stock of given SKU for target date
                        $stock_info = $this->Stock->get_product_instock_by_sku_and_date($productSpecs[0]['id'], $target_time, $selected_store);
                        //error_log('stock info:'.json_encode($stock_info));
                        $productsList[$key]['pinfo']['instock'] = $stock_info['stock']; // stocks can be sold for target date
                    }

                    if($val['product_cat'] == 119){
                        $killTime = $val['kill_time'];
                        $nowTime = strtotime('now');
                        $kill  =  strtotime($killTime);
                        if($nowTime > $kill){
                            $products[$key]['is_kill'] = 1;
                        }
                        $endTime = $val['kill_time'];
                    }
                    $productsList[$key]['product_quantity'] = 0;
                    $provider_id = $val['provider_id'];
                    $supplier = $this->mSupplier->get_detail_supplier($provider_id);
                    $productsList[$key]['supplier'] = $supplier;

                    $brand = $this->Brand->get($val['product_brand'],false);
                    $productsList[$key]['brand'] = $brand;
                    $productsList[$key]['cat'] = $cat;

                }

                $catLsit = $this->Carts->get_cart_products($uid);
                if($catLsit){
                    foreach($productsList as   $key => $val) {

                        foreach($catLsit as   $k => $v) {

                            if($val['product_id'] == $v['product_id']){

                                $productsList[$key]['product_quantity'] = $v['product_quantity'];
                                $totalMoney = $totalMoney+$v['product_quantity']*$val['sale_price'];
                            }
                        }
                    }

                    //计算购物车总数
                    foreach($catLsit as   $k => $v) {
                        $count = $count + $v['product_quantity'];
                    }
                }
                $this->assign('products2', $productsList);
                $this->assign('endTime', $endTime);

            }
            if($isFirst== 1){
                $products = $this->Product->getRecommedList();
            }else{
                $products = $this->Product->getNewEst($this->cacheId);
            }
            //必须要添加一个规格
            foreach ($products as $key => $val) {
                $cat = $this->Product->getCatInfo($val['product_cat']);
                $productSpecs = $this->Product->getProductSpecs($val['product_id']);
                if($productSpecs){
                    $products[$key]['pinfo'] = $productSpecs[0];
                    // get stock of given SKU for target date
                    $stock_info = $this->Stock->get_product_instock_by_sku_and_date($productSpecs[0]['id'], $target_time, $selected_store);
                    //error_log('stock info:'.json_encode($stock_info));
                    $products[$key]['pinfo']['instock'] = $stock_info['stock']; // stocks can be sold for target date
                }

                if($val['product_cat'] == 119){
                    $killTime = $val['kill_time'];
                    $nowTime = strtotime('now');
                    $kill  =  strtotime($killTime);
                    if($nowTime > $kill){
                        $products[$key]['is_kill'] = 1;
                    }
                }
                $products[$key]['product_quantity'] = 0;
                $provider_id = $val['provider_id'];
                $supplier = $this->mSupplier->get_detail_supplier($provider_id);
                $products[$key]['supplier'] = $supplier;

                $brand = $this->Brand->get($val['product_brand'],false);
                $products[$key]['brand'] = $brand;
                $products[$key]['cat'] = $cat;


            }

            $productList = $this->Carts->get_cart_products($uid);
            if($productList){
                 foreach($products as   $key => $val) {
                    
                      foreach($productList as   $k => $v) {
                          
                          if($val['product_id'] == $v['product_id']){
                          
                             $products[$key]['product_quantity'] = $v['product_quantity'];
                             $totalMoney = $totalMoney+$v['product_quantity']*$val['sale_price'];
                          }
                      }
                 }
                  
                //计算购物车总数
                 foreach($productList as   $k => $v) {     
                   $count = $count + $v['product_quantity'];
                 }
            }
            // HACK:check current weekday to find prefix should be added
            $weekday = date('N', strtotime($_COOKIE['deliver_date']));
            if (4 == $weekday) {
                // Thursday is "HALF-day" sale
                //$this->assign('prefix', '半价日-');
            }

            $cat = $this->Product->getCatInfo($cat_id);
            $cartCss = '';
            if($cat['is_activity'] == 1){
                $cartCss = 'gorupcart';
            }
            $this->assign('cartCss', $cartCss);
            $this->assign('isHaveMs', $isHaveMs);

            $this->assign('prefix', '');
            $this->assign('isFirst', $isFirst);
            $this->assign('count', $count);
            $this->assign('total', $totalMoney);
            $this->assign('products', $products);
            $this->assign('cat_id', $cat_id);
            $this->show('./index/ajax_list_item.tpl');
            // 分类下面无子分类
        }
   }
   
  public function ajaxGetCartProducts(){
  
     $this->loadModel('Carts');
     $this->loadModel('User');
     $this->loadModel('Product');
     $this->loadModel('Stock');

     $openid = $this->getOpenId();
     $uinfo = $this->User->getUserInfo($openid);
     $uid = $uinfo['uid'];
     $totalMoney = 0;
      
      $this->Smarty->cache_lifetime = 0;
      
      // FIXME: check_time should be set based on which day user select
      if (isset($_COOKIE['deliver_date'])) {
          $date = $_COOKIE['deliver_date'];
          $target_time = strtotime($date);
      } else {
          $target_time = time();
      }

      $product = $this->Carts->get_cart_products($uid);
      $productList = array();
      foreach ($product as $key => $val) {

          if($val['product_cat'] == 119){
              $killTime = $val['kill_time'];
              $nowTime = strtotime('now');
              $kill  =  strtotime($killTime);
              if($nowTime < $kill){
                  array_push($productList, $val);
              }else{
                  $this->Carts->remove_product($uid,$val['product_id'],$val['spec_id']);

              }
          }else{
              array_push($productList, $val);
          }

      }

      foreach ($productList as   $key => $val) {
          $productSpecs = $this->Product->getProductSpecs($val['product_id']);



          if($productSpecs){
              $productList[$key]['pinfo'] = $productSpecs[0];
              // get stock of given SKU for target date
              $stock_info = $this->Stock->get_product_instock_by_sku_and_date($productSpecs[0]['id'], $target_time);
              $productList[$key]['pinfo']['instock'] = $stock_info['stock']; // stocks can be sold for target date
              $totalMoney = $totalMoney+$val['product_quantity']*$productSpecs[0]['sale_price'];
              
          }
      }
       $count = 0; 
      foreach($productList as   $k => $v) {     
          $count += $v['product_quantity'];      
      } 
     $this->assign('product_list', $productList);
     $this->assign('count', $count);
     $this->assign('total', $totalMoney);

      $this->show('./index/ajax_cart_item.tpl');
     
  }

   public function ajaxRemoveProduct(){

       $this->loadModel('Carts');
       $this->loadModel('User');
       $this->loadModel('Product');

       $productId = $_POST['product_id'];
       $specId = $_POST['spec_id'];

       
       $openid = $this->getOpenId();
   
       $uinfo = $this->User->getUserInfo($openid);
       $uid = $uinfo['uid'];
       $this->Carts->remove_product($uid,$productId,$specId);
       $productList = $this->Carts->get_cart_products($uid);
       foreach ($productList as   $key => $val) {
          $productSpecs = $this->Product->getProductSpecs($val['product_id']);
          if($productSpecs){
              $productList[$key]['pinfo'] = $productSpecs[0];
              $totalMoney = $totalMoney+$val['product_quantity']*$productSpecs[0]['sale_price'];
              
          }
      }
      $count = 0; 
      foreach($productList as   $k => $v) {     
          $count += $v['product_quantity'];      
      } 
      //error_log("=============productList==========".json_encode(count($productList)));
     $this->assign('product_list', $productList);
     $this->assign('count', $count);
     $this->show('./index/ajax_cart_item.tpl');
  }
  
  public function ajaxRemoveProductUPdateData(){
  
     $this->loadModel('Carts');
     $this->loadModel('User');
     $this->loadModel('Product');
     $openid = $this->getOpenId();
     $uinfo = $this->User->getUserInfo($openid);
     $uid = $uinfo['uid'];
     $totalMoney = 0;
     
     $productId = $_POST['pid'];
     $specId = $_POST['sid'];
     $this->Carts->remove_product($uid,$productId,$specId);
     
     $productList = $this->Carts->get_cart_products($uid);
     foreach ($productList as   $key => $val) {
          $productSpecs = $this->Product->getProductSpecs($val['product_id']);
          if($productSpecs){
              $productList[$key]['pinfo'] = $productSpecs[0];
              $totalMoney = $totalMoney+$val['product_quantity']*$productSpecs[0]['sale_price'];
              
          }
      }
     $count = 0; 
     foreach($productList as   $k => $v) {     
          $count += $v['product_quantity'];      
     }
     
    $topCats = $this->Product->getCatList(0);
    foreach($topCats as   $c_k => $c_v) {  

                foreach($productList as   $p_k => $p_v) {  

                  if($c_v['cat_id'] == $p_v['product_cat']){

                       $topCats[$c_k]['count'] = $topCats[$c_k]['count']+$p_v['product_quantity'];

                  }
                }
         } 
     
     $data =  array(
           'count' => $count,
           'total' =>$totalMoney,
           'topCats' =>$topCats
     );
    $this->echoJson($data);
     
  }
    
    public function getDeliverDateList()
    {
        $weekday_name = array('U', '周一', '周二', '周三', '周四', '周五', '周六', '周日');
        $totalDays = 7;

        $current_time = time();
        $current_hour = intval(date('G', $current_time));
        if ($current_hour >= 13) 
            $current_time += 3600*24*1;

        $grand_open = strtotime('2016-01-27');
        if ($current_time < $grand_open) {
            $current_time = $grand_open;
        }
        
        $delivers = array();
        // select {$totalDays} deliverable days
        $thisTime = $current_time;
        global $config;
        while(count($delivers) < $totalDays){
            // search
            $found = false;
            do {
                $thisDay = date('Ymd', $thisTime);
                if (in_array($thisDay, $config->holidays[date('Y', $thisTime)]))
                    $thisTime += 3600*24;
                else
                    $found = true;
            }while(!$found);
            // set to array
            $date_str = date('y-n-d', $thisTime);
            $weekday = date('N', $thisTime);
            $weekday_str = $weekday_name[$weekday];
            $day = array(
                         'date' => $date_str,
                         'weekday' => $weekday_str,
                         );
            $delivers[] = $day;
            // move to next day
            $thisTime += 3600*24;
        }
        /*
        for ($i=0; $i < $totalDays; $i++) {
            $time = $i*3600*24 + $current_time;
            $weekday = date('N', $time);
            if (7 == $weekday) continue;
            $date_str = date('Y-m-d', $time);
            $weekday_str = $weekday_name[$weekday];
            
            $day = array(
                         'date' => $date_str,
                         'weekday' => $weekday_str,
                         );
            $delivers[] = $day;
        }
         */
        $today_str = date('Y-m-d');
        $tommrrow_str = date('Y-m-d', strtotime('+1 day'));
        if ($today_str == $delivers[0]['date']) {
            $delivers[0]['weekday'] = '今天';
            if ($tommrrow_str == $delivers[1]['date']) {
                $delivers[1]['weekday'] = '明天';
            }
        } else if ($tommrrow_str == $delivers[0]['date']) {
            $delivers[0]['weekday'] = '明天';
        }

        echo htmlspecialchars(json_encode($delivers), ENT_NOQUOTES);
    }
    
    public function setDeliverDate($Q)
    {
        if (!$Q->date)
            echo htmlspecialchars(json_encode(array('err' => -1)), ENT_NOQUOTES);
        // 1 hour by default
        //error_log('set day cookie:'.$Q->date);
        $this->sCookie('deliver_date', $Q->date, 3600*1, '/');
        echo htmlspecialchars(json_encode(array('err' => 0)), ENT_NOQUOTES);
    }
    
    public function setDeliverStore($Q)
    {
        if (!$Q->id) {
            echo htmlspecialchars(json_encode(array('err' => -1)), ENT_NOQUOTES);
            exit(0);
        }

        $this->sCookie('deliver_store', $Q->id, 3600*24*60, '/');
        echo htmlspecialchars(json_encode(array('err' => 0)), ENT_NOQUOTES);
    }

	public function setStore()
    {
        $this->loadModel('Stores');
        $stores = $this->Stores->getAllActivated();
        $this->assign('stores', $stores);
		$this->show('./index/set_store.tpl');
	}
    
    public function clearStore()
    {
        $this->sCookie('deliver_store', 0, -10, '/');
    }


    public function error_404(){
      $this->show('./health/health_page.tpl');
    }
}
