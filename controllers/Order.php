<?php

// 支付授权目录 112.124.44.172/wshop/
// 支付请求示例 index.php
// 支付回调URL http://112.124.44.172/wshop/?/Order/payment_callback
// 维权通知URL http://112.124.44.172/wshop/?/Service/safeguarding
// 告警通知URL http://112.124.44.172/wshop/?/Service/warning

/**
 * 订单类
 */
class Order extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }

    // 支付回调页面 代付
    public function payment_notify_req() {
        // postStr
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        $reqId = str_replace('req-', '', $postObj->out_trade_no);
        // 微信交易单号
        $transaction_id = $postObj->transaction_id;
        // 更新订单状态
        $this->Db->query(sprintf("UPDATE `order_reqpay` SET `wepay_serial` = '%s',`dt` = NOW() WHERE `id` = %s AND `openid` = '%s';", $transaction_id, $reqId, $postObj->openid));
        // 邮件通知
        if ($reqId > 0) {
            // order_reqpay
            $this->loadModel('mOrder');
            $this->loadModel('WechatSdk');
            $orderId = $this->Dao->select('order_id')->from('order_reqpay')->where("id = $reqId")->getOne();
            // 检查募集成功
            $orderInfo = $this->mOrder->getOrderInfo($orderId);
            $reqEd = $this->mOrder->getOrderReqAmount($orderId);
            if ($reqEd == $orderInfo['order_amount']) {
                // 成功
                $this->Dao->update(TABLE_ORDERS)->set(array('status' => 'payed'))->where("order_id = $orderId")->exec();
                Messager::sendText(WechatSdk::getServiceAccessToken(), $orderInfo['wepay_openid'], "您有一笔代付进度已经到达100%！请进入个人中心查看");
            } else {
                Messager::sendText(WechatSdk::getServiceAccessToken(), $orderInfo['wepay_openid'], "您有一笔订单成功获得代付！请进入个人中心查看");
            }
        }
        // 返回success
        echo "<xml><return_code><![CDATA[SUCCESS]]></return_code></xml>";
    }

    // 支付回调页面
    public function payment_notify() {
        global $config;
         $this->loadModel('User');
         $this->loadModel('mOrder');
         $this->loadModel('mCashPay');
        $this->loadModel('mGroup');
        $this->loadModel('Product');
        $this->loadModel('WechatSdk');

        // postStr
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        // orderid 
        $out_trade_no = $postObj->out_trade_no;
        $isMode = strpos($out_trade_no,'dq') === 0;
        $mode = 1;//0线下餐厅支付  1商品购买支付

        //中英体检卡的订单加载逻辑
        $fullOutTradeNo = str_replace($config->out_trade_no_prefix, '', $postObj->out_trade_no);
        $partnerHealthPackagePrefix = substr($fullOutTradeNo,0,5);
        $isHealthPackage = $partnerHealthPackagePrefix == $config->zy_order_midfix;

		if($isMode){
    	   $mode = 0; 
    	   $orderId = str_replace('dq', '', $postObj->out_trade_no);
    	   $len = strlen($orderId);
           $orderNum = substr($orderId,$len-6,$len);
           $orderId =  preg_replace('/^0*/', '',$orderNum);
         	
         	$pay = $this->mCashPay->getCrashPayInfo($orderId);
         
          if($pay && !$pay['is_send']){
          
            $code = $this->mCashPay->getCode();          
            $openid = "oalpuuDbXtJL4oIBzDBNW8n9ZdoY";//点沁
            $this->mCashPay->paySuccessNotify($orderId,$openid);
            $this->mCashPay->payUserNotify($orderId,$postObj->openid); 

             $statusArray =  array(
                             'status' => 'payed',
                             'code'=>$code,
                             'is_send'=>1
                          );
            $this->mCashPay->updateOrder($orderId,$statusArray);
          }   
		}else if($isHealthPackage){ //判断是否为中英体检卡的订单
            $this->loadModel('mHealthPackage');
//            $this->mHealthPackage->wxPayNotify($fullOutTradeNo,$postObj->transaction_id);
            $this->mHealthPackage->wxPayNotify($fullOutTradeNo,$postObj);
        }else{
    	   $mode = 1; 
    	   $orderId = str_replace($config->out_trade_no_prefix, '', $postObj->out_trade_no);
    	   
		
        
        $len = strlen($orderId);
        $orderNum = substr($orderId,$len-6,$len);
        $orderId =  preg_replace('/^0*/', '',$orderNum);
 
        // 微信交易单号
        $transaction_id = $postObj->transaction_id;
        // 更新订单状态
        $wepay_serial = $this->Dao->select('wepay_serial')->from(TABLE_ORDERS)->where("order_id = $orderId")->getOne(false);

        if ($wepay_serial == '') {
            $UpdateSQL = sprintf("UPDATE `orders` SET `wepay_serial` = '%s',`status` = 'payed',`wepay_openid` = '%s' WHERE `order_id` = %s AND `status` <> 'payed';", $transaction_id, $postObj->openid, $orderId);
            
            $r1 = $this->Db->query($UpdateSQL);

            // 邮件通知
            if ($r1 !== false && $orderId > 0) {

                $orderInfo = $this->mOrder->GetSimpleOrderInfo($orderId);
                //error_log("=============payment_notify=============".json_encode($orderInfo));
                if($orderInfo){
                    
                    $uinfo = $this->User->getUserInfoRaw($postObj->openid);
                    $uid = $uinfo['client_id'];

                    $this->loadModel('mAward');
                    $award = $this->mAward->getAwardDetail($orderInfo[0]['order_amount']);
                    if($award){
                        $this->loadModel('Coupons');
                        $this->loadModel('UserCoupon');
                        // add coupon to user
                        $rtnCode = $this->UserCoupon->insertUserCoupon($award['coupon_id'], $uid, true,'order');
                        $this_coupon = $this->Coupons->get_coupon_info(($award['coupon_id']));
                        $discount_val = intval($this_coupon['discount_val']/100);
                        $content = "偷偷送你一张（".$discount_val."元）优惠券~不要告诉别人呦\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康美食吧~";
                        $url = $config->domain.'?/Coupon/user_coupon/';
                        Messager::sendNotification(WechatSdk::getServiceAccessToken(), $postObj->openid, $content, $url);

                    }
                    // 支付成功奖励？
                      $balance_amount =$orderInfo[0]['balance_amount'];
                      $id = $this->moneyPay($uid,$balance_amount);

                    if($orderInfo[0]['is_tuan'] == 1){
                        $order_product_list = $this->mOrder->GetOrderDetails(intval($orderInfo[0]['order_id']));
                        $product_info = $this->Product->get_simple_product_info($order_product_list[0]['product_id']);
                        $this->mGroup->johnGroup($orderInfo[0]['order_id'],$orderInfo[0]['client_id'],$orderInfo[0]['group_id'],$product_info);
                    }
                }
                
                

                $this->loadModel('User');
                $this->loadModel('mOrder');
                $this->loadModel('mOrderDistribute');
                //添加配送单
                //error_log("======== begin to go to create_distribute_list_by_order_id wechat callback ===================". $pay_amount);
                $this->mOrderDistribute->create_distribute_list_by_order_id($orderId);
                
                $this_order_info = $this->mOrder->get_order_info_by_id($orderId);
                $this->mOrder->cutInstock($orderId, $this_order_info['store_id']);
                // 商户订单通知
                @$this->mOrder->comNewOrderNotify($orderId);
                // 用户订单通知 模板消息
                @$this->mOrder->userNewOrderNotify($orderId, $postObj->openid);
                // 导入订单数据到个人信息
                @$this->User->importFromOrderAddress($orderId);
                // 积分结算
                @$this->mOrder->creditFinalEstimate($orderId);
                // 减库存
                
                
            }

            // 返回success
            echo "<xml><return_code><![CDATA[SUCCESS]]></return_code></xml>";
        }
       }
    }


     /**
    * 生成订单
   * // openid , 产品 list ,地址, 付款类型,发票抬头，发票内容 ，总额，需支付金额，在线支付金额，余额支付，运费
    */
    public function  createOrder()
    {
        $this->loadModel('mOrder');
        $this->loadModel('User');
        $this->loadModel('Coupons');
        $this->loadModel('mProductSpec');
        $this->loadModel('Product');
        $this->loadModel('Stock');
        $this->loadModel('mUserAddress');
        $this->loadModel('mGroup');

        $openid = $this->getOpenId();
        $this->loadModel('Carts');
        $uinfo = $this->User->getUserInfo($openid);
        $uid = $uinfo['uid'];
        $addrData = $_POST['addrData'];
        $reciHead = $_POST['reciHead'];
        $reciCont = $_POST['reciTex'];
        $fromUid = $_POST['from_uid'];
        $isTuan = $_POST['isTuan'];
        $groupId = $_POST['groupId'];

        $pid = $_POST['pid'];
        // format time
        $time = $_POST['time'];
        $time = str_replace('月', '-', $time);
        $time = str_replace('日', '', $time);
        
        // check if user wins MUJI - promotion in 20160308
        $is_moji_winner = $this->Dao->select("id")->from('20160308_gifts')->where("`uid` = $uid AND delivered=0")->getOne();
        if ($is_moji_winner AND (time()<strtotime('2016-03-11 18:00:00'))) {
            $user_note = $_POST['user_note'] . ' 同时配送MUJI奖品';
            // update deliver status
            $this->Dao->update('20160308_gifts')->set(array('delivered' => 1))->where("`uid` = $uid")->exec();
        } else {
            $user_note = $_POST['user_note'];
        }
        
        $coupon = $_POST['coupon'];
        $userBalance = $uinfo['balance'];
        $isbalance = 1;
        
        $pay_type = 0;//默认 0  在线支付  1 余额支付  2 在线+余额度
        $yun = 0;
        
        
        $online_amount = 0;
        $balance_amount = 0;

        $address = $this->mUserAddress->get_user_address_by_id($addrData);

//        if(!Controller::inWechat()){
//            $this->show('./index/error.tpl');
//            die(0);
//        }

        if($isTuan == 1 || $isTuan == 2){


            $product = $this->Product->getProductInfo($pid,false);
            $product['pinfo'] =  $this->Product->getProductInfoWithSpec($product['product_id'],$product['spec_id']);

            if($product['product_online'] == 0){
                $this->echoMsg(-1,$product['product_name']."已下架");
                die(0);
            }

            if($isTuan == 1 && strtotime($product['kill_time']) <time()){
                $this->echoMsg(-1,"商品活动已结束");
                die(0);
            }

            $group = $this->mGroup->getGroupDetailById($groupId);


            if($group){
                if($group['status'] == 1 || $group['status'] == -1){
                    $this->echoMsg(-1,"亲，你来晚了 此团已结束");
                    die(0);
                }
                if($uid == $group['uid']){
                    $this->echoMsg(-1,"亲，你是改团团长，不能参与次团");
                    die(0);
                }
               $join =  $this->mGroup->getGroupJoinByGroupId($groupId,$uid);
                if($join){
                    $this->echoMsg(-1,"亲，你已参与次团，不能重复参团");
                    die(0);
                }

                if($group['status'] == 0){
                    $now = time();
                    $groupEndTime = $group['end_time'];
                    if($groupEndTime <= $now){
                        $serArray = array('status'=>-1);
                        $this->mGroup->updateGroup($group['id'],$serArray);
                        $group['status'] = -1;
                        $this->mGroup->groupFailMsg($group['id']);
                        $this->echoMsg(-1,"亲，你来晚了 拼团已结束");
                        die(0);
                    }
                }
            }



            if($isTuan == 1){
                $totalAmount = $product['group_price'];
                $productList = array();
                array_push($productList,$product);
                $yun  = $this->Carts->calc_product_yun($productList,$address);
                $totalAmount += $yun;
            }else{
                $productList = array();
                array_push($productList,$product);
                $yun  = $this->Carts->calc_product_yun($productList,$address);
                $totalAmount = $product['specs'][0]['sale_price'];
                $totalAmount += $yun;

            }


            $pay_amount = $totalAmount - $uinfo['balance'];
            if($pay_amount <= 0){
                $pay_amount = 0;
            }

           $orderId = $this->mOrder->createGroupOrder($openid,$product,$addrData,$totalAmount,$pay_amount,$user_note,$groupId,$isTuan,$yun);

        }else{
    	//验证库存
        $cartList = $this->Carts->get_user_order_cart($uid);
        if(count($cartList) == 0){
            $this->echoMsg(-1,"购物车为空,请添加你要购买的商品");
            die(0);
        }
        //$ret[] = array('pid' => intval($matchs[1]), 'spid' => intval($matchs[2]), 'count' => intval($count));
        $product_stock_not_enouph_msg_arr = array();
        $stock_not_enouph = '库存不足';
        $hint = '';
        // split $time to get deliver date
        $check_time = strtotime(substr($time, 0, 10)); //'2015-01-01'
        $is_abroad = 0;//0 不是国内  1是国外
        foreach ($cartList as $key =>$cart){
        	//$product_spec_info = $this->mProductSpec->get_spec_sale_price($cart['spid']);
        	$product_info = $this->Product->get_simple_product_info($cart['pid']);
            if($product_info['yun_fa'] == 1 || $product_info['yun_fa']== 2){

                $is_abroad = 1;
            }
            if($product_info['product_online'] == 0){
                $this->echoMsg(-1,$product_info['product_name']."已下架");
                die(0);
            }

            if($product_info['product_cat'] == '119'){
                $killTime = $product_info['kill_time'];
                $nowTime = strtotime('now');
                $kill  =  strtotime($killTime);
                if($nowTime > $kill){
                    $this->echoMsg(-1,$product_info['product_name']."秒杀过期");
                    die(0);
                }
            }
            $stock_info = $this->Stock->get_product_instock_by_sku_and_date($cart['spid'], $check_time, $_COOKIE['deliver_store']);

        	if($stock_info['stock']<$cart['count']){ //库存
        		$hint = $product_info['product_name'].$stock_not_enouph;
        		$product_stock_not_enouph_msg_arr[] = $hint;
        	}
        }

        $amount = $this->mOrder->sumOrderAmount($cartList);
        $providerList = $this->Carts->getCartProductSupplier($uid);

        $yun = 0;
        foreach($providerList as $pk => $pv){

            $provider_id =  $pv['provider_id'];
            $productList = $this->Carts->get_cart_products_by_provider($uid,$provider_id);
            foreach ($productList as   $key => $val) {
                $productList[$key]['pinfo'] =  $this->Product->getProductInfoWithSpec($val['product_id'],$val['spec_id']);
            }
            $providerList[$pk]['productList'] = $productList;

            if($address){
                $providerList[$pk]['yun_cost'] = $this->Carts->calc_product_yun($productList,$address);
            }else{
                $providerList[$pk]['yun_cost'] = 0;
            }
            $yun += $providerList[$pk]['yun_cost'];
        }
        $mAddress = $this->mUserAddress->get_user_address_by_id($addrData);
        if($is_abroad && $mAddress['identity'] == ''){

            $this->echoMsg(2,$mAddress['id']);
            die(0);
        }
        $redAmount = 0;
      if($coupon != ""){
          $arr = explode(",",$coupon);

          foreach($arr as $u){
              $couponInfo = $this->Coupons->get_coupon_info($u); 
              if($couponInfo['coupon_type'] == 1 || $couponInfo['coupon_type'] == 2){
                  
                  $couponAmount = $this->mOrder->cal_reduce_amount_by_coupon_id($amount,$u,$uid,false);
                  $redAmount = $redAmount + $couponAmount;
              }
         }
      }

        $totalAmount = $amount + $yun;//总额

        $pay_amount = $totalAmount - $redAmount - $uinfo['balance'];
        if($pay_amount <= 0){
            $pay_amount = 0;
        }

        $orderId = $this->mOrder->createOrder($openid,$cartList,$addrData,$pay_type,$reciHead,$reciCont,$totalAmount,$pay_amount,$online_amount,$balance_amount,$yun,$coupon,$isbalance,$user_note, $time, $_COOKIE['deliver_store'],$fromUid);
		
		
		 if($coupon != ""){
      
            $arr = explode(",",$coupon);
            foreach($arr as $u){
                 $couponInfo = $this->Coupons->get_coupon_info($u);
                 $this->Coupons->use_selected_coupon($couponInfo,$uid,time());
            }
         }
            $this->Carts->del_cart($uid);

        }
        //清空购物车
        $this->echoMsg(1,$orderId);
        
    }
    
    
    
    public function ajaxOrderPay(){
        $this->loadModel('User');
        $this->loadModel('mOrder');
        $this->loadModel('Coupons');
        $this->loadModel('mProductSpec');
        $this->loadModel('Product');
        $this->loadModel('mOrderDistribute');
        $this->loadModel('mGroup');
        $this->loadModel('WechatSdk');

        $this->loadModel('Stock');
        $orderId = $_POST['orderId'];
        $openid = $this->getOpenId();
        
        $uinfo = $this->User->getUserInfo($openid);
        $uid = $uinfo['uid'];
        $userBalance = $uinfo['client_money'];
        global $config;

        if($orderId <= 0){
           $this->echoMsg(-1,"无效订单");
        }else{
           $checkResult = $this->mOrder->isValidOrder($orderId);
           $orderInfo = $this->mOrder->GetSimpleOrderInfo($orderId);


           if($checkResult == ""){
            // error_log("=============checkResult===================订单已支付或者订单不存在".$checkResult);
               $this->echoMsg(-1,"订单已支付或者订单不存在");
           }else{
	           	//只检查未支付订单中的库存
	           	if($orderInfo[0]['status'] == 'unpay'){
	           		$order_product_list = $this->mOrder->GetOrderDetails(intval($orderInfo[0]['order_id']));

	           		//$ret[] = array('pid' => intval($matchs[1]), 'spid' => intval($matchs[2]), 'count' => intval($count));
	           		$product_stock_not_enouph_msg_arr = array();
	           		$stock_not_enouph = '库存不足';
	           		$hint = '';
	           		//$check_time = time();
                    $deliver_time = $orderInfo[0]['exptime'];
                    $check_time = strtotime(substr($deliver_time, 0, 10)); //'2015-01-01'
	           		foreach($order_product_list as $key => $order_product){
	           			//$product_spec_info = $this->mProductSpec->get_spec_sale_price($order_product['product_price_hash_id']);

	           			$stock_info = $this->Stock->get_product_instock_by_sku_and_date($order_product['product_price_hash_id'], $check_time, $_COOKIE['deliver_store']);

	           			$product_info = $this->Product->get_simple_product_info($order_product['product_id']);
                        if($product_info['product_online'] == 0){
                            $this->echoMsg(-1,$product_info['product_name']."已下架");
                            die(0);
                        }

	           			if($stock_info['stock']<$order_product['product_count']){ //库存
	           				$hint = $product_info['product_name'].$stock_not_enouph;
	           				$product_stock_not_enouph_msg_arr[] = $hint;
	           			}
	           		}
                    $voucher = '';
                    $voucher = $this ->getGoodsTag($order_product_list);

                    if($orderInfo[0]['is_tuan'] == 1){

                        $order_product_list = $this->mOrder->GetOrderDetails(intval($orderInfo[0]['order_id']));
                        $product_info = $this->Product->get_simple_product_info($order_product_list[0]['product_id']);
                        if(strtotime($product_info['kill_time']) <time()){
                            $stateArray =  array(
                                'status' => 'closed'
                            );
                            $this->mOrder->updateOrder($orderInfo[0]['order_id'],$stateArray);
                            $this->echoMsg(-1,"商品活动已结束");
                            die(0);
                        }

                        $isExsit = $this->mGroup->getGroupDetail($orderInfo[0]['client_id'],$order_product_list[0]['product_id']);
                        if($isExsit && $isExsit['status'] == 0 && $orderInfo[0]['group_id'] == 0){
                            $this->echoMsg(-1,"亲，此商品你已开了团!");
                            $serArray = array('status'=>-1);
                            $stateArray =  array(
                                'status' => 'closed',
                            );
                            $this->mOrder->updateOrder($orderId,$stateArray);
                            $group['status'] = -1;
                            $this->mGroup->groupFailMsg($group['id']);
                            die(0);
                        }


                        if($orderInfo[0]['group_id'] != 0){
                            $userGroup = $this->mOrder->getUserGroupList($orderInfo[0]['client_id'],$orderInfo[0]['group_id']);
                            if($userGroup ){
                                $stateArray =  array(
                                    'status' => 'closed'
                                );
                                $this->mOrder->updateOrder($orderInfo[0]['order_id'],$stateArray);
                                $this->echoMsg(-1,"亲，你已参加了此拼团");
                                die(0);
                            }
                        }

                        $group = $this->mGroup->getGroupDetailById($orderInfo[0]['group_id']);
                        if($group){
                            if($group['status'] == 1 || $group['status'] == -1){
                                $stateArray =  array(
                                    'status' => 'closed'
                                );
                                $this->mOrder->updateOrder($orderInfo[0]['order_id'],$stateArray);
                                $this->echoMsg(-1,"亲，你来晚了,此拼团已结束");
                                die(0);
                            }

                            if($group['status'] == 0){
                                $now = time();
                                $groupEndTime = $group['end_time'];
                                if($groupEndTime <= $now){
                                    $stateArray =  array(
                                        'status' => 'closed'
                                    );
                                    $this->mOrder->updateOrder($orderInfo[0]['order_id'],$stateArray);
                                    $this->echoMsg(-1,"亲，你来晚了 拼团已结束");
                                    die(0);
                                }
                            }
                        }
                    }



	           	}
            
             $totalAmount = $orderInfo[0]['order_amount'];//总额度 包括运费

               $pay_amount  =  $totalAmount;   //实际支付额度
               $redAmount = 0;
               $coupon =  $orderInfo[0]['user_coupon'];
               if($coupon!= ""){
                   $arr = explode(",",$coupon);

                   foreach($arr as $u){
                       $couponInfo = $this->Coupons->get_coupon_info($u);
                       if($couponInfo['coupon_type'] == 1 || $couponInfo['coupon_type'] == 2){

                           $couponAmount = $this->mOrder->cal_reduce_amount_by_coupon_id($totalAmount,$u,$uid,false);
                           $redAmount = $redAmount + $couponAmount;
                       }
                   }
               }

             $pay_amount =   $totalAmount-$redAmount;
             $balance_amount = 0;//用户余额支付
             if($userBalance >= $pay_amount){

                 $balance_amount = $pay_amount;
                 $pay_amount = 0;
             }else{
                 $pay_amount = $pay_amount - $userBalance;
                 $balance_amount = $userBalance;
             }

             if($pay_amount == 0){
                   $pay_type = 1;
                   $id = $this->moneyPay($uid,$balance_amount);
              
                         $stateArray =  array(
                             'status' => 'payed',
                             'pay_type' => $pay_type,
                             'balance_amount' =>$balance_amount,
                             'pay_amount' =>$pay_amount
                          );
                      $this->mOrder->updateOrder($orderId,$stateArray);
                      $this->mOrder->comNewOrderNotify($orderId);
                      //加入配送单
                      $this->mOrderDistribute->create_distribute_list_by_order_id($orderId);

                    if($orderInfo[0]['is_tuan'] == 1){
                        $order_product_list = $this->mOrder->GetOrderDetails(intval($orderInfo[0]['order_id']));
                        $product_info = $this->Product->get_simple_product_info($order_product_list[0]['product_id']);

                        $this->mGroup->johnGroup($orderId,$uid,$orderInfo[0]['group_id'],$product_info);
                    }
                 $this->loadModel('mAward');

                 $award = $this->mAward->getAwardDetail($orderInfo[0]['order_amount']);
                 if($award){
                     $this->loadModel('Coupons');
                     $this->loadModel('UserCoupon');
                     // add coupon to user
                     $rtnCode = $this->UserCoupon->insertUserCoupon($award['coupon_id'], $uid, true,'order');
                     $this_coupon = $this->Coupons->get_coupon_info(($award['coupon_id']));
                     $discount_val = intval($this_coupon['discount_val']/100);
                     $content = "偷偷送你一张（".$discount_val."元）优惠券~不要告诉别人呦\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康美食吧~";
                     $url = $config->domain.'?/Coupon/user_coupon/';
                     Messager::sendNotification(WechatSdk::getServiceAccessToken(), $orderInfo[0]['wepay_openid'], $content, $url);

                 }
                    $this->echoMsg(1,'成功');
             }else{
                 $pay_type = 2;
                 $orderNum =$orderInfo[0]['serial_number'];
                 if($pay_amount != $orderInfo[0]['pay_amount']){
                     $orderNum = $this->mOrder->generateOrderNum($orderId);
                     $serArray = array('serial_number'=>$orderNum);
                     $this->mOrder->updateOrder($orderId,$serArray);
                 }
                 $result = $this->wechatPay($pay_amount,$orderNum,$voucher);
                 $stateArray =  array(
                     'pay_type' => $pay_type,
                     'balance_amount' =>$balance_amount,
                     'pay_amount' =>$pay_amount
                 );
                 $this->mOrder->updateOrder($orderId,$stateArray);
                 $this->echoMsg(2,$result);
             }

           }
         }

    
    
    }
    
    
    
    /**
    * 余额支付
    */
    public function moneyPay($uid,$balance_amount){
    
  
       $this->loadModel('User');
       $id = $this->User->mantUserBalance($balance_amount,$uid, $type = User::MANT_BALANCE_DIS);
       return $id;
   }

  
    
    /**
     * Ajax获取订单请求数据包
     */
    public function wechatPay($amount,$id,$voucher) {
        global $config;
        $this->loadModel('mOrder');
        
        $orderId = $id;


        $openid = $this->getOpenId();
        // 订单总额


        $totalFee = round($amount * 100,2);

       
        $nonceStr = $this->Util->createNoncestr();

        $timeStamp = strval(time());

        $pack = array(
            'appid' => APPID,
            'body' => $config->shopName,
            'timeStamp' => $timeStamp,
            'mch_id' => PARTNER,
            'nonce_str' => $nonceStr,
            'notify_url' => $config->order_wxpay_notify,
            'out_trade_no' => $config->out_trade_no_prefix . $orderId,
            'spbill_create_ip' => $this->getIp(),
            'total_fee' => $totalFee,
            'trade_type' => 'JSAPI',
            'openid' => $openid,
            'goods_tag'=>$voucher
        );

        $pack['sign'] = $this->Util->paySign($pack);

        $xml = $this->Util->toXML($pack);
       //error_log("========ret=========".$xml);
        $ret = Curl::post('https://api.mch.weixin.qq.com/pay/unifiedorder', $xml);

        error_log("========ret=========".$ret);

        $postObj = json_decode(json_encode(simplexml_load_string($ret, 'SimpleXMLElement', LIBXML_NOCDATA)));

        //error_log("================postObj==========".$postObj->prepay_id);
        if (empty($postObj->prepay_id) || $postObj->return_code == "FAIL") {
            // 支付发起错误
            $this->log('wepay_error:' . $postObj->return_msg);
		
        }


        #var_dump($postObj);

        $packJs = array(
            'appId' => APPID,
            'timeStamp' => $timeStamp,
            'nonceStr' => $nonceStr,
            'package' => "prepay_id=" . $postObj->prepay_id,
            'signType' => 'MD5'
        );

        $JsSign = $this->Util->paySign($packJs);

        $packJs['timestamp'] = $timeStamp;

        $packJs['paySign'] = $JsSign;

        return $packJs;


    }
  
    /**
     * cookie
     * 计算订单总量
     * @return <float>
     */
    private function countOrderSum($orderid) {
        $sum = $this->Db->query("SELECT `order_amount` FROM `orders` WHERE `order_id` = $orderid;");
        return $sum[0]['order_amount'];
    }

    /**
     * expressDetail 查看物流情况
     */
    public function expressDetail($Query) {

        global $config;
        $this->loadModel('mUserAddress');
        $this->loadModel('User');
        $this->loadModel('mQuestion');
        $this->loadModel('mOrder');
        $this->loadModel('mYun');
        $this->loadModel('mSupplier');
        $this->loadModel('Product');
        $this->loadModel('Carts');
        $this->loadModel('mExpress');
        $this->loadModel('mGroup');



        if (!isset($Query->order_id) && $Query->order_id > 0) {
            exit(0);
        }
        $this->loadModel('JsSdk');

        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        $openId = $this->getOpenId();
        $this->User->wechatAutoReg($openId);

        $this->cacheId = $openId . $Query->order_id;
        $uinfo = $this->User->getUserInfo($openId);

        $openid = $openId;//$this->getOpenId();
        if (!$this->isCached()) {

            $openIds = explode(',', $this->getSetting('order_notify_openid'));

            $Query->order_id = addslashes($Query->order_id);

            // 订单信息
            $orderData = $this->Db->getOneRow("SELECT * FROM `orders` WHERE `order_id` = $Query->order_id;");

            $openIds[] = $orderData['wepay_openid'];

            if (!in_array($openId, $openIds)) {
                echo 0;
            } else {
                $this->loadModel('Product');
                //$orderProductsList = $this->Db->query("SELECT `catimg`,`pi`.product_name,`pi`.product_id,`sd`.product_count,`sd`.product_discount_price,`sd`.product_price_hash_id FROM `orders_detail` sd LEFT JOIN `products_info` pi on pi.product_id = sd.product_id WHERE `sd`.order_id = " . $Query->order_id);
                $expressCode = include dirname(__FILE__) . '/../config/express_code.php';

               $address = $this->mUserAddress->get_user_address_by_id($orderData['address_id']);
               $this->Smarty->assign('address', $address);

                //$orderData['address'] = $address['city'].$address['address'];
                $orderData['express_com1'] = $expressCode[$orderData['express_com']];
                $orderData['statusX'] = $config->orderStatus[$orderData['status']];

                $orderProviderList = $this->Db->query("select * from orders_detail where order_id = ".$Query->order_id." group by provider_id");
                foreach($orderProviderList as $pk => $pv){

                    $provider_id =  $pv['provider_id'];

                    $express = $this->mYun->getOrderExpress($Query->order_id,$provider_id);

                    $expressDetail = $this->mExpress->get_express_detail($express['express_id']);
                    $express['express_name'] = $expressDetail['name'];
                    $orderProviderList[$pk]['express'] = $express;
                    $orderProviderList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
                    $orderProductsList = $this->Db->query("SELECT `catimg`,`pi`.product_name,`pi`.product_id,`sd`.product_count,`sd`.product_discount_price,`sd`.product_price_hash_id FROM `orders_detail` sd LEFT JOIN `products_info` pi on pi.product_id = sd.product_id WHERE `sd`.order_id = " . $Query->order_id." and `sd`.provider_id = ".$provider_id);
                    $product_id = $orderProductsList[0]['product_id'];
                    $product = $this->Product->getProductInfo($product_id,false);
                    $yun = $this->mYun->get_detail_yun_sys($product['yun_no']);
                    $orderProviderList[$pk]['yun'] = $yun;
                    $orderProviderList[$pk]['product_list'] = $orderProductsList;

                    if($address){
                        $orderProviderList[$pk]['yun_cost'] = $this->Carts->calc_product_yun_order($orderProductsList,$address);
                    }else{
                        $orderProviderList[$pk]['yun_cost'] = 0;
                    }

                    if($orderData['is_tuan'] == 1){
                        $orderProviderList[$pk]['yun_cost'] = 0;
                    }



                }

                $this->Smarty->assign('orderdetail', $orderData);
                $this->Smarty->assign('orderProviderList', $orderProviderList);
                $this->Smarty->assign('title', '订单详情');
            }
        }
        $isSend = $this->mQuestion->isSendCoupon($uinfo['uid'],$Query->order_id);
        $isComment = 0;
        if($isSend){
             $isComment = 1;
        }


        $isTuan  = $orderData['is_tuan'];
        if($isTuan == 1){
            $group = $this->mGroup->getGroupDetailByOrderId($orderData['order_id']);
            if(!$group){
                $join =  $this->mGroup->getGroupJoinByOrderId($orderData['order_id']);
                $group = $this->mGroup->getGroupDetailById($join['group_id']);
            }
            $this->assign('group', $group);
        }



        $signPackage = $this->JsSdk->GetSignPackage();
        $comment = $this->mOrder->getComment($Query->order_id);
        $this->assign('comment',$comment);
        $this->assign('signPackage', $signPackage);
        $this->assign('isComment', $isComment);
        $this->assign('uinfo', $uinfo);
        $this->assign('isTuan', $isTuan);


        $this->show();
    }

    /**
     * 订单取消
     * @todo a lot
     */
    public function cancelOrder() {
        $orderId = $_POST['orderId'];
        
        $this->loadModel('mOrder');
        $ordersArr = $this->mOrder->GetSimpleOrderInfo($orderId);
        $order = $ordersArr[0];
        if($order['status'] != 'unpay'){
          $cancelSql = "UPDATE " . TABLE_ORDERS . " SET `status` = 'canceled' WHERE `order_id` = $orderId;";
        }else{
          $cancelSql = "UPDATE " . TABLE_ORDERS . " SET `status` = 'closed' WHERE `order_id` = $orderId;";
        }
        $rst = $this->Db->query($cancelSql);
        # echo $cancelSql;
        echo $rst > 0 ? "1" : "0";
    }

    /**
     * ajax确认收货 | 过期自动确认订单
     * @param type $Q
     * @return boolean
     */
    public function confirmExpress($Q) {
        // orders >> received
        $this->loadModel('mOrder');
        $this->loadModel('WechatSdk');
        $orderIds = array();
        $recycle = isset($Q->rec);
        if ($recycle) {
            $expDay = $this->getSetting('order_confirm_day');
            $expDate = date('Y-m-d', strtotime('-' . $expDay . ' DAY'));
            $idStr = $this->Dao->select("GROUP_CONCAT(order_id)")->from(TABLE_ORDERS)->where("`send_time` <= '$expDate' AND `status` = 'delivering'")->getOne();
            if ($orderIds == '') {
                return false;
            } else {
                $orderIds = explode(',', $idStr);
            }
        } else {
            $orderIds[] = intval($this->pPost('orderId'));
        }
        foreach ($orderIds as $orderId) {
            if ($orderId > 0) {
                $updateSql = "UPDATE `orders` SET status = 'received',`receive_time` = NOW() WHERE `order_id` = $orderId;";
                // 推广结算
                $orderData = $this->mOrder->GetOrderDetail($orderId);
                $companyCom = $orderData['company_com'];
                if ($companyCom != '0' && $companyCom > 0) {
                    // 代理商结算
                    $clientId = $orderData['client_id'];
                    $orderCount = $orderData['product_count'];
                    // todo model
                    foreach ($orderData['products'] as $productId => $count) {
                        $_rst = $this->Db->query("UPDATE `" . COMPANY_SPREAD . "` SET `turned` = `turned` + 1 WHERE `com_id` = '$companyCom' AND `product_id` = $productId;");
                        if (!$_rst) {
                            $this->Db->query("INSERT INTO `" . COMPANY_SPREAD . "` (`product_id`,`com_id`,`turned`) VALUES ($productId,'$companyCom',1);");
                        }
                    }
                    $companyInfo = $this->Dao->select()->from('companys')->where("id=$companyCom")->getOneRow();
                    // 代理回报比例
                    $percent = floatval($companyInfo['return_percent']);
                    // 代理Openid
                    $openid = $companyInfo['openid'];
                    // 代理UID
                    $comUid = $companyInfo['uid'];
                    // 代理所获得收益
                    $comAmount = floatval($orderData['order_amount'] * $percent);
                    // 查询二级分销
                    // 上级代理ID
                    $comcom = $this->Dao->select('client_comid')->from('clients')->where("client_id=$comUid")->getOne();
                    if ($comcom !== false) {
                        $comcomIncome = $comAmount * floatval($this->settings['com_sale_pcent']);
                        $comAmount = $comAmount - $comcomIncome;
                        // 二级回报
                        $this->Db->query("INSERT INTO `company_income_record` (`amount`,`date`,`client_id`,`order_id`,`com_id`,`pcount`) VALUE ($comcomIncome, NOW(), $clientId, $orderId, '$comcom',$orderCount);");
                    }
                    // 第一级回报
                    $this->Db->query("INSERT INTO `company_income_record` (`amount`,`date`,`client_id`,`order_id`,`com_id`,`pcount`) VALUE ($comAmount, NOW(), $clientId, $orderId, '$companyCom',$orderCount);");
                    Messager::sendText(WechatSdk::getServiceAccessToken(), $openid, date('Y-m-d') . " 您名下的会员总额为" . $orderData['order_amount'] . "的订单已完成，您获得 $comAmount 元收益！");
                }
                $ret = $this->Db->query($updateSql);
                if ($recycle) {
                    return $ret;
                } else {
                    echo $ret;
                }
            } else {
                if ($recycle) {
                    return false;
                } else {
                    echo 0;
                }
            }
        }
    }

    /**
     * 订单发货
     */
    public function ExpressReady() {
        $this->Smarty->caching = false;
        $this->loadModel('mOrder');
        $this->loadModel('WechatSdk');

        $orderId = intval($_POST['orderId']);
        $expressCode = $_POST['ExpressCode'];
        $expressCompany = $_POST['expressCompany'];
        $expressStaff = $this->post('expressStaff');
        $tmpId = 'Mb_Sy1m-1onfxMsI9FNGdBgKtnrAHE8D1P5p8DMdJMs';
        $notifyOpenid = array();
        if (!empty($expressStaff)) {
            $notifyOpenid[] = $expressStaff;
        }

        $expressList = include dirname(__FILE__) . '/../config/express_code.php';

        if ($this->mOrder->despacthGood($orderId, $expressCode, $expressCompany)) {
            global $config;
            $orderData = $this->Db->getOneRow("SELECT `oa`.`order_id`,`oa`.`tel_number`,`od`.wepay_openid,`od`.client_id,`od`.serial_number,`od`.order_time,`oa`.user_name,`od`.order_amount FROM `orders` `od` LEFT JOIN `orders_address` `oa` ON `oa`.order_id = `od`.order_id WHERE `oa`.order_id = $orderId;");
            $notifyOpenid[] = $orderData["wepay_openid"];
            // wechat notify
            foreach ($notifyOpenid as $openid) {
                Messager::sendTemplateMessage($tmpId, $openid, array(
                    'first' => '您有一笔订单已发货',
                    'keyword1' => "#" . $orderData['serial_number'],
                    'keyword2' => $expressList[$expressCompany],
                    'keyword3' => $expressCode,
                    'remark' => '点击详情 随时查看订单状态'), $config->domain . "?/Order/expressDetail/order_id=$orderData[order_id]");
            }
            // wechat notify test
            // assign
            echo 1;
        } else {
            echo 0;
        }
    }

    /*
     * @HttpPost only
     * 获取快递跟踪情况
     * @return <html>
     */

    public function ajaxGetExpressDetails() {
        $typeCom = $_POST["com"]; //快递公司
        $typeNu = $_POST["nu"];  //快递单号
        $url = "http://api.ickd.cn/?id=105049&secret=c246f9fa42e4b2c1783ef50699aa2c4d&com=$typeCom&nu=$typeNu&type=html&encode=utf8";
        //优先使用curl模式发送数据
        $res = Curl::get($url);
        echo $res;
    }

    /**
     * ajax 订单退款处理
     */
    public function orderRefund() {
    
        $this->loadModel('mOrder');
        $this->loadModel('User');
        $this->loadModel('mGroup');

        $orderId = intval($this->pPost('id'));
        $groupId = $this->pPost('groupId');
        $pay_amount = floatval($this->pPost('pay_amount'));
        $balance_amount = floatval($this->pPost('balance_amount'));


        $orderInfo = $this->mOrder->GetOrderDetail($orderId);
        $uinfo = $this->User->getUserInfo($orderInfo['wepay_openid']);
        if($balance_amount > 0){

           $this->User->mantUserBalance($balance_amount, $uinfo['uid'], $type = User::MANT_BALANCE_ADD);
            if($orderInfo['order_amount'] == $balance_amount){
                $this->mOrder->updateOrderStatus($orderId,"refunded",false);
            }
            $this->mGroup->add_group_log($orderInfo['client_id'],$orderId,$groupId,$balance_amount,"退还余额".$balance_amount."元到账户");
            echo 1;
        }
        if($pay_amount >0){
             $ret = $this->mOrder->orderRefund($orderId,$pay_amount);
            if($ret == 'SUCCESS') {
                $this->mGroup->add_group_log($orderInfo['client_id'],$orderId,$groupId,$pay_amount,"退还余额".$pay_amount."到微信账户");
                $this->mOrder->updateOrderStatus($orderId,"refunded",false);
                echo 1;
            }else{
                echo 0;
            }
        }

        //if($orderInfo['pay_type'] == '1'){
          //余额退款
         //  $this->User->mantUserBalance($amount, $uinfo['uid'], $type = User::MANT_BALANCE_ADD);
          // $this->mOrder->updateOrderStatus($orderId,"refunded",false);
        //}else if($orderInfo['pay_type'] == '2'){
        
          // $balance = $orderInfo['balance_amount'];
           //$online = $orderInfo['online_amount'];
          // error_log("================pay_time 2 ===================balance".$balance."===========online==========".$online);
          // $this->User->mantUserBalance($balance, $uinfo['uid'], $type = User::MANT_BALANCE_ADD);
          // $ret = $this->mOrder->orderRefund($orderId, $online);
           //if($ret == 'SUCCESS') {
           //    $this->mOrder->updateOrderStatus($orderId,"refunded",false);
             
           //}

                
        //}else{
       //微信退款
        // 退款结果
        //$ret = $this->mOrder->orderRefund($orderId, $amount);
        // 可退款金额
        //$rAmount = $this->mOrder->getUnRefunded($orderId);
        // 已退款金额
        //$rAmounted = $this->mOrder->getRefunded($orderId);
   
             //if($ret == 'SUCCESS') {
                // 申请已提交 进一步处理订单
                //if ($rAmount == $amount || $rAmount < 0.01) {
                    // 已经全部退款
                  //  $this->mOrder->updateOrderStatus($orderId, 'refunded', $rAmounted + $rAmount);
    
                    
                //} else {
                    // 部分退款
                 //   $this->mOrder->updateOrderStatus($orderId, 'canceled', $rAmounted + $amount);
               // }
              //  echo 1;
            //} else {
            //    echo 0;
          //  }
      // }
      
    }

    /**
     * 检查限购
     * @param type $key
     * @return boolean
     */
    private function checkPromLimit($key) {
        if ($key == '') {
            return false;
        } else {
            $matchs = array();
            preg_match("/p(\d+)m(\d+)/is", $key, $matchs);
            // product id
            $pid = intval($matchs[1]);
            $uid = $this->getUid();
            $limitDay = $this->Dao->select('product_prom_limitdays')->from(TABLE_PRODUCTS)->where("product_id = $pid")->getOne();
            $orderS = $this->Db->query("select order_time as `date` from orders_detail `dt`
left join orders `od` on `od`.order_id = `dt`.order_id
where `dt`.product_id = $pid and `od`.client_id = $uid
and 
(`status` = 'payed' or `status` = 'delivering' or `status` = 'received')");
            foreach ($orderS as $od) {
                if ($od['date'] > $limitDay) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * 代付
     * @param type $Q
     */
    public function reqPay($Q) {
        if (isset($Q->id) && $Q->id > 0) {
            $orderId = intval($Q->id);

            $this->cacheId = $orderId;

            if (!$this->isCached()) {

                $this->loadModel('User');
                $this->loadModel('mOrder');
                $this->loadModel('JsSdk');

                $orderInfo = $this->mOrder->getOrderInfo($orderId);

                $orderDetail = $this->mOrder->GetOrderDetailList($orderId);

                $userInfo = $this->User->getUserInfoRaw($orderInfo['client_id']);

                $reqEd = $this->mOrder->getOrderReqAmount($orderId);

                $reqCount = $this->mOrder->getOrderReqCount($orderId);

                // 参与朋友
                $reqList = $this->mOrder->getOrderReqList($orderId);

                $signPackage = $this->JsSdk->GetSignPackage();

                $this->assign('signPackage', $signPackage);
                $this->assign('userInfo', $userInfo);
                $this->assign('orderInfo', $orderInfo);
                $this->assign('orderDetail', $orderDetail);
                $this->assign('reqed', $reqEd);
                $this->assign('reqcount', $reqCount);
                $this->assign('reqlist', $reqList);
                $this->assign('isfinish', $reqEd == $orderInfo['order_amount']);
            }

            $this->show();
        }
    }

    /**
     * ajax检查购物车
     */
    public function checkCart() {
        if (empty($_POST['data'])) {
            $this->echoJson(array());
        } else {
            $this->loadModel('Product');
            $this->Smarty->caching = false;
            $data = json_decode($_POST['data'], true);
            $pdList = array();
            $matchs = array();
            foreach ($data as $key => $count) {
                preg_match("/p(\d+)m(\d+)/is", $key, $matchs);
                $pid = intval($matchs[1]);
                if (count($this->Product->checkExt($pid)) === 0) {
                    $pdList[] = $key;
                }
            }
            $this->echoJson($pdList);
        }
    }

    /**
     * 下单成功页面
     * 提示分享，返回首页，返回个人中心选项
     */
    public function order_success($Query) {
        $orderAddress = $this->Db->getOneRow("SELECT * FROM `orders_address` WHERE `order_id` = $Query->orderid;");
        $this->assign('orderAddress', $orderAddress);
        $this->assign('title', '下单成功');
        $this->show();
    }

    /**
     * 订单评价
     * @param type $Query
     */
    public function commentOrder($Query) {
        $orderId = intval($Query->order_id);
        $this->loadModel('User');
        $this->loadModel('mOrder');
        $openId = $this->getOpenId();
        $this->User->wechatAutoReg($openId);
    
        if ($orderId > 0) {
            $this->Load->model('mOrder');
            $orderData = $this->mOrder->GetOrderDetail($orderId);
            $this->assign('order_id', $orderId);
            $this->assign('title', '订单评价');
     
            $this->show();
        }
    }
    
    
    

    /**
     * 订单评价
     */
    public function addComment() {
        $content = $this->pPost('comment');
        $score = intval($this->pPost('score'));
        $orderId = intval($this->pPost('order_id'));
        $tag = $this->pPost('tag');
        $openId = $this->getOpenId();
        $this->loadModel('mOrder');
    
        
        
        if ($orderId > 0 && !empty($openId)) {
      	
      		$comment = $this->mOrder->getComment($orderId);
      		if($comment){
      			$this->echoMsg(2);
      		}else{
            if ($this->mOrder->checkOrderBelong($openId, $orderId)) {
                // 检查订单归属
                if ($this->mOrder->addComment($openId, $tag, $content, $score,$orderId)) {
                    $this->echoMsg(1);
                } else {
                    $this->echoMsg(-1);
                }
            } else {
                $this->echoMsg(-1);
            }
           }
        } else {
            $this->echoMsg(-1);
        }
    }
    
    /**
    * 未支付订单 状态重置为无效 时间为60分钟
    */
    public function resetOrderByTime(){
        $this->loadModel('mOrder');
        $time =date('Y-m-d H:i:s',time());
        $status = 'unpay';
        $where = " WHERE status = '".$status."'";
        $orderList = $this->mOrder->queryOrderList($where);      
        foreach ($orderList as $index => $order) {
           
            $orderTime =  $orderList[$index]['order_time'];
            $min=floor((strtotime($time)-strtotime($orderTime))%86400/60);
            $date=floor((strtotime($time)-strtotime($orderTime))/86400);
            
            if($min >=60 || $date >=1){
                $id = $orderList[$index]['order_id'];
                $this->mOrder->updateOrderStatus($orderList[$index]['order_id'],"closed",false);
            } 
        }
    }
    
   /**
   * 已发货订单 2个小时未确认收货自动默认为 收货
   */ 
   public function resetRecevedByTime(){
   
        $this->loadModel('mOrder');
        $time =date('Y-m-d H:i:s',time());
        $status = 'delivering';
        $where = " WHERE status = '".$status."'";
        $orderList = $this->mOrder->queryOrderList($where);      
        
        foreach ($orderList as $index => $order) {
           
            $orderTime =  $orderList[$index]['send_time'];
            $min=floor((strtotime($time)-strtotime($orderTime))%86400/60);
            $date=floor((strtotime($time)-strtotime($orderTime))/86400);
            
            if($min >=120 || $date >=1){
                $id = $orderList[$index]['order_id'];
                $data = array(
                  'status' => 'received',
                   'receive_time' => time()
                 );
                $this->mOrder->updateOrder($orderList[$index]['order_id'],$data);
                
            } 
        }
   }


    public function  lookExpress($query){


        $this->loadModel('mYun');
        $this->loadModel('mExpress');
        $express_num = $query->express_num;
        $express_no = $query->express_no;
        $appkey = "9fe68b06-5be2-45c7-8098-58eef41f08fc";
        $requestUrl = "http://api.kdniao.cc/Ebusiness/EbusinessOrderHandle.aspx";

        $requestData= "{'OrderCode':'','ShipperCode':'".$express_no."','LogisticCode':'".$express_num."'}";
        $businessId = "1260818";
        $datas = array(
            'EBusinessID' => $businessId,
            'RequestType' => '1002',
            'RequestData' => urlencode($requestData) ,
            'DataType' => '2',
        );
        $datas['DataSign'] = urlencode(base64_encode(md5($requestData.$appkey)));
        $result=$this->sendPost($requestUrl, $datas);

        $data = json_decode($result);
        $isSuccess  = $data->Success;
        if($isSuccess == 'Success'){
           $list =  $data->Traces;
            $this->assign("list",array_reverse($list));
            $this->assign("state",$data->State);
        }
        $express = $this->mExpress->getExpressDetail($express_no);
        $this->assign("express",$express);
        $this->assign("express_num",$express_num);

        $this->show('./order/logistics.tpl');



    }


    function sendPost($url, $datas) {
        $temps = array();
        foreach ($datas as $key => $value) {
            $temps[] = sprintf('%s=%s', $key, $value);
        }
        $post_data = implode('&', $temps);
        $url_info = parse_url($url);
        if($url_info['port']=='')
        {
            $url_info['port']=80;
        }
        $httpheader = "POST " . $url_info['path'] . " HTTP/1.0\r\n";
        $httpheader.= "Host:" . $url_info['host'] . "\r\n";
        $httpheader.= "Content-Type:application/x-www-form-urlencoded\r\n";
        $httpheader.= "Content-Length:" . strlen($post_data) . "\r\n";
        $httpheader.= "Connection:close\r\n\r\n";
        $httpheader.= $post_data;
        $fd = fsockopen($url_info['host'], $url_info['port']);
        fwrite($fd, $httpheader);
        $gets = "";
        $headerFlag = true;
        while (!feof($fd)) {
            if (($header = @fgets($fd)) && ($header == "\r\n" || $header == "\n")) {
                break;
            }
        }
        while (!feof($fd)) {
            $gets.= fread($fd, 128);
        }
        fclose($fd);

        return $gets;
    }


    public function  sendExpress(){

        global $config;
        $this->loadModel('mYun');
        $this->loadModel('mExpress');
        $this->loadModel('mOrder');
        $this->loadModel('WechatSdk');
        $this->loadModel('User');
        $this->loadModel('mUserAddress');



        $orderId = $this->post("orderId");
        $providerId = $this->post("providerId");
        $expressId = $this->post("expressId");
        $code = $this->post("code");
        if(trim($code) == ''){
            echo -1;
            die(0);
        }

        $express =  $this->mExpress->get_express_detail($expressId);
        $this->mExpress->add_package($orderId,$expressId,$express['express_no'],$providerId,$code);

        $order = $this->mOrder->get_order_info_by_id($orderId);
        $uinfo = $this->User->getUserInfo($order['client_id']);
        $address = $this->mUserAddress->get_user_address_by_id($order['address_id']);

        $products_str = '产品：';
        $orderDetail = $this->Db->query("SELECT `pi`.product_name,`sd`.product_count FROM `orders_detail` sd LEFT JOIN `products_info` pi on pi.product_id = sd.product_id WHERE `sd`.order_id = " . $orderId." and sd.provider_id = ".$providerId);;
        foreach ($orderDetail as $dt) {
            $products_str .= $dt['product_name'] . ' ' . $dt['product_count'] .'件' ." ,";
        }
        $addressStr = $address['province'].$address['city'].$address['area'].$address['address'];
        Messager::sendTemplateMessage($config->messageTpl['express_send'], $uinfo['client_wechat_openid'], array(
            'first' => '您的订单已发货，邮差正快马加鞭速递中:',
            'keyword1' => $products_str,
            'keyword2' => $express['name'],
            'keyword3' => $code,
            'keyword4' => $addressStr,
            'remark' => '亲爱的主上大人，您的订单已出库 点击查看'
        ), $this->getBaseURI() . "?/Order/lookExpress/express_no=".$express['express_no']."&express_num=".$code);
        $orderProviderList = $this->mOrder->getOrderDetailProviderId($orderId);
        error_log("======orderProviderList=========".json_encode($orderProviderList));

        $isAllSend = 1;
        foreach($orderProviderList as $key => $val){

           $expressPackage =  $this->mExpress->get_order_package($orderId,$val['provider_id']);
            error_log("======expressPackage========".json_encode($expressPackage));
           if(!$expressPackage){
               $isAllSend = 0;
           }
        }

        if($isAllSend == 1)
        {
            $data = array(
                'status' => 'delivering',
            );
            $this->mOrder->updateOrder($orderId,$data);
        }

        echo 1;
    }




   public function  providerOrderList($query){


       $this->loadModel("mSupplier");
       $this->loadModel("mOrder");
       $this->loadModel("mExpress");

       $name = $query->name;
       $supplier = $this->mSupplier->get_detail_supplier_by_name($name);
       if($supplier){

           $provider_id = $supplier['id'];
           $list = $this->mOrder->GetProviderOrderList($provider_id);
           $isNotEmpty = 0;//
           foreach($list as $key => $val){

              $order_id =  $val['order_id'];
              $express = $this->mExpress->get_order_package($order_id,$provider_id);
              if($express){
                  $list[$key]['issend'] = 1;
              }else{
                  $list[$key]['issend'] = 0;
                  $isNotEmpty = 1;

              }

           }
           $this->assign("orders",$list);

           $this->assign("empty",$isNotEmpty);
       }
       $this->show("./provider/orderlist.tpl");

   }

  public function providerOrderDetail($query){

      global $config;
      $this->loadModel('mUserAddress');
      $this->loadModel('User');
      $this->loadModel('mQuestion');
      $this->loadModel('mOrder');
      $this->loadModel('mYun');
      $this->loadModel('mSupplier');
      $this->loadModel('Product');
      $this->loadModel('Carts');
      $order_id = $query->order_id;
      $provider_id =  $query->provider_id;

      $orderData = $this->mOrder->GetProviderOrderDetail($order_id,$provider_id);
      $address = $this->mUserAddress->get_user_address_by_id($orderData['address_id']);
      $this->Smarty->assign('address', $address);
      $this->assign("order",$orderData);
      $this->assign("provider_id",$provider_id);

      $this->show("./provider/expressdetail.tpl");

  }

  public function  sendProviderExpress($query){


      $this->loadModel('mExpress');
      $this->loadModel('mSupplier');
      $this->loadModel('mYun');
      $this->loadModel('mOrder');


      $orderId = $query->order_id;
      $providerId = $query->provider_id;
      $expressList = $this->mExpress->getExpressList();

      $orderDetail = $this->Db->query("SELECT `pi`.yun_no,`pi`.product_name,`sd`.product_count FROM `orders_detail` sd LEFT JOIN `products_info` pi on pi.product_id = sd.product_id WHERE `sd`.order_id = " . $orderId." and sd.provider_id = ".$providerId);;
      if($orderDetail){
         $yun_no =  $orderDetail[0]['yun_no'];
         $yun  =  $this->mYun->get_detail_yun_sys($yun_no);
          $this->assign("yun",$yun);

      }

      $supplier = $this->mSupplier->get_detail_supplier($providerId);
      $url = "/?/Order/providerOrderList/name=".$supplier['admin_name'];
      $this->assign("expressList",$expressList);
      $this->assign("order_id",$orderId);
      $this->assign("provider_id",$providerId);
      $this->assign("url",$url);

      $this->show("./provider/delivery.tpl");

  }


    public  function getGoodsTag($order_product_list){
        $this->loadModel('mSetting');
        $settings = $this->mSetting->getSettingByKey('goods_tag_id',true);
        $product_id = array_column($order_product_list,'product_id');
        $tag = "";
        foreach($product_id as $v){
            if(in_array($v,$settings)){
                $tag = 'tag'.$v;
                break;
            }
        }
        return $tag;
    }

}
