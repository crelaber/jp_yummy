<?php



/**
 * 分享
 */
class Share extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }

   /**
   * 分享页面
   */
   public function share_view(){
      $this->loadModel('User');
      $this->loadModel('JsSdk');
      $this->loadModel('mShare');
      $this->loadModel('mShareSetting');
      $this->loadModel('Coupons');      

       if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
        $openid = $this->getOpenId();
        $this->User->wechatAutoReg($openid); 
      
      $uinfo = $this->User->getUserInfo($openid);
      $uid = $uinfo['uid'];
      $signPackage = $this->JsSdk->GetSignPackage();

      $shareType =  $this->mShare->getShareType();

      $this->assign('signPackage', $signPackage);
      $this->assign('uid', $uid);
       $this->assign('shareType', $shareType);
       $this->assign('time', time());
      $this->assign('uinfo', $uinfo);
      $this->show('./share/invite_friends.tpl');
   }
   
   /**
   *分享须知页面
   */
   public function share_note_view(){
      $this->loadModel('User');
      $this->loadModel('mShareSetting');
      $this->loadModel('Coupons');
      if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
      $user_share_coupon_id =  mShareSetting::$user_share_coupon_id;
      $where = " where key_m = '".$user_share_coupon_id."'";
      $setting = $this->mShareSetting->getShareSetting($where);
      $coupnId =$setting['value_m'];
      $couponInfo = $this->Coupons->get_coupon_info($coupnId);
      if($couponInfo){

        $couponInfo['coupon_value'] =  $couponInfo['discount_val']/100;
          error_log("===================couponInfo=======".json_encode($couponInfo));
        }
       $this->assign('couponInfo', $couponInfo);
       $openid = $this->getOpenId();
       $this->User->wechatAutoReg($openid); 
       $this->show('./share/invite_note.tpl');
   }
   /**
   * 已分享的页面
   */
   public function share_wallet_view($data){
      global $config;
      $this->loadModel('JsSdk');
      $this->loadModel('User');
      $type = $data->type;

      $this->loadModel('UserCoupon');
      $this->loadModel('Coupons');
      $this->loadModel('mShare');
      $this->loadModel('mShareSetting');
      $this->loadModel('WechatSdk');
      
      
      $signPackage = $this->JsSdk->GetSignPackage();
	  $share_des =array('御前红包驾到！闲杂人等，还不速速让开','红包在手，欢迎剁手','来呀，互相伤害呀~', '我也曾平凡，直到遇见了红包',  '嘴上说着不要，身体却很诚实~','乖，给你红包~','没有人品就没有大红包，谢谢！');
      if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
        $openid = $this->getOpenId();
        $this->User->wechatAutoReg($openid); 
      
      $uinfo = $this->User->getUserInfo($openid);
      $uid = $uinfo['uid'];

      if($uinfo['client_phone'] == ''){
          //绑定手机号
          $isTake = 0;
      }else{
         //发用户优惠券
        $shareTake =   $this->mShare->getShareTake($type,$uid);
        if($shareTake){
            $isTake = 1;
            $couponInfo =  $this->Coupons->get_coupon_info($shareTake['coupon_id']);
        }else{
            $isTake = 2;
            $desc_count = count($share_des);
            $selectDescCount = rand(0, $desc_count-1);
            $list = $this->mShare->getShareCouponList($type);
            $count = count($list);
            $selectCount = rand(0, $count-1);
            $couponInfo =  $this->Coupons->get_coupon_info($list[$selectCount]['coupon_id']);
            $this->UserCoupon->insertUserCoupon($list[$selectCount]['coupon_id'],$uid, true,'share');

            $this->mShare->createShareTake($uid,$share_des[$selectDescCount],$couponInfo['id'],$couponInfo['discount_val']/100,$type);
            $content = "偷偷送你（".(int) ($couponInfo['discount_val'] / 100)."元）~不要告诉别人呦\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可~";
            $url = $config->domain.'?/Coupon/user_coupon/';
            Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, $content, $url);
        }

      }
       $couponInfo['coupon_value'] = $couponInfo['discount_val'] / 100;
       $this->assign('couponInfo', $couponInfo);
       $takeList =  $this->mShare->getShareTakeList($type);
       foreach($takeList as $key => $val){

           $takeList[$key]['uinfo'] =$this->User->getUserInfo($val['uid']);
       }
       $this->assign('takeList', $takeList);
      $this->assign('signPackage', $signPackage);
      $this->assign('uinfo', $uinfo);
       $this->assign('isTake', $isTake);
       $this->assign('type', $type);
       $this->show('./share/shared_wallet.tpl');
   
   }
   
   
   
   public function ajaxCreateShare(){
       global $config;
       $this->loadModel('mShare');
       $this->loadModel('User');
       $this->loadModel('mShareSetting');
       $this->loadModel('UserCoupon');
       $this->loadModel('mShareSetting');
       $this->loadModel('Coupons');
       $this->loadModel('WechatSdk');
   
       if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
        $openid = $this->getOpenId();
        $this->User->wechatAutoReg($openid); 
        $type = 0;//0 用户中心分享 1表示订单分享 
        $uinfo = $this->User->getUserInfo($openid);
        $uid = $uinfo['uid'];
        $check_time = time();
        $share = $this->mShare->todayShare($uid);
        $id = 0;
        $coupnId = '';
        if($type == '0'){
               //用户优惠券分享
              $user_share_coupon_id =  mShareSetting::$user_share_coupon_id;
              $where = " where key_m = '".$user_share_coupon_id."'";
              $setting = $this->mShareSetting->getShareSetting($where);
              $coupnId =$setting['value_m'];
              
           }
        if($share){
           $id = $share['id'];
        }else{
           $id = $this->mShare->createShare($uid,0,0,$type,$coupnId);

        }
        
      $couponInfo = $this->Coupons->get_coupon_info($coupnId);
      if($couponInfo){
        $couponInfo['coupon_value'] =  $couponInfo['discount_val']/100;
      }
      $this->UserCoupon->insertUserCoupon($coupnId, $uid, true,'share');  
      $content = "偷偷送你（".$couponInfo['coupon_value']."元）~不要告诉别人呦\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康美食吧~";
      $url = $config->domain.'?/Coupon/user_coupon/';   
      Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, $content, $url);
        
      echo $id;
        
   }
   
   
   public function ajaxBindPhone($data){

       $this->loadModel('User');
       $openid = $this->getOpenId();
       $uinfo = $this->User->getUserInfo($openid);
       $uid = $uinfo['uid'];
       $phone = $data->phone;
       error_log("=====phone============".$phone);
       $dataArray =  array(
                             'client_phone' =>  $phone
                          );
       $id = $this->User->updateUserInfo($uid,$dataArray);
       echo $id;
   }

}
