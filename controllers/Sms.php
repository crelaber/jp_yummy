<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/6/4
 * Time: 10:57
 */

class Sms extends Controller {
    private $sms_owner_company;

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        global $config;
        $this->sms_owner_company = '【'.$config->sms_owner_company.'】';
    }


    public function sendSmsVerfiyCode($Q){
        $mobile = $Q->mobile;
        if(!TokenHelper::isMobile($mobile)){
            $this->apiFail('手机号码不正确');
            die(0);
        }

        $this->loadModel('SmsApi');
        $code = mt_rand(100000,999999);
        $content = $this->sms_owner_company . '您好，您的验证码是'. $code;
        global $config;
        $result = $this->SmsApi->sendSMS($mobile, $content);
        error_log('sms source resonse====>'.$result);
        if($result['errcode'] == 200){
            $data['code'] = $code;
            $this->apiSuccess($data);
        }else{
            $this->apiFail($result['msg']);
        }

    }


    public function sendCardInfo(){
        $mobile = '18516077928';
//        $mobile = '15210227948';
//        $mobile = '18721665513';
        $packageId =1 ;
        $cardNo = 'CARD001';
        $cardSecret = 'aadd1123344';
        $this->loadModel('SmsApi');
        $validTime = date('Y-m-d');
        $result = $this->SmsApi->sendHealthPackagePaySuccessSMS($mobile,$packageId,$cardNo,$cardSecret,$validTime);
        $this->apiSuccess($result);
    }


}