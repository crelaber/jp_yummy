<?php

// 支付授权目录 112.124.44.172/wshop/
// 支付请求示例 index.php
// 支付回调URL http://112.124.44.172/wshop/?/Order/payment_callback
// 维权通知URL http://112.124.44.172/wshop/?/Service/safeguarding
// 告警通知URL http://112.124.44.172/wshop/?/Service/warning

/**
 * 配送类
 */
class Turnplate extends Controller {
	
	const TPL = './views/';
	

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('mOrderDistribute');
        $this->loadModel('WdminAdmin');
    }
    

    /**
     * 重新设置发货时间 的弹出框
     */
    public function index($Q){

		$this->assign('order_id',$Q->order_id);
    	$this->show(self::TPL.'turnplate/index.tpl');


    }

	/*
	 * $data = array(
		'angle' => $angle ,
		'prize_num'=>$index,
		'prize_name'=>$prize_info['prize_name']
	);*/
	public function init_game($Q){
		$this->loadModel('UserCoupon');
		$this->loadModel('TurnplateSwitch');
		$this->loadModel('mOrder');
		$result = array();
		$draw_times = 0;
		if($Q->order_id){
			$switch_info  = $this->TurnplateSwitch->get_switch_info($Q->order_id);
			$order_info = $this->mOrder->getOrderInfo($Q->order_id);
			if(!$switch_info){
				$this->TurnplateSwitch->insert_turnplate_switch($Q->order_id);
				$draw_times = 1;
			}else{
				$is_used = $switch_info['is_used'];
				if($is_used == 0){
					$draw_times = 1;
				}else{
					$draw_times = 0;
				}
			}

			//限制非本订单的用户抽奖的次数
//			$uid = $this->getUid();
//			if($order_info['client_id'] != $uid){
//				$draw_times = 0;
//			}

			//Mock data
			if($Q->order_id == -1){
				$draw_times = 100;
			}

		}

		//抽奖次数
		$result['draw_times'] = $draw_times;
		$prize_info = PrizeUtil::gen_angle();
		$result['prize_info'] = $prize_info;
		$where = 'come_from="turnplate"';
		$winner_list = $this->UserCoupon->get_all_user_coupon($where,$orderby='add_time desc');
		$count = 0;
		$limit_count = 3;
		$list = array();

		$share_des =array('以后健康沙拉有着落了','我的手气最好么','健康沙拉？嗯，可以试试', '比手气，你们都不行',  '哈哈~手气不错','发财啦','人生第一桶金','手气不错~感动ing','麻麻再也不用担心我没健康沙拉吃了','转优惠券我最专业！','专业抢优惠券20年','转优惠券~博士毕业~','目测楼上手气好','优惠券多多益善！','有土豪的味道');
		foreach($winner_list as $key => $val){
			if($count >= $limit_count){
				break;
			}
			$des_count = rand(0,count($share_des)-1);
			$des = $share_des[$des_count];
			$val['desc'] = $des;
			$list[] = $val;
			$count++;
		}
		$result['winner_list'] = $list;
		$this->echoApiMsg(200,'SUCCESS',$result);
	}


	public function game_end($Q){
		$this->loadModel('UserCoupon');
		$this->loadModel('Coupons');
		$this->loadModel('TurnplateSwitch');
		$this->loadModel('User');
		$this->loadModel('mOrder');
		//生产环境和正式环境的开关
//		$is_produce = false;
		$is_produce = true;

		//关闭订单的可抽奖状态
		$order_id = $Q->order_id;
		if($order_id){
			$this->TurnplateSwitch->close_turnplate_switch($order_id);
		}

		$result = array();
		$prize_info = PrizeUtil::gen_angle();
		$result['prize_info'] = $prize_info;
		//发送优惠券
//		$uid = $this->getUid();
		$openId = $this->getOpenId();
		error_log('turnplate open id =====>'.$openId);
		$uinfo = $this->User->getUserInfo($openId);
		$uid = $uinfo['uid'];
		error_log('turnplate uid id =====>'.$uid);
		$coupon_id = PrizeUtil::get_coupon_id($Q->coupon_num,$is_produce);
		$this->UserCoupon->insertUserCoupon($coupon_id, $uid,true,'turnplate');
		$coupon_info = $this->Coupons->get_coupon_info($coupon_id);
		$effective_end = $coupon_info['effective_end'];
		$result['effective_end_desc'] = date('Y.m.d',$effective_end);
		$result['coupon_id'] = $coupon_info['id'];
		$result['coupon_num'] = intval($coupon_info['discount_val'])/100;
//		$user_info = $this->User->getUserInfoRaw($uid);
		$order_info = $this->mOrder->getOrderInfo($Q->order_id);
		$address_id = $order_info['address_id'];
		$user_addr = $this->Db->getOneRow("SELECT * FROM `user_address` WHERE id = ".$address_id);
		$result['mobile'] = $user_addr['phone'];
		if(!$is_produce){
			$result['link'] = 'http://test.icheerslife.com/';
		}else{
			$result['link'] = 'http://www.icheerslife.com/';
		}

		$this->echoApiMsg(200,'SUCCESS',$result);
	}




}
