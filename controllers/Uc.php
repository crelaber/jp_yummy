<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Uc extends Controller {

    const COOKIEXP = 36000;

    /**
     * 
     * @param type $ControllerName
     * @param type $Action
     * @param type $QueryString
     */
    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('User');
    }

    /**
     * 登陆页面
     * @param type $Query
     */
    public function login() {
        $this->show();
    }

    /**
     * user Home
     * 用户中心首页
     */
   public function home() {

 	   $this->loadModel('mOrder');
       $this->loadModel('UserLevel');
       $this->loadModel('UserCoupon');
       $this->loadModel('User');
        // get openid
       $Openid = $this->getOpenId();


        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }

        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        // get uid
        $Uid = $uinfo['uid'];
        	
       $this->loadModel('JsSdk');
       $signPackage = $this->JsSdk->GetSignPackage();
       $this->assign('signPackage', $signPackage);

        // page cacheid
        $this->cacheId = $Openid;

        if (!$this->isCached()) {
          
            // 回收过期订单
            $this->mOrder->orderReclycle($Uid);
            if (!$Uid) {
                // uid cookie 过期或者未注册
                    if (!empty($Openid)) {
                        if (!$this->User->checkUserExt($Openid)) {
                            // 用户在微信里面 但是居然不存在这个用户
                            $this->redirect($this->root . '?/Uc/wechatPlease');
                        } else {
                            // 获取uid
                            $Uid = $this->User->getUidByOpenId($Openid);
                        }
                        $userInfo = $this->User->getUserInfoRaw($Uid);
                    }
            } else {
                // 用户已注册
                $userInfo = $this->User->getUserInfoRaw($Uid);
                // 刷新微信头像
                if (time() - strtotime($userInfo['client_head_lastmod']) > 432000 && Controller::inWechat()) {
                    $AccessCode = WechatSdk::getAccessCode($this->uri, "snsapi_userinfo");
                    if ($AccessCode !== FALSE) {
                        // 获取到accesstoken和openid
                        $Result = WechatSdk::getAccessToken($AccessCode);
                        // 微信用户资料
                        $WechatUserInfo = WechatSdk::getUserInfo($Result->access_token, $Result->openid);
                    }
                    $head = preg_replace("/\/0/", "", $WechatUserInfo->headimgurl);
                    $this->Db->query("UPDATE `clients` SET `client_head` = '$head',`client_head_lastmod` = NOW() WHERE `client_wechat_openid` = '$Result->openid';");
                }
                
                
            }


            // 刷新uid cookie
            $this->sCookie('uid', $Uid, Uc::COOKIEXP);

			
			$query = "where client_id = ".$Uid;
			$count = 0;
			$couponCount = 0;
			$orderList = $this->mOrder->queryOrderList($query);
            error_log("==========orderList===================".json_encode($orderList));
			if($orderList){
				$count = count($orderList);
			}
            $couponList = $this->UserCoupon->getUserCouponListByState($Uid,0);
          	if($couponList){
          					$couponCount = count($couponList);
          	
          	}

            $list = $this->User->getFansList($Uid);
            $sList = array();
            foreach($list as $key => $val){
               $secList =   $this->User->getFansList($val['client_id']);
               foreach($secList as $k=>$v){

                   array_push($sList,$v);
               }

            }
            $fans = 0;
            if($list){
                $fans = count($list);
            }
            $sfans = count($sList);
            $tfans = $fans+$sfans;
            $this->assign('level', $this->UserLevel->getLevByUid($Uid));
            $this->assign('count_envs', $this->Db->getOne("SELECT COUNT(`id`) AS `count` FROM `client_envelopes` WHERE `uid` = '$Uid' AND `count` > 0 AND `exp` > NOW();"));
            $this->assign('count_like', $this->Db->getOne("SELECT COUNT(`id`) AS `count` FROM `client_product_likes` WHERE `openid` = '$Openid';"));
            $this->assign('count', $count);
            $this->assign('fans', $fans);
            $this->assign('sfans', $sfans);
            $this->assign('tfans', $tfans);
            $this->assign("coupons",$couponList);
            $this->assign('couponCount', $couponCount);
            $this->assign('userinfo', $userInfo);


        }
       $this->assign('uinfo', $uinfo);

       $userInfo = $this->User->getUserInfoRaw($Uid);
		if($userInfo && $userInfo['client_phone'] == ''){
		  $this->show('./uc/register.tpl');
		  return ;
		}
        $this->show();
    }

    public function wechatPlease() {
        $this->show();
    }

    public function companyReg() {
        $this->assign('title', '代理申请');
        $this->assign('openid', $this->getOpenId());
        $this->show();
    }

    public function envslist() {
        $this->loadModel('Envs');
        $Openid = $this->getOpenId();
        // 微信注册
        $this->User->wechatAutoReg($Openid);
        $envs = $this->Envs->getUserEnvs($this->getUid());
        $this->assign('envs', $envs);
        $this->assign('title', '我的红包');
        $this->show();
    }

    /**
     * companySpread
     */
    public function companySpread() {
        // 统计数据 
        $uid = $this->pCookie('uid');
        $this->loadModel('User');
        # $userInfo = $this->User->getUserInfo();
        if (!$this->isCompany($uid)) {
            header('Location:' . $this->root . '?/WechatWeb/proxy/');
        } else {
            $comid = $this->Dao->select('id')->from('companys')->where("uid=$uid")->getOne();
            $userInfo = $this->User->getUserInfoRaw();
            $this->assign('userinfo', $userInfo);
            $spreadData = $this->Db->getOneRow("select sum(readi) as readi,sum(turned) as turned from company_spread_record WHERE com_id = '$comid';");
            // 转化率
            $spreadData['turnrate'] = sprintf('%.2f', $spreadData['readi'] > 0 ? ($spreadData['turned'] / $spreadData['readi']) : 0);
            // 总收益
            $spreadData['incometot'] = $this->Db->getOne("SELECT sum(amount) AS amount FROM `company_income_record` WHERE com_id = '$comid';");
            $spreadData['incometot'] = $spreadData['incometot'] > 0 ? $spreadData['incometot'] : 0;
            // 今日收益
            $spreadData['incometod'] = $this->Db->getOne("SELECT sum(amount) AS amount FROM `company_income_record` WHERE com_id = '$comid' AND to_days(date) = to_days(now());");
            $spreadData['incometod'] = $spreadData['incometod'] > 0 ? $spreadData['incometod'] : 0;
            // 昨日收益
            $spreadData['incometotyet'] = $this->Db->getOne("SELECT sum(amount) AS amount FROM `company_income_record` WHERE com_id = '$comid' AND to_days(date) = to_days(now()) - 1;");
            $spreadData['incometotyet'] = $spreadData['incometotyet'] > 0 ? $spreadData['incometotyet'] : 0;
            // 名下用户总数
            $spreadData['ucount'] = $this->Db->getOne("SELECT count(*) AS amount FROM `company_users` WHERE comid = '$comid';");
            $spreadData['ucount'] = $spreadData['ucount'] > 0 ? $spreadData['ucount'] : 0;
            // 名下用户列表
            $spreadData['ulist'] = $this->Dao->select()->from('clients')->where('client_comid=' . $comid)->exec();
            foreach ($spreadData['ulist'] as &$l) {
                $r = $this->Db->getOneRow("SELECT COUNT(*) as count, SUM(amount) as am FROM `company_income_record` WHERE com_id = $comid AND client_id = $l[client_id];");
                $l['od'] = $r['count'];
                $l['oamount'] = $r['am'] > 0 ? sprintf('%.2f', $r['am']) : '0.00';
            }
            $this->assign('stat_data', $spreadData);
            $this->assign('title', '我的推广');
            $this->show();
        }
    }

    /**
     * 订单列表
     * @param type $Query
     */
    public function orderlist($Query) {
        $this->loadModel('JsSdk');
        $this->loadModel('User');
        $openid = $this->getOpenId();
//        if(!Controller::inWechat()){
//
//            $this->show('./index/error.tpl');
//            die(0);
//        }
        $uinfo = $this->User->getUserInfo($openid);
        $this->User->wechatAutoReg($openid);
        $this->Smarty->caching = false;
        !isset($Query->status) && $Query->status = '';
        $this->assign('uinfo', $uinfo);
        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
        $this->assign('status', $Query->status);
        $this->assign('title', '我的订单');
        $this->show();
    }

    /**
     * Ajax订单列表
     * @param type
     */
    public function ajaxOrderlist($Query) {

        $openid = $this->pCookie('uopenid');
        $this->loadModel('mOrder');
        $this->loadModel('mUserAddress');
        $this->loadModel('User');
        $this->loadModel('mQuestion');
        $this->loadModel('mYun');
        $this->loadModel('mSupplier');
        $this->loadModel('Product');
        $this->loadModel('Carts');
        $this->loadModel('mExpress');
        $this->loadModel('mGroup');

        if ($openid == '') {
            die(0);
        } else {
            !isset($Query->page) && $Query->page = 0;
            $limit = (5 * $Query->page) . ",5";
            $this->cacheId = $openid . $limit . $Query->status;
            $this->Smarty->cache_lifetime = 5;

            if (!$this->isCached()) {
                global $config;
                $this->loadModel('Product');
                if ($Query->status == '' || !$Query->status) {
                    $SQL = "SELECT * FROM `orders` WHERE `wepay_openid` = '$openid' ORDER BY `order_time` DESC LIMIT $limit;";
                } else {
                    if ($Query->status == 'canceled') {
                        $SQL = "SELECT * FROM `orders` WHERE `wepay_openid` = '$openid' AND `status` = '$Query->status'  ORDER BY `order_time` DESC LIMIT $limit;";
                    } else if ($Query->status == 'received') {
                        // 待评价订单列表
                        $SQL = "SELECT * FROM `orders` WHERE `wepay_openid` = '$openid' AND `status` = '$Query->status' AND `is_commented` = 0 ORDER BY `order_time` DESC LIMIT $limit;";
                    } else {
                        // 其他普通列表
                        $SQL = "SELECT * FROM `orders` WHERE `wepay_openid` = '$openid' AND `status` = '$Query->status' ORDER BY `order_time` DESC LIMIT $limit;";
                    }
                }
                $orders = $this->Db->query($SQL);
                foreach ($orders AS &$_order) {
                    // 是否为代付
                    $_order['isreq'] = $_order['status'] == 'reqing';
                    $_order['isreq'] = $_order['isreq'] || $this->Dao->select('')->count()->from(TABLE_ORDER_REQS)->where("order_id = $_order[order_id] AND `wepay_serial` <> ''")->getOne() > 0;
                    $_order['statusX'] = $config->orderStatus[$_order['status']];
                    $_order['order_time'] = $this->Util->dateTimeFormat($_order['order_time']);
                    $address = $this->mUserAddress->get_user_address_by_id($_order['address_id']);
                    $isTuan  = $_order['is_tuan'];
                    if($isTuan == 1){
                        $group = $this->mGroup->getGroupDetailByOrderId($_order['order_id']);
                        if(!$group){
                           $join =  $this->mGroup->getGroupJoinByOrderId($_order['order_id']);
                           $group = $this->mGroup->getGroupDetailById($join['group_id']);
                        }
                        $_order['group'] = $group;
                    }
                    $orderProviderList = $this->Db->query("select * from orders_detail where order_id = ".$_order['order_id']." group by provider_id");
                    foreach($orderProviderList as $pk => $pv){

                        $provider_id =  $pv['provider_id'];
                        $express = $this->mYun->getOrderExpress($_order['order_id'],$provider_id);
                        $expressDetail = $this->mExpress->get_express_detail($express['express_id']);
                        $express['express_name'] = $expressDetail['name'];
                        $orderProviderList[$pk]['express'] = $express;
                        $orderProviderList[$pk]['mSupplier'] =  $this->mSupplier->get_detail_supplier($provider_id);
                        $orderProductsList = $this->Db->query("SELECT `catimg`,`pi`.product_name,`pi`.product_id,`sd`.product_count,`sd`.product_discount_price,`sd`.product_price_hash_id FROM `orders_detail` sd LEFT JOIN `products_info` pi on pi.product_id = sd.product_id WHERE `sd`.order_id = " . $_order['order_id']." and `sd`.provider_id = ".$provider_id);
                        $product_id = $orderProductsList[0]['product_id'];
                        $product = $this->Product->getProductInfo($product_id,false);
                        $yun = $this->mYun->get_detail_yun_sys($product['yun_no']);
                        $orderProviderList[$pk]['yun'] = $yun;
                        $orderProviderList[$pk]['product_list'] = $orderProductsList;

                        if($address){
                            $orderProviderList[$pk]['yun_cost'] = $this->Carts->calc_product_yun_order($orderProductsList,$address);
                        }else{
                            $orderProviderList[$pk]['yun_cost'] = 0;
                        }

                        if($_order['is_tuan'] == 1){
                            $orderProviderList[$pk]['yun_cost'] = 0;
                        }
                    }
                    $_order['orderProviderList'] = $orderProviderList;

                }
                $this->assign('orders', $orders);
            }
        }
        $this->show();
    }

    /**
     * 查看订单详情
     * @param type $orderid
     */
    public function viewOrder() {
        $this->show();
    }

    /**
     * 判断是否微代理
     */
    private function isCompany($openid) {
        return $this->Db->query("SELECT `uid` FROM `companys` WHERE `uid` = '$openid';");
    }

    public function selectOrderAddress($Query) {
        !isset($Query->body) && $Query->body = 'false';
        $uid = $this->pCookie('uid');
        $ret = $this->Db->query("SELECT * FROM `client_order_address` WHERE `client_id` = $uid;");
        $this->assign('addrs', $ret);
        $this->assign('bodyonly', $Query->body == 'true' ? true : false);
        $this->show();
    }

    public function ajaxAddAddress() {
        $uid = $this->pCookie('uid');
        $name = $this->post('name');
        $tel = $this->post('tel');
        $addr = $this->post('addr');
        $ret = $this->Db->query("INSERT INTO `client_order_address` (client_id,`name`,`tel`,`address`) VALUES ($uid,'$name','$tel','$addr');");
        echo $ret;
    }

    /**
     * 我的收藏页面
     */
    public function uc_likes() {
        $this->assign('title', '我的收藏');
        $this->show();
    }

    /**
     * 获取收藏列表
     * @param type $Query
     */
    public function ajaxLikeList($Query) {
        $openid = $this->getOpenId();
        $this->cacheId = $openid . $Query->page;
        if (!$this->isCached()) {
            !isset($Query->page) && $Query->page = 0;
            $limit = ($Query->page * 10) . ',10';
            $this->loadModel('User');
            $likeList = $this->User->getUserLikes($openid, $limit);
            if ($likeList !== false) {
                $this->assign('loaded', count($likeList));
                $this->assign('likeList', $likeList);
            } else {
                $this->assign('loaded', 0);
            }
        }
        $this->show();
    }

    /**
     * ajax编辑收藏
     */
    public function ajaxAlterProductLike() {
        $this->loadModel('User');
        $openid = $this->getOpenId();
        $id = $this->post('id');
        if ($id > 0 && $openid != '') {
            // add
            echo $this->User->addUserLike($openid, $id);
        } else if ($id < 0 && $openid != '') {
            // delete
            $id = abs($id);
            echo $this->User->deleteUserLike($openid, $id);
        } else {
            echo 0;
        }
    }

    /**
     * ajax获取用户分组
     */
    public function getAllGroup() {
        $this->loadModel('SqlCached');
        // file cached
        $cacheKey = 'ucajaxGetCategroys';
        $fileCache = new SqlCached();
        $ret = $fileCache->get($cacheKey);
        if (-1 === $ret) {
            $this->loadModel('UserLevel');
            $lev = $this->UserLevel->getList();
            $levs = array();
            foreach ($lev as $l) {
                $levs[] = array('dataId' => $l['id'], 'name' => $l['level_name']);
            }
            $cats = $this->toJson($levs);
            $fileCache->set($cacheKey, $cats);
            echo $cats;
        } else {
            echo $ret;
        }
    }
    
   public function ajaxSendCode(){
    	
    	$phone = $_POST['phone'];
   	    
   	    $fp = fopen("http://qiezilife.com/SmsService/SmsServlet?phone=".$phone, 'r');
        stream_get_meta_data($fp);
        while(!feof($fp)) {
            $result .= fgets($fp, 1024);
        }
        fclose($fp);
        
        $resp = json_decode($result, true);
        $state = $resp['state'];
        $code = $resp['code'];
        $this->sCookie($phone,$code,60);
        if($state == 0){
        
        	$this->echoMsg(1,'验证码已发送');
        }else{
            $this->echoMsg(1,'验证码发送失败');
        }
    }
    
    public function ajaxVeriCode(){
    
        $openid = $this->getOpenId();
    
        $this->loadModel('User');

   		$code = $_POST['code'];
   		$phone = $_POST['phone'];
   		$session_code = $this->pCookie($phone);

   		error_log("===============code========".$code."=========phone".$phone."=======session_code====".$session_code);
    	if($code == ''){
            $this->echoMsg(-1,"请填写验证码");
        
        }else if($code != $session_code){
            $this->echoMsg(-1,"验证码错误或已过期");
        }else{

          $userInfo = $this->User->getUserInfoRaw($openid);
          if($userInfo){
             $data = array('client_phone' => $phone);
             $this->User->updateUserInfo($userInfo['client_id'], $data);
          }
          

          //设置注册奖励
          $uid = $userInfo['client_id'];
          $awardSettings = $this->Dao->select("value")->from('wshop_settings')->where("`key` = 'award_settings'")->getOne();
          error_log('awardSettings ================>'.$awardSettings);
          if(!empty($awardSettings)){
              $awards = json_decode($awardSettings, true);
              if ($awards['reg_award'] AND ($awards['reg_award']['type']>0)) {
                  global $config;
                  //
                  $awardType = $awards['reg_award']['type'];
                  $value = $awards['reg_award']['value'];
                  $this->loadModel('Coupons');
                  $this->loadModel('WechatSdk');
                  if($awardType==1){ //奖励的是优惠券
                      $this->loadModel('UserCoupon');
                      $rtnCode = $this->UserCoupon->regAwardCoupon($value,$uid);
                      error_log('reg award coupn info , rtn_code is ================>'.$rtnCode);
                      if ($rtnCode > 0) {
                          $this_coupon = $this->Coupons->get_coupon_info(($value));
                          if($this_coupon['discount_type'] == 1){
                             $discount_val = intval($this_coupon['discount_val']/10);
                            Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, "亲，终于等到你~ 这张（".$discount_val."折）优惠券给你预留好久了\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康商品吧~", $config->domain.'?/Coupon/user_coupon/');
                          
                          }else{
                            $discount_val = intval($this_coupon['discount_val']/100);
                            Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, "亲，终于等到你~ 这张（".$discount_val."元）优惠券给你预留好久了\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康商品吧~", $config->domain.'?/Coupon/user_coupon/');
                            
                          }

                          // send weixin msg
                      }
                  }else if($awardType==2){  //奖励的是账户余额
                      //因为用户是初次注册，此时的余额还为0，所以不需要先查询余额。
                      $money = $value/100;
                      $this->User->updateUserMoneyByOpenId($openid,$money);
                      error_log('reg award is money ================>'.$money);
                      // send weixin msg
                      Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, "亲，终于等到你~ 这张（".$money."元）优惠券给你预留好久了\n适用范围：CheersLife全部商品\n使用规则：在下单时选中优惠券抵用即可\n快来享用健康商品吧~", $config->domain.'?/Coupon/user_coupon/');
                  }
              }
          }


       
          $this->echoMsg(1,"绑定成功");
        }
        
        
    }
    
    /**
     * 批量发送微信消息   
     */
    public function ajax_batch_send_wechat_msg(){
    	
    	global $config;
    	$open_ids = $this->pPost('open_ids');
    	$msg_type = $this->pPost('msg_type');
    	$content = $this->pPost('msg_content');
    	$url =$this->pPost('msg_url');
    	if(empty($content)){
    		$this->echoMsg(-1,'请输入推送的消息内容');
    		die(0);
    	}
    	
    	if($msg_type == 1){
    		if(empty($url)){
    			$this->echoMsg(-1,'请输入连接地址');
    			die(0);
    		}
    	}
    	if($url){
    		$url = 'http://'.$this->getBaseURI() .'/'. urldecode($url);
    	}
    	$open_ids_arr = explode(",",$open_ids);
    	$access_token = WechatSdk::getServiceAccessToken();
    	error_log('token====>'.$access_token);
    	if($msg_type == 1){
    		error_log('========================send url link msg========================');
    		foreach ($open_ids_arr as $key => $val){
    			$ret = Messager::sendNotification($access_token, $val, $content, $url);
    			error_log('【'.$val . '】 send status=====>'.$this->toJson($ret));
    		}
    	}else{
    		error_log('========================send text msg========================');
    		foreach ($open_ids_arr as $key => $val){
    			$ret = Messager::sendText($access_token, $val, $content);
    			error_log('【'.$val . '】 send status=====>'.$this->toJson($ret));
    		}
    	}
    	$this->echoMsg(1,"发送成功");
    }


    public function  sfansList(){

        $this->loadModel('User');
        $this->loadModel('mOrder');
        $Openid = $this->getOpenId();
        if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }

        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        // get uid
        $Uid = $uinfo['uid'];
        $list = $this->User->getFansList($Uid);
        $sList = array();
        foreach($list as $key => $val){
            $secList =   $this->User->getFansList($val['client_id']);
            $from_user = $this->User->getUserInfo($val['client_id']);

            foreach($secList as $k=>$v){

                $v['from_user'] =$from_user;
                array_push($sList,$v);
            }

        }

        foreach($sList as $key => $val){

            $uid = $val['client_id'];
            $money = $this->User->getFansMoney(1,$uid);
            $fansMoney = 0;
            if($money && $money[0]['amount']){
                $fansMoney = $money[0]['amount'];
            }
            $sList[$key]['fansMoney'] = $fansMoney;
            $orderMoney = $this->mOrder->getUserOrderMoney($uid);
            $orderList = $this->mOrder->getUserOrderList($uid);
            $orderCount = 0;
            if($orderList){
                $orderCount = count($orderList);
            }
            $oMoney =  0;
            if($orderMoney && $orderMoney[0]['amount']){
                $oMoney= $orderMoney[0]['amount'];
            }
            $sList[$key]['orderMoney']  = $oMoney;
            $sList[$key]['orderCount'] =$orderCount;
        }


        $this->assign('list', $sList);
        $this->show('./uc/sc_fans.tpl');

    }

    public  function  fansList(){

        $this->loadModel('User');
        $this->loadModel('mOrder');
        $Openid = $this->getOpenId();


        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }

        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        // get uid
        $Uid = $uinfo['uid'];
        $list = $this->User->getFansList($Uid);
        foreach($list as $key => $val){

            $uid = $val['client_id'];
            $money = $this->User->getFansMoney(1,$uid);
            $fansMoney = 0;
            error_log("=====fansMoeny=====".json_encode($money));
            if($money && $money[0]['amount']){
                $fansMoney = $money[0]['amount'];
            }
            $list[$key]['fansMoney'] = $fansMoney;
            $orderMoney = $this->mOrder->getUserOrderMoney($uid);
            $orderList = $this->mOrder->getUserOrderList($uid);
            $orderCount = 0;
            if($orderList){
                $orderCount = count($orderList);
            }
            $oMoney =  0;
            if($orderMoney && $orderMoney[0]['amount']){
                $oMoney= $orderMoney[0]['amount'];
            }
            $list[$key]['orderMoney']  = $oMoney;
            $list[$key]['orderCount'] =$orderCount;

        }

        $this->assign('list', $list);
        $this->show('./uc/my_fans.tpl');


    }

    public function present(){
        $this->loadModel('User');
        $Openid = $this->getOpenId();
        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        $this->assign("uinfo",$uinfo);
        $this->show();
    }
    public function present_history(){
        $this->loadModel('User');
        $Openid = $this->getOpenId();
        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        $uid = $uinfo['client_id'];
        $list = $this->User->getPresentList($uid);
        foreach($list as $key=>$val){

            $uinfo = $this->User->getUserInfo($val['uid']);
            $list[$key]['uinfo'] = $uinfo;
            $list[$key]['add_time'] = date('Y-m-d',$val['add_time']);
        }
        $this->assign('list',$list);
        $this->show();
    }

    public function  do_present($query){

        $uid = $query->uid;
        $this->loadModel('User');
        $uinfo = $this->User->getUserInfo($uid);
        if($uinfo){

            if($uinfo['account'] <= 50){
                $this->echoMsg(-1,'提现额度必须大于50才能提现');


            }else{

                $data = array();
                $data['account'] = 0;
                $data['all_account']= $uinfo['all_account']+$uinfo['account'];
                $this->User->updateUserInfo($uid,$data);
                $log = array();
                $log['uid'] = $uid;
                $log['amount'] = $uinfo['account'];
                $log['des'] = '提现';
                $log['order_id'] = '';
                $log['add_time'] = time();
                $log['from_uid'] = $uid;
                $log['status'] = 0;
                $log['type'] = 2;
                $this->User->insert_user_money_log($log);
                $this->echoMsg(1,'提交成功，提现结果,请注意提现历史!');

            }
        }else{

            $this->echoMsg(-1,'用户不存在');

        }

    }

    public function qa_code(){

        $this->loadModel('User');
        $Openid = $this->getOpenId();
        if(!Controller::inWechat() && !$this->debug){

            $this->show('./index/error.tpl');
            die(0);
        }
        // 微信自动注册
        $this->User->wechatAutoReg($Openid);
        $uinfo = $this->User->getUserInfo($Openid);
        $uid = $uinfo['client_id'];


        global $config;
        $dir = $config->codePicLink;
        $filename = $uinfo['client_id']."_head.jpg";
        //  $this->GrabImage($uinfo['client_head']."/0",$filename);
        $url = $uinfo['client_head']."/0";
        $user_qr_code = $dir.$uinfo['client_id']."_code.jpg";
        $head_name =$dir.$uinfo['client_id']."_head.jpg";
        $wx_qr_code = $dir.$uinfo['client_id']."_wx.jpg";
        if(!file_exists($head_name)){
            $this->downloadFile($url,$head_name);
        }

        if(!file_exists($user_qr_code)){
            $dst =  $dir."mycode_infor.png";
            $invitation =  imagecreatefrompng($dst);
            $cmd = '/usr/bin/convert '.$head_name.' -resize 400x400^ -gravity center -crop 400x400+0+0 +repage \( +clone -threshold -1 -negate -fill white -draw "circle 200,200 200,0" \)  -alpha off -compose copy_opacity -composite -auto-orient '. $dir.$uinfo['client_id'].'_circle_head.png';
            $result = exec($cmd);
            imagedestroy($head_name);
            $avatar_r = imagecreatefrompng($dir.$uinfo['client_id']."_circle_head.png");
            $v = imagecopyresized($invitation, $avatar_r, 295, 41, 0, 0, 160, 160, 400, 400);
            imagedestroy($avatar_r);
            imagejpeg($invitation,$user_qr_code);

            include_once(dirname(__FILE__) . "/../lib/phpqrcode/qrlib.php");
            $track_url = $config->domain . '?/from_uid='.$uinfo['client_id'];
            $errorCorrectionLevel = 'L';  // 纠错级别：L、M、Q、H
            $matrixPointSize = 13; // 点的大小：1到10
            QRcode::png($track_url, $wx_qr_code, $errorCorrectionLevel, $matrixPointSize, 2);

            $qrcodeImage = imagecreatefromjpeg($wx_qr_code);
            imagecopyresized($invitation, $qrcodeImage, 246, 699, 0, 0,258,258, 430,430);
            imagedestroy($qrcodeImage);
            imagejpeg($invitation,$user_qr_code);
            imagedestroy($invitation);
        }

        $img ="http://".$_SERVER['SERVER_NAME']."/uploads/code/".$uinfo['client_id']."_code.jpg";
        $this->assign("img",$img);
        $this->show();


//        $token = WechatSdk::getServiceAccessToken();
//        $ticket = WechatSdk::getCQrcodeTicket($token,$uid,WechatSdk::QR_SCENE);
//        $wx_url = WechatSdk::getCQrcodeImage($ticket);
//        error_log("=====wx_url=====".$wx_url);
//        if(file_exists($wx_qr_code)){
//            @unlink ($wx_qr_code);
//        }
//        $this->downloadFile($wx_url,$wx_qr_code);
//
//        if(file_exists($user_qr_code)){
//            @unlink ($user_qr_code);
//        }
//
//
//        $dst =  $dir."mycode_infor.png";
//        $cmd = '/usr/bin/convert '.$head_name.' -resize 400x400^ -gravity center -crop 400x400+0+0 +repage \( +clone -threshold -1 -negate -fill white -draw "circle 200,200 200,0" \)  -alpha off -compose copy_opacity -composite -auto-orient '. $dir.$uinfo['client_id'].'_circle_head.png';
//        $result = exec($cmd);
//        imagedestroy($head_name);
//        $invitation =  imagecreatefrompng($dst);
//        $avatar_r = imagecreatefrompng($dir.$uinfo['client_id']."_circle_head.png");
//        $v = imagecopyresized($invitation, $avatar_r, 100, 680, 0, 0, 135, 135, 400, 400);
//        imagedestroy($avatar_r);
//        imagejpeg($invitation,$user_qr_code);
//
//        $qrcodeImage = imagecreatefromjpeg($wx_qr_code);
//        imagecopyresized($invitation, $qrcodeImage, 175, 300, 0, 0,324,324, 430,430);
//        imagedestroy($qrcodeImage);
//        imagejpeg($invitation,$user_qr_code);
//
//        imagedestroy($invitation);
//        $img ="http://".$_SERVER['SERVER_NAME']."/uploads/code/".$uinfo['client_id']."_code.jpg";
//        $this->assign("img",$img);
//        $this->show();
    }


    public function downloadFile($url,$filename){

        $ch = curl_init ($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);

        $img = curl_exec ($ch);
        curl_close ($ch);

        $fp = fopen($filename,'w');
        fwrite($fp, $img);
        fclose($fp);
    }


}
