<?php

/**
 * @author <ycchen@iwshop.cn>
 */
class Xlsx extends Controller {



    public function exportExcel($query){

        global $config;
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/Reader/Excel2007.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/Reader/Excel5.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/IOFactory.php';

        $list = $query->odlist;



        $fileName = "CheersLife";
        $headArr = array("订单编号","收货人","身份证","联系方式","收获地址","订单金额","运费","商品名字","商品数量","商品价格","优惠金额","下单时间","供应商");
        $sql = "select * from orders o left JOIN orders_detail od on o.order_id = od.order_id where od.detail_id in ($list)";


        $orderList = $this->Db->query($sql);
        $this->loadModel('Product');
        $this->loadModel('mSupplier');
        $this->loadModel('Coupons');

        $d = array();
        foreach ($orderList as $index => $order) {

            $address_id = $orderList[$index]['address_id'];
            $address = $this->Db->query("SELECT * FROM `user_address` WHERE id = $address_id;");
            $orderList[$index]['address'] = $address[0];
            $orderList[$index]['statusX'] = $config->orderStatus[$orderList[$index]['status']];
            $product = $this->Product->getProductInfoWithSpec($orderList[$index]['product_id'],$orderList[$index]['product_price_hash_id']);
            $orderList[$index]['product'] = $product;

            $supper = $this->mSupplier->get_detail_supplier($orderList[$index]['provider_id']);
            $orderList[$index]['supplier'] = $supper;

            $coupon = $orderList[$index]['user_coupon'];
            $amount  = 0;
            if($coupon!= ""){
                $arr = explode(",",$coupon);

                foreach($arr as $u){
                    $couponInfo = $this->Coupons->get_coupon_info($u);
                    if($couponInfo['coupon_type'] == 1 || $couponInfo['coupon_type'] == 2){

                        $amount +=  (int)$couponInfo['discount_val'] /100;
                    }
                }
            }

            $data = array();
            $data[0] = $order['serial_number'];//$order['serial_number'];
            $data[1] = $orderList[$index]['address']['user_name'];//$orderList[$index]['address']['user_name'];
            $data[2] = $orderList[$index]['address']['identity'];//$orderList[$index]['address']['user_name'];
            $data[3] = $orderList[$index]['address']['phone'];//$orderList[$index]['address']['phone'];
            $data[4] = $orderList[$index]['address']['province'].$orderList[$index]['address']['city'].$orderList[$index]['address']['area'].$orderList[$index]['address']['address'];//$orderList[$index]['address']['address'];
            $data[5] =$orderList[$index]['order_amount'];
            $data[6] =$orderList[$index]['order_yunfei'];
            $data[7] =$orderList[$index]['product']['product_name'];
            $data[8] =$orderList[$index]['product_count'];
            $data[9] =$orderList[$index]['product']['sale_prices'];
            $data[10] = $amount;
            $data[11] =$orderList[$index]['order_time'];
            $data[12] =$orderList[$index]['supplier']['name'];

            $d[] = $data;


        }




        $this->getExcel($fileName,$headArr,$d);

    }

    public  function getExcel($fileName,$headArr,$data){
        if(empty($data) || !is_array($data)){
            die("data must be a array");
        }
        if(empty($fileName)){
            exit;
        }
        $date = date("Y_m_d",time());
        $fileName .= "_{$date}.xlsx";

        //创建新的PHPExcel对象
        $objPHPExcel = new PHPExcel();
        $objProps = $objPHPExcel->getProperties();

        //设置表头
        $key = ord("A");
        foreach($headArr as $v){
            $colum = chr($key);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);
            $key += 1;
        }

        $column = 2;
        $objActSheet = $objPHPExcel->getActiveSheet();
        foreach($data as $key => $rows){ //行写入
            $span = ord("A");
            foreach($rows as $keyName=>$value){// 列写入
                $j = chr($span);
                $objActSheet->setCellValue($j.$column, ' '.$value);
                $span++;
            }
            $column++;
        }

        $fileName = iconv("utf-8", "gb2312", $fileName);
        //重命名表
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        //设置活动单指数到第一个表,所以Excel打开这是第一个表
        $objPHPExcel->setActiveSheetIndex(0);
        //将输出重定向到一个客户端web浏览器(Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment; filename=\"$fileName\"");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //$objWriter->save('php://output'); //文件通过浏览器下载
        $this->SaveViaTempFile($objWriter);
        exit;

    }


    public function  SaveViaTempFile($objWriter){
        $filePath = '' . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
        $objWriter->save($filePath);
        readfile($filePath);
        unlink($filePath);
    }

}
