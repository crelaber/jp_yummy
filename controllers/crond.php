<?php
    
    class crond extends Controller
    {
        
        public function __construct($ControllerName, $Action, $QueryString) {
            parent::__construct($ControllerName, $Action, $QueryString);
        }
        
        public function daily()
        {
            error_log('daily crond started');
            // update product stock
            $data = array();
            $data['stock_date'] = time();

            //'stock_date,sku_id,sku_name,avaliable,produce,instock'
            $this->loadModel('Sku');
            // get products list
            $sku_list = $this->Sku->get_all_active_skus();
            if ($sku_list) {
                $this->loadModel('Stock');
                foreach ($sku_list AS $val) {
                    $data['sku_id'] = $val['id'];
                    $data['sku_name'] = $val['name'];
                    $data['avaliable'] = 0;
                    $data['produce'] = 0;
                    $ret = $this->Stock->add_product($data);
                }
            }
        }


        public function sendActivityNotify(){
            $userTable = 'clients';
            if($userList = $this->Dao->select()->from($userTable)->exec(false)){
                $openids = array_column($userList,'client_wechat_openid');
                $openids = array_unique($openids);
                $baseUrl = $this->getBaseModuleRequestPath('sendCheerslifeNewProductActity');
                error_log('$baseUrl==================>'.$baseUrl);
                global $config;
                $tplId = $config->new_product_notify_tpl_msg_id ;
                foreach ($openids as $openid){
                    $param = [
                        'openid' => $openid,
                        'tpl_id' => $tplId,
                    ];
                    $url = $this->buildModuleRequestUrl($baseUrl,$param);
                    //异步调用http请求
                    HttpHelper::sendAsynRequest($url);
                }
            }
        }

        public function sendCheerslifeNewProductActity($Q){
            $openid = $Q->openid;
            $tplId = $Q->tpl_id;
            global $config;
            $this->loadModel('WechatSdk');
            //推送信息
            $result = Messager::sendTemplateMessage($tplId, $openid, array(
                'first' => " 中秋国庆豪礼必备",
                'keyword1' => '净省100！新西兰KGF黑糖礼盒套装，再送价值108的新西兰黑糖蜜一份',
                'keyword2' => '待办',
                'remark' => '立即抢购'
            ), $config->cheerslife_new_activity_link);
            error_log($result);
            echo $result;
        }

        /**
         * 构建模块对应的url
         * @param $path 请求的路径
         * @param $params 请求的参数
         * @return string
         */
        public function buildModuleRequestUrl($url,$params = []){
            if($params){
                $param = http_build_query($params);
                $url = $url.'/'.$param;
            }
            return $url;
        }

        public function getBaseModuleRequestPath($path){
            global $config;
            $domain = $config->domain;
            $baseUrl = $domain.'?/crond/';
            $url = $baseUrl.$path;
            return $url;
        }


        public function sendTplMsg(){

        }



    }

?>