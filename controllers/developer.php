<?php
    
    class developer extends Controller
    {
        
        public function __construct($ControllerName, $Action, $QueryString) {
            parent::__construct($ControllerName, $Action, $QueryString);
        }
        

        /**
         * 验证token授权接口
         * http://cheerslife.dev.com/?/developer/authorization/appid=qz12377e8273c001f7&appsecret=d413961dd74c7aed9ec3f6f091ed81fc7c890c9&noncestr=1234567890qwertyuiopasdfghjklzxc&timestamp=1486617486&signature=683292c8e23bd2dfb6e450679ae6060b946058a4
         */
        public function authorization($Q){
            $appid = $Q->appid;
            $appsecret = $Q->appsecret;
            $noncestr  = $Q->noncestr;
            $timestamp  = $Q->timestamp;
            $signature  = $Q->signature;
            //生成token
            $result = TokenHelper::getTokenFromApi($appid,$appsecret,$noncestr,$timestamp,$signature);
            //生成签名
//            $result = TokenHelper::generateSignFromApi($appid,$appsecret,$noncestr,$timestamp);
            $this->apiJson($result['errcode'],$result['msg'],$result['data']);
        }


    }

?>