<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author       <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class vConvert extends Controller {

    public function convertTemplate($Q){
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/Reader/Excel2007.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/Reader/Excel5.php';
        include dirname(__FILE__) . '/../lib/PHPExcel/Classes/PHPExcel/IOFactory.php';

        $type = $Q->type;
        if($type == 'word'){
            $filePath = './exports/question_lib_template.xlsx';
            $data = ExcelUtil::formatQuestionLibData($filePath);
            $rows = array();
            foreach($data as $key => &$val){
//                if(!empty($val['question']) and !empty($val['text'])){
                    $rows[] = $val;
//                }
            }
        }else if($type == 'pic'){
            $filePath = './exports/pic_lib_template.xlsx';
            $data = ExcelUtil::formatPicLibData($filePath);
            $rows = array();
            foreach($data as $key => &$val){
                $rows[] = $val;
            }
        }else if($type == 'pic_cd'){
            $filePath = './exports/output/chengdou_pic_lib_data.xlsx';
            $data = ExcelUtil::formatPicLibData($filePath);
            $rows = array();
            foreach($data as $key => &$val){
                $rows[] = $val;
            }
        }else if($type == 'word_kbt'){
            $filePath = './exports/output/kangbite.xlsx';
            $data = ExcelUtil::formatPicLibData($filePath);
            $rows = array();
            foreach($data as $key => &$val){
                $rows[] = $val;
            }
        }

        echo json_encode($rows);

    }

}
