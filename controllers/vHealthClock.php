<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author       <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class vHealthClock extends Controller {

    const LOGIN_KEY = 'health_punch_login_uid';
    const PAGE_HOME_KEY = 'health_punch_home_page';
    const PAGE_LOW_LEVEL_KEY = 'health_punch_low_level_page';
    const PAGE_HIGH_LEVEL_KEY = 'health_punch_low_level_page';
    const EXCHANGE_FAIL_REASON = 'exchange_fail_reason';

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('mHealthClock');
        $this->getShareSign();
    }

    /**
     * 初级打卡
     */
    public function lowLevel($Q){
        try{
            error_log('request from low level===========>');
            $result = $this->validateParamFlow($Q);
            $this->getShareSign();
            $uid = $result['uid'];
            $userInfo = $this->mHealthClock->getPartnerUserInfo($uid);
            $punchTimes = $userInfo['low_level_times']+1;
            $homePage = $this->buildParamPageUrl($Q,'home');
            $this->buildLowLevelParam($Q);
            //判断超过了14天的情况
//            if($userInfo['low_level_times'] >= mHealthClock::LOW_LEVEL_LAST_DAYS){
//                $this->assign('homePage',$homePage);
//                $this->getLowLevelFinishStatus($userInfo);
//                $this->show('./health_clock/low_level_punch_result.tpl');
//                die(0);
//            }

            //判断当天是否已经打卡
            if($todayPunchInfo = $this->mHealthClock->getCurrentDayPunchInfo($uid,false)){
                $this->assign('homePage',$homePage);
                $this->getLowLevelFinishStatus($userInfo);
                $this->show('./health_clock/low_level_punch_result.tpl');
                die(0);
                //以下是针对答对还是答错的逻辑，由于开始业务逻辑没有理清楚
//            $questionDataList = json_decode($todayPunchInfo['question_data'],true);
//            $userAnswerInfo = $questionDataList[0];
//            //当天的答案是正确的，直接跳到结果页
//            if($userAnswerInfo['answer_status']){
//                $homePage = $this->buildParamPageUrl($Q,'home');
//                $this->assign('homePage',$homePage);
//                $this->getLowLevelFinishStatus();
//                $this->show('./health_clock/low_level_punch_result.tpl');
//                die(0);
//            }else{
//                //回答错误还可以记录答题，直到正确为止
//                $challengeIndex = $todayPunchInfo['challenge_index'];
//                $dayIndex = $todayPunchInfo['day_index'];
//                $this->assign('dayIndex',$dayIndex);
//                $this->assign('challengeIndex',$challengeIndex);
//                $this->buildLowLevelParam($Q);
//                $this->show('./health_clock/low_level_punch.tpl');
//                die(0);
//            }
            }

            $limitDay = mHealthClock::LOW_LEVEL_LAST_DAYS;
            $circle = ($punchTimes - $punchTimes % $limitDay)/$limitDay;
            $circle = ($punchTimes % $limitDay == 0) ? $circle: $circle + 1;
            //防止超出题目的次数
            $circle = $circle > 3 ? 1 : $circle;
            $this->assign('circle',$circle);

            $challengeIndex = $this->mHealthClock->getLowLevelMaxChallengeIndex();
            //计算日期
            $dayIndexResult = $this->mHealthClock->calLowLevelDay($uid);
            $dayIndex = $dayIndexResult['dayIndex'];
            $this->assign('dayIndex',$dayIndex);
            $this->assign('challengeIndex',$challengeIndex);
            $this->assign('punchTimes',$punchTimes);

            $reminder = $punchTimes % $limitDay;
            $showDay = $reminder == 0 ? $limitDay :$reminder;
            $this->assign('showDay',$showDay);
            $this->show('./health_clock/low_level_punch.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }

    /**
     * 初级打卡结果页
     */
    public function lowLevelResult(){
        try{
            $this->getShareSign();
            $shareInfo = $this->getShareInfo('lowLevel','zy_group');
            $this->assign('shareInfo',$shareInfo);
            $homePage = $this->pCookie(self::PAGE_HOME_KEY);
            $this->getLowLevelFinishStatus();
            $this->assign('homePage',$homePage);
            $this->show('./health_clock/low_level_punch_result.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }
    /**
     * 活动未开始
     */
    public function noActivity($Q){
        $this->show('./health_clock/no_activity.tpl');
    }

    /**
     * BMI结果页面
     */
    public function BMIresult($Q){
        $this->show('./health_clock/bmi_introduce.tpl');
    }
    /**
     * 高级打卡
     */
    public function highLevel($Q){
        try{
            error_log('request from high level===========>');
            $result = $this->validateParamFlow($Q);
            //分享所需要的签名信息
            $uid = $result['uid'];
            $this->getShareSign();
            //计算日期
            $dayIndexResult = $this->mHealthClock->calHighLevelDay($uid);
            $dayIndex = $dayIndexResult['dayIndex'];
            $userInfo = $this->mHealthClock->getPartnerUserInfo($uid);
            global $config;
            //测试条件时候要用，因此可以根据开关来控制
            if($config->open_permission_validate){
                //判断是否已经有足够的权限
//                $titles = $userInfo['titles'];

                $titles = $this->mHealthClock->getUserAllTitles($uid);

                if(!$titles){
                    $this->show('./health_clock/no_open.tpl');
                    die(0);
                }
                if($titles){
                    $levels = array_column($titles,'level');
                    $maxLevel = max($levels);
                    if($maxLevel < 2){
                        $this->show('./health_clock/no_open.tpl');
                        die(0);
                    }
                }
            }

            //构建访问地址,为了访问初次访问可以使用
            $highLevelPage = $this->buildParamPageUrl($Q,'highLevel');
            $this->sCookie(self::PAGE_HIGH_LEVEL_KEY,$highLevelPage);
            //构建访问地址,为了访问初次访问可以使用
            $homePage = $this->buildParamPageUrl($Q,'home');
            $this->sCookie(self::PAGE_HOME_KEY,$homePage);

            //判断是否已经录入了健康信息
            $healthInfo = $this->mHealthClock->getUserLatestHealthInfo($uid);
            if(!$healthInfo){
                $startPage = $this->buildHealthPageUrl('healthInfo');
                $this->assign('startPage',$startPage);
                $this->show('./health_clock/start_clock.tpl');
                die(0);
            }

            //第一次进入已经录入了信息但是并没开始打卡
//        if($userInfo['high_level_times'] == 0){
//            $healthInfo['bmi'] = $healthInfo['bmi'] / 100;
//            $this->assign('healthInfo',$healthInfo);
//            $startPage = $this->pCookie(self::PAGE_HIGH_LEVEL_KEY);
//            $this->assign('startPage',$startPage);
//            $this->getWeightPlan($latestHealthInfo['bmi']);
//            $this->show('./health_clock/health_result.tpl');
//            die(0);
//        }
            $lastDay = mHealthClock::HIGH_LEVEL_LAST_DAYS;

            //当为29天，57天等重新开始新的周期时候，需要重新录入健康信息
            if(($dayIndex - 1)% $lastDay == 0){
                $healthTimes = $healthInfo['times_index'];
                $punchTimes = $userInfo['high_level_times'];
                $reminder = $punchTimes % $lastDay;
                //计算当前打卡的周期和录入的健康信息的周期
                $currentTimes = ( $punchTimes - $reminder ) / $lastDay;
                //如果周期相等表示还没有录入健康信息
                if($healthTimes == $currentTimes){
                    $redirectPage = $this->buildHealthPageUrl('healthResult');
                    $this->assign('redirectPage',$redirectPage);
                    $this->show('./health_clock/health_info.tpl');
                    die(0);
                }
            }

            //减脂的类型，1为正常，2为增肌，3位减脂
            $weightType = $healthInfo['weight_type'];
            $caseDetail = $this->mHealthClock->getCaseDetailByTypeAndDay($weightType,$dayIndex);
            $status = 0;
            if($todayPunchInfo = $this->mHealthClock->getCurrentDayPunchInfo($uid,true)){
                $status = 1;
                //因为计算的时间索引有自动加1了，所以在这里要减一
                $dayIndex = $dayIndex - 1;
            }

            $this->assign('dayIndex',$dayIndex);
            $highLevelTime = $userInfo['high_level_times'] + 1 ;
            //是否显示提示
            $showHint =  $userInfo['high_level_times'] > 0 && $highLevelTime % mHealthClock::USER_TITLE_PUNCH_BASE_NUM == 0 ? 1 : 0;
            $this->assign('showHint',$showHint);
            $this->assign('status',$status);
            $this->assign('caseDetail',$caseDetail);
            //分享的页面
            $sharePage = $this->buildSharePage($Q,'highLevel');
            $this->assign('sharePage',$sharePage);

            //分享的信息
            $shareInfo = $this->getShareInfo('highLevel',$Q->come_from);
            $this->assign('shareInfo',$shareInfo);

            $this->show('./health_clock/high_level_punch.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }


    /**
     * 获取分享的签名信息
     */
    public function home($Q){
        try{
            error_log('request from home===========>');
            $result = $this->validateParamFlow($Q);
            //分享所需要的签名信息
            $this->getShareSign();
            $uid = $result['uid'];
            $highLevelMaxLastId = $this->mHealthClock->getMaxHighLevelRecordId($uid);
            $lowLevelLastId = $this->mHealthClock->getMaxLowLevelRecordId($uid);

            //最后的maxid，用于分页加载用
            $this->assign('highLevelMaxLastId',$highLevelMaxLastId);
            $this->assign('lowLevelLastId',$lowLevelLastId);

            //构建访问的链接地址
            $lowLevelPage = $this->buildParamPageUrl($Q,'lowLevel');
            $this->assign('lowLevelPage',$lowLevelPage);
            $highLevelPage = $this->buildParamPageUrl($Q,'highLevel');
            $this->assign('highLevelPage',$highLevelPage);
            //构建访问地址,为了访问初次访问可以使用
            $homePage = $this->buildParamPageUrl($Q,'home');
            $this->sCookie(self::PAGE_HOME_KEY,$homePage);

            //兑换页面
            $exchangePage = $this->buildHealthPageUrl('exchange');
            $this->assign('exchangePage',$exchangePage);

            $userInfo = $this->mHealthClock->getPartnerUserInfo($uid);
            $titles = $userInfo['titles'];
            $currentLevel = 0;
            if($titles){
                $this->assign('titles',$titles);
                $levels = array_column($titles,'level');
                $currentLevel = $levels[0];
            }
            $this->assign('currentLevel',$currentLevel);
            $this->assign('titles',$titles);
            $this->assign('uid',$uid);


            $sharePage = $this->buildSharePage($Q,'home');
            $this->assign('sharePage',$sharePage);

            //分享的信息
            $shareInfo = $this->getShareInfo('home',$Q->come_from);
            error_log('current share info is =============>'.json_encode($shareInfo));
            $this->assign('shareInfo',$shareInfo);

            $this->show('./health_clock/index.tpl');

        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }

    /**
     * 奖品列表页
     */
    public function prizeList(){
        $this->show('./health_clock/prize_list.tpl');
    }


    /**
     * 健康信息录入页面
     */
    public function healthInfo(){
        try{
            $uid = $this->pCookie(self::LOGIN_KEY);

            $highLevelPage = $this->pCookie(self::PAGE_HIGH_LEVEL_KEY);
            $this->assign('startPage',$highLevelPage);
            //如果没有录入健康信息
            $latestHealthInfo = $this->mHealthClock->getUserLatestHealthInfo($uid);
            if(!$latestHealthInfo){
                $redirectPage = $this->buildHealthPageUrl('healthResult');
                $this->assign('redirectPage',$redirectPage);
                $this->show('./health_clock/health_info.tpl');
            }else{
                $latestHealthInfo['bmi'] = $latestHealthInfo['bmi'] / 100;
                $this->assign('healthInfo',$latestHealthInfo);
                $this->getWeightPlan($latestHealthInfo['bmi']);
                $this->show('./health_clock/health_result.tpl');
            }
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }


    /**
     * 奖品列表
     */
    public function exchange(){
        try{
            global $config;
            $yearMonth = date('Y-m');
            $openDay = $config->exchange_open_day;
            $exchangeOpenDay = $yearMonth.'-'.$openDay;
            $exchangeOpenTime = $config->exchange_open_time;
            $today = date('Y-m-d');
            $currentTime = time();
            $openTime = $exchangeOpenDay . " " . $exchangeOpenTime;
            $this->assign('openDay',$openDay);

            $openDayTimestamp = strtotime($openTime);
            //限购日期
            if($today != $exchangeOpenDay){
                //日期小于当天
//                if($currentTime < $openDayTimestamp){
//                    $this->show('./health_clock/no_activity.tpl');
//                    die(0);
//                }else{
//                    //日期大于设置的时间，则提示活动已经结束
//                    $this->show('./health_clock/exchange_close.tpl');
//                    die(0);
//                }

                $this->show('./health_clock/no_activity.tpl');
                die(0);

            }
            //为开奖的日期，小于当天设置的时间点，还是显示未开始
            if($currentTime < $openDayTimestamp){
                $this->show('./health_clock/no_activity.tpl');
                die(0);
            }
            $uid = $this->pCookie(self::LOGIN_KEY);
            error_log('exchange method current uid is ============>'.$uid);
            $userInfo = $this->mHealthClock->getPartnerUserInfo($uid);
            //显示礼品详情页
            $prizeList = $this->mHealthClock->getAvailablePrizeList($uid,$userInfo);
            $this->assign('prizeList',$prizeList);
            $this->assign('showMore',$userInfo['cust_no']?0:1);
            $bindUrl =$config->zy_callback_api_domain.$config->zy_hch_user_bind_path;
            $this->assign('bindUrl',$bindUrl);
            $this->show('./health_clock/prize_list.tpl');

        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }


    }

    /**
     * 录入体重信息
     */
    public function doInputHealthInfo($Q){
        try{
            $height = $Q->height;
            if(!is_numeric($height)){
                $this->apiFail('height is invalid');
                die(0);
            }
            $weight = $Q->weight;
            if(!is_numeric($weight)){
                $this->apiFail('weight is invalid');
                die(0);
            }
            $targetWeight = $Q->target_weight;
            if(!is_numeric($targetWeight)){
                $this->apiFail('target_weight is invalid');
                die(0);
            }
            $sex = $Q->sex;
            if(!is_numeric($sex)){
                $this->apiFail('sex is invalid');
                die(0);
            }
            $validSexValues = [1,2];
            if(!in_array($sex,$validSexValues)){
                $this->apiFail('sex is invalid');
                die(0);
            }
            $uid = $this->pCookie(self::LOGIN_KEY);
            $result = $this->mHealthClock->doInputHealthInfo($uid,$height,$weight,$targetWeight,$sex);
            if($result > 0){
                $this->apiSuccess();
            }else{
                $this->apiFail('系统错误');
            }

        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }


    /**
     * 健康信息结果页面
     */
    public function healthResult(){
        try{
            $uid = $this->pCookie(self::LOGIN_KEY);
            $latestHealthInfo = $this->mHealthClock->getUserLatestHealthInfo($uid);
            if(!$latestHealthInfo){
                $this->show('./health_clock/error.tpl');
                die(0);
            }
            $latestHealthInfo['bmi'] = $latestHealthInfo['bmi'] / 100;
            $this->getWeightPlan($latestHealthInfo['bmi']);
            $this->assign('healthInfo',$latestHealthInfo);
            $startPage = $this->pCookie(self::PAGE_HIGH_LEVEL_KEY);
            $this->assign('startPage',$startPage);
            $this->show('./health_clock/health_result.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }


    /**
     * 高级打卡请求
     */
    public function doHighLevelPunch(){
        try{
            $uid = $this->pCookie(self::LOGIN_KEY);
            $uid = $uid ? $uid : 1;
            $result = $this->mHealthClock->doHighLevelPunch($uid);
            if($result > 0){
                $this->apiSuccess();
            }else{
                $this->apiFail('系统错误，请联系管理员');
            }
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }

    /**
     * 初级打卡请求
     */
    public function doLowLevelPunch($Q){
        try{
            $spendTime = $Q->spend_time;
            if(!is_numeric($spendTime) || $spendTime <= 0){
                $this->apiFail('spend time invalid');
                die(0);
            }
            $challengeIndex = $Q->challenge_index;
            if(!is_numeric($spendTime) || $spendTime <= 0){
                $this->apiFail('spend time invalid');
                die(0);
            }
            $questionData = $Q->question_data;
            if(!$questionData){
                $this->apiFail('question_data invalid');
                die(0);
            }
            if($questionDataObj = json_decode($questionData,true)){
                $this->apiFail('question_data is not  json ');
                die(0);
            }
            $uid = $this->pCookie(self::LOGIN_KEY);
            $uid = $uid ? $uid : 1;
            $result = $this->mHealthClock->doLowLevelPunch($uid,$challengeIndex,$spendTime,$questionData);
            if($result['id'] > 0){
                $obj['status'] = $result['status'];
                $this->apiSuccess($obj);
            }else{
                $this->apiFail('系统错误，请联系管理员');
            }
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }


    /**
     * 分享的链接action
     */
    public function share($Q){
        try{
            $comeFrom = $Q->come_from;
            $page = $Q->page;

            global $config;
            $validComefromValues  = $config->fruit_punch_user_come_from;
            if(!in_array($comeFrom,$validComefromValues)){
                error_log("share method param come is error");
                $this->show('./health_clock/error.tpl');
                die(0);
            }

            $validPages = ['home','highLevel','lowLevel'];
            if(!in_array($page,$validPages)){
                error_log("share method param page is error");
                $this->show('./health_clock/error.tpl');
                die(0);
            }


            if($comeFrom == 'qiezi'){
                $this->loadModel('User');
                $token = $config->stable_token;
                $openid = $this->getOpenId();
                // 微信自动注册
                $this->User->wechatAutoReg($openid);
                $uinfo = $this->User->getUserInfo($openid);
                $partnerUid = $uinfo['client_id'];
                $partnerUname = $uinfo['client_nickname'];
                $paramObj = [
                    'uid' => $partnerUid,
                    'uname' => $partnerUname,
                    'come_from' => $comeFrom,
                    'token' => $token,
                    'sign' => $this->mHealthClock->generateSign($partnerUid,$partnerUname,$token,$comeFrom)
                ];
                $param = http_build_query($paramObj);
                $url = $this->domain.'vHealthClock/'.$page."/".$param;

            }else if($comeFrom =='zy_group'){
                $paramObj = [
                    'come_from' => $comeFrom,
                    'page' => $page,
                ];
                $param = http_build_query($paramObj);
                $url = $config->zy_share_url . $param;
            }
            //跳转到对应的页面中
            header("location:" . $url);
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }

    /**
     * 分页加载记录数据
     */
    public function ajaxGetRecordList($Q){
        try{
            $lastId = $Q->last_id;
            if(!is_numeric($lastId) || $lastId < 0){
                $this->apiFail('lastid invalid');
                die(0);
            }
//        if(!$uid = $Q->uid){
//            $this->apiFail('uid invalid');
//            die(0);
//        }
            $type = $Q->type;
            $validTypes = ['high','low'];
            if(!in_array($type,$validTypes)){
                $this->apiFail('type invalid');
                die(0);
            }
            $uid = $this->pCookie(self::LOGIN_KEY);
            $uid = $uid ? $uid : 1;
            $isHighLevel = $type == 'high' ? true :false;
            $data = $this->mHealthClock->getPunchRecordPageData($uid,$lastId,$isHighLevel);
            $this->apiSuccess($data);

        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }

    /**
     * 生成加密的秘钥
     */
    public function generateEncodeKey(){
        $this->loadModel('mHealthClock');
        $key = $this->mHealthClock->generateUrlEncodeKey();
        $this->apiSuccess($key);
    }

    public function ajaxGetPrizeNote($Q) {
        global $config;
        if ($Q->id > 0) {
            $prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($Q->id);
            $content = $prizeInfo ? $prizeInfo['note'] : '';
            echo $content;
        }
    }

    public function generateSign($Q){
        if(!$partnerUid = $Q->uid){
            $this->apiFail('param uid invalid');
            die(0);
        }
        if(!$partnerUname = $Q->uname){
            $this->apiFail('param uname invalid');
            die(0);
        }
        if(!$token = $Q->token){
            $this->apiFail('param token invalid');
            die(0);
        }
        $comeFrom = $Q->come_from;
        if(!$comeFrom){
            $this->apiFail('param come_from invalid');
            die(0);
        }
        $sign = $this->mHealthClock->generateSign($partnerUid,$partnerUname,$token,$comeFrom);
        return $this->apiSuccess($sign);
    }


    public function generateVisitAddr($Q){
        if(!$partnerUid = $Q->uid){
            $this->apiFail('param uid invalid');
            die(0);
        }
        if(!$partnerUname = $Q->uname){
            $this->apiFail('param uname invalid');
            die(0);
        }
        if(!$token = $Q->token){
            $this->apiFail('param token invalid');
            die(0);
        }
        $comeFrom = $Q->come_from;
        if(!$comeFrom){
            $this->apiFail('param come_from invalid');
            die(0);
        }
        $tokenResult = TokenHelper::validateTokenFromApi($token);
        if($tokenResult['errcode'] != 200){
            $this->apiFail('param token  invalid');
            die(0);
        }
        $paramObj = [
            'uid' => $partnerUid,
            'uname' => urldecode($partnerUname),
            'token' => $token,
            'come_from' => $comeFrom,
            'sign' => $this->mHealthClock->generateSign($partnerUid,$partnerUname,$token,$comeFrom)
        ];
        $param = http_build_query($paramObj);
        $urlPaths = ['home','lowLevel','highLevel'];
        $apiPath = $this->domain.'?/vHealthClock/';
        $urls = [];
        foreach ($urlPaths as $path){
            $url = $apiPath . $path . '/'.$param;
            array_push($urls,$url);
        }
        return $this->apiSuccess($urls);
    }

    /**
     * 中英的中断提醒，为每日中断定时异步任务的接口方法
     */
    public function sendInterruptTplMsg($Q){
        if(!$openid = $Q->uid){
            $this->apiFail('uid invalid');
            die(0);
        }
        $isHighLevel = $Q->is_high_level;
        if(!is_numeric($isHighLevel) || !in_array($isHighLevel,[0,1])){
            $this->apiFail('is_high_level invalid');
            die(0);
        }
        $dayIndex = $Q->day_index;
        if(!is_numeric($dayIndex)){
            $this->apiFail('day_index invalid');
            die(0);
        }
        $result = $this->mHealthClock->sendInterruptTplMsgToZy($openid,$dayIndex,false);
        $this->apiSuccess($result);
    }

    /**
     * 中英的完成打卡提醒
     */
    public function sendFinishRemindTplMsg($Q){
        if(!$openid = $Q->uid){
            $this->apiFail('uid invalid');
            die(0);
        }
        if(!$userName = $Q->user_name){
            $this->apiFail('user_name invalid');
            die(0);
        }
        $isHighLevel = $Q->is_high_level;
        if(!is_numeric($isHighLevel) || !in_array($isHighLevel,[0,1])){
            $this->apiFail('is_high_level invalid');
            die(0);
        }
        $result = $this->mHealthClock->sendFinishRemindTplMsgToZy($openid,$userName,$isHighLevel);
        $this->apiSuccess($result);
    }


    /**
     * 中英的奖品开抢提醒
     */
    public function sendAwardRemindTplMsg($Q){
        global $config;
        if(!$openid = $Q->uid){
            $this->apiFail('uid invalid');
            die(0);
        }
        $isHighLevel = $Q->is_high_level;
        if(!is_numeric($isHighLevel) || !in_array($isHighLevel,[0,1])){
            $this->apiFail('is_high_level invalid');
            die(0);
        }
//        $openid = $config->zy_tpl_msg_test_openid;
        $isHighLevel = $isHighLevel ? true : false;
        $result =  $this->mHealthClock->sendAwardRemindTplMsgToZy($openid,$isHighLevel);
        $this->apiSuccess($result);
    }


    /**
     * 发送中断的模板消息定时任务,这个定时任务要作两个链接，一个是高级打卡的中断，一个是初级打卡的中断
     * 高级打卡的： /?/vHealthClock/sendInterruptMsgCrondJob/is_high_level=1
     * 初级打卡的：/?/vHealthClock/sendInterruptMsgCrondJob/is_high_level=0
     */
    public function sendInterruptMsgCrondJob($Q){
        $isHighLevel = $Q->is_high_level;
        if(!is_numeric($isHighLevel) || !in_array($isHighLevel,[0,1])){
            $this->apiFail('is_high_level invalid');
            die(0);
        }
        $result = $this->mHealthClock->sendInterruptMsgCronJob($isHighLevel);
        $this->apiSuccess($result);
    }


    public function sendPrizeData(){
        global $config;
        $uid = $config->zy_tpl_msg_test_openid;
        $prizeName = 'FIT365智能手环';
        $prizeGetTime = time();
        $validTime = null;
        $result = ZyHealthClockHelper::sendExchangeData($uid, $prizeName, $prizeGetTime, $validTime);
        return $this->apiSuccess($result);
    }
    /**
     * 发送抢请领奖的定时任务
     */
    public function sendAwardMsgCrondJob(){
        $result = $this->mHealthClock->sendAwardMsgCrondJob();
        $this->apiSuccess($result);
    }

    public function getQualificationUserList(){
        $result = $this->mHealthClock->getQualificationUserList();
        $this->apiSuccess($result);
    }


    public function exchangeOrder($Q){
        try{
            if(!$prizeId = $Q->prize_id){
                $this->show('./health_clock/error.tpl');
                die(0);
            }
            if(!$prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($prizeId)){
                error_log('prize_id is invalid');
                $this->show('./health_clock/error.tpl');
                die(0);
            }
            $uid = $this->pCookie(self::LOGIN_KEY);
            if(!$uid){
                $this->showExchangeFailPage('兑换失败，用户不存在');
            }

            //判断是否有权限
            if(!$this->mHealthClock->hasAwardPermission($uid)){
                $this->showExchangeFailPage('亲，您本期未完成打卡，请继续参与并完成打卡计划，即可参与下期的中英健康日活动');
            }

            //判断是否有权限
            if(!$this->mHealthClock->canExchange($uid)){
                $this->showExchangeFailPage('亲，您已领取过礼品，请继续参与并完成打卡计划，即可参与下期的中英健康日活动');
            }

            //创建订单
            if($orderId = $Q->order_id){
                $order = $this->mHealthClock->getOrderInfoById($orderId);
            }else{
                $order = $this->mHealthClock->createExchageOrder($uid,$prizeId);
            }

            $prizeList = [];
            array_push($prizeList,$prizeInfo);
            $address = $this->mHealthClock->getUserEnableAddr($uid);
            $this->assign('address',$address);
            $this->assign('prizeList',$prizeList);
            $this->assign('prizeInfo',$prizeInfo);
            $order['order_time'] = date('Y-m-d H:i',$order['order_time']);
            $this->assign('order',$order);

            global $config;
            $expressCity = $config->zy_hch_prize_express_city;
            $this->assign('expressCity',$expressCity);
            $this->show('./health_clock/exchage_order.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }


    }

    public function prizeDetail($Q){
        try{
            if(!$id = $Q->id){
                $this->showExchangeFailPage('奖品不存在');
            }

            if(!$prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($id)){
                $this->showExchangeFailPage('奖品不存在');
            }

            //判断是从预览来还是从
            $from = $Q->from;
            if($from != 'preview'){
                $uid = $this->pCookie(self::LOGIN_KEY);
                error_log('user id is ============>'.$uid);
                $userInfo = $this->mHealthClock->getPartnerUserInfo($uid);
                $isCustomer = $userInfo['cust_no'] ? 1 : 0;
                //面向的是用户还是客户，1表示面向中英用户，0表示面向的中英客户
                $isToUser = $prizeInfo['is_to_user'];
                //不是中英的客户，同时奖品面向的中英的用户
                if(!$isCustomer){
                    if(!$isToUser){
                        error_log('prize is oriented to customer');
                        $this->showExchangeFailPage('您无权查看该商品');
                    }
                }
                if(!$hasPermission = $this->mHealthClock->hasAwardPermission($uid)){
                    error_log('user is in blacklist or has no permission====》'.$uid);
                    $this->showExchangeFailPage('亲，您本期未完成打卡，请继续参与并完成打卡计划，即可参与下期的中英健康日活动');
                }

                //判断是否有权限
                if(!$this->mHealthClock->canExchange($uid)){
                    error_log('user has no enough times====》'.$uid);
                    $this->showExchangeFailPage('亲，您已领取过礼品，请继续参与并完成打卡计划，即可参与下期的中英健康日活动');
                }
                $this->assign('hasPermission',$hasPermission);

                //判断奖品是否有效，防止通过连接的形式访问已经过期的奖品
                $isPrizeValid = 0;
                $currentMonth = date('Ym');
                $prizeBelongTime = $prizeInfo['belong_time'];
                if($currentMonth == $prizeBelongTime){
                    global $config;
                    $yearMonth = date('Y-m');
                    $openDay = $config->exchange_open_day;
                    $exchangeOpenDay = $yearMonth.'-'.$openDay;
                    $exchangeOpenTime = $config->exchange_open_time;
                    $today = date('Y-m-d');
                    $currentTime = time();
                    $openTime = $exchangeOpenDay . " " . $exchangeOpenTime;
                    $openDayTimestamp = strtotime($openTime);
                    $currentDay = date('d');
                    if($currentDay == $openDay && $prizeInfo['stock'] > 0){
                        $isPrizeValid = 1;
                    }
                }
                $this->assign('isPrizeValid',$isPrizeValid);


//            $discountPrizeTypes = [2,3,4];
                $discountPrizeTypes = [4];
                $showImg = in_array($prizeInfo['prize_type'],$discountPrizeTypes) ? 0 : 1;
                $this->assign('showImg',$showImg);
                //初始化奖品队列，增加排队机制
                $this->mHealthClock->initPrizeQueueCache($id);
            }
            $this->assign('from',$from);
            $this->assign('prizeInfo',$prizeInfo);
            $this->show('./health_clock/prize_detail.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }


    public function submitExchange(){
        try{
            $uid = $this->pCookie(self::LOGIN_KEY);
            if(!$orderId = $this->pPost('order_id')){
                $msg = '抢购失败，订单编号不能为空';
                $this->cacheExchangeFailReason(false,'','',$msg);
                $this->apiFail($msg);
                die(0);
            }

            if(!$addressId = $this->pPost('address_id')){
                $msg = '抢购失败，地址不能为空';
                $this->cacheExchangeFailReason(false,'','',$msg);
                $this->apiFail($msg);
                die(0);
            }

            $msg = '';
            $orderStatus = '';
            if(!$prizeId = $this->pPost('prize_id')){
                $msg = '抢购失败，奖品不能为空';
                $this->cacheExchangeFailReason(false,'','',$msg);
                $this->apiFail($msg);
                die(0);
            }


            //礼品是否存在
            if(!$prizeInfo = $this->mHealthClock->getExchangePrizeInfoById($prizeId)){
                $msg = '抢购失败，奖品不存在';
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_NOT_EXISTS',$msg);
                $this->apiFail($msg);
                die(0);
            }
            //是否下线
            if(!$prizeInfo['is_online']){
                $msg = '抢购失败，该奖品已下线';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_OFF_LINE',$msg);
                die(0);
            }
            //是否已经删除
            if($prizeInfo['is_delete']){
                $msg = '抢购失败，该奖品已下线';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_WAS_DELETED',$msg);
                die(0);
            }

            //判断奖品是否为当月
            $currentMonth = date('Y').date('m');
            if($prizeInfo['belong_time'] != $currentMonth){
                $msg = '抢购失败，当前奖品无效';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_BELONG_TIME_ERR',$msg);
                die(0);
            }

            //验证是否是再黑名单上还是没有权限
            if(!$this->mHealthClock->hasAwardPermission($uid)){
                $msg = '亲，您本期未完成打卡，请继续参与并完成打卡计划，即可参与下期的中英健康日活动';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'HAS_NO_PRIVILLIGE',$msg);
                die(0);
            }

            //判断抢购次数是否足够
            if(!$this->mHealthClock->canExchange($uid)){
                $msg = '亲，您已领取过礼品，请继续参与并完成打卡计划，即可参与下期的中英健康日活动';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_EXCHANGE_LIMITED',$msg);
                die(0);
            }

            //验证成功之后是否为重复提交，用户有足够的兑换次数，且单个订单是成功的
            if($order = $this->mHealthClock->getOrderInfoById($orderId)){
                if($order['status'] == 'SUCC'){
                    $msg = '抢购失败，亲，请不要重复提交';
                    $this->apiFail($msg);
                    $this->cacheExchangeFailReason(false,'','',$msg);
                    die(0);
                }
            }


            //出队列
            if(!$topQueueValue = $this->mHealthClock->popPrizeQueue($prizeId)){
                $msg = '兑换失败，奖品已抢完';
                $this->apiFail($msg);
                $this->cacheExchangeFailReason(true,$orderId,'PRIZE_IS_SOLD_OUT',$msg);
                die(0);
            }

            if($result = $this->mHealthClock->submitExchange($uid,$prizeId,$orderId,$addressId,$topQueueValue)){
                $couponTypes = [2,3,4];
                if(in_array($prizeInfo['prize_type'],$couponTypes)){
                    $this->sCookie('is_coupon',1);
                }
                $this->apiSuccess();
            }else{
                $this->apiFail('系统错误，请联系管理员');
            }

        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }

    /**
     * 兑换失败的页面
     */
    public function exchangeFail(){
        $msg = $this->pCookie(self::EXCHANGE_FAIL_REASON);
        $msg = $msg ? $msg : '兑换失败';
        $this->assign('msg',$msg);
        $redirectUrl = $this->pCookie(self::PAGE_HOME_KEY);
        $this->assign('redirectUrl',$redirectUrl);
        $this->show('./health_clock/exchange_fail.tpl');
    }
    /**
     * 兑换失败的页面
     */
    public function exchangeSuccess(){
        global $config;
        $isCoupon = $this->pCookie('is_coupon');
        if($isCoupon){
            $redirectUrl = $this->pCookie(self::PAGE_HOME_KEY);
        }else{
            $redirectUrl = $config->zy_callback_api_domain.$config->zy_hch_user_center_path;
        }
//        $redirectUrl = $config->zy_callback_api_domain.$config->zy_hch_user_center_path;
        $this->assign('redirectUrl',$redirectUrl);
        $this->assign('isCoupon',$isCoupon);

        //清楚coupon的缓存
        $this->sCookie('is_coupon',null);
        $this->show('./health_clock/exchange_success.tpl');
    }

    public function selectAddress($Q){

        try{
            $uid = $this->pCookie(self::LOGIN_KEY);
            if(!$uid){
                error_log('user cookie  is expired');
                $this->show('./health_clock/error.tpl');
                die(0);
            }
            $orderId = $Q->order_id;
            $this->assign('orderId',$orderId);
            $prizeId = $Q->prize_id;
            $this->assign('prizeId',$prizeId);
            $addressList = $this->mHealthClock->getUserAddressList($uid);
            $this->assign('addressList',$addressList);
            $this->show('./health_clock/choose_address.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }
    }

    /**
     * 编辑地址
     */
    public function editAddress($Q){
        try{
            $orderId = $Q->order_id;
            $this->assign('orderId',$orderId);
            $prizeId = $Q->prize_id;
            $this->assign('prizeId',$prizeId);
            $this->show('./health_clock/edit_address.tpl');
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }

    /**
     * 更新默认的地址接口
     */
    public function updateEnableAddress(){
        try{
            if(!$uid = $this->pCookie(self::LOGIN_KEY)){
                $this->apiFail('用户登陆已失效');
                die(0);
            }
            $addressId = $this->pPost('id');
            $addressList = $this->mHealthClock->getUserAddressList($uid);
            $addressIds = array_column($addressList,'id');
            if(!in_array($addressId,$addressIds)){
                $this->apiFail('地址不正确');
                die(0);
            }
            $result = $this->mHealthClock->updateEnableAddress($uid,$addressId);
            if($result){
                $this->apiSuccess();
            }else{
                $this->apiFail('系统错误');
            }
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }

    /**
     * 保存地址
     */
    public function saveAddress(){
        try{
            $addressInfo = $this->validateUserAddressInfo();
            $id = $this->mHealthClock->insertUserAddress($addressInfo['uid'],$addressInfo['user_name'],$addressInfo['province'],$addressInfo['city'],$addressInfo['area'],$addressInfo['address'],$addressInfo['phone']);
            if($id > 0){
                $this->apiSuccess();
            }else{
                $this->apiFail('系统错误');
            }
        }catch (RedisException $re){
            $this->showRedisExp();
        }catch (Exception $e){
            $this->showSystemBusyErr();
        }

    }


    public function checkCustom($Q){
        if(!$openid = $Q->openid){
            $this->apiFail('openid invalid');
            die(0);
        }
        //返回
        $response = ZyHealthClockHelper::checkZyCustomer($openid);
        $this->apiSuccess($response);
    }

    public function getLimitedTime(){
        global $config;
        $openDay = $config->exchange_open_day;
        $currentDay = intval(date('d'));
        if($currentDay >= $openDay){
            $month = strtotime('+1 month');
            $month = date('m月',$month);
        }else{
            $month = date('m月');
        }
        $limitedTime = $month.$openDay.'日';
        echo $limitedTime;
    }



    /**
     * 获取分享的签名信息
     */
    protected function getShareSign(){
        $this->loadModel('JsSdk');
        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage', $signPackage);
    }




    /**
     * 验证基础参数的流程，同时包含注册的步骤等
     */
    protected function validateParamFlow($requestParam){
        global $config;
        $isUpgrade = $config->is_upgrade;
        if($isUpgrade){
            $this->show('./health_clock/upgrade.tpl');
            die(0);
        }

        if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }
        $partnerUid = $requestParam->uid;
        $partnerUname = $requestParam->uname;
        $token = $requestParam->token;
        $comeFrom = $requestParam->come_from;
        $sign = $requestParam->sign;
        if(!$partnerUid  ||  !$token ||  !$comeFrom || !$sign){
            error_log("param is error");
            $this->show('./health_clock/error.tpl');
            die(0);
        }


        $validComefromValues  = $config->fruit_punch_user_come_from;
        if(!in_array($comeFrom,$validComefromValues)){
            error_log("come_from is error");
            $this->show('./health_clock/error.tpl');
            die(0);
        }
        //第一重验证，验证token的合法性
        //验证token是否合法
        $tokenResult = TokenHelper::validateTokenFromApi($token);
        if($tokenResult['errcode'] != 200){
            error_log("token is error");
            $this->show('./health_clock/error.tpl');
            die(0);
        }
        $partnerUname = $this->formatPartnerName($partnerUname);
        //验证签名
        $serverSign = $this->mHealthClock->generateSign($partnerUid,$partnerUname,$token,$comeFrom);
        error_log('server sign===============>'.$serverSign);
        error_log('client sign===============>'.$sign);
        if($sign != $serverSign){
            error_log("sign is error");
            $this->show('./health_clock/error.tpl');
            die(0);
        }

        //来源于茄子平台，直接走授权流程得到用户id和昵称
//        if($comeFrom == $validComefromValues[0]){
//            $this->loadModel('User');
//            // get openid
//            $openid = $this->getOpenId();
//            // 微信自动注册
//            $this->User->wechatAutoReg($openid);
//            $uinfo = $this->User->getUserInfo($openid);
//            $partnerUid = $uinfo['client_id'];
//            $partnerUname = $uinfo['client_nickname'];
//        }

        //自动判断注册
        $partnerUid = $this->mHealthClock->autoRegPartnerUser($partnerUid,$partnerUname,$comeFrom);
        $this->sCookie(self::LOGIN_KEY,$partnerUid);
        return [
            'uid' => $partnerUid,
            'uname' => $partnerUname,
        ];

    }


    protected function buildLowLevelParam($Q){
        //构建访问地址
        $lowLevelPage = $this->buildParamPageUrl($Q,'lowLevel');
        $this->sCookie(self::PAGE_LOW_LEVEL_KEY,$lowLevelPage);
        //构建访问地址
        $indexPage = $this->buildParamPageUrl($Q,'home');
        $this->sCookie(self::PAGE_HOME_KEY,$indexPage);

        $resultPage = $this->buildHealthPageUrl('lowLevelResult');
        $this->assign('resultPage',$resultPage);
        $homePage = $this->buildParamPageUrl($Q,'home');
        $this->assign('homePage',$homePage);
        //分享的页面
        $sharePage = $this->buildSharePage($Q,'lowLevel');
        $this->assign('sharePage',$sharePage);
        $shareInfo = $this->getShareInfo('lowLevel',$Q->come_from);
        $this->assign('shareInfo',$shareInfo);
    }

    /**
     * 构建页面访问的url
     */
    protected function buildParamPageUrl($Q, $page = 'home'){
        $partnerUid = $Q->uid;
        $partnerUname = $Q->uname;
        $token = $Q->token;
        $comeFrom = $Q->come_from;
        $sign = $Q->sign;
        $paramObj = [
            'uid' => $partnerUid,
            'uname' => urldecode($partnerUname),
            'token' => $token,
            'come_from' => $comeFrom,
            'sign' => $sign
        ];
        $param = http_build_query($paramObj);
        $url = $this->domain.'?/vHealthClock/'.$page.'/'.$param;
        return $url;

    }

    /**
     * 构建健康打卡的页面
     */
    protected function buildHealthPageUrl($page){
        $url = $this->domain.'?/vHealthClock/'.$page;
        return $url;
    }

    /**
     * 构建分享的url
     */
    protected function buildSharePage($requestParam,$page){
        $comeFrom = $requestParam->come_from;
        $paramObj = [
            'come_from' => $comeFrom,
            'page' => $page
        ];
        $param = http_build_query($paramObj);
        $url = $this->domain.'?/vHealthClock/share/'.$param;
        return $url;
    }


    protected function formatPartnerName($partnerName){
        return str_replace('%25','%',$partnerName);
    }


    /**
     * 获取分享的信息
     */
    protected function getShareInfo($page,$comeFrom='qiezi'){
        global $config;
        $shareInfoObj = $config->zy_health_clock_share_info;
        $shareInfo = $shareInfoObj[$page];
//        if($comeFrom == 'qiezi'){
            $url = $this->domain.$config->qiezi_share_url;
//        }else{
//            $url = $config->zy_share_url;
//        }
        $shareInfo['link'] = $url.sprintf($shareInfo['link'],$comeFrom);
        $shareInfo['icon'] = $this->domain.$shareInfo['icon'];
        error_log('share info==========>');
        error_log(json_encode($shareInfo));
        return $shareInfo;
    }

    protected function getWeightPlan($bmi){
        if($bmi < 18.5){
            $plan = "您的体重有些偏瘦，建议您执行增肌方案。";
        }else if($bmi >=18.5 && $bmi <= 24){
            $plan = "您的身材非常完美，建议您执行正常方案。";
        }else if($bmi >= 24){
            $plan = "您的体重已经超标，建议您执行减重方案。";
        }
        $this->assign('plan',$plan);
    }

    protected function getLowLevelFinishStatus($userInfo =  null){
        $uid = $this->pCookie(self::LOGIN_KEY);
        $userInfo = $userInfo ? $userInfo : $this->mHealthClock->getPartnerUserInfo($uid);
        $punchTimes = $userInfo['low_level_times'];
        $reminder = $punchTimes % mHealthClock::LOW_LEVEL_LAST_DAYS;
        $finishStatus = ($punchTimes > 0 && $reminder == 0 ) ? 1:0;
        if($finishStatus){
            $this->assign('finishStatus',1);
        }else{
            $lastDay = mHealthClock::LOW_LEVEL_LAST_DAYS;
            $diffDay = $lastDay - $reminder;
            $this->assign('diffDay',$diffDay);
        }
    }

    /**
     * 验证地址信息
     */
    protected function validateUserAddressInfo(){
        if(!$province = $_POST['province']){
            $this->apiFail('省份不能为空');
            die(0);
        }
        if(!$city = $_POST['city']){
            $this->apiFail('城市不能为空');
            die(0);
        }
        if(!$area = $_POST['area']){
            $this->apiFail('区域不能为空');
            die(0);
        }
        if(!$address = $_POST['address']){
            $this->apiFail('姓名不能为空');
            die(0);
        }
        if(!$userName = $_POST['user_name']){
            $this->apiFail('姓名不能为空');
            die(0);
        }
        if(!$phone = $_POST['phone']){
            $this->apiFail('电话号码不能为空');
            die(0);
        }
        $uid = $this->pCookie(self::LOGIN_KEY);
        $data = [
            'uid' =>$uid,
            'province' =>$province,
            'city' =>$city,
            'area' =>$area,
            'address' =>$address,
            'user_name' =>$userName,
            'phone' =>$phone,
        ];
        return $data;
    }


    /**
     * 缓存兑换失败的原因
     */
    protected function cacheExchangeFailReason($updateOrder = false , $orderId = '',$orderStatus = '',$msg = ''){
        if($updateOrder){
            $failReason['order_status'] = $orderStatus;
            $failReason['msg'] = $msg;
            $value = json_encode($failReason);
            $data = [
                'fail_reason' => $msg,
                'status' => $orderStatus,
                'update_time' => time(),
            ];
            //更改订单的状态
            $updateData = $this->mHealthClock->updateOrderInfoByOrderId($orderId,$data);
        }
        $this->sCookie(self::EXCHANGE_FAIL_REASON,$msg);
    }

    /**
     * 显示兑换失败页面
     */
    protected function showExchangeFailPage($msg){
        $this->assign('msg',$msg);
        $redirectUrl = $this->pCookie(self::PAGE_HOME_KEY);
        $this->assign('redirectUrl',$redirectUrl);
        $this->show('./health_clock/exchange_fail.tpl');
        die(0);
    }

    /**
     * 显示redis异常
     */
    public function showRedisExp(){
        $this->show('./health_clock/too_mange_people.tpl');
        die(0);
    }

    /**
     * 显示redis异常
     */
    public function showSystemBusyErr(){
        $this->show('./health_clock/system_busy.tpl');
        die(0);
    }

}
