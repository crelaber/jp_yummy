<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author       <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class vHealthPackage extends Controller {

    private $login_key = 'login_uid';
    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
    }


    /**
     * For Produce
     * 套餐详情页接口
     */
    public function detail($Q){

        if(!Controller::inWechat() && !$this->debug){
            $this->show('./index/error.tpl');
            die(0);
        }

        $partnerUid = $Q->uid;
        $partnerUname = $Q->uname;
        $token = $Q->token;
        $package = $Q->package;
        $sign = $Q->sign;
        $validPackages = array(111,300,500,800,1000);
        //停止服务
        $package = in_array($package,$validPackages) ? $package : "";
        if(!$partnerUid || !$partnerUname ||  !$token ||  !$package || !$sign){
            error_log("param is error");
            $this->show('./health/error.tpl');
            die(0);
        }
        error_log("no encode partnerUname is =====>$partnerUname");
        error_log("token is =====>$token");
        error_log("other param is =====>$partnerUid,$partnerUname,$package,$sign");
        error_log("url decode token is =====>".urldecode($token));

        error_log("encode partnerUname is =====>".urldecode($partnerUname));
        global $config;
        $package_mapping = $config->package_mappings;
        $valid_package_type = array_keys($package_mapping);
        if(!in_array($package,$valid_package_type)){
            error_log("package is error");
            $this->show('./health/error.tpl');
            die(0);
        }
        //第一重验证，验证token的合法性
        //验证token是否合法
        $tokenResult = TokenHelper::validateTokenFromApi($token);
        if($tokenResult['errcode'] != 200){
            error_log("token is error");
            $this->show('./health/error.tpl');
            die(0);
        }

        //第二重验证，验证签名的合法性
        $packageSign = TokenHelper::generateHealthPackageDetailSign($token,$partnerUid,$partnerUname,$package);
        error_log("current package sign is  =====>$packageSign");
        if($sign != $packageSign){
            error_log("sign is error,input====>".$sign.",server sign is =====>".$packageSign);
            $this->show('./health/error.tpl');
            die(0);
        }

        //手动的去请求下openid
        $openid = $this->getOpenId();

        //自动注册用户
        $this->loadModel('mHealthPackage');
        if(!$partnerUser = $this->mHealthPackage->getPartnerUserByPartnerUid($partnerUid)){
            $appid = $config->zy_appid;
            $openid = $this->getOpenId();
            $fromPlatform = 'zy_group';
            error_log('openid====>'.$openid);
            $this->loadModel('User');
            $this->User->wechatAutoReg($openid,'',$fromPlatform,urldecode($partnerUname));
            $user = $this->User->getUserInfo($openid);
            error_log('user========>'.json_encode($user));
            $uid = $user['uid'];
            //自动注册partner_id
            $this->mHealthPackage->autoRegPartnerUser($uid,$partnerUid,$partnerUname,$fromPlatform,$appid );
        }else{
            $uid = $partnerUser['uid'];
        }

        //缓存用户信息到cookie中
//        $this->sCookie($this->login_key,$uid);
        $this->sCookie($this->login_key,$partnerUid);

        $package_map = $package_mapping[$package];
        $package_id = $package_map['packege_id'];

        $item_img_ids = [1,2,3,4];
        //套餐信息
        if($package_info = $this->mHealthPackage->get_package_info_by_id($package_id)){
            $package_item_img_url = $config->health_package_item_url;
            $base_url = $package_item_img_url . $package . '/';
            foreach ($item_img_ids as $id){
                $key = 'item_img_'.$id;
                $package_info[$key] = $base_url .$id .'.jpg';
            }

            $this->assign('package_info',$package_info);
        }
        $excludeCitys = $package_map['exclude_city']?$package_map['exclude_city']:[];
        $citys = $this->mHealthPackage->get_city_list($excludeCitys);
        $this->assign('citys',implode(',',$citys));

        $this->assign('package',$package);

        $this->loadModel('JsSdk');
        $signPackage = $this->JsSdk->GetSignPackage();
        $this->assign('signPackage',$signPackage);

        $this->show('./health/health_page.tpl');

    }


    /**
     * For Produce
     * 提交订单
     */
    public function submitOrder(){
        if(!Controller::inWechat() && !$this->debug){
            $this->apiFail("请在微信中操作");
            die(0);
        }
        global $config;

        $city = $this->pPost('city');
        if(!$city){
            $this->apiFail("请选择体检城市");
            die(0);
        }

        $package = $this->pPost('package');
        $package_mapping = $config->package_mappings;
        $valid_package_type = array_keys($package_mapping);
        if(!in_array($package,$valid_package_type)){
            $this->apiFail("套餐参数错误");
            die(0);
        }


        $quanlity = $this->pPost('quanlity');
        if($quanlity <= 0){
            $this->apiFail("请选择购买的套餐数量");
            die(0);
        }
        $sexArr = [1,2];
        $sex = $this->pPost('user_sex');
        if(!in_array($sex,$sexArr)){
            $this->apiFail("请选择性别");
            die(0);
        }
        $marryStatusArr = [0,1];
        $isMarried = $this->pPost('user_married_status');
        if(!in_array($isMarried,$marryStatusArr)){
            $this->apiFail("请选择婚姻状态");
            die(0);
        }
        $phone = $this->pPost('phone');
        if(!$phone){
            $this->apiFail("请输入正确的手机号码");
            die(0);
        }
        $code = $this->pPost('code');
        $sessionCode = $this->pCookie($phone);
        if(!$code || $code != $sessionCode){
            $this->apiFail("请输入正确的验证码");
            die(0);
        }
        //清除短信验证码，防止使用重复的验证码做校验
//        $this->sCookie($phone,null);

        $package_id = $this->pPost('package_id');
        $this->loadModel('mHealthPackage');
        if(!$package_info = $this->mHealthPackage->get_package_info_by_id($package_id)){
            $this->apiFail("套餐不存在");
            die(0);        }

        if(!$partnerUid = $this->pCookie($this->login_key)){
            $this->apiFail("用户身份不合法");
            die(0);
        }


        $partnerUser = $this->mHealthPackage->getPartnerUserByPartnerUid($partnerUid);
//        $partnerUser = $this->mHealthPackage->getPartnerUserByUid($uid);
        //组装订单所需要的数据
        $requestParam = $_POST;
        unset($requestParam['code']);
        unset($requestParam['package']);
        $package_map = $package_mapping[$package];
        $requestParam['total_amount'] = $quanlity * $package_info['sale_price'];
        //康康套餐编码
        $requestParam['package_code'] = $package_map['kangkang_package_code'];
        //套餐类型
        $requestParam['package_type'] = $package;
        $requestParam['uid'] = $partnerUser['uid'];
        $requestParam['partner_uid'] = $partnerUser['partner_uid'];
        $requestParam['partner_user_name'] = $partnerUser['partner_name'];
        $requestParam['order_time'] = time();
        $requestParam['pay_type'] = 1;
        error_log('requestParam=======>'.json_encode($requestParam));
        //获取openid
        $openid = $this->getOpenId();
        error_log('request open id =======>'.$openid);
        $result = $this->mHealthPackage->createOrder($requestParam,$config->zy_order_midfix,true,$openid);
        $data['biz'] = $result;
        $this->apiSuccess($data);
    }



    /**
     * For Produce
     * 支付成功的页面
     */
    public function paySuccess(){
        global $config;
        $rtnUrl = $config->health_package_pay_success_url;
        $this->assign('rtnUrl',$rtnUrl);
        $this->show('./health/success.tpl');
    }
    /**
     * For Produce
     * 支付失败的页面
     */
    public function payFail(){
        global $config;
        $rtnUrl = $config->health_package_pay_success_url;
        $this->assign('rtnUrl',$rtnUrl);
        $this->show('./health/fail.tpl');
    }


    /**
     * For Produce
     * 支付失败的页面
     */
    public function help(){
        $this->show('./health/help.tpl');
    }


    /**
     * For Produce
     * 发送短信验证码接口
     */
    public function ajaxSendCode(){
        $phone = $_POST['phone'];
        if(!TokenHelper::isMobile($phone)){
            $this->apiFail('手机号码不正确');
            die(0);
        }
        $url = $this->domain.'?/Sms/sendSmsVerfiyCode/mobile='.$phone;
        $response = HttpHelper::curlRequest($url);
        error_log('response-=====>'.$response);
        $result = json_decode($response,true);
        if($result['errcode'] == 200){
            $data = $result['data'];
            $code = $data['code'];
            $this->sCookie($phone,$code,60);
            $this->apiSuccess([],'验证码已发送');
        }else{
            $this->apiFail('验证码发送失败');
        }
    }


    /**
     * For Produce
     * 获取城市对应的体检中心接口
     */
    public function getHospital($Q){

        global $config;
        $package_mapings = $config->package_mappings;
        $city = $Q->city;
        if(!$city){
            $this->apiFail('city is invalid');
            die(0);
        }
        $city = HttpHelper::unicodeDecode(urldecode($city));
        $package_types = array_keys($package_mapings);
        $package = $Q->package;
        if(!$package || !in_array($package,$package_types) ){
            $this->apiFail('package is invalid');
            die(0);
        }

        $this->loadModel('mHealthPackage');
        $package_info = $package_mapings[$package];
        $exclude_list = $package_info['exclude_hospital'] ? $package_info['exclude_hospital'] : [];
        $hospitalList = $this->mHealthPackage->get_hospital_by_city($city, $exclude_list);
        $this->apiSuccess($hospitalList);

    }



    /**
     * For Test
     * 获取城市列表接口
     */
    public function getCityList(){
        $this->loadModel('mHealthPackage');
        $citys = $this->mHealthPackage->get_city_list();
        $this->apiSuccess(implode(',',$citys));
    }



    /**
     * For Test
     * 获取康康卡号的测试接口
     */
    public function getKKCard($Q){
        $orderId = time();
        $phone = '13412342345';
        $packageCode = 'TC201604081502500001';
        $sex = 1;
        $married = 1;
        $result = TokenHelper::getKKCardOrderResult($orderId,$phone,$packageCode,$sex,$married);
        $this->apiSuccess($result);
    }

    /*
     * For Produce
     * 请求kk的生成卡号的接口,因为康康限制了ip所以只能在服务器上通过http请求的方式进行调用，而不能直接调用model方法使用
     * http://cheerslife.dev.com/?/vHealthPackage/generateKKCard/orderId=100084&phone=13212342234&packageCode=TC20160481502500001&sex=1&married=1
     * 错误的返回
     * {
     *   errcode: -1,
     *   msg: "状态码：100013，非法请求，当前用户没有该套餐权限！",
     *   data: null
     * }
     *
     * 正确的返回
     * {
     *   errcode: 200,
     *   msg: "SUCCESS",
     *   data: {
     *       ip ：
     *       isKKPackage: 1,
     *       cardNum: "011980634",
     *       cardPwd: "729346",
     *       orderId: "93262",
     *       thirdNum: "100084",
     *       onlyNum: "100084"
     *   }
     *}
     */
    public function generateKKCard($Q){
        $orderId = $Q->orderId;
        if(!$orderId){
            $this->apiFail('orderId invalid');
            die(0);
        }
        $phone = $Q->phone;
        if(!$phone){
            $this->apiFail('phone invalid');
            die(0);
        }
        $packageCode = $Q->packageCode;
        if(!$packageCode){
            $this->apiFail('packageCode invalid');
            die(0);
        }
        $sex = $Q->sex;
        if(!$sex){
            $this->apiFail('sex invalid');
            die(0);
        }
        $married = $Q->married;
        $validMarriedValue = [0,1];
        if(!in_array($married,$validMarriedValue)){
            $this->apiFail('married invalid');
            die(0);
        }
        $result = TokenHelper::getKKCardOrderResult($orderId,$phone,$packageCode,$sex,$married);
        if($result['errcode'] == 200){
            $data = $result['data'];
            $data['ip'] = $_SERVER["REMOTE_ADDR"];
            $this->apiSuccess($data);
        }else{
            $this->apiFail($result['msg']);
        }
    }


    /**
     * For Test
     * 中英回调api测试接口
     */
    public function getZyApi(){
        $partnerUid = 'ogQulwMi4GzYFHdlSAruEeojDtJg';
        $time = time();
        $cardNo = 'KKTest003'.$time;
        $cardSecret = '123456890';
        $validTime = '2018-06-09';
        $packageName = '300元商务体检套餐';
        $result = TokenHelper::invokeZyApi($partnerUid, $cardNo, $cardSecret,$validTime,$packageName);
        echo json_encode($result);
    }

    /**
     * For Test
     * 清除cookie接口
     */
    public function clearCookie(){
        $this->sCookie('uid',null);
        echo 1;
    }

    /**
     * For Test
     * 获取订单序列号接口
     */
    public function getOutTradeNo($Q){
        global $config;
        $this->loadModel('mHealthPackage');
        $orderId = $Q->orderId;
        $outTradeNo = $this->mHealthPackage->generateOutTradeNo($orderId,$config->zy_order_midfix);
        echo $outTradeNo;
    }


    /**
     * For Test
     * 微信支付回调测试接口
     */
    public function testOrderPayNotify($Q){
        global $config;
        $package = $Q->package;
        $package_mapping = $config->package_mappings;
        $valid_package_type = array_keys($package_mapping);
        if(!in_array($package,$valid_package_type)){
            $this->apiFail("套餐参数错误");
            die(0);
        }


        $quanlity = $Q->quanlity;
        if($quanlity <= 0){
            $this->apiFail("请选择购买的套餐数量");
            die(0);
        }
        $sexArr = [1,2];
        $sex = $Q->user_sex;
        if(!in_array($sex,$sexArr)){
            $this->apiFail("请选择性别");
            die(0);
        }
        $marryStatusArr = [0,1];
        $isMarried = $Q->user_married_status;
        if(!in_array($isMarried,$marryStatusArr)){
            $this->apiFail("请选择婚姻状态");
            die(0);
        }
        $phone = $Q->phone;
        if(!$phone){
            $this->apiFail("请输入正确的手机号码");
            die(0);
        }
        $package_map = $package_mapping[$package];
        $package_id = $package_map['packege_id'];
        $this->loadModel('mHealthPackage');
        if(!$package_info = $this->mHealthPackage->get_package_info_by_id($package_id)){
            $this->apiFail("套餐不存在");
            die(0);
        }

        $uid = $Q->uid;

        $partnerUser = $this->mHealthPackage->getPartnerUserByUid($uid);
        //组装订单所需要的数据
        $requestParam = [];
        foreach ($Q as $key =>$val){
            $requestParam[$key] = $val;
        }
        unset($requestParam['code']);
        unset($requestParam['package']);
        $requestParam['package_id'] = $package_id;

        $requestParam['total_amount'] = $quanlity * $package_info['sale_price'];
        //康康套餐编码
        $requestParam['package_code'] = $package_map['kangkang_package_code'];
        //套餐类型
        $requestParam['package_type'] = $package;
        $requestParam['uid'] = $uid;
        $requestParam['partner_uid'] = $partnerUser['partner_uid'];
        $requestParam['partner_user_name'] = $partnerUser['partner_name'];
        $requestParam['order_time'] = time();
        $requestParam['pay_type'] = 1;
        error_log('requestParam=======>'.json_encode($requestParam));
        $isPrduce = false;
        $result = $this->mHealthPackage->createOrder($requestParam,$config->zy_order_midfix,$isPrduce);
        $outTradeNo = $result['outTradeNo'];
        $wxPostObj = [];
        $wxPostObj->transaction_id = time()+$result['orderId'];
        $this->mHealthPackage->wxPayNotify($outTradeNo,$wxPostObj,$isPrduce);
    }


    /**
     * For Test
     * 获取详情页的参数签名接口
     */
    public function getPackageSign($Q){
        $partnerUid = $Q->uid;
        $partnerUname = $Q->uname;
        $token = $Q->token;
        $package = $Q->package;
        if(!$partnerUid || !$partnerUname ||  !$token ||  !$package){
            $this->apiFail('参数错误');
            die(0);
        }


        $packageSign = TokenHelper::generateHealthPackageDetailSign($token,$partnerUid,$partnerUname,$package);
        $this->apiSuccess($packageSign);


    }


    public function getUserPackageAddress($Q){
        $partnerUid = $Q->uid;
        $partnerUname = $Q->uname;
        if(!$partnerUid || !$partnerUname){
            $this->apiFail('参数错误');
            die(0);
        }
        global  $config;
        $token = $config->stable_token;
        $packageMapping = $config->package_mappings;
        $packageTypes = array_keys($packageMapping);

        $baseUrl = $this->domain .'?/vHealthPackage/detail/';
        $urls = [];
        foreach ($packageTypes as $package){
            $params = [
                'uid' => $partnerUid,
                'uname' => urldecode($partnerUname),
                'token' => $token,
                'package' => $package,
                'sign' => TokenHelper::generateHealthPackageDetailSign($token,$partnerUid,$partnerUname,$package)
            ];

            $paramStr = http_build_query($params);
            $urls[$package] =$baseUrl.$paramStr;
        }
        $this->apiSuccess($urls);
    }
}
