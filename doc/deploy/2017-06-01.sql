DROP TABLE IF EXISTS `partner_health_package_info`;
CREATE TABLE `partner_health_package_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '' COMMENT '套餐名称',
  `name_desc` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT '' COMMENT '套餐描述',
  `sale_price` int(8) DEFAULT '0' COMMENT '实际售价，单位为分',
  `market_price` int(8) DEFAULT '0' COMMENT '市场价，单位为分',
  `package_item_desc` varchar(100) DEFAULT '' COMMENT '套餐项目描述',
  `expired_time_desc` varchar(40) DEFAULT '' COMMENT '套餐有效期描述',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `partner_health_package_info` VALUES ('1', '300元套餐', '128个项目精英活力套餐', '针对心血管，糖尿病筛查', '30000', '62000', '128个项目', '有效期一年', '1496297640', '1496297640');
INSERT INTO `partner_health_package_info` VALUES ('2', '500元套餐', '130精英体检套餐', '针对心血管，糖尿病，内部筛查', '50000', '70000', '130个项目', '有效期一年', '1496297640', '1496297640');
INSERT INTO `partner_health_package_info` VALUES ('3', '800元套餐', '143个夕阳红套餐', '针对心血管，糖尿病，内部筛查', '80000', '100000', '143个项目', '有效期一年', '1496297640', '1496297640');
INSERT INTO `partner_health_package_info` VALUES ('4', '1000元套餐', '150个职业精英套餐', '针对心血管，糖尿病，等150个内项筛查', '100000', '120000', '160个项目', '有效期一年', '1496297640', '1496297640');

DROP TABLE IF EXISTS `partner_hp_hospital`;
CREATE TABLE `partner_hp_hospital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(15) DEFAULT '' COMMENT '体检省份',
  `city` varchar(30) DEFAULT '' COMMENT '体检城市',
  `operation_type` varchar(15) DEFAULT '' COMMENT '运营类型',
  `name` varchar(100) DEFAULT '' COMMENT '体检中心名称',
  `address` varchar(150) DEFAULT '' COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of partner_hp_hospital
-- ----------------------------
INSERT INTO `partner_hp_hospital` VALUES ('1', '广东', '广州', '自营', '爱康国宾广州环市东体检分院三层', '广州市越秀区环市东路496号广发花园大厦3层');
INSERT INTO `partner_hp_hospital` VALUES ('2', '广东', '广州', '自营', '爱康国宾广州花城大道南天广场体检分院五层', '天河区珠江新城花城大道1号5层');
INSERT INTO `partner_hp_hospital` VALUES ('3', '广东', '广州', '自营', '爱康国宾广州天河华港花园体检分院', '东方一路华港花园20-24号3层');
INSERT INTO `partner_hp_hospital` VALUES ('4', '广东', '广州', '自营', '爱康国宾广州环市东体检分院四层', '广州市越秀区环市东路496号广发花园大厦，四层');
INSERT INTO `partner_hp_hospital` VALUES ('5', '广东', '广州', '自营', '爱康国宾广州林和西中泰体检分院', '林和西路161号中泰国际广场5层');
INSERT INTO `partner_hp_hospital` VALUES ('6', '广东', '深圳', '自营', '爱康国宾深圳科技园科兴分院', '高新科技园中区科苑路科兴科学园B座3单元3A层');
INSERT INTO `partner_hp_hospital` VALUES ('7', '广东', '深圳', '自营', '爱康国宾深圳福田分院', '福田区滨河路北彩田路东交汇处联合广场B座裙楼B201、203');
INSERT INTO `partner_hp_hospital` VALUES ('8', '广东', '深圳', '自营', '爱康国宾深圳罗湖分院', '宝安南路3044号天地大厦一、三楼');
INSERT INTO `partner_hp_hospital` VALUES ('9', '广东', '深圳', '自营', '爱康国宾深圳华强分院', '福田区深南中路2008号华联大厦917室');
INSERT INTO `partner_hp_hospital` VALUES ('10', '北京', '北京', '自营', '爱康国宾北京亚运村慧忠北里分院', '朝阳区慧忠北里105楼B室段京师科技大厦第2楼');
INSERT INTO `partner_hp_hospital` VALUES ('11', '北京', '北京', '自营', '爱康国宾北京白石桥分院', '北京市海淀区中关村南大街甲32号中关村科技发展 大厦5层');
INSERT INTO `partner_hp_hospital` VALUES ('12', '北京', '北京', '自营', '爱康国宾北京建国门分院', '\n北京市朝阳区建华南路17号现代柏联大厦2层');
INSERT INTO `partner_hp_hospital` VALUES ('13', '北京', '北京', '自营', '爱康国宾北京磁器口分院', '\n北京市东城区珠市口东大街6号珍贝大厦2层\n');
INSERT INTO `partner_hp_hospital` VALUES ('14', '北京', '北京', '自营', '爱康国宾北京酒仙桥分院', '朝阳区南十里居28号东润枫景小区');
INSERT INTO `partner_hp_hospital` VALUES ('15', '北京', '北京', '自营', '爱康国宾北京中关村体检分院七层', '海淀北一街2号爱奇艺创新大厦');
INSERT INTO `partner_hp_hospital` VALUES ('16', '北京', '北京', '自营', '爱康国宾北京白云路分院', '北京市西城区莲花池东路甲5号院白云时代大厦1号楼二层');
INSERT INTO `partner_hp_hospital` VALUES ('17', '北京', '北京', '自营', '爱康国宾北京西直门分院5层', '西直门南大街2号成铭大厦D座 5层');
INSERT INTO `partner_hp_hospital` VALUES ('18', '北京', '北京', '自营', '爱康国宾北京宣武门分院', '\n北京市西城区上斜街32号');
INSERT INTO `partner_hp_hospital` VALUES ('19', '北京', '北京', '自营', '爱康国宾北京公主坟分院', '西三环中路19号国宜广场三层东侧及一层大厅');
INSERT INTO `partner_hp_hospital` VALUES ('20', '北京', '北京', '自营', '爱康国宾北京西直门分院4层', '西直门南大街2号成铭大厦D座 4层');
INSERT INTO `partner_hp_hospital` VALUES ('21', '北京', '北京', '自营', '爱康国宾北京空港体检中心', '\n顺平路南法信段9号院顺捷大厦A座2层203-204');
INSERT INTO `partner_hp_hospital` VALUES ('22', '北京', '北京', '自营', '爱康国宾北京郡王府分院', '朝阳公园南路21号');
INSERT INTO `partner_hp_hospital` VALUES ('23', '北京', '北京', '自营', '爱康国宾北京知春路分院', '北京市海淀区知春路甲48号盈都大厦D座3层3029号');
INSERT INTO `partner_hp_hospital` VALUES ('24', '北京', '北京', '自营', '爱康国宾北京总部基地分院', '北京市丰台区南四环西路总部基地一区31号楼(厦门银行旁)');
INSERT INTO `partner_hp_hospital` VALUES ('25', '北京', '北京', '自营', '爱康国宾北京安华桥分院', '北三环裕民中路12号(现代阳光大厦)');
INSERT INTO `partner_hp_hospital` VALUES ('26', '北京', '北京', '自营', '爱康国宾北京中关村体检分院九层', '海淀北一街2号爱奇艺创新大厦');
INSERT INTO `partner_hp_hospital` VALUES ('27', '北京', '北京', '自营', '爱康国宾北京丽都分院', '北京市朝阳区将台路丽都饭店西北角商业楼5号楼3楼');
INSERT INTO `partner_hp_hospital` VALUES ('28', '四川', '成都', '自营', '爱康国宾成都红照壁航天科技分院', '人民南路一段新光华街航天科技大厦5层');
INSERT INTO `partner_hp_hospital` VALUES ('29', '四川', '成都', '自营', '爱康国宾成都高新分院', '\n高新区天韵路150号高新国际广场D座三楼');
INSERT INTO `partner_hp_hospital` VALUES ('30', '四川', '成都', '自营', '爱康国宾成都外双楠分院', '天府大道中段天府三街19号新希望国际C座3层');
INSERT INTO `partner_hp_hospital` VALUES ('31', '四川', '成都', '自营', '爱康国宾成都骡马市分院', '\n四川省成都市青羊区青龙街18号-附14罗马国际大厦4楼');
INSERT INTO `partner_hp_hospital` VALUES ('32', '四川', '成都', '自营', '爱康国宾成都城南分院', '天府大道中段天府三街19号新希望国际C座3层');
INSERT INTO `partner_hp_hospital` VALUES ('33', '四川', '绵阳', '自营', '绵阳高新火炬广场分院', '四川省绵阳市高新区火炬北路附21号三汇金座4F');
INSERT INTO `partner_hp_hospital` VALUES ('34', '福建', '福州', '自营', '爱康国宾福州鼓楼分院', '六一北路328号金源花园三层');
INSERT INTO `partner_hp_hospital` VALUES ('35', '山东', '烟台', '自营', '爱康国宾烟台开发区长江路分院', '长江路97号国家羽毛球训练基地');
INSERT INTO `partner_hp_hospital` VALUES ('36', '山东', '烟台', '自营', '爱康国宾烟台莱山区港城东大街分院', '港城东大街588号第三城国际大厦副楼1-3层');
INSERT INTO `partner_hp_hospital` VALUES ('37', '山东', '烟台', '自营', '爱康国宾烟台芝罘区南大街分院', '芝罘区毓璜顶北路96号(沃尔玛超市北50米路西)');
INSERT INTO `partner_hp_hospital` VALUES ('38', '山东', '潍坊', '自营', '潍坊爱康国宾东方路分院', '东方路与卧龙东街交叉口东北角');
INSERT INTO `partner_hp_hospital` VALUES ('39', '山东', '潍坊', '自营', '潍坊爱康国宾新华路体检分院', '潍坊市奎文区民生东街88号');
INSERT INTO `partner_hp_hospital` VALUES ('40', '湖南', '长沙', '自营', '爱康国宾长沙新建西路阳光分院', '芙蓉中路三段509号中远公馆大厦1-3层');
INSERT INTO `partner_hp_hospital` VALUES ('41', '湖南', '长沙', '自营', '爱康国宾长沙八一桥华天酒店分院', '芙蓉中路593号潇湘华天大酒店六楼整层');
INSERT INTO `partner_hp_hospital` VALUES ('42', '河北', '石家庄', '美年', '美年大健康桥西分院，石家庄市桥西区友谊北大街75号', '石家庄市桥西区友谊北大街75号');
INSERT INTO `partner_hp_hospital` VALUES ('43', '江苏', '南京', '自营', '爱康国宾南京江宁分院', '双龙大道1118号金轮新都汇2层');
INSERT INTO `partner_hp_hospital` VALUES ('44', '江苏', '南京', '自营', '爱康国宾南京鼓楼分院', '鼓楼区中央路19号金峰大厦3层-3A层');
INSERT INTO `partner_hp_hospital` VALUES ('45', '江苏', '南京', '自营', '爱康国宾南京新街口分院', '中山东路145号全民健身中心19F');
INSERT INTO `partner_hp_hospital` VALUES ('46', '江苏', '无锡', '自营', '爱康国宾无锡新吴茂业分院', '无锡市江阴市滨江西路8号');
INSERT INTO `partner_hp_hospital` VALUES ('47', '辽宁', '沈阳', '自营', '爱康国宾沈阳青年大街分院', '\n沈河区青年大街166号');
INSERT INTO `partner_hp_hospital` VALUES ('48', '辽宁', '沈阳', '自营', '爱康国宾沈阳太原街分院', '和平区太原南街236号');
INSERT INTO `partner_hp_hospital` VALUES ('49', '湖北', '武汉', '自营', '武汉青年路市博物馆分院', '江汉区青年路373号市博物馆一楼4号门');
INSERT INTO `partner_hp_hospital` VALUES ('50', '河南', '郑州', '慈铭', '慈铭郑州分院，郑州郑东新区商鼎路康平路口向北500米路西', '郑州郑东新区商鼎路康平路口向北500米路西');
INSERT INTO `partner_hp_hospital` VALUES ('51', '黑龙江', '哈尔滨', '慈铭', '慈铭哈尔滨分院，哈尔滨道里区建国公园通达街451号', '哈尔滨道里区建国公园通达街451号');
INSERT INTO `partner_hp_hospital` VALUES ('52', '上海', '上海', '自营', '爱康国宾上海浦东八佰伴分院', '张杨路560号中融恒瑞大厦6楼西区');
INSERT INTO `partner_hp_hospital` VALUES ('53', '上海', '上海', '自营', '爱康国宾上海中山公园南延安西路分院', '定西路1018号宁电投大厦第2层');
INSERT INTO `partner_hp_hospital` VALUES ('54', '上海', '上海', '自营', '爱康国宾上海西藏南路老西门体检分院', '西藏南路768号安基大厦4层');
INSERT INTO `partner_hp_hospital` VALUES ('55', '上海', '上海', '自营', '爱康国宾上海曹家渡一品分院', '上海市静安区康定路1437号鑫康苑2层');
INSERT INTO `partner_hp_hospital` VALUES ('56', '上海', '上海', '自营', '爱康国宾上海五角场万达广场分院', '上海市杨浦区国宾路36号万达广场B座5层');
INSERT INTO `partner_hp_hospital` VALUES ('57', '上海', '上海', '自营', '爱康国宾上海中环一品分院', '普陀区上海市普陀区真光路1288号百联中环购物广场4层');
INSERT INTO `partner_hp_hospital` VALUES ('58', '上海', '上海', '自营', '上海元化门诊部', '长宁区天山路30号3楼（近哈密路）');
INSERT INTO `partner_hp_hospital` VALUES ('59', '上海', '上海', '自营', '爱康国宾上海外滩延安东路分院', '江西南路29号二楼');
INSERT INTO `partner_hp_hospital` VALUES ('60', '上海', '上海', '自营', '爱康国宾上海陆家嘴分院', '\n浦东区浦东大道2554号');
INSERT INTO `partner_hp_hospital` VALUES ('61', '安徽', '合肥', '慈铭', '慈铭合肥分院，安徽省合肥市高新区黄山路669号云栖苑综合楼', '安徽省合肥市高新区黄山路669号云栖苑综合楼');
INSERT INTO `partner_hp_hospital` VALUES ('62', '安徽', '合肥', '慈铭', '美年合肥分院，政务区潜山路与南二环交口新城国际大厦A座23层', '美年合肥分院，政务区潜山路与南二环交口新城国际大厦A座23层');
INSERT INTO `partner_hp_hospital` VALUES ('63', '山东', '济南', '爱康高端', '爱康卓悦济南纬二路山东商会大厦分院', '南纬二路51号山东商会大厦');
INSERT INTO `partner_hp_hospital` VALUES ('64', '山东', '济南', '慈铭', '明湖分院，济南市天桥区少年路2-16号（市少年宫对面）', '济南市天桥区少年路2-16号');
INSERT INTO `partner_hp_hospital` VALUES ('65', '山东', '济南', '慈铭', '奥体分院，济南市奥体中心中心商业区B6号', '济南市奥体中心中心商业区B6号');
INSERT INTO `partner_hp_hospital` VALUES ('66', '山东', '青岛', '爱康高端', '爱康卓悦青岛香格里拉分院', '香港中路9号');
INSERT INTO `partner_hp_hospital` VALUES ('67', '山东', '青岛', '慈铭', '青岛市南区燕儿路8号凯悦中心5F', '青岛市南区燕儿路8号凯悦中心5F');

-- ----------------------------
-- Table structure for partner_hp_order
-- ----------------------------
DROP TABLE IF EXISTS `partner_hp_order`;
CREATE TABLE `partner_hp_order` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `out_trade_no` varchar(50) DEFAULT '' COMMENT '订单序列号',
  `uid` int(10) DEFAULT '0' COMMENT '用户编号',
  `partner_uid` varchar(100) DEFAULT '' COMMENT '合作方用户编号',
  `partner_user_name` varchar(100) DEFAULT '' COMMENT '合作方用户名',
  `package_id` int(10) DEFAULT '0' COMMENT '对应的体检套餐编号，和套餐表关联凭证',
  `quanlity` int(6) DEFAULT '0' COMMENT '购买的数量',
  `total_amount` int(10) DEFAULT '0' COMMENT '总金额',
  `phone` char(11) DEFAULT '' COMMENT '购买的手机号码',
  `user_sex` tinyint(1) DEFAULT '1' COMMENT '性别，1为男，2位女',
  `user_married_status` tinyint(1) DEFAULT '0' COMMENT '婚姻状态，0为已婚，1为未婚',
  `package_code` varchar(30) DEFAULT '' COMMENT '康康的套餐编号',
  `package_type` varchar(15) DEFAULT '' COMMENT '套餐类型，值有300,500,800,1000四个',
  `city` varchar(30) DEFAULT '' COMMENT '体检城市',
  `bill_no` varchar(100) DEFAULT '' COMMENT '支付平台的订单流水号',
  `payment_source` varchar(20) DEFAULT 'UNKNOWN' COMMENT '支付来源，有WXPAY和ALIPAY两种',
  `pay_amount` int(10) DEFAULT '0' COMMENT '支付的金额，单位为分',
  `pay_type` tinyint(1) DEFAULT '1' COMMENT '支付的类型，1为微信支付，2位余额支付，3为微信加余额支付',
  `paid_time` int(10) DEFAULT '0' COMMENT '支付时间',
  `order_time` int(10) DEFAULT '0' COMMENT '下单时间',
  `cancel_time` int(10) DEFAULT '0' COMMENT '取消订单时间',
  `refund_time` int(10) DEFAULT '0' COMMENT '退款时间',
  `status` varchar(15) DEFAULT 'UNPAY' COMMENT '支付的状态，UNPAY:未支付,ONPAY:付款中,SUCC:付款成功,FAIL:付款失败,CANCEL:取消订单,REFUND:已退款',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for partner_hp_order_card
-- ----------------------------
DROP TABLE IF EXISTS `partner_hp_order_card`;
CREATE TABLE `partner_hp_order_card` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) DEFAULT '0' COMMENT '订单编号',
  `card_no` varchar(30) DEFAULT '' COMMENT '康康卡号',
  `card_secret` varchar(50) DEFAULT '' COMMENT '康康体检卡卡密',
  `uid` int(10) DEFAULT '0' COMMENT '用户编号',
  `partner_uid` varchar(100) DEFAULT '' COMMENT '合作方用户编号',
  `partner_user_name` varchar(100) DEFAULT '' COMMENT '合作方用户名称',
  `package_code` varchar(30) DEFAULT '' COMMENT '康康套餐编号',
  `package_type` varchar(8) DEFAULT '' COMMENT '套餐类型',
  `add_time` int(10) DEFAULT 0 COMMENT '添加时间',
  `remote_ip` int(10) DEFAULT  '' COMMENT '远程ip',
  `valid_time` int(10) DEFAULT 0 COMMENT '有效期',,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for partner_user
-- ----------------------------
DROP TABLE IF EXISTS `partner_user`;
CREATE TABLE `partner_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT '0' COMMENT '用户id，cheerslife平台',
  `partner_uid` varchar(150) DEFAULT '' COMMENT '第三方平台用户编号',
  `partner_name` varchar(50) DEFAULT '' COMMENT '第三方平台用户名字',
  `reg_time` int(10) DEFAULT '0' COMMENT '注册时间',
  `partner_short_name` varchar(15) DEFAULT NULL COMMENT '第三方平台名称缩写',
  `appid` varchar(50) DEFAULT NULL COMMENT '第三方平台appid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `partner_user`
	ADD UNIQUE KEY `idx_unique_uid`(`uid`,`partner_uid`) USING HASH;

ALTER TABLE `clients`
	ADD COLUMN `from_platform` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'qiezi' COMMENT '来源的平台' AFTER `all_account`;
