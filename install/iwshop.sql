/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : yummy

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-03-15 16:02:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for 20160308_gifts
-- ----------------------------
DROP TABLE IF EXISTS `20160308_gifts`;
CREATE TABLE `20160308_gifts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '获奖用户id',
  `wechat_openid` varchar(64) NOT NULL COMMENT '获奖用户微信id',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '获奖时间',
  `delivered` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否已经配送',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_account` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_permission` tinyint(2) DEFAULT '0',
  `admin_last_login` datetime DEFAULT NULL,
  `admin_ip_address` varchar(255) DEFAULT NULL,
  `admin_auth` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`admin_account`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for api_invoke_count
-- ----------------------------
DROP TABLE IF EXISTS `api_invoke_count`;
CREATE TABLE `api_invoke_count` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(25) DEFAULT '0' COMMENT '商户对应的uid',
  `merchant_no` char(10) DEFAULT '0' COMMENT '商户号',
  `intf_name` varchar(50) DEFAULT '' COMMENT '接口名称',
  `request_day` varchar(12) DEFAULT '' COMMENT '请求日期，格式为2016-01-01',
  `last_request_time` int(11) DEFAULT '0' COMMENT '最后请求时间',
  `request_count` int(8) DEFAULT '0' COMMENT '请求次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `client_id` int(25) NOT NULL AUTO_INCREMENT COMMENT '会员卡号',
  `client_nickname` varchar(512) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_name` varchar(512) COLLATE utf8mb4_bin NOT NULL COMMENT '会员姓名',
  `client_sex` varchar(1) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '会员性别',
  `client_phone` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '会员电话',
  `client_email` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_head` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_head_lastmod` datetime DEFAULT NULL,
  `client_password` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员密码',
  `client_level` tinyint(3) DEFAULT '0' COMMENT '会员种类1为普通会员0为合作商',
  `client_wechat_openid` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '会员微信openid',
  `client_joindate` date NOT NULL COMMENT '入会日期',
  `client_province` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_city` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_address` varchar(60) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员住址',
  `client_money` float(15,2) NOT NULL DEFAULT '0.00' COMMENT '会员存款',
  `client_credit` int(15) NOT NULL DEFAULT '0' COMMENT '会员积分',
  `client_remark` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '会员备注',
  `client_groupid` int(11) DEFAULT '0',
  `client_storeid` int(10) DEFAULT '0' COMMENT '会员所属店号',
  `client_personid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `client_comid` int(11) DEFAULT '0',
  `client_autoenvrec` tinyint(4) DEFAULT '0',
  `unionid` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_com` tinyint(4) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  `first_login` tinyint(1) DEFAULT '1' COMMENT '初次登录标识',
  `from_uid` int(11) unsigned DEFAULT '0' COMMENT '推荐人UID',
  PRIMARY KEY (`client_id`),
  KEY `index_openid` (`client_wechat_openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2686 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for clients_group
-- ----------------------------
DROP TABLE IF EXISTS `clients_group`;
CREATE TABLE `clients_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_autoenvs
-- ----------------------------
DROP TABLE IF EXISTS `client_autoenvs`;
CREATE TABLE `client_autoenvs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_credit_record
-- ----------------------------
DROP TABLE IF EXISTS `client_credit_record`;
CREATE TABLE `client_credit_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `amount` int(5) DEFAULT NULL,
  `dt` datetime DEFAULT NULL,
  `reltype` tinyint(2) DEFAULT NULL,
  `relid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_envelopes
-- ----------------------------
DROP TABLE IF EXISTS `client_envelopes`;
CREATE TABLE `client_envelopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `envid` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_envelopes_type
-- ----------------------------
DROP TABLE IF EXISTS `client_envelopes_type`;
CREATE TABLE `client_envelopes_type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `type` int(11) DEFAULT '0',
  `req_amount` float DEFAULT NULL,
  `dis_amount` float DEFAULT NULL,
  `pid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_level
-- ----------------------------
DROP TABLE IF EXISTS `client_level`;
CREATE TABLE `client_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level_name` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `level_credit` int(11) NOT NULL,
  `level_discount` float DEFAULT NULL,
  `level_credit_feed` float DEFAULT NULL,
  `upable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for client_messages
-- ----------------------------
DROP TABLE IF EXISTS `client_messages`;
CREATE TABLE `client_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `msgtype` tinyint(2) DEFAULT '0',
  `msgcont` text,
  `msgdirect` tinyint(4) DEFAULT '0',
  `autoreped` tinyint(4) DEFAULT '0',
  `send_time` datetime DEFAULT NULL,
  `sreaded` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_message_session
-- ----------------------------
DROP TABLE IF EXISTS `client_message_session`;
CREATE TABLE `client_message_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `unread` int(11) DEFAULT '0',
  `undesc` varchar(255) DEFAULT NULL,
  `lasttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_order_address
-- ----------------------------
DROP TABLE IF EXISTS `client_order_address`;
CREATE TABLE `client_order_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel` varchar(255) COLLATE utf8_bin NOT NULL,
  `postal_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for client_product_likes
-- ----------------------------
DROP TABLE IF EXISTS `client_product_likes`;
CREATE TABLE `client_product_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni` (`openid`,`product_id`) USING BTREE,
  KEY `uopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for client_sign_record
-- ----------------------------
DROP TABLE IF EXISTS `client_sign_record`;
CREATE TABLE `client_sign_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dt` date DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `openid` varchar(150) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dt` (`dt`,`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for companys
-- ----------------------------
DROP TABLE IF EXISTS `companys`;
CREATE TABLE `companys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL DEFAULT '0',
  `name` varchar(200) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `return_percent` float(5,3) DEFAULT '0.050',
  `money` float DEFAULT '0',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `bank_personname` varchar(255) DEFAULT NULL,
  `person_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `utype` tinyint(4) DEFAULT NULL,
  `verifed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`name`,`email`,`phone`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for company_bills
-- ----------------------------
DROP TABLE IF EXISTS `company_bills`;
CREATE TABLE `company_bills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comid` int(11) DEFAULT NULL,
  `bill_amount` float(10,2) DEFAULT NULL,
  `bill_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for company_income_record
-- ----------------------------
DROP TABLE IF EXISTS `company_income_record`;
CREATE TABLE `company_income_record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float(11,2) NOT NULL DEFAULT '0.00',
  `date` datetime NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `pcount` int(11) NOT NULL,
  `is_seted` tinyint(4) DEFAULT '0',
  `is_reqed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_spread_record
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record`;
CREATE TABLE `company_spread_record` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `com_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `product_id` int(11) NOT NULL,
  `readi` int(11) NOT NULL DEFAULT '1',
  `turned` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_spread_record_details
-- ----------------------------
DROP TABLE IF EXISTS `company_spread_record_details`;
CREATE TABLE `company_spread_record_details` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `spread_id` int(11) NOT NULL,
  `cclient_id` int(11) NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for company_users
-- ----------------------------
DROP TABLE IF EXISTS `company_users`;
CREATE TABLE `company_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for crash_pay
-- ----------------------------
DROP TABLE IF EXISTS `crash_pay`;
CREATE TABLE `crash_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_time` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `discount_amount` double(8,2) DEFAULT NULL,
  `serial_number` varchar(30) DEFAULT NULL,
  `uid` varchar(255) DEFAULT '''unpay'',''payed''' COMMENT 'unpay',
  `status` enum('unpay','payed') NOT NULL DEFAULT 'unpay',
  `code` varchar(9) DEFAULT NULL,
  `is_send` varchar(2) DEFAULT '0' COMMENT '0 代表未发 1 代表已发',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ephone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for enterprise_users
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_users`;
CREATE TABLE `enterprise_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniopenid` (`openid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for envs_robblist
-- ----------------------------
DROP TABLE IF EXISTS `envs_robblist`;
CREATE TABLE `envs_robblist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `on` int(11) DEFAULT NULL,
  `remains` int(11) DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for envs_robrecord
-- ----------------------------
DROP TABLE IF EXISTS `envs_robrecord`;
CREATE TABLE `envs_robrecord` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `envsid` int(11) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for gmess_category
-- ----------------------------
DROP TABLE IF EXISTS `gmess_category`;
CREATE TABLE `gmess_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL,
  `parent` int(11) DEFAULT '0',
  `sort` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_page
-- ----------------------------
DROP TABLE IF EXISTS `gmess_page`;
CREATE TABLE `gmess_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `desc` varchar(255) DEFAULT NULL,
  `catimg` varchar(255) DEFAULT NULL,
  `thumb_media_id` varchar(255) DEFAULT NULL,
  `media_id` varchar(255) DEFAULT NULL,
  `createtime` date DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gmess_send_stat
-- ----------------------------
DROP TABLE IF EXISTS `gmess_send_stat`;
CREATE TABLE `gmess_send_stat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` int(11) NOT NULL,
  `send_date` datetime DEFAULT NULL,
  `send_count` int(11) DEFAULT NULL,
  `read_count` int(11) DEFAULT '0',
  `share_count` int(11) DEFAULT '0',
  `receive_count` int(11) DEFAULT NULL,
  `send_type` tinyint(4) DEFAULT '0',
  `msg_type` enum('text','images') DEFAULT 'images',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ingredients_instock
-- ----------------------------
DROP TABLE IF EXISTS `ingredients_instock`;
CREATE TABLE `ingredients_instock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ingd_cat` smallint(8) NOT NULL DEFAULT '0' COMMENT '食材类别id',
  `ingd_name` varchar(32) NOT NULL DEFAULT 'Unknown' COMMENT '食材名称',
  `ingd_unit` tinyint(8) NOT NULL DEFAULT '0' COMMENT '计量单位: 0-g， 1-kg， 2-ml，3-l，4-个',
  `instock` int(12) NOT NULL DEFAULT '0' COMMENT '库存数',
  `ingd_threshold` int(12) NOT NULL DEFAULT '0' COMMENT '警戒库存值',
  `last_update` int(11) NOT NULL DEFAULT '0' COMMENT '上次更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ingredients_stock_change
-- ----------------------------
DROP TABLE IF EXISTS `ingredients_stock_change`;
CREATE TABLE `ingredients_stock_change` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `ingd_id` int(10) NOT NULL DEFAULT '0' COMMENT '配料类别代码',
  `instock` int(24) NOT NULL DEFAULT '0' COMMENT '变动后库存',
  `change_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '变更类型：1-入库  2-出库 3-损耗减计',
  `change_val` int(16) NOT NULL DEFAULT '0' COMMENT '变动值',
  `change_price` int(20) DEFAULT '0' COMMENT '变动总金额（分为单位）',
  `spec` text COMMENT '该物料规格(如克数/袋，ml/瓶等)',
  `barcode` varchar(32) DEFAULT 'Unknown' COMMENT '该物料条码',
  `vendor` varchar(48) NOT NULL DEFAULT 'Unknown' COMMENT '物料供应商',
  `change_note` text COMMENT '变动备注',
  `change_user` varchar(24) NOT NULL DEFAULT 'Unknown' COMMENT '物料变动操作人',
  `change_time` int(12) NOT NULL DEFAULT '0' COMMENT '变动时间',
  `uid` int(12) NOT NULL DEFAULT '0' COMMENT '本条数据操作人',
  `add_time` int(12) NOT NULL DEFAULT '0' COMMENT '记录添加时间',
  `log` text COMMENT '操作log',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `client_id` int(20) DEFAULT NULL COMMENT '客户编号',
  `order_time` datetime DEFAULT NULL COMMENT '订单交易时间',
  `receive_time` datetime DEFAULT NULL,
  `send_time` datetime DEFAULT NULL,
  `order_balance` float DEFAULT '0',
  `order_yunfei` float(11,2) DEFAULT '0.00',
  `order_amount` float(10,2) DEFAULT '0.00' COMMENT '总价',
  `order_refund_amount` float DEFAULT '0',
  `company_com` varchar(255) COLLATE utf8_bin DEFAULT '0',
  `envs_id` int(11) DEFAULT '0',
  `product_count` int(11) DEFAULT '0',
  `order_dixian` float(10,2) NOT NULL DEFAULT '0.00',
  `serial_number` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `wepay_serial` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `wepay_openid` varchar(255) COLLATE utf8_bin DEFAULT '',
  `wepay_unionid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `bank_billno` varchar(255) COLLATE utf8_bin DEFAULT '',
  `leword` text COLLATE utf8_bin,
  `status` enum('unpay','payed','received','canceled','closed','refunded','delivering','reqing') COLLATE utf8_bin NOT NULL DEFAULT 'unpay' COMMENT '订单状态',
  `express_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `express_com` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `exptime` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `enterprise_id` int(11) DEFAULT '0',
  `reci_head` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `reci_cont` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `reci_tex` float DEFAULT NULL,
  `staff_id` int(20) DEFAULT NULL COMMENT '职员工号',
  `store_id` int(10) NOT NULL DEFAULT '0' COMMENT '商店编号',
  `is_commented` tinyint(1) DEFAULT '0',
  `pay_type` int(2) DEFAULT '1' COMMENT '现金 (1)，余额支付(0), 在线+ 余额支付(2)',
  `user_coupon` varchar(100) CHARACTER SET utf8 DEFAULT '' COMMENT '使用的优惠券id集合',
  `address_id` varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '配送地址 ID',
  `pay_amount` float(10,2) NOT NULL COMMENT '优惠后的金额',
  `online_amount` float(10,2) NOT NULL COMMENT '线上支付的',
  `balance_amount` float(10,2) NOT NULL COMMENT '余额支付的',
  `isbalance` int(2) DEFAULT '1' COMMENT '允许余额支付',
  `come_from` varchar(100) CHARACTER SET utf8 DEFAULT 'qiezi' COMMENT '订单来源',
  `notes` varchar(200) CHARACTER SET utf8 DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_address
-- ----------------------------
DROP TABLE IF EXISTS `orders_address`;
CREATE TABLE `orders_address` (
  `addr_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `user_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `tel_number` varchar(255) COLLATE utf8_bin NOT NULL,
  `province` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `postal_code` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `hash` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`addr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for orders_comment
-- ----------------------------
DROP TABLE IF EXISTS `orders_comment`;
CREATE TABLE `orders_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `starts` tinyint(4) DEFAULT NULL,
  `content` tinytext,
  `mtime` datetime DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `anonymous` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `openid` (`openid`(191)) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail` (
  `order_id` int(20) NOT NULL COMMENT '订单编号',
  `product_id` int(20) NOT NULL COMMENT '商品编号',
  `product_count` int(10) NOT NULL COMMENT '商品数量',
  `product_discount_price` double(11,2) DEFAULT '0.00',
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_price_hash_id` int(11) NOT NULL DEFAULT '0',
  `is_returned` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=467 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for order_distribute
-- ----------------------------
DROP TABLE IF EXISTS `order_distribute`;
CREATE TABLE `order_distribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '配送编号',
  `order_serial_no` varchar(32) NOT NULL DEFAULT '0' COMMENT '订单编号',
  `address_id` int(11) NOT NULL DEFAULT '0' COMMENT '配送地址编号',
  `exp_time` varchar(100) DEFAULT '' COMMENT '配送时间',
  `unpay_amount` int(11) DEFAULT '0' COMMENT '未支付金额,预留给货到付款的状态',
  `express_code` varchar(30) DEFAULT '' COMMENT '配送方式的编码',
  `courier` varchar(30) DEFAULT '' COMMENT '配送员名称',
  `status` varchar(20) DEFAULT '' COMMENT '配送状态',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `update_time` int(10) DEFAULT '0' COMMENT '最后更新时间',
  `operater_id` int(10) DEFAULT '0' COMMENT '操作人编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=324 DEFAULT CHARSET=utf8 COMMENT='订单配送表';

-- ----------------------------
-- Table structure for order_reqpay
-- ----------------------------
DROP TABLE IF EXISTS `order_reqpay`;
CREATE TABLE `order_reqpay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `wepay_serial` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `amount` float NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `dt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for pageview_records
-- ----------------------------
DROP TABLE IF EXISTS `pageview_records`;
CREATE TABLE `pageview_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `openid` varchar(255) COLLATE utf8_bin DEFAULT '',
  `ip` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `referer` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for products_info
-- ----------------------------
DROP TABLE IF EXISTS `products_info`;
CREATE TABLE `products_info` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `product_code` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '商品条码',
  `product_type` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品类型',
  `is_package` char(2) CHARACTER SET utf8 DEFAULT '0' COMMENT '组合类型，0为单品，1为组合',
  `product_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `show_inhome` tinyint(4) DEFAULT '0',
  `product_subname` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品颜色',
  `product_size` varchar(40) CHARACTER SET utf8 DEFAULT NULL COMMENT '商品大小',
  `product_cat` int(11) NOT NULL DEFAULT '1' COMMENT '商品类别id',
  `product_brand` int(11) DEFAULT '0' COMMENT '商品品牌id',
  `product_readi` int(11) NOT NULL DEFAULT '0',
  `product_desc` longtext COLLATE utf8_bin COMMENT '商品描述',
  `product_subtitle` text COLLATE utf8_bin COMMENT '商品子标题',
  `product_serial` int(11) DEFAULT '0' COMMENT '商品所属系列',
  `product_weight` varchar(11) COLLATE utf8_bin DEFAULT '0.00' COMMENT '商品重量',
  `product_online` tinyint(4) DEFAULT '1',
  `product_credit` int(11) DEFAULT '0' COMMENT '商品积分',
  `product_prom` int(11) DEFAULT '0',
  `product_prom_limit` int(11) DEFAULT '0',
  `product_prom_limitdate` varchar(0) COLLATE utf8_bin DEFAULT NULL,
  `product_prom_limitdays` int(11) DEFAULT '0',
  `product_prom_discount` int(11) DEFAULT '0',
  `product_expfee` float(5,2) DEFAULT '0.00' COMMENT '商品快递费用',
  `market_price` float(11,2) DEFAULT NULL,
  `catimg` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `store_id` int(10) NOT NULL DEFAULT '0',
  `sort` int(10) DEFAULT '0',
  `delete` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_brand
-- ----------------------------
DROP TABLE IF EXISTS `product_brand`;
CREATE TABLE `product_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) DEFAULT NULL,
  `brand_img1` varchar(255) DEFAULT NULL,
  `brand_img2` varchar(255) DEFAULT NULL,
  `brand_cat` int(11) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniname` (`brand_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `cat_descs` text COLLATE utf8_bin,
  `cat_image` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cat_parent` int(11) NOT NULL DEFAULT '0',
  `cat_level` int(11) DEFAULT '0',
  `cat_order` int(11) NOT NULL DEFAULT '0',
  `cat_apply_to` tinyint(2) DEFAULT '0' COMMENT '应用于商品分类，0表示单品，1表示套餐',
  `is_open` tinyint(1) DEFAULT '0' COMMENT '是否对外开放',
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_enterprise_discount
-- ----------------------------
DROP TABLE IF EXISTS `product_enterprise_discount`;
CREATE TABLE `product_enterprise_discount` (
  `productId` int(11) DEFAULT NULL,
  `entId` int(11) DEFAULT NULL,
  `discount` float(5,2) DEFAULT NULL,
  UNIQUE KEY `uni` (`productId`,`entId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_images
-- ----------------------------
DROP TABLE IF EXISTS `product_images`;
CREATE TABLE `product_images` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(512) COLLATE utf8_bin NOT NULL,
  `image_sort` tinyint(4) DEFAULT '0',
  `image_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`),
  KEY `index_product` (`product_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_ingredient
-- ----------------------------
DROP TABLE IF EXISTS `product_ingredient`;
CREATE TABLE `product_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT '0' COMMENT '产品编号',
  `ingd_id` int(11) DEFAULT '0' COMMENT '食材编号',
  `ingd_name` varchar(32) DEFAULT '' COMMENT '食材名称',
  `need_number` int(5) DEFAULT '0' COMMENT '食材名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_instock
-- ----------------------------
DROP TABLE IF EXISTS `product_instock`;
CREATE TABLE `product_instock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_date` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  `sku_id` int(11) NOT NULL DEFAULT '0' COMMENT 'SKU id',
  `sku_name` varchar(64) DEFAULT 'Unknown' COMMENT '产品名称',
  `produce` int(8) DEFAULT '0' COMMENT '当日生产数',
  `avaliable` int(8) DEFAULT '0' COMMENT '当日可售数',
  `instock` int(8) DEFAULT '0' COMMENT '当日库存数',
  `sold` int(8) DEFAULT '0' COMMENT '当日销售数',
  `loss` int(8) DEFAULT '0' COMMENT '当日损耗数',
  `user_note` text,
  `op_log` text COMMENT '操作记录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=983 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_onsale
-- ----------------------------
DROP TABLE IF EXISTS `product_onsale`;
CREATE TABLE `product_onsale` (
  `product_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `sale_prices` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '售价',
  `store_id` int(8) NOT NULL DEFAULT '0' COMMENT '商店编号',
  `discount` int(3) NOT NULL DEFAULT '100' COMMENT '折扣',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_package_ingredient
-- ----------------------------
DROP TABLE IF EXISTS `product_package_ingredient`;
CREATE TABLE `product_package_ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT '0' COMMENT '套餐对应的产品编号',
  `sub_product_id` int(11) DEFAULT '0' COMMENT '单品编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_proportion
-- ----------------------------
DROP TABLE IF EXISTS `product_proportion`;
CREATE TABLE `product_proportion` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_id` int(10) DEFAULT '0' COMMENT '食材或者商品的id，和p_type进行配合使用',
  `product_id` int(10) DEFAULT '0' COMMENT '商品编号',
  `p_type` varchar(10) DEFAULT '' COMMENT '配比类型，单品为single，套餐为package',
  `p_value` int(3) DEFAULT '0' COMMENT '配比值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_serials
-- ----------------------------
DROP TABLE IF EXISTS `product_serials`;
CREATE TABLE `product_serials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_name` varchar(255) DEFAULT NULL COMMENT '序列名称',
  `serial_image` varchar(255) DEFAULT NULL,
  `serial_desc` varchar(255) DEFAULT NULL,
  `relcat` tinyint(4) DEFAULT NULL,
  `relevel` tinyint(4) DEFAULT NULL,
  `sort` varchar(255) DEFAULT '0' COMMENT '排序',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_spec
-- ----------------------------
DROP TABLE IF EXISTS `product_spec`;
CREATE TABLE `product_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `spec_det_id1` int(11) DEFAULT NULL,
  `spec_det_id2` int(11) DEFAULT NULL,
  `sale_price` float(11,2) DEFAULT NULL,
  `market_price` float(11,2) DEFAULT '0.00',
  `instock` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_view_record
-- ----------------------------
DROP TABLE IF EXISTS `product_view_record`;
CREATE TABLE `product_view_record` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for product_wechat_recomemt
-- ----------------------------
DROP TABLE IF EXISTS `product_wechat_recomemt`;
CREATE TABLE `product_wechat_recomemt` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for product_wechat_recommend
-- ----------------------------
DROP TABLE IF EXISTS `product_wechat_recommend`;
CREATE TABLE `product_wechat_recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for question
-- ----------------------------
DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `add_time` int(11) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for share
-- ----------------------------
DROP TABLE IF EXISTS `share`;
CREATE TABLE `share` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `uid` int(25) NOT NULL COMMENT '用户 id',
  `add_time` int(11) DEFAULT NULL COMMENT '分享的时间',
  `share_money` double(4,2) DEFAULT NULL COMMENT '分享的总金额',
  `is_valid` char(2) DEFAULT NULL COMMENT '是否有效(0 是有效 1 是无效)',
  `type` char(2) DEFAULT NULL COMMENT '分享的类型(0 表示 用户中心分享  1表示 订单分享)',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券 id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for share_setting
-- ----------------------------
DROP TABLE IF EXISTS `share_setting`;
CREATE TABLE `share_setting` (
  `key_m` varchar(128) NOT NULL,
  `value_m` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for share_user_take
-- ----------------------------
DROP TABLE IF EXISTS `share_user_take`;
CREATE TABLE `share_user_take` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `uid` int(25) NOT NULL COMMENT '领取人的 uid',
  `add_time` int(11) DEFAULT NULL COMMENT '分享时间',
  `des` varchar(200) DEFAULT NULL COMMENT '分享描述',
  `coupon_id` int(10) DEFAULT NULL COMMENT '优惠券 id',
  `coupon_money` double(6,2) DEFAULT NULL COMMENT '本次分享优惠的金额',
  `share_id` int(25) DEFAULT NULL COMMENT '分享记录的 id',
  `from_uid` int(25) DEFAULT NULL COMMENT '链接来源',
  `create_share_uid` int(25) DEFAULT NULL COMMENT '创建人的 uid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_access_token
-- ----------------------------
DROP TABLE IF EXISTS `shop_access_token`;
CREATE TABLE `shop_access_token` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(25) NOT NULL COMMENT '用户 id',
  `access_token` varchar(100) DEFAULT '' COMMENT 'access_token',
  `expired_time` int(10) DEFAULT '0' COMMENT 'access_token过期时间',
  `add_time` int(11) DEFAULT NULL COMMENT '添加的时间',
  `last_update_time` int(11) DEFAULT '0' COMMENT '最后更新的时间',
  `refresh_token` varchar(100) DEFAULT '' COMMENT 'refresh_token',
  `refresh_expired_time` int(10) DEFAULT '0' COMMENT 'refresh_token过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_cart
-- ----------------------------
DROP TABLE IF EXISTS `shop_cart`;
CREATE TABLE `shop_cart` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `product_id` int(10) DEFAULT '0' COMMENT '商品id',
  `product_type` tinyint(4) DEFAULT '0' COMMENT '商品类型：0 – 普通商品   1-换购商品',
  `product_quantity` int(8) DEFAULT '1' COMMENT '商品数量',
  `use_coupon` int(8) DEFAULT '0' COMMENT '使用的优惠券id',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  `spec_id` int(8) DEFAULT NULL COMMENT '产品规格 id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3184 DEFAULT CHARSET=utf8 COMMENT='购物车表';

-- ----------------------------
-- Table structure for shop_charge_card
-- ----------------------------
DROP TABLE IF EXISTS `shop_charge_card`;
CREATE TABLE `shop_charge_card` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  `serial_no` varchar(12) DEFAULT '' COMMENT '充值卡序号',
  `charge_code` varchar(12) DEFAULT '' COMMENT '充值卡密码,12个字符，数字+大小写英文字母',
  `amount` int(20) DEFAULT '0' COMMENT '面值：分为单位',
  `sale_price` int(20) DEFAULT '0' COMMENT '售价：分为单位',
  `is_delivered` tinyint(1) DEFAULT '0' COMMENT '已发出制卡：0 - 否    1 - 是',
  `is_activated` tinyint(1) DEFAULT '0' COMMENT '是否已激活：0-未激活  1-已激活',
  `is_used` tinyint(1) DEFAULT '0' COMMENT '已使用：0 – 否  1 – 是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1043 DEFAULT CHARSET=utf8 COMMENT='充值卡表';

-- ----------------------------
-- Table structure for shop_charge_card_500
-- ----------------------------
DROP TABLE IF EXISTS `shop_charge_card_500`;
CREATE TABLE `shop_charge_card_500` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  `serial_no` varchar(12) DEFAULT '' COMMENT '充值卡序号',
  `charge_code` varchar(12) DEFAULT '' COMMENT '充值卡密码,12个字符，数字+大小写英文字母',
  `amount` smallint(8) DEFAULT '0' COMMENT '面值：分为单位',
  `sale_price` smallint(8) DEFAULT '0' COMMENT '售价：分为单位',
  `is_delivered` tinyint(1) DEFAULT '0' COMMENT '已发出制卡：0 - 否    1 - 是',
  `is_activated` tinyint(1) DEFAULT '0' COMMENT '是否已激活：0-未激活  1-已激活',
  `is_used` tinyint(1) DEFAULT '0' COMMENT '已使用：0 – 否  1 – 是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1035 DEFAULT CHARSET=utf8 COMMENT='充值卡表';

-- ----------------------------
-- Table structure for shop_coupons
-- ----------------------------
DROP TABLE IF EXISTS `shop_coupons`;
CREATE TABLE `shop_coupons` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `coupon_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '优惠券类型，0为商品类，1为订单类',
  `coupon_name` varchar(64) NOT NULL DEFAULT 'Coupon' COMMENT '优惠券名称',
  `coupon_detail` text COMMENT '优惠券详细信息',
  `coupon_cover` varchar(64) DEFAULT '' COMMENT '优惠券图片',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `available_start` int(10) DEFAULT '0' COMMENT '发放时间段：开始时间',
  `available_end` int(10) DEFAULT '0' COMMENT '发放时间段：结束时间',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) DEFAULT '0' COMMENT '最后更新时间',
  `effective_start` int(10) DEFAULT '0' COMMENT '有效期开始时间',
  `effective_end` int(10) DEFAULT '0' COMMENT '有效期结束时间',
  `discount_type` tinyint(4) DEFAULT '0' COMMENT '折扣类型：0-固定金额  1-比例',
  `discount_val` int(10) DEFAULT '0' COMMENT '折扣值：固定金额时，单位为分；比例折扣时，单位为1%',
  `applied` text COMMENT '接受该优惠券的商品品类或id集合的JSON字符串',
  `coupon_terms` text COMMENT '该优惠券使用条件的JSON字符串',
  `coupon_stock` int(10) DEFAULT '0' COMMENT '发放的总数。不限时指定为负数',
  `coupon_stock_left` int(10) DEFAULT '0' COMMENT '剩余可用数量。不限时指定为负数',
  `coupon_limit` text COMMENT '使用限制条件的JSON字符串。比如不可与某优惠券复用，不可自身叠加等',
  `bundled` text COMMENT '换购类优惠券的商品id，别的优惠券为空',
  `is_activated` tinyint(1) DEFAULT '0' COMMENT '是否激活：0-停用   1-启用',
  `coupon_log` text COMMENT '该优惠券的编辑历史JSON字符串，格式为：时间|编辑人|编辑内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='优惠券表';

-- ----------------------------
-- Table structure for shop_coupons_terms
-- ----------------------------
DROP TABLE IF EXISTS `shop_coupons_terms`;
CREATE TABLE `shop_coupons_terms` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `term_name` varchar(64) NOT NULL DEFAULT 'Term' COMMENT '使用条件类别名称',
  `term_detail` text COMMENT '条件类别详细描述',
  `term_table` varchar(64) NOT NULL DEFAULT '' COMMENT '条件类别对应的table名称',
  `term_column` varchar(64) DEFAULT '' COMMENT '条件类别对应的列名称',
  `term_operate` varchar(20) DEFAULT '' COMMENT '操作符',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='优惠券使用条件表';

-- ----------------------------
-- Table structure for shop_merchant_info
-- ----------------------------
DROP TABLE IF EXISTS `shop_merchant_info`;
CREATE TABLE `shop_merchant_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `app_id` char(18) DEFAULT '' COMMENT 'appid',
  `app_secret` varchar(50) DEFAULT '' COMMENT 'app_secret',
  `salt` varchar(8) DEFAULT '' COMMENT '加密盐',
  `uid` int(25) DEFAULT '0' COMMENT '商户对应的uid',
  `merchant_no` char(10) DEFAULT '0' COMMENT '商户号',
  `merchant_account` varchar(25) DEFAULT '' COMMENT '商户号账户',
  `merchant_password` varchar(8) DEFAULT '' COMMENT '商户号密码',
  `add_time` int(11) DEFAULT '0' COMMENT '创建时间',
  `last_update_time` int(11) DEFAULT '0' COMMENT '最后更新时间',
  `last_login_time` int(11) DEFAULT '0' COMMENT '最后登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for suppliers
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `enable` varchar(4) NOT NULL DEFAULT '0' COMMENT '(1表示 上一次用的)',
  `area` varchar(255) DEFAULT NULL,
  `is_delete` varchar(2) DEFAULT '0' COMMENT '0 表示未删  1表示删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_charge_card
-- ----------------------------
DROP TABLE IF EXISTS `user_charge_card`;
CREATE TABLE `user_charge_card` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `client_uid` int(10) DEFAULT '0' COMMENT '拥有人id',
  `add_time` int(10) DEFAULT '0' COMMENT '获取时间',
  `charge_card_id` varchar(12) DEFAULT '' COMMENT '充值卡密码,12个字符，数字+大小写英文字母',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户充值卡表';

-- ----------------------------
-- Table structure for user_charge_log
-- ----------------------------
DROP TABLE IF EXISTS `user_charge_log`;
CREATE TABLE `user_charge_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `client_uid` int(10) DEFAULT '0' COMMENT '用户编号',
  `charge_type` varchar(20) DEFAULT '' COMMENT '支付类型',
  `amount` int(8) DEFAULT '0' COMMENT '充值金额',
  `pay_amount` double(10,2) DEFAULT '0.00' COMMENT '实际支付的金额',
  `charge_time` int(10) DEFAULT '0' COMMENT '充值时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='用户充值记录表';

-- ----------------------------
-- Table structure for user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `user_coupon`;
CREATE TABLE `user_coupon` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '优惠券使用条件类别id，主键，自增',
  `uid` int(10) DEFAULT '0' COMMENT '创建人id',
  `coupon_id` int(8) DEFAULT '0' COMMENT '使用的优惠券id',
  `is_used` tinyint(1) DEFAULT '0' COMMENT '是否已经使用，0为未使用，1为已使用',
  `add_time` int(10) DEFAULT '0' COMMENT '创建时间',
  `come_from` varchar(20) DEFAULT 'order' COMMENT '优惠券来源',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=688 DEFAULT CHARSET=utf8 COMMENT='用户优惠券表';

-- ----------------------------
-- Table structure for wechat_autoresponse
-- ----------------------------
DROP TABLE IF EXISTS `wechat_autoresponse`;
CREATE TABLE `wechat_autoresponse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `message` text,
  `rel` int(11) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wechat_subscribe_record
-- ----------------------------
DROP TABLE IF EXISTS `wechat_subscribe_record`;
CREATE TABLE `wechat_subscribe_record` (
  `recordid` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) COLLATE utf8_bin NOT NULL,
  `date` date DEFAULT NULL,
  `dv` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`recordid`)
) ENGINE=InnoDB AUTO_INCREMENT=2566 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for wshop_banners
-- ----------------------------
DROP TABLE IF EXISTS `wshop_banners`;
CREATE TABLE `wshop_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_href` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `banner_position` tinyint(4) DEFAULT '0',
  `reltype` tinyint(4) DEFAULT NULL,
  `relid` varchar(255) COLLATE utf8_bin DEFAULT '0',
  `sort` tinyint(4) DEFAULT '0',
  `exp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Table structure for wshop_discountcode
-- ----------------------------
DROP TABLE IF EXISTS `wshop_discountcode`;
CREATE TABLE `wshop_discountcode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keywords` varchar(255) DEFAULT NULL,
  `code_total` int(11) DEFAULT NULL,
  `code_remains` int(11) DEFAULT NULL,
  `code_discount` float(5,2) DEFAULT '0.00',
  `template` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_discountcodes
-- ----------------------------
DROP TABLE IF EXISTS `wshop_discountcodes`;
CREATE TABLE `wshop_discountcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codes` varchar(255) DEFAULT NULL,
  `qid` int(11) DEFAULT '0',
  `openid` varchar(255) DEFAULT NULL,
  `rectime` datetime DEFAULT NULL,
  `isvalid` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_discountcode_record
-- ----------------------------
DROP TABLE IF EXISTS `wshop_discountcode_record`;
CREATE TABLE `wshop_discountcode_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(255) DEFAULT NULL,
  `rectime` datetime DEFAULT NULL,
  `codeid` int(11) DEFAULT NULL,
  `qid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_expresstaff
-- ----------------------------
DROP TABLE IF EXISTS `wshop_expresstaff`;
CREATE TABLE `wshop_expresstaff` (
  `id` int(11) NOT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `headimg` varchar(255) DEFAULT NULL,
  `uname` varchar(255) DEFAULT NULL,
  `isnotify` tinyint(1) DEFAULT '0',
  `isexpress` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for wshop_menu
-- ----------------------------
DROP TABLE IF EXISTS `wshop_menu`;
CREATE TABLE `wshop_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relid` int(11) DEFAULT NULL,
  `reltype` tinyint(4) DEFAULT NULL,
  `relcontent` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_recomment_company
-- ----------------------------
DROP TABLE IF EXISTS `wshop_recomment_company`;
CREATE TABLE `wshop_recomment_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` enum('unfix','fixed','close') DEFAULT 'unfix',
  `content` text,
  `comid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_search_record
-- ----------------------------
DROP TABLE IF EXISTS `wshop_search_record`;
CREATE TABLE `wshop_search_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_settings
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings`;
CREATE TABLE `wshop_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(512) DEFAULT NULL,
  `last_mod` datetime NOT NULL,
  PRIMARY KEY (`key`),
  KEY `index_key` (`key`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_settings_expfee
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings_expfee`;
CREATE TABLE `wshop_settings_expfee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `province` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `citys` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ffee` float DEFAULT NULL,
  `ffeeadd` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for wshop_settings_section
-- ----------------------------
DROP TABLE IF EXISTS `wshop_settings_section`;
CREATE TABLE `wshop_settings_section` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `pid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `banner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `relid` int(5) DEFAULT NULL,
  `bsort` tinyint(5) DEFAULT '0',
  `ftime` datetime DEFAULT NULL,
  `ttime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Table structure for wshop_spec
-- ----------------------------
DROP TABLE IF EXISTS `wshop_spec`;
CREATE TABLE `wshop_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_name` varchar(255) NOT NULL,
  `spec_remark` varchar(255) DEFAULT NULL,
  `spec_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for wshop_spec_det
-- ----------------------------
DROP TABLE IF EXISTS `wshop_spec_det`;
CREATE TABLE `wshop_spec_det` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spec_id` int(11) NOT NULL,
  `det_name` varchar(255) NOT NULL,
  `det_sort` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
