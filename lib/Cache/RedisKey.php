<?php
/*首页数据超时时间*/
define('CACHE_TIME_AN_HOUR',10 * 60);
/*緩存時間1半個小時*/
define('CACHE_TIME_HALF_AN_HOUR',10 * 30);
/*緩存時間1天*/
define('CACHE_TIME_ONE_DAY',24 * 10 * 60);

/*緩存時間10天*/
define('CACHE_TIME_TEN_DAY',10 * 24 * 10 * 60);


/*用戶信息*/
define('PREFIX_USER_INFO','USER_INFO_');
/*分类信息*/
define('PREFIX_CATE_INFO','CATE_INFO_');
/*分类信息*/
define('PREFIX_PRODUCT_INFO','PRODUCT_INFO_');
/*單個分類下的列表*/
define('PREFIX_CATE_LIST','CATE_LIST_');
/*緩存時間1天*/
define('PREFIX_USER_LATEST_ORDER','USER_LATEST_ORDER');

define('PREFIX_SETTING_LIST','SETTING_LIST');
define('PREFIX_SETTING_NAME','SETTING_NAME_');
