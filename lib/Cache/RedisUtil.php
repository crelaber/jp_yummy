<?php

/**
 * 数据库REDIS操作类
 *
 * @package		WeCenter
 * @subpackage	System
 * @category	Libraries
 * @author		WeCenter Dev Team
 */
class RedisUtil
{

	private static $common_prefix ;
	protected static $_instance = null; //静态实例

	public function __construct(){ //私有的构造方法
		global $config;
		self::$common_prefix =$config->redis_run_model;
		self::$_instance = new Redis();
		self::$_instance->connect($config->redis_host,$config->redis_port);
		if($config->auth){
			self::$_instance->auth($config->auth);
		}
		$db_index = $config->redis_index;
		self::$_instance->select($db_index);
	}

	//获取静态实例
	private static function get_instance(){
		if(!self::$_instance){
			new self;
		}
		return self::$_instance;

	}

	/**
	 * 设置值
	 * @param $key string
	 * @param $val array | string 设置的值
	 */
	protected static function _set($key,$val,$life_time = 0){
		$redis = self::get_instance();
		$public_prefix = self::$common_prefix;
		$key = $public_prefix.$key;
		//设置有效期
		if($life_time){
			$redis->setex($key,$life_time,$val);
		}else{
			$redis->set($key,$val);
		}
	}

	/**
	 * 获取值
	 * @param $key string
	 * @return string
	 */
	protected static function _get($key){
		$redis = self::get_instance();
		$result = $redis->get(self::$common_prefix.$key);
		return $result;
	}

	/**
	 * 获取redis中对应key的值的公共方法
	 * @param $key string
	 * @return array
	 */
	public static function get($key){
		$json = self::_get($key);
		$data = $json && json_decode($json,true)?json_decode($json,true):$json;
		return $data;
	}

	/**
	 * 获取redis中对应key的值的公共方法
	 * @param $key string
	 * @param $val 值
	 * @param $life_time 有效期
	 */
	public static function set($key,$val,$life_time = 0){
		$data = is_array($val) ? json_encode($val) : $val;
		self::_set($key,$data,$life_time);
	}


	public static function update($redis_key,$origin_data,$update_data,$cache_time = CACHE_TIME_ONE_DAY){
		if($origin_data){
			if(count($update_data)>0){
				foreach ($update_data as $key => $val){
					$origin_data[$key] = $val;
				}
			}
			self::set($redis_key,$origin_data,$cache_time);
		}
	}



	/**
	 * 删除指定的值
	 * @param $key string
	 */
	public static function delete($key,$use_prefix = true){
		$redis = self::get_instance();
		$public_prefix = $use_prefix?self::$common_prefix:'';
		$key = $public_prefix.$key;
		return $redis->delete($key);
	}

	/**
	 * 获取所有的key
	 * @return array
	 */
	public static function get_keys(){
		$redis = self::get_instance();
		$pattern = self::$common_prefix.'*';
		return $redis->keys($pattern);
	}
	/**
	 * 获取原始的key
	 * @param type $key
	 * @return type
	 */
	public static function get_key($key){
		$origin_key = substr($key, strlen(self::$common_prefix));
		return $origin_key;
	}


	/**
	 * 自增方法，数据一般为整形，调用1次增加1
	 * 
	 * @param string $key 键名
	 * @return boolean 更新状态
	 */
	public static function incr($key){
		$redis = self::get_instance();
		$result = $redis->incr(self::$common_prefix.$key);
		return $result;
	}
	
	/**
	 * 对应的键名是否存在
	 * 
	 * @param string $key 键名
	 * @return boolean 状态
	 */
	public static function is_exist($key){
		$redis = self::get_instance();
		$result = $redis->exists(self::$common_prefix.$key);
		return $result;
	}
	
	/**
	 * 获取
	 * 
	 * @param string $key 键名
	 * @return boolean 状态
	 */
	public static function keys($pattern = '*'){
		$redis = self::get_instance();
		$full_pattern = self::$common_prefix.$pattern;
		if($pattern != '*'){
		    $full_pattern = self::$common_prefix.$pattern.'*';
		}
		return $redis->keys($full_pattern);
	}
	
	/**
	 * 构造函数，避免重复构造redis的所有方法
	 * 
	 * @param string $name
	 * @param type $value
	 */
	public function __callStatic($name, $arguments){
	    $redis = self::get_instance();
	    if($arguments){
		$key = $arguments[0];
		$full_key = self::$common_prefix.$key;
		$arguments[0] = $full_key;
	    }else{
		$arguments = array();
	    }

	    $len = count($arguments);
	    $return = "";
	    switch($len){
		case 0:{
		   $return = $redis->$name();
		   break;
		}
		case 1:{
		    $return = $redis->$name($arguments[0]);
		    break;
		}
		case 2:{
		    $return = $redis->$name($arguments[0], $arguments[1]);
		    break;
		}
		case 3:{
		    $return = $redis->$name($arguments[0], $arguments[1], $arguments[2]);
		    break;
		} 
		default:{
		    $return = call_user_func(array(&$redis, $name), $arguments);
		}
	    }
	    return $return;
	}
}