<?php

class APIHelper{

    /**
     * 说明： 生成app_id
     * 算法: 前缀(qz)+3位随机数字+13位随机字符串
     * 输出参数： String
     */
    public static function genAppId(){
        $prefix = "qz";
        $random_num_str = self::createRandomNumberStr();
        $random_suffix_str = self::createRandomStr(13);
        return $prefix.$random_num_str.$random_suffix_str;
    }

    /**
     * 说明： 生成app_secret
     * 算法：app_id + 6为随机盐(需要存储),通过MD5加密，再通过shal加密md5字符串得到sha1串，随机取其中任意一个，拼凑成40位长度的字符串
     * 输出参数： String
     */
    public static function genAppSecret($app_id, $salt){
        $code_len = 40;
        $str = $app_id+'_'+$salt;
        $md5 = md5($str);
        $sha1 = sha1($md5);
        $code = '';
        $str_len = strlen($sha1);
        for($i=0;$i<$code_len;$i++){
            $start = rand(0,$str_len);
            $code = $code.substr($sha1,$start,1);
        }
        return $code;
    }



    /**
     * 说明： 生成merchant_no
     * 算法： 随机的10位数字串
     * 输出参数： String
     */
    public static function genMerchantNo(){
        return self::createRandomNumberStr(10);
    }

    /**
     * 说明： 生成merchant_account
     * 算法： $merchant_no与@连接再接$merchant_no
     * 输出参数： String
     */
    public static function genMerchantAccount($merchant_no){
        return $merchant_no.'@'.$merchant_no;
    }

    /**
     * 说明： 生成merchant_password
     * 算法： 随机的6位字符串
     * 输出参数： String
     */
    public static function genMerchantPassword(){
        return self::createRandomStr(6);
    }

    /**
     * 说明： 生成access_token
     * 算法：app_secret + app_secret + 随机盐 +当前时间戳->Md5->des加密
     * 输出参数： String
     */
    public static function genAccessToken($app_id, $app_secret, $salt = '', $refresh=false){
        $time = time();
        $source_str = $app_id .'_'. $app_secret .'_' . $salt .'_' . $time;
        //生成refresh_token
        if($refresh){
            $source_str = '_refresh';
        }
        $md5 = md5($source_str);
//        $access_token = self::desEncrypt($md5);
//        $access_token = sha1($md5);
        $access_token = DESHelper::encrypt($md5);
        $access_token = str_replace("==","",$access_token);
        $access_token = str_replace("+","_",$access_token);
        $access_token = str_replace(" ","",$access_token);
        return $access_token;
    }

    /**
     * 说明： 生成签名
     * 将参数按照key的首字母进行升序排列组合成一个字符串,然后通过sha1加密算法得到签名
     * $special_keys 需要特殊处理的key集合，格式为array('key1','key2');
     * $special_keys_values 需要特殊处理的key集合的值，格式为array('key1'=>val1,'key2'=>val2);,必须与$special_keys中的key一一对应
     *
     * 输出参数： String
     */
    public static function genSignature($postParams, $paramsKeyArr, $specialKeys=array(), $specialKeysValues=array()){
        foreach($postParams as $key => $val){
            if(in_array($key,$paramsKeyArr)){
                $keyArr[] = $key;
            }
        }
        $signature = '';
        if(count($keyArr) > 0){
            sort($keyArr);
            foreach($keyArr as $key2 => $val2){
                if($specialKeys and in_array($val2,$specialKeys)){
                    $data[$val2] = $specialKeysValues[$val2];
                }else{
                    $data[$val2] = $postParams[$val2];
                }
            }

            $source_str_arr = '';
            foreach($data as $dk => $dv){
                $source_str_arr[] = $dk.'='.$dv;
            }
            $source_str = implode('&',$source_str_arr);
            $signature = sha1($source_str);
        }
        return $signature;
    }



    /**
     * 说明： 随机串算法
     * 输出参数： String
     */
    public static function createRandomStr($code_len=12){

        $BYTE_LEN = 512 ;

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if (function_exists('openssl_random_pseudo_bytes')) {
            $random_string = openssl_random_pseudo_bytes($BYTE_LEN);
        }else{
            $random_string = substr(str_shuffle(str_repeat($pool, 5)), 0, $BYTE_LEN);
        }

        $code = self::md5sha1EncryptStr($random_string,$code_len);
        return $code;
    }

    /**
     * 说明： 获取指定长度的随机数字
     * 输出参数： String
     */
    public static function createRandomNumberStr($len=3){
        $num_str = '';
        for($i=0;$i<$len;$i++){
            $random_num =rand(0,9);
            $num_str = $num_str . $random_num;
        }
        return $num_str;
    }


    /**
     * 说明： 通过md5和sha1加密然后截取指定长度的字符串
     * 输出参数： String
     */
    public static function md5sha1EncryptStr($str, $code_len){
        $md5 = md5($str);
        $sha1 = sha1($md5);
        $start = mt_rand(0, 25);
        $code = substr($sha1, $start, $code_len);
        return $code;
    }


    /**
     * 说明： DES加密
     * 输出参数： String
     */
    public static function desEncrypt($encrypt, $key=""){
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB),MCRYPT_RAND);
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt, MCRYPT_MODE_ECB, $iv);
        $encode = base64_encode($passcrypt);
        return $encode;
    }

    /**
     * 说明： DES解密
     * 输出参数： String
     */
    public static function desDecrypt($decrypt, $key="") {
        $decoded = base64_decode ( $decrypt );
        $iv = mcrypt_create_iv ( mcrypt_get_iv_size ( MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB ),MCRYPT_RAND);
        $decrypted = mcrypt_decrypt ( MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_ECB, $iv );
        return $decrypted;
    }


}
