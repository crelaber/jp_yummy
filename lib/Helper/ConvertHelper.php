<?php

/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/8/15
 * Time: 11:16
 */
class ConvertHelper
{
    public static function convertUnderline($str)
    {
        $str = preg_replace_callback('/([-_]+([a-z]{1}))/i',function($matches){
            return strtoupper($matches[2]);
        },$str);
        return $str;
    }

    /*
     * 驼峰转下划线
     */
    public static function humpToLine($str){
        $str = preg_replace_callback('/([A-Z]{1})/',function($matches){
            return '_'.strtolower($matches[0]);
        },$str);
        return $str;
    }

    public static function convertHump(array $data){
        $result = [];
        foreach ($data as $key => $item) {
            if (is_array($item) || is_object($item)) {
                $result[self::humpToLine($key)] = self::convertHump((array)$item);
            } else {
                $result[self::humpToLine($key)] = $item;
            }
        }
        return $result;
    }

}