<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/5/27
 * Time: 10:23
 */

class ErrorCode {

    public static function getErrMsg($key){
        $errList = self::getErrList();
        return $errList[$key] ? $errList[$key] : $errList['exception'];
    }

    protected static function getErrList(){
        return $error_code = [
            'success' => [
                'code' => 200 ,
                'msg' => 'SUCCESS'
            ],
            'fail' => [
                'code' => -1 ,
                'msg' => 'FAILED'
            ],
            'error' => [
                'code' => 500 ,
                'msg' => 'ERROR'
            ],
            'exception' => [
                'code' => 10000 ,
                'msg' => 'exception is occurd'
            ],

            'uid_invalid' => [
                'code' => 10004 ,
                'msg' => 'uid is invalid'
            ],


            'token_invalid' => [
                'code' => 20001 ,
                'msg' => 'token is invalid'
            ],
            'token_expired' => [
                'code' => 20002 ,
                'msg' => 'token is expired'
            ],

            'appid_invalid' => [
                'code' => 20004 ,
                'msg' => 'appid is invalid'
            ],

            'appsecret_invalid' => [
                'code' => 20005,
                'msg' => 'appsecret is invalid'
            ],
            'timestamp_invalid' => [
                'code' => 20006,
                'msg' => 'timestamp is invalid'
            ],

            'sign_invalid' => [
                'code' => 20007,
                'msg' => 'signature is invalid'
            ],

            'noncestr_invalid' => [
                'code' => 20008 ,
                'msg' => 'noncestr is invalid'
            ],

            'appid_secret_not_match' => [
                'code' => 20009,
                'msg' => 'appid and appsecret not match'
            ],
            'partner_uid_invalid' => [
                'code' => 20010,
                'msg' => 'partner_uid is invalid'
            ],
            'partner_uname_invalid' => [
                'code' => 20011,
                'msg' => 'partner uname is invalid'
            ],
            'health_package_type_invalid' => [
                'code' => 20012,
                'msg' => 'package type is invalid'
            ],
            'health_package_sign_invalid' => [
                'code' => 20013,
                'msg' => 'signature is invalid'
            ],
        ];
    }
}

