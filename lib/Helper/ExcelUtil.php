<?php


/**
 * PHPExcel
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 */
class ExcelUtil
{

	/*
    * 将excel转换为数组 by aibhsc
    * */
	public static function formatQuestionLibData($filePath='', $sheet=0){
		if(empty($filePath) or !file_exists($filePath)){die('file not exists');}
		$PHPReader = new PHPExcel_Reader_Excel2007();        //建立reader对象
		if(!$PHPReader->canRead($filePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
			if(!$PHPReader->canRead($filePath)){
				echo 'no Excel';
				return ;
			}
		}
		$PHPExcel = $PHPReader->load($filePath);        //建立excel对象
		$currentSheet = $PHPExcel->getSheet($sheet);        //**读取excel文件中的指定工作表*/
		$allColumn = $currentSheet->getHighestColumn();        //**取得最大的列号*/
		$allRow = $currentSheet->getHighestRow();        //**取得一共有多少行*/
		$data = array();
		$rows = array();
		$divided = 14;
        $reminder = 0;
        $times = 0;
        $count = 0;
        $index = 1;
		for($rowIndex=2;$rowIndex<=$allRow;$rowIndex++){        //循环读取每个单元格的内容。注意行从1开始，列从A开始
			for($colIndex='A';$colIndex<=$allColumn;$colIndex++){
				$addr = $colIndex.$rowIndex;
				$cell = $currentSheet->getCell($addr)->getValue();
				if($cell instanceof PHPExcel_RichText){ //富文本转换字符串
					$cell = $cell->__toString();
				}
				$value = '';
				if($colIndex == 'A'){
					$questionId = $cell ? intval($cell) : $value;
					$reminder = $questionId % $divided;
                    $reminder = ($questionId &&  $reminder == 0)?$divided:$reminder;
                    $times = ($questionId - $reminder)/$divided;
//					$data["question_id"] = $reminder ;
					$data["question_id"] = 1 ;
				}

				if($colIndex == 'B'){
					$data["question"] = $cell ? $cell : $value;
				}
				if($colIndex == 'C'){
					$data["answers"] = $cell ? $cell : $value;
					if($cell and $cell == "yes"){
						$data["answers"] ="[1]";
					}else{
						$data["answers"] ="[0]";
					}
				}

				$data["score"] = 10;
				$data["options"] = '["错", "对"]';

				if($colIndex == 'D'){
					if($cell){
						$data["text"] = $cell;
					}else{
						$data["text"] = $value;
					}
				}
//				$data[$rowIndex][$colIndex] = $cell;
			}
            if(!isset($obj["day".$index])){
                $obj["day".$index] = [];
            }
            array_push($obj["day".$index],$data);
            $index++;
            $index = (($index -1) > 0 && ($index-1)%$divided == 0) ?  1 : $index%$divided;
            $index = $index == 0 ? $divided:$index;
            $objCount = ($rowIndex - 1);
            if($objCount > 0 && ($objCount % $divided == 0)){
                $rows[$count] = $obj;
                $count++;
                $obj = [];
            }
		}
		return $rows;
	}


	/*
    * 将excel转换为数组 by aibhsc
    * */
	public static function formatPicLibData($filePath='', $sheet=0){
		if(empty($filePath) or !file_exists($filePath)){die('file not exists');}
		$PHPReader = new PHPExcel_Reader_Excel2007();        //建立reader对象
		if(!$PHPReader->canRead($filePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
			if(!$PHPReader->canRead($filePath)){
				echo 'no Excel';
				return ;
			}
		}
		$PHPExcel = $PHPReader->load($filePath);        //建立excel对象
		$currentSheet = $PHPExcel->getSheet($sheet);        //**读取excel文件中的指定工作表*/
		$allColumn = $currentSheet->getHighestColumn();        //**取得最大的列号*/
		$allRow = $currentSheet->getHighestRow();        //**取得一共有多少行*/
		$data = array();
		$rows = array();
		for($rowIndex=2;$rowIndex<=$allRow;$rowIndex++){        //循环读取每个单元格的内容。注意行从1开始，列从A开始
			$option = array();
			for($colIndex='A';$colIndex<=$allColumn;$colIndex++){

				$addr = $colIndex.$rowIndex;
				$cell = $currentSheet->getCell($addr)->getValue();
				if($cell instanceof PHPExcel_RichText){ //富文本转换字符串
					$cell = $cell->__toString();
				}
				$value = '';

				if($colIndex == 'A'){
					$data['question_id'] = $cell ? intval($cell) : $value;
				}
				if($colIndex == 'B'){
					$option[] = $cell ? $cell : $value;
				}
				if($colIndex == 'C'){
					$option[] = $cell ? $cell : $value;
				}
				if($colIndex == 'D'){
					$option[] = $cell ? $cell : $value;
				}
				if($colIndex == 'E'){
					$option[] = $cell ? $cell : $value;
				}
				$data['options']= $option;
				$data['score'] = 10;

				if($colIndex == 'F'){
					if($cell == 'A'){
						$data['answers'] ='[0]';
					}else if($cell == 'B'){
						$data['answers'] ='[1]';
					}else if($cell == 'C'){
						$data['answers'] ='[2]';
					}else if($cell == 'D'){
						$data['answers'] ='[3]';
					}
				}
				if($colIndex == 'G'){
					$result = $cell ? $cell : $value;
					$data['question_img'] = $result.'.jpg';
				}
			}
			$rows[] = $data;
		}
		return $rows;
	}

	/**
	 * 解析成都的题库
	 */
	public static function convertChengdouExcel($filePath='', $sheet=0){
		if(empty($filePath) or !file_exists($filePath)){die('file not exists');}
		$PHPReader = new PHPExcel_Reader_Excel2007();        //建立reader对象
		if(!$PHPReader->canRead($filePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
			if(!$PHPReader->canRead($filePath)){
				echo 'no Excel';
				return ;
			}
		}
		$PHPExcel = $PHPReader->load($filePath);        //建立excel对象
		$currentSheet = $PHPExcel->getSheet($sheet);        //**读取excel文件中的指定工作表*/
		$allColumn = $currentSheet->getHighestColumn();        //**取得最大的列号*/
		$allRow = $currentSheet->getHighestRow();        //**取得一共有多少行*/
		$data = array();
		$rows = array();
		for($rowIndex=3;$rowIndex<=$allRow;$rowIndex++){        //循环读取每个单元格的内容。注意行从1开始，列从A开始
			$option_data = array();
			for($colIndex='A';$colIndex<=$allColumn;$colIndex++){

				$addr = $colIndex.$rowIndex;
				$cell = $currentSheet->getCell($addr)->getValue();
				if($cell instanceof PHPExcel_RichText){ //富文本转换字符串
					$cell = $cell->__toString();
				}
				$value = '';
				if($colIndex == 'A'){
					$data['question_id'] = $cell ? intval($cell) : $value;
				}
				//cell B 为干扰因素
				if($colIndex == 'B'){
					$option_data = self::alynasisOtherOptions($cell);
				}

				//cell D为正确答案
				if($colIndex == 'D'){
					$data['answer_'.$option_data['right_answer']] = $cell;
					$data['right_answer_text'] = $cell;
				}
				//cell E为图片
				if($colIndex == 'E'){
					$data['answer_img']  = $cell;
				}

				//cell C为答案描述
				if($colIndex == 'C'){
					$data['text'] = $cell;
				}

				foreach($option_data as $key => $val){
					$data[$key] = $val;
				}


			}
			$rows[] = $data;
		}
		return $rows;
	}

	/*
	 * 返回的结构
	 * {"right_answer_index":2,"right_answer":"C","answer_A":"\u7403\u830e\u7518\u84dd","answer_B":"\u7ea2\u841d\u535c","answer_D":"\u9752\u841d\u535c"}
	 *
	 * */
	public static function alynasisOtherOptions($cell_value){
		$options = explode("、",$cell_value);
		$len = count($options);
		$len = count($options);
		$index = rand(0,$len-1);
		$other_option_len = 3;
		$index_arr = array();
		for($i=0;$i<$other_option_len;$i++){
			$index = rand(0,$len-1);
			while(in_array($index,$index_arr)){
				$index = rand(0,$len-1);
			}

			$index_arr[] =  $index;
		}

		$answers = array('A','B','C','D');
		$right_answer_index = rand(0,3);

		$options_text_arr = array();

		$option_index_len = count($index_arr);
		for($n=0;$n<$option_index_len;$n++){
			$options_text_arr[] = $options[$index_arr[$n]];
		}

		$data = array();
		$data['right_answer_index']=$right_answer_index;
		$data['right_answer']=$answers[$right_answer_index];


		$curr_answer = $answers;
		$clen = count($answers);
		for($j=0;$j<$clen;$j++){
			if($curr_answer[$j] == $data['right_answer']){
				array_splice($curr_answer,$j,1);
				break;
			}
		}


		$opt_len = count($curr_answer);
		for($k=0;$k<$opt_len;$k++){
			$data['answer_'.$curr_answer[$k]] = $options_text_arr[$k];
		}
		return $data;
	}

	/**
	 * 生成pic对应的excel文件
	 */
	public static function writePicLibExcel($data, $outputdir, $file_name = 'pic_lib_data.xlsx'){
		$resultPHPExcel = new PHPExcel();
		//设值
		$resultPHPExcel->getActiveSheet()->setCellValue('A1', '问题序号');
		$resultPHPExcel->getActiveSheet()->setCellValue('B1', '选项A');
		$resultPHPExcel->getActiveSheet()->setCellValue('C1', '选项B');
		$resultPHPExcel->getActiveSheet()->setCellValue('D1', '选项C');
		$resultPHPExcel->getActiveSheet()->setCellValue('E1', '选项D');
		$resultPHPExcel->getActiveSheet()->setCellValue('F1', '答案');
		$resultPHPExcel->getActiveSheet()->setCellValue('G1', '图片文件名');
		$resultPHPExcel->getActiveSheet()->setCellValue('H1', '答案描述');
		$i = 2;
		foreach($data as $key => $item){
			$resultPHPExcel->getActiveSheet()->setCellValue('A' . $i, $item['question_id']);
			$resultPHPExcel->getActiveSheet()->setCellValue('B' . $i, $item['answer_A']);
			$resultPHPExcel->getActiveSheet()->setCellValue('C' . $i, $item['answer_B']);
			$resultPHPExcel->getActiveSheet()->setCellValue('D' . $i, $item['answer_C']);
			$resultPHPExcel->getActiveSheet()->setCellValue('E' . $i, $item['answer_D']);
			$resultPHPExcel->getActiveSheet()->setCellValue('F' . $i, $item['right_answer']);
			$resultPHPExcel->getActiveSheet()->setCellValue('G' . $i, $item['answer_img']);
			$resultPHPExcel->getActiveSheet()->setCellValue('H' . $i, $item['text']);
			$i ++;
		}

		$outputFileName = $outputdir . $file_name;
		$xlsWriter = new PHPExcel_Writer_Excel5($resultPHPExcel);
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header('Content-Disposition:inline;filename="'.$outputFileName.'"');
		header("Content-Transfer-Encoding: binary");
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: no-cache");
		$xlsWriter->save( $outputFileName );
		echo 'success';
	}

}
