<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/5/27
 * Time: 10:40
 */
class HttpHelper{

    public static function curlRequest($url, $postData = array(), $launch = 'post', $contentType = 'text/html',$headers = null,$decodeParam = false)
    {
        $result = "";
        try {
            $header = array("Content-Type:" . $contentType . ";charset=utf-8");
            if($headers){
                $header = $headers;
            }

            if (!empty($_SERVER['HTTP_USER_AGENT'])) {        //是否有user_agent信息
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
            }
            $cur = curl_init();
            curl_setopt($cur, CURLOPT_URL, $url);
            curl_setopt($cur, CURLOPT_HEADER, 0);
            curl_setopt($cur, CURLOPT_HTTPHEADER, $header);
            curl_setopt($cur, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($cur, CURLOPT_TIMEOUT, 30);
            //https
            curl_setopt($cur, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($cur, CURLOPT_SSL_VERIFYHOST, FALSE);
            if (isset($user_agent)) {
                curl_setopt($cur, CURLOPT_USERAGENT, $user_agent);
            }
            curl_setopt($cur, CURLOPT_ENCODING, 'gzip');
            if (is_array($postData)) {
                if ($postData && count($postData) > 0) {
                    $params = http_build_query($postData);
                    if($decodeParam){
                        $params = urldecode($params);
                    }

                    if ($launch == 'get') {        //发送方式选择
                        curl_setopt($cur, CURLOPT_HTTPGET, $params);
                    } else {
                        curl_setopt($cur, CURLOPT_POST, true);
                        curl_setopt($cur, CURLOPT_POSTFIELDS, $params);
                    }
                }
            } else {
                if (!empty($postData)) {
                    $params = $postData;
                    if ($launch == 'post') {
                        curl_setopt($cur, CURLOPT_POST, true);
                        curl_setopt($cur, CURLOPT_POSTFIELDS, $params);
                    }
                }
            }
            $result = curl_exec($cur);
            curl_close($cur);
        } catch (Exception $e) {

        }
        return $result;
    }


    public static function sendVerifyCode($phone){
        global $config;
        $url = $config->sms_server_url.$phone;
        $data = self::curlRequest($url);
        return $data['code'] ? $data['code'] : "";

    }


    public static function unicodeDecode($name){
        $json = '{"str":"'.$name.'"}';
        $arr = json_decode($json,true);
        if(empty($arr)) return '';
        return $arr['str'];
    }


    /**
     * 获取远程异步实例
     */
    public static function getAsynWeixinInstance(){
        global $config;
        $path = $config->workman_jsonrpc_dir.'/Applications/JsonRpc/Clients/RpcClient.php';
        include_once $path;
        $address_array = array('tcp://0.0.0.0:23469');
        RpcClient::config($address_array);
        $weixin = RpcClient::instance('Weixin');
        return $weixin;
    }

    /**
     * 异步发送http请求
     */
    public static function sendAsynRequest($url,$method = 'get',$params = []  ){
        $client = self::getAsynWeixinInstance();
        $params = [
            'url' => $url,
            'params' => $params,
            'method' => $method,
        ];
        $client->asend_sendAsynHttpRequest($params);
    }


    /**
     * 发送HTTP请求方法，目前只支持CURL发送请求
     * @param  string $url    请求URL
     * @param  array  $params 请求参数
     * @param  string $method 请求方法GET/POST
     * @return array  $data   响应数据
     */
    public static function doRequest($url, $params, $method = 'GET', $header = array(), $multi = false) {
        $opts = array(
            CURLOPT_TIMEOUT => 30,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => $header
        );
        /* 根据请求类型设置特定参数 */
        switch(strtoupper($method)) {
            case 'GET' :
                $opts[CURLOPT_URL] = $url . '&' . http_build_query($params);
                break;
            case 'POST' :
                //判断是否传输文件
                $params = $multi ? $params : http_build_query($params);
                $params = urldecode($params);
                $opts[CURLOPT_URL] = $url;

                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;
            default :
                throw new Exception('不支持的请求方式！');
        }

        /* 初始化并执行curl请求 */
        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        $data = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($error)
            throw new Exception('请求发生错误：' . $error);
        return $data;
    }

}