<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017/2/7
 * Time: 14:06
 */

class TokenHelper
{
    /**
     * 生成token的方法
     */
    public static function getTokenFromApi($appid,$appsecret,$noncestr,$timestamp,$signature){
        global $config;
        $queryData = [
            'appid' => $appid,
            'appsecret' => $appsecret,
            'noncestr' => $noncestr,
            'timestamp' => $timestamp,
            'signature' => $signature,
        ];
        $apiPath = $config->generate_token_api_path;
        $result = self::getTokenApiResult($apiPath,$queryData);
        return $result;
    }

    /**
     * 验证token的方法
     */
    public static function validateTokenFromApi($token){
        global $config;
        $queryData = [
            'token' => $token
        ];
        $apiPath = $config->valid_token_api_path;
        return self::getTokenApiResult($apiPath,$queryData);
    }


    /**
     * 生成签名接口
     */
    public static function generateSignFromApi($appid,$appsecret,$noncestr,$timestamp){
        $queryData = [
            'appid' => $appid,
            'appsecret' => $appsecret,
            'noncestr' => $noncestr,
            'timestamp' => $timestamp,
        ];
        $apiPath = '/api/developer/valid/sign';
        return self::getTokenApiResult($apiPath,$queryData);
    }

    /**
     * 调用api接口获取对应的结果
     */
    protected static function getTokenApiResult($apiPath,$queryData){
        global $config;
        $url = $config->token_api_url ? $config->token_api_url : 'http://sp.qiezilife.com';
        $params = http_build_query($queryData);
        $url = $url.$apiPath .'?'. $params;
        $response = HttpHelper::curlRequest($url);
        $result = json_decode($response,true);
        return $result;
    }


    /**
     * 生成体检套餐签名
     */
    public static function generateHealthPackageDetailSign($token,$uid,$uname,$package){
        global $config;
//        $uname  = urldecode($uname);
        $signSecret = $config->health_package_sign_secret;
        $str = $token . $uid .$signSecret . $uname.$package;
        error_log('sign str======>'.$str);
        $encode = md5($str);
        return $encode;
    }



    /**
     * 获取康康的下单结果，如果请求成功，则返回数组，否则返回空数组
     *   "isKKPackage": 1,
     *   "cardNum": "051871685",
     *   "cardPwd": "138997",
     *   "orderId": "93183",
     *   "thirdNum": "89390",
     *   "onlyNum": "89390"
     */
    public static function getKKCardOrderResult($orderId,$phone,$packageCode,$sex,$married){
        global $config;
        $appid = $config->kangkang_appid;
        $appkey = $config->kangkang_appkey;
        $onlyCode = $config->kangkang_onlycode;
        $requestData = [
            'action' => 'saveKKorder',
            'appKey' => $appkey,
            'appId' => $appid,
            'onlyNum' => $orderId,
            'sex' => $sex,
            'married' => $married,
            'contactTel' => $phone,
            'packageCode' => $packageCode,
        ];
        $signature = self::getKKSignature($requestData);
        $requestData['onlyCode'] = $onlyCode;
        $requestData['signature'] = $signature;
        $requestUrl = $config->kangkang_order_url;
//        $response = HttpHelper::curlRequest($requestUrl,$requestData,'get');
        $params = http_build_query($requestData);
        $url = $requestUrl.'?'. $params;
        error_log('kk request url====>'.$url);
        error_log('kk request param====>'.json_encode($requestData));
        $response = HttpHelper::curlRequest($url);
        error_log('kk response====>'.$response);
        $result = json_decode($response,true);
        $code = intval($result['code']);
        $data['errcode'] = 200;
        $data['msg'] = 'SUCCESS';
        if($code == 10000){
            $list = $result['list'];
            if($list && count($list) > 0){
                $data['data'] = $list[0];
            }
        }else{
            $data['errcode'] = -1;
            $data['msg'] = '状态码：'.$result['code'].'，'.$result['msg'];
            $data['data'] = [];
        }
        return $data;
    }

    /**
     * 调用康康的接口生成卡号
     */
    public static function invokeKKCard($orderInfo){
        global $config;
        $domain = $config->domain;
        $url = '?/vHealthPackage/generateKKCard/';
        $midfix = $config->zy_order_midfix;
        $param = [
            'orderId' => str_replace($midfix,'',$orderInfo['out_trade_no']),
            'phone' => $orderInfo['phone'],
            'packageCode' => $orderInfo['package_code'],
            'sex' => $orderInfo['user_sex'],
            'married' => $orderInfo['user_married_status'],
        ];
        $paramStr = http_build_query($param);
        $url = $domain.$url . $paramStr;
        $response = HttpHelper::curlRequest($url);
        $result = json_decode($response,true);
        $cardInfo = [];
        if($result['errcode'] == 200){
            $data = $result['data'];
            $cardInfo['errcode'] = 200;
            $cardInfo['msg'] = 'SUCCESS';
            $cardInfo['card_no'] = $data['cardNum'];
            $cardInfo['card_secret'] = $data['cardPwd'];
            $time = time();
            $cardInfo['add_time'] =$time;
            //有效期为一年
            $validTime = $time + 60*60*24*365;
            $cardInfo['valid_time'] = $validTime;
        }else{
            $cardInfo['errcode'] = -1;
            $cardInfo['msg'] = $result['msg'];
        }
        return $cardInfo;
    }

    /**
     * 康康的数据签名算法
     */
    public static function getKKSignature($requestData){
        $sign = '';
        if($requestData && count($requestData) > 0){
            foreach ($requestData as $val){
                $sign .= $val;
            }
            $sign = md5($sign);
        }
        return $sign;
    }

    public function buildPackageItemImg($package){

    }


    /**
     * 中英的回调接口
     * @param $partnerUid 合作方用户编号
     * @param $cardNo 康康的卡号
     * @param $cardSecret 康康的卡密
     * @param $validTime 卡号的有效期，时间格式为2017-06-05
     * @param $packageName 套餐名称
     * errcode 为0时候成功，其他都为失败 ， errmsg 为错误信息
     * @return array
     */
    public static function invokeZyApi($partnerUid, $cardNo, $cardSecret,$validTime,$packageName){
        global $config;
        //回调地址
        $url = $config->zy_callback_api_domain.$config->zy_callback_api_path;
        $time = self::getMillisecond();
        $actionName = $packageName;
        $prop = 1;
        $src = 'wechat';
//        '/WX/interface/addWxActivityPrize.do?cust_sign=__UID__&act_name=__ACTION_NAME__&prize_num=__CARD_NO__&prize_pwd=__CARD_SECRET__&prop=__PROP__&timestamp=__TIME_STAMP__&valid_time=__CARD_VALID_TIME__&src=__SRC__&sign=__SIGN__';
        $url = str_replace('__UID__',$partnerUid,$url);
        $url = str_replace('__ACTION_NAME__',$actionName,$url);
        $url = str_replace('__CARD_NO__',$cardNo,$url);
        $url = str_replace('__CARD_SECRET__',$cardSecret,$url);
        $url = str_replace('__PROP__',$prop,$url);
        $url = str_replace('__TIME_STAMP__',$time,$url);
        $url = str_replace('__CARD_VALID_TIME__',$validTime,$url);
        $url = str_replace('__SRC__',$src,$url);
        //签名的加密秘钥
        $signEncodeSecret = $config->zy_callback_api_encode_secret;
        $sign = self::getZyApiSign($actionName,$cardSecret,$cardNo,$partnerUid,$prop,$time,$signEncodeSecret,$src,$validTime);
        $url = str_replace('__SIGN__',$sign,$url);
        error_log('callback api url ====>'.$url);
        $response = HttpHelper::curlRequest($url);
        $result = json_decode($response,true);
        return $result;
    }

    /**
     * 中英的参数签名接口
     * sign =Md5（act_name+cust_sign+ prize_pwd + prize_num +prop +src+ valid_time+timestamp+key）
     */
    private static function getZyApiSign($actionName,$cardSecret,$cardNo,$partnerUid,$prop,$timestamp,$encodeKey,$src,$validTime){
        $signTxt = $actionName.$partnerUid.$cardSecret.$cardNo.$prop.$src.$validTime.$timestamp.$encodeKey;
        $sign = md5($signTxt);
        error_log('sign text ===>'.$signTxt);
        error_log('sign ===>'.$sign);
        return $sign;
    }


    /**
     * 获取系统的毫秒时间
     */
    public static function  getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }


    /**
     * 验证手机号是否正确
     * @author honfei
     * @param number $mobile
     * @return boolean
     */
    public static function isMobile($mobile) {
        if (!is_numeric($mobile)) {
            return false;
        }
        return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
    }

}