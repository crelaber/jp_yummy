<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2017/2/7
 * Time: 14:06
 */

class ZyHealthClockHelper
{

    const TPL_MSG_MARK_INTERRUPT = 'A';
    const TPL_MSG_MARK_FINISH_PUNCH = 'B';
    const TPL_MSG_MARK_PUNCH_AWARD = 'C';

    /**
     * 打卡中断提醒
     * @param $uid 用户编号
     * @param $dayIndex 待打卡日期
     * @param $isHighLevel 是否为高级打卡
     * @return array
     */
    public static function sendInterruptRemindTplMsg($uid,$dayIndex,$isHighLevel = false){
        global $config;
        $paramObj = self::buildBaseTplParam($uid,$isHighLevel,self::TPL_MSG_MARK_INTERRUPT,$config);
        $paramObj['days'] = $dayIndex;
        $key = $config->zy_tpl_msg_encode_key;
        $signContent = urldecode($paramObj['planName']).$paramObj['days'].urldecode($paramObj['url']).$paramObj['timestamp'].$key;
        error_log('sendInterruptTplMsg sign content is =========> '.$signContent);
        $sign = md5($signContent);
        error_log(' sign is =========> '.$signContent);
        $paramObj['sign'] = $sign;
        error_log(' business param =========> '.json_encode($paramObj));
        $result = self::invokeZySendTplMsg($paramObj);
        $paramObj['signContent'] = $signContent ;
        $paramObj['response'] = $result ;
        return $paramObj;
    }


    /**
     * 完成打卡提醒模板通知
     * @param $uid 用户编号
     * @param $userName 用户昵称
     * @param $isHighLevel 是否为高级打卡
     * @return array
     */
    public static function sendFinishPunchRemindTplMsg($uid,$userName,$isHighLevel = false){
        global $config;
        $paramObj = self::buildBaseTplParam($uid,$isHighLevel,self::TPL_MSG_MARK_FINISH_PUNCH,$config);
        $userName = urlencode($userName);
        $paramObj['userName'] = $userName;
        //限购日期，如果当前日期超过了限定的开放时间，则自动的跳到下个月
//        $openDay = $config->exchange_open_day;
        $openDay = 15;
        $currentDay = intval(date('d'));
        if($currentDay >= $openDay){
            $month = strtotime('+1 month');
            $month = date('m月',$month);
        }else{
            $month = date('m月');
        }
        $limitedTime = $month.$openDay.'日';

        $limitedTime = urlencode($limitedTime);
        $paramObj['limitedTime'] = $limitedTime;

        $key = $config->zy_tpl_msg_encode_key;
        $signContent = urldecode($paramObj['planName']).urldecode($paramObj['url']).urldecode($paramObj['limitedTime']).urldecode($paramObj['userName']).$paramObj['timestamp'].$key;
        error_log('sendFinishPunchTplMsg sign content is =========> '.$signContent);
        $sign = md5($signContent);
        error_log(' sign is =========> '.$signContent);
        $paramObj['sign'] = $sign;
        error_log(' business param =========> '.json_encode($paramObj));
        $result = self::invokeZySendTplMsg($paramObj);
        $paramObj['signContent'] = $signContent ;
        $paramObj['response'] = $result ;
        return $paramObj;
    }

    /**
     * 领取奖励提醒模板通知
     * @param $uid 用户编号
     * @param $isHighLevel 是否为高级打卡
     * @return array
     */
    public static function sendAwardRemindTplMsg($uid,$isHighLevel = false){
        global $config;
        $paramObj = self::buildBaseTplParam($uid,$isHighLevel,self::TPL_MSG_MARK_PUNCH_AWARD,$config);

        $key = $config->zy_tpl_msg_encode_key;
        $signContent = urldecode($paramObj['planName']).urldecode($paramObj['url']).$paramObj['timestamp'].$key;
        error_log('sendAwardRemindTplMsg sign content is =========> '.$signContent);
        $sign = md5($signContent);
        error_log(' sign is =========> '.$signContent);
        $paramObj['sign'] = $sign;
        error_log(' business param =========> '.json_encode($paramObj));
        $result = self::invokeZySendTplMsg($paramObj);
        $paramObj['signContent'] = $signContent ;
        $paramObj['response'] = $result ;
        return $paramObj;
    }


    public static function sendExchangeData($uid, $prizeName, $prizeGetTime, $validTime, $prizeDesc = '精彩礼品', $src = '健康打卡', $prizeNo = '', $prizeSecret = '', $prizeGrade = 'vip', $isShow = '1'){
        ///WX/interface/addWxActivityPrize.do?cust_sign=__UID__&rec_time=__REC_TIME__&act_name=__ACTION_NAME__&act_desc=__ACT_DESC__&prize_num=__PRIZE_NO__&prize_pwd=__PRIZE_PWD__&valid_time=__VALID_TIME__&src=__SRC__&prop=__PIRZE_TYPE__&timestamp=__TIMESTAMP__&sign=__SIGN__&prize_grade=__PRIZE_GRADE__&is_show=__IS_SHOW__
        global $config;
        //回调地址
        $url = $config->zy_callback_api_domain.$config->zy_exchage_api_path;
        $time = TokenHelper::getMillisecond();
        $prop = 0;
        $actionName = urlencode($prizeName);
        $validTime = is_numeric($validTime)?date('Y-m-d',$validTime):'';
        $recTime = urlencode(date('Y-m-d',$prizeGetTime));
        $url = str_replace('__UID__',$uid,$url);
        $url = str_replace('__REC_TIME__',$recTime,$url);
        $url = str_replace('__ACTION_NAME__',$actionName,$url);
        $url = str_replace('__ACT_DESC__',urlencode($prizeDesc),$url);
        $url = str_replace('__PRIZE_NO__',$prizeNo,$url);
        $url = str_replace('__PRIZE_PWD__',$prizeSecret,$url);
        $url = str_replace('__PIRZE_TYPE__',$prop,$url);
        $url = str_replace('__TIMESTAMP__',$time,$url);
        $url = str_replace('__VALID_TIME__',$validTime,$url);
        $url = str_replace('__SRC__',$src,$url);
        $url = str_replace('__PRIZE_GRADE__',$prizeGrade,$url);
        $url = str_replace('__IS_SHOW__',$isShow,$url);
        //签名的加密秘钥
        $signEncodeSecret = $config->zy_callback_api_encode_secret;
        $sign = self::getExchangeSign($prizeName, $prizeDesc, $prizeGrade,$prizeSecret,$prizeNo,$uid,$prop,$recTime,$src,$validTime,$time,$signEncodeSecret);
        $url = str_replace('__SIGN__',$sign,$url);
        error_log('callback api url ====>'.$url);
        $response = HttpHelper::curlRequest($url);
        $result = json_decode($response,true);
        return $result;

    }

    /**
     * 中英的参数签名接口
     * sign =Md5（act_name+cust_sign+ prize_pwd + prize_num +prop +src+ valid_time+timestamp+key）
     */
    private static function getExchangeSign($actionName, $actionDesc, $prizeGrade,$prizePwd,$prizeNo,$uid,$prop,$recTime,$src,$validTime,$timestamp,$encodeKey){
//        if($validTime){
//            $signTxt = $actionName.$actionDesc.$prizeGrade.$uid.$prop.$recTime.$src.$validTime.$timestamp.$encodeKey;
//        }else{
//            $signTxt = $actionName.$actionDesc.$prizeGrade.$uid.$prop.$recTime.$src.$timestamp.$encodeKey;
//        }

        $signTxt = $actionName.$actionDesc.$prizeGrade.$prizePwd.$prizeNo.$uid.$prop.$recTime.$src.$validTime.$timestamp.$encodeKey;

        $sign = md5($signTxt);
        error_log('sign text ===>'.$signTxt);
        error_log('sign ===>'.$sign);
        return $sign;
    }



    /**
     * 构建基础的模板消息参数
     * @param $uid 用户编号
     * @param $isHighLevel 是否为高级打卡
     * @param $mark 模板的类型
     * @param $config 配置文件
     * @return array
     */
    public static function buildBaseTplParam($uid,$isHighLevel,$mark,$config){
        if($isHighLevel){
            $planName = $config->high_level_name;
            $page = 'highLevel';
        }else{
            $planName = $config->low_level_name;
            $page = 'lowLevel';
        }
        $url = self::buildMidRedirectUrl($page,'zy_group');
        $paramObj = self::getZyBaseTplParam($mark,$planName,$url,$uid);
        return $paramObj;
    }


    /**
     * 获取中英发送模板消息的基础参数
     */
    public static function getZyBaseTplParam($mark,$planName,$url,$openid){
        return [
            'mark' => $mark,
            'planName' => urlencode($planName),
            'url' => urlencode($url),
            'openid' => $openid,
            'timestamp' => TokenHelper::getMillisecond()
        ];
    }


    /**
     * 调用中英的发送隔膜板消息的接口
     */
    public static function invokeZySendTplMsg($paramObj){
        global $config;
        $baseUrl = $config->zy_tpl_msg_action;
        $param = http_build_query($paramObj);
        $url = $baseUrl.$param;
        $response = HttpHelper::curlRequest($url);
        return $response;
    }

    /**
     * 检测是否是客户的接口
     */
    public static function checkZyCustomer($openid){
        global $config;
        $requestUrl = $config->zy_hch_validate_customer_api;
        $appid = $config->zy_hch_validate_customer_appid;
        $token = $config->zy_hch_validate_customer_token;
        $paramObj = [
            'request' => [
                'customerPKType' => 'OPENID',
                'customerPK' => $openid,
            ]
        ];
        $param = json_encode($paramObj);
        $timestamp = TokenHelper::getMillisecond();
        $randomStr = strtoupper(md5($timestamp));
        $randomStr = substr($randomStr,0,10);
        $signText = $token.$timestamp.$appid.$randomStr.$param;
        $sign = strtoupper(md5($signText));
        $headers = [
            'Appid:'.$appid,
            'Timestamp:'.$timestamp,
            'Nonce:'.$randomStr,
            'Signature:'.$sign,
            'Content-Type:application/json',
        ];
        $response = HttpHelper::doRequest($requestUrl, $param,'POST',$headers,true);
        $custNo = self::getCustNo($response);
        $result = [
            'cust_no' => $custNo,
            'response' => $response,
        ];
        return $result;
    }

    /**
     * 获取客户编号
     */
    public static function getCustNo($response){
        $custNo = "";
        $result = json_decode($response,true);
        if(isset($result['response'])){
            $response = $result['response'];
            $custNo = isset($response['pCustno']) ? $response['pCustno'] : "";
        }
        return $custNo;
    }


    /**
     * 构建中英的中转地址
     */
    public static function buildMidRedirectUrl($page,$comeFrom = 'qiezi'){
        global $config;
        $baseUrl = $config->zy_share_url;
        $paramObj = [
            'come_from' => $comeFrom,
            'page' => $page,
        ];
        $param = http_build_query($paramObj);
        $url = $baseUrl.$param;
        return $url;
    }

    public static function getRandomBaseChallengeIndex(){
        global $config;
        $numStr = $config->challenge_index_base_num;
        $numArr = explode(",",$numStr);
        $num = mt_rand($numArr[0],$numArr[1]);
        return $num;
    }
}