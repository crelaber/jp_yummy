<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Carts extends Model {

    const MANT_ADD = '+';
    const MANT_DIS = '-';
	
	
	
	/**
	 *说明： 修改购物车的商品
	 */
	 public function update_cart_product($uid,$product_id,$quantity = 1,$spec_id,$provider_id){
	 
	     	 
	       $this->remove_product($uid,$product_id,$spec_id);
	     	     
	 	   return   $this->Dao->insert("shop_cart", '`uid`,`product_id`,`product_quantity`,`add_time`,`spec_id`,`provider_id`')->values(array($uid, $product_id, $quantity, time(),$spec_id,$provider_id))->exec();
	 
	 }



    /**
     *
     * 获取购物车供应商的数据
     */
    public function getCartProductSupplier($uid){


        return $this->Dao->select ()->from ( 'shop_cart' )->where ( 'uid', $uid )->groupby( 'provider_id' )->exec (false);


    }

	 
	 /**
	 * 检查购物是否存在某个商品
	 */
	 public function checkProductExt($product_id,$uid,$spec_id ){
	 
	    $ret = $this->Db->query("SELECT COUNT(*) AS count FROM `shop_cart` WHERE `product_id` = '$product_id'  and `spec_id` = $spec_id and `uid` = $uid;");
        return $ret[0]['count'] > 0;
	 }
	
	/**
	*删除购物车
	*/
	public function del_cart($uid){
	
	    return $this->Dao->delete()->from(TABLE_SHOP_CART)->where("uid=" . $uid)->exec();
	
	}
	
	/**
	 *根据商品id删除购物车中的商品
	 */
	public function del_cart_product_by_product_id($product_id){
		return $this->Dao->delete()->from(TABLE_SHOP_CART)->where("product_id=" . $product_id)->exec();
	}
	
	
	/**
	 * 说明： 删除一个商品
	 * 输入参数：用户标识，商品标识
	 * 输出参数： true/false
	 */
	public function remove_product($uid, $product_id,$spec_id){
		return $this->Dao->delete()->from(TABLE_SHOP_CART)->where("uid=" . $uid)->aw('product_id='.$product_id)->aw('spec_id='.$spec_id)->exec();
	}
	
	/**
	 * 说明： 更改商品数量
	 * 输入参数：用户标识，商品标识，数量
	 * 输出参数： true/false
	 */
	public function change_product_quantity($uid,$product_id,$quantity){
		return $this->Dao->update(TABLE_SHOP_CART)->set(array(
				'quantity' => $quantity
		))->where("uid=" . $uid)->aw('product_id='.$product_id)->exec();
	}
	
	/**
	 * 说明： 设置商品关联的优惠券
	 * 输入参数：用户标识，商品标识，优惠券标识
	 * 输出参数： true/false
	 */
	public function use_coupon_with_product($uid,$product_id,$coupon_id){
		return $this->Dao->update(TABLE_SHOP_CART)->set(array(
				'use_coupon' => $coupon_id
		))->where("uid=" . $uid)->aw('product_id='.$product_id)->exec();
	}
	
	/**
	 * 说明： 获取用户名下购物车中的所有商品
	 * 输入参数：用户标识
	 * 输出参数： 商品列表/false
	 */
	public function get_cart_products($uid){
		 $products = $this->Dao->select()->from(TABLE_SHOP_CART)->alias('od')
                            ->leftJoin(TABLE_PRODUCTS)->alias('po')
                            ->on('po.product_id = od.product_id')
                            ->where("od.uid = $uid")->aw('po.product_online',"!=","0")->exec(false);
         return $products;
	}


    public function get_user_order_cart($uid){
        $sql = "select product_id as pid , spec_id as spid, product_quantity as count from shop_cart where uid = ".$uid;
        return  $this->Db->query($sql);
    }

    public function get_cart_products_by_provider($uid,$provider_id){
        $products = $this->Dao->select()->from(TABLE_SHOP_CART)->alias('od')
            ->leftJoin(TABLE_PRODUCTS)->alias('po')
            ->on('po.product_id = od.product_id')
            ->where("od.uid = $uid")->aw("od.provider_id = $provider_id")->exec(false);
        return $products;
    }

	/**
	 * 说明： 获取购物车中的所有优惠券
	 * 输入参数：用户标识
	 * 输出参数： 优惠券列表/false
	 */
	public function get_coupons($uid){
		return $this->Dao->select()->from(TABLE_SHOP_CART)->where('uid='.$uid)->exec();
	}
	
	
	
	
	
	/**
	 * 说明：根据购物车内商品数量和优惠券等，计算购物车金额
	 * 输入参数：用户标识
	 * 输出参数： float/false
	 */
	public function calc_cart_amount($uid){
		$this->loadModel('mProductSpec');
		$this->loadModel('Coupons');
		$this->loadModel('mOrder');
		$product_list = $this->get_cart_products($uid);
		$product_ids = array();
		$total_amount = 0;
		foreach ($product_list as $key => $val){
			///获取商品对应的价格
// 			$product_info = $this->Product->get_simple_product_info($val['product_id']);
			//获取商品的规格信息
			$product_spec_info = $this->mProductSpec->get_spec_sale_price($val['spec_id']);
			$price = $product_spec_info['sale_price'];
			error_log("product id : " .$val['product_id'].",spec_id : ".$val['spec_id'].",unit price:".$price.",product number is :".$val['product_quantity']);
			$product_total_price = $price * $val['product_quantity'];
			
			$coupon_id = $val['use_coupon'];
			error_log("coupon id ====> ".$coupon_id);
			if($coupon_id > 0){
				$reduce_amount = 0;
				//获取coupon信息
				$coupon_info = $this->Coupons->get_coupon_info($coupon_id);
				if($coupon_info){
					//获取折扣的价格
					$reduce_amount = $this->mOrder->cal_single_coupon_reduce_amount($product_total_price,$coupon_info);
				}
				
				//获取折扣之后的总价
				$product_total_price = $product_total_price - $reduce_amount;
			}
			error_log("reduce amount : ".$reduce_amount.",sing product total_amount : ".$product_total_price);
			$total_amount = $product_total_price + $total_amount;
			
		}
		error_log("all products total price : " . $total_amount);
		return $total_amount;
	}


    public  function calc_product_yun_order($productList,$address){


        $this->loadModel('mProductSpec');

        $aboard_free = 3000000;
        $home_free = 2000000;

        $this->loadModel('mYun');
        $yun = 10;

        $data = array();
        foreach($productList as $key=>$val){


            $product_spec_info = $this->mProductSpec->get_spec_sale_price($val['product_price_hash_id']);
            $price = $product_spec_info['sale_price'];
            $product_total_price = $price * $val['product_count'];
            $product_id = $val['product_id'];
            $product_quantity = $val['product_count'];
            $product = $this->Product->getProductInfo($product_id,false);

            if($product['type'] != "1"){
                $data[$key]['provider_id'] = $product['provider_id'];
                $data[$key]['product_id'] = $product_id;
                $data[$key]['yun_no'] = $product['yun_no'];
                $data[$key]['yun_fa'] = $product['yun_fa'];
                $data[$key]['product_quantity'] = $product_quantity;
                $data[$key]['weight'] = $product['product_weight']*$product_quantity;
                $data[$key]['key'] = $product['provider_id'].$product['yun_no'];
                $data[$key]['price'] = $product_total_price;
            }else{
                $yun = 0;
            }

        }
        error_log("==========product_list=========".json_encode($productList));

        $this->loadModel('mYun');
        $list = $this->sort_weight($data);

        error_log("==========list=========".json_encode($list));

        $cityname = $address['province'];
        if($list){
            $yun = 0;
        }
        foreach($list as $key => $val){


            $price =  $val['price'];
            $yun_fa = $val['yun_fa'];
            $isCac = 0;

            if($yun_fa == 1 || $yun_fa == 2){

                if($price >= $aboard_free){

                    $isCac = 1;
                }

            }else if($yun_fa == 3){

                if($price >= $home_free){

                    $isCac = 1;
                }
            }


            $yunCost =  $this->mYun->getYunCost($val['yun_no'],$cityname);
            $weight = $val['weight'];
            if($yunCost){

                if($weight <=$yunCost['first_w']){
                    $yun_tmp = $yunCost['first_p'];

                }else{
                    $yun_tmp = $yunCost['first_p']+ ceil($weight-$yunCost['first_w'])*$yunCost['second_p'];
                }


            }else{
                $yun_tmp = 20;
            }

            if($isCac == 1) $yun_tmp = 0;
            $yun += $yun_tmp;

        }

        return $yun;
    }


    public  function calc_product_yun($productList,$address){


        $this->loadModel('mProductSpec');

        $aboard_free = 30000000;
        $home_free = 200000000;

        $this->loadModel('mYun');
        $yun = 10;

        $data = array();
        foreach($productList as $key=>$val){


            $product_spec_info = $this->mProductSpec->get_spec_sale_price($val['spec_id']);
            $price = $product_spec_info['sale_price'];
            $product_total_price = $price * $val['product_quantity'];
            $product_id = $val['product_id'];
            $product_quantity = $val['product_quantity'];
            $product = $this->Product->getProductInfo($product_id,false);
            if($product['type'] != "1"){
                $data[$key]['provider_id'] = $product['provider_id'];
                $data[$key]['product_id'] = $product_id;
                $data[$key]['yun_no'] = $product['yun_no'];
                $data[$key]['yun_fa'] = $product['yun_fa'];
                $data[$key]['product_quantity'] = $product_quantity;
                $data[$key]['weight'] = $product['product_weight']*$product_quantity;
                $data[$key]['key'] = $product['provider_id'].$product['yun_no'];
                $data[$key]['price'] = $product_total_price;

            }else{
                $yun = 0;
            }
        }
        error_log("==========product_list=========".json_encode($productList));

        $this->loadModel('mYun');
        $list = $this->sort_weight($data);

        error_log("==========list=========".json_encode($list));

        $cityname = $address['province'];
        if($list){
            $yun = 0;
        }
        foreach($list as $key => $val){



            $price =  $val['price'];
            $yun_fa = $val['yun_fa'];
            $isCac = 0;

            if($yun_fa == 1 || $yun_fa == 2){

                if($price >= $aboard_free){

                    $isCac = 1;
                }

            }else if($yun_fa == 3){

                if($price >= $home_free){

                    $isCac = 1;
                }
            }


            $yunCost =  $this->mYun->getYunCost($val['yun_no'],$cityname);
            $weight = $val['weight'];
            if($yunCost){

                if($weight <=$yunCost['first_w']){
                    $yun_tmp = $yunCost['first_p'];

                }else{
                    $yun_tmp = $yunCost['first_p']+ ceil($weight-$yunCost['first_w'])*$yunCost['second_p'];
                }


            }else{
                $yun_tmp = 20;
            }

            if($isCac == 1) $yun_tmp = 0;
            $yun += $yun_tmp;

        }

        return $yun;
    }


    public  function calc_cart_yun($uid,$address){


        $this->loadModel('mProductSpec');

        $aboard_free = 300;
        $home_free = 200;

        $this->loadModel('mYun');
        $yun = 10;
        $productList = $this->get_cart_products($uid);

        $data = array();
        foreach($productList as $key=>$val){


            $product_spec_info = $this->mProductSpec->get_spec_sale_price($val['spec_id']);
            $price = $product_spec_info['sale_price'];
            $product_total_price = $price * $val['product_quantity'];
            $product_id = $val['product_id'];
            $product_quantity = $val['product_quantity'];
            $product = $this->Product->getProductInfo($product_id,false);
            $data[$key]['provider_id'] = $product['provider_id'];
            $data[$key]['product_id'] = $product_id;
            $data[$key]['yun_no'] = $product['yun_no'];
            $data[$key]['yun_fa'] = $product['yun_fa'];
            $data[$key]['product_quantity'] = $product_quantity;
            $data[$key]['weight'] = $val['product_weight']*$product_quantity;
            $data[$key]['key'] = $product['provider_id'].$product['yun_no'];
            $data[$key]['price'] = $product_total_price;
        }
        error_log("==========product_list=========".json_encode($productList));

        $this->loadModel('mYun');
        $list = $this->sort_weight($data);

        error_log("==========list=========".json_encode($list));

        $cityname = $address['province'];
        if($list){
            $yun = 0;
        }
        foreach($list as $key => $val){



            $price =  $val['price'];
            $yun_fa = $val['yun_fa'];
            $isCac = 0;

            if($yun_fa == 1 || $yun_fa == 2){

                if($price >= $aboard_free){

                    $isCac = 1;
                }

            }else if($yun_fa == 3){

                if($price >= $home_free){

                    $isCac = 1;
                }
            }


            $yunCost =  $this->mYun->getYunCost($val['yun_no'],$cityname);
            $weight = $val['weight'];
            if($yunCost){

                if($weight <=$yunCost['first_w']){
                    $yun_tmp = $yunCost['first_p'];

                }else{
                    $yun_tmp = $yunCost['first_p']+ ceil($weight-$yunCost['first_w'])*$yunCost['second_p'];
                }


            }else{
                $yun_tmp = 20;
            }

            if($isCac == 1) $yun_tmp = 0;
            $yun += $yun_tmp;

        }

        return $yun;
    }


    function sort_weight($list = array()){


        $keys = array();
        $weigh_arr = array();
        $product_id_arr = array();
        $provider_id_arr = array();
        $yun_no = array();
        $key_arr = array();
        $price_array = array();
        $yun_fa = array();
        $data =array();

        if(!$list || count($list) < 0){
            return ;
        }

        foreach ($list as $key => $val){
            $result = array();

            //去除重复的keys
            if(!in_array($val['key'],$keys)){
                $keys[] = $val['key'];
            }

            if(in_array($val['key'],$keys)){

                if(!$weigh_arr[$val['key']]){
                    $weigh_arr[$val['key']] = 0;
                }



                error_log("====product_total_price====".$val['spec_id']);
                $provider_id_arr[$val['key']] = $val['provider_id'];
                $product_id_arr[$val['key']] = $val['product_id'];
                $key_arr[$val['key']] = $val['key'];
                $weigh_arr[$val['key']] = $weigh_arr[$val['key']] + $val['weight'];
                $price_array[$val['key']] = $price_array[$val['key']] +$val['price'];
                $yun_no[$val['key']] =  $val['yun_no'];
                $yun_fa[$val['key']] =  $val['yun_fa'];

            }

        }

        $result = array();
        foreach($key_arr as $k => $item){
            $data['product_id'] = $product_id_arr[$k];
            $data['provider_id'] = $provider_id_arr[$k];
            $data['key'] = $key_arr[$k];
            $data['weight'] = $weigh_arr[$k];
            $data['yun_no'] = $yun_no[$k];
            $data['yun_fa'] = $yun_fa[$k];
            $data['price'] = $price_array[$k];
            $result[] = $data;
        }

        return $result;

    }


}
