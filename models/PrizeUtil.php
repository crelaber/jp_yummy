<?php

class PrizeUtil extends Model{
    /*
     * 经典的概率算法，
     * $proArr是一个预先设置的数组，
     * 假设数组为：array(100,200,300，400)，
     * 开始是从1,1000 这个概率范围内筛选第一个数是否在他的出现概率范围之内，
     * 如果不在，则将概率空间，也就是k的值减去刚刚的那个数字的概率空间，
     * 在本例当中就是减去100，也就是说第二个数是在1，900这个范围内筛选的。
     * 这样 筛选到最终，总会有一个数满足要求。
     * 就相当于去一个箱子里摸东西，
     * 第一个不是，第二个不是，第三个还不是，那最后一个一定是。
     * 这个算法简单，而且效率非常 高，
     * 关键是这个算法已在我们以前的项目中有应用，尤其是大数据量的项目中效率非常棒。
     */
    public static function get_rand($proArr) {
        $result = '';
        //概率数组的总概率精度
        $proSum = array_sum($proArr);
        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }

        }
        unset ($proArr);
        return $result;
    }




    /*
     * 奖项数组
     * 是一个二维数组，记录了所有本次抽奖的奖项信息，
     * 其中id表示中奖等级，prize表示奖品，v表示中奖概率。
     * 注意其中的v必须为整数，你可以将对应的 奖项的v设置成0，即意味着该奖项抽中的几率是0，
     * 数组中v的总和（基数），基数越大越能体现概率的准确性。
     * 本例中v的总和为100，那么平板电脑对应的 中奖概率就是1%，
     * 如果v的总和是10000，那中奖概率就是万分之一了。
     *
     */

    public static function get_prize_info(){

        $prize_arr = array(
            '0' => array('id'=>0,'prize_num'=>10,'prize'=>'10元优惠券','v'=>5),
            '1' => array('id'=>1,'prize_num'=>8,'prize'=>'8元优惠券','v'=>5),
            '2' => array('id'=>2,'prize_num'=>6,'prize'=>'6元优惠券','v'=>10),
            '3' => array('id'=>3,'prize_num'=>5,'prize'=>'5元优惠券','v'=>15),
            '4' => array('id'=>4,'prize_num'=>4,'prize'=>'4元优惠券','v'=>25),
            '5' => array('id'=>5,'prize_num'=>3,'prize'=>'3元优惠券','v'=>40),
        );



        /*
         * 每次前端页面的请求，PHP循环奖项设置数组，
         * 通过概率计算函数get_rand获取抽中的奖项id。
         * 将中奖奖品保存在数组$res['yes']中，
         * 而剩下的未中奖的信息保存在$res['no']中，
         * 最后输出json个数数据给前端页面。
         */
        foreach ($prize_arr as $key => $val) {
            $arr[$val['id']] = $val['v'];
        }
        $rid = self::get_rand($arr); //根据概率获取奖项id

        $res['yes']['prize_name'] = $prize_arr[$rid]['prize']; //中奖项
        $res['yes']['prize_index'] = $prize_arr[$rid]['prize_num']; //中奖项
        unset($prize_arr[$rid]); //将中奖项从数组中剔除，剩下未中奖项
        shuffle($prize_arr); //打乱数组顺序
        for($i=0;$i<count($prize_arr);$i++){
            $pr[] = $prize_arr[$i]['prize'];
        }
        $res['no'] = $pr;
//        print_r($res['yes']);

        return $res['yes'];
    }

    public static function gen_angle(){
        $prize_info = self::get_prize_info();
        $index = $prize_info['prize_index'];
        $angle = 90;
        switch($index){
            case 3 :  //3元
                $angle = 330;
                break;
            case 4 :  //4元
                $angle = 270;
                break;
            case 5 :  //5元优惠券
                $angle = 30;
                break;
            case 6 : //6元
                $angle = 210;
                break;
            case 8 : //8元
                $angle = 90;
                break;
            case 10 : //10元
                $angle = 150;
                break;
        }

        $data = array(
            'angle' => $angle ,
            'prize_num'=>$index,
            'prize_name'=>$prize_info['prize_name']
        );

        return $data;
    }


    /**
     * 获取coupon_id
     */
    public static function get_coupon_id($coupon_num,$is_produce = false){
        $coupon_prefix = 'coupon_num_';
        $coupon_ids = array();
        if($is_produce){
            $coupon_ids = array(
                $coupon_prefix.'3'=> 52,
                $coupon_prefix.'4'=> 53,
                $coupon_prefix.'5'=> 54,
                $coupon_prefix.'6'=> 55,
                $coupon_prefix.'8'=> 57,
                $coupon_prefix.'10'=> 59,
            );
        }else{
            $coupon_ids = array(
                $coupon_prefix.'3'=> 101,
                $coupon_prefix.'4'=> 102,
                $coupon_prefix.'5'=> 103,
                $coupon_prefix.'6'=> 104,
                $coupon_prefix.'8'=> 106,
                $coupon_prefix.'10'=> 108,
            );

        }
        return $coupon_ids[$coupon_prefix.$coupon_num];
    }


}