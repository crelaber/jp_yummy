<?php
/*header("Content-type:text/html; charset=UTF-8");*/

/* *
 * 类名：ChuanglanSmsApi
 * 功能：创蓝接口请求类
 * 详细：构造创蓝短信接口请求，获取远程HTTP数据
 * 版本：1.3
 * 日期：2017-04-12
 * 说明：
 * 以下代码只是为了方便客户测试而提供的样例代码，客户可以根据自己网站的需要，按照技术文档自行编写,并非一定要使用该代码。
 * 该代码仅供学习和研究创蓝接口使用，只是提供一个参考。
 */

class SmsApi extends Model  {

    private $companySign ;
    /**
     * SmsApi constructor.
     */
    public function __construct()
    {
        global $config;
        $this->companySign = '【'.$config->sms_owner_company.'】';
    }


    /**
     * 发送支付成功之后的卡号和卡密短信
     */
    public function sendHealthPackagePaySuccessSMS($mobile,$packageId,$cardNo,$cardSecret,$validTime){
        global $config;
        $this->loadModel('mHealthPackage');
        $packageInfo = $this->mHealthPackage->get_package_info_by_id($packageId);
        $bookUrl = $config->zy_package_book_url;
//        $msg = $this->companySign."尊享的中英人寿客户！您购买的【".$packageInfo['name']."】账号：".$cardNo."密码：".$cardSecret." 登录体检网预约，请通过链接".$bookUrl." 或拨打中英人寿体检客服电话010-56989772预约体检！";
        $msg = $this->companySign."尊敬的中英人寿客户！您购买的【".$packageInfo['name']."】账号：".$cardNo."密码：".$cardSecret." 有效期至：".$validTime." 登录体检网预约，请通过链接 ".$bookUrl." 或拨打中英人寿体检客服电话010-56989772预约体检！";
//        $msg = $this->companySign.'您好，您购买的【'.$packageInfo['name'].'】体检卡，卡号：'.$cardNo."，卡密：".$cardSecret.'，请妥善保管，体检预约网站';
        $result = $this->sendSMS($mobile,$msg);
        return $result;
    }

    /**
     * 发送验证码短信
     */
    public function sendVerifyCodeSMS($mobile){
        $code = mt_rand(100000,999999);
        $content = $this->companySign . '您好，您的验证码是'. $code;
        $this->sendSMS($mobile, $content);
    }

	/**
	 * 发送短信
	 *
	 * @param string $mobile 		手机号码
	 * @param string $msg 			短信内容
	 * @param string $needstatus 	是否需要状态报告
	 */
	public function sendSMS( $mobile, $msg, $needstatus = 'true') {
		global $config;
		
		//创蓝接口参数
		$postArr = array (
			'account'  =>  $config->sms_api_account,
			'password' => $config->sms_api_password,
			'msg' => urlencode($msg),
			'phone' => $mobile,
			'report' => $needstatus
        );
		
		$response = $this->curlPost( $config->sms_api_send_url , $postArr);

        $output=json_decode($response,true);
        if(isset($output['code']) && $output['code'] == '0'){
            $result = [
                'errcode' => 200,
                'msg' => 'success'
            ];
        }else{
            $msg = '状态码：'.$output['code'].'，'.$output['errorMsg'];
            $result = [
                'errcode' => -1,
                'msg' => $msg
            ];
        }
        return $result;
	}
	
	/**
	 * 发送变量短信
	 *
	 * @param string $msg 			短信内容
	 * @param string $params 	最多不能超过1000个参数组
	 */
	public function sendVariableSMS( $msg, $params) {
		global $config;
		
		//创蓝接口参数
		$postArr = array (
			'account' => $config->sms_api_account,
			'password' =>$config->sms_api_password,
			'msg' => $msg,
			'params' => $params,
			'report' => 'true'
        );
		
		$result = $this->curlPost( $config->sms_api_variable_url, $postArr);
		return $result;
	}
	
	
	/**
	 * 查询额度
	 *
	 *  查询地址
	 */
	public function queryBalance() {
		global $config;
		
		//查询参数
		$postArr = array ( 
		    'account' => $config->sms_api_account,
		    'password' => $config->sms_api_password,
		);
		$result = $this->curlPost($config->sms_api_balance_query_url, $postArr);
		return $result;
	}

	/**
	 * 通过CURL发送HTTP请求
	 * @param string $url  //请求URL
	 * @param array $postFields //请求参数 
	 * @return mixed
	 */
	private function curlPost($url,$postFields){
		$postFields = json_encode($postFields);
		$ch = curl_init ();
		curl_setopt( $ch, CURLOPT_URL, $url ); 
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8'
			)
		);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt( $ch, CURLOPT_TIMEOUT,1); 
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0);
		$ret = curl_exec ( $ch );
        if (false == $ret) {
            $result = curl_error(  $ch);
        } else {
            $rsp = curl_getinfo( $ch, CURLINFO_HTTP_CODE);
            if (200 != $rsp) {
                $result = "请求状态 ". $rsp . " " . curl_error($ch);
            } else {
                $result = $ret;
            }
        }
		curl_close ( $ch );
		return $result;
	}


	public function getSmsSign(){
    }
	
}