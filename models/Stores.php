<?php

    class Stores extends Model
    {

        public function gets()
        {
            $sections = $this->Dao->select()->from(TABLE_STORES)->where("(ttime IS NULL AND ftime IS NULL)")->ow("(NOW() <= ttime aNd ftime <= NOW())")->orderby('bsort')->desc()->exec();
            return $sections;
        }

        public function getAll()
        {
            return $this->Dao->select()->from(TABLE_STORES)->orderby('id')->exec();
        }
        
        public function getAllActivated()
        {
            return $this->Dao->select()->from(TABLE_STORES)->where('activated = 1')->orderby('id')->exec();
        }
        
        public function get($id)
        {
            return $this->Dao->select()->from(TABLE_STORES)->where("id = $id")->getOneRow();
        }
        
        public function delete_store_by_id($id)
        {
            return $this->Dao->update(TABLE_STORES)->set(array('activated' => 0))->where("id = $id")->exec(false);
        }
        
        public function get_store_by_address($address)
        {
            // get GPS for given address
            $url = "http://api.map.baidu.com/geocoder/v2/?address=$address&output=json&ak=0NnLgeO4V61jARaU0PMOT0OB";
            $ret = Curl::get($url);
            $obj=json_decode($ret);
            $status = $obj->status;
            if($status != 0){
                return false;;
            }
            $confidence = $obj->result->confidence;
            if($confidence < 70){
                return false;
            }
            $location = $obj->result->location;
            $lat = $location->lat;
            $lng = $location->lng;
            // get all activated stores and find closest to address
            $activated_stores = $this->Dao->select()->from(TABLE_STORES)->where('activated = 1')->orderby('id')->exec();
            $selected_store_id = 0;
            $closest_distance = 100000; // km
            foreach ($activated_stores AS $val) {
                // calc distance
                $store_lat = $val['store_latitude'];
                $store_lng = $val['store_longitude'];
                $c_url =  "http://api.map.baidu.com/direction/v1/routematrix?output=json&origins=$lat,$lng&destinations=$store_lat,$store_lng&ak=0NnLgeO4V61jARaU0PMOT0OB&mode=walking";
                $c_ret = Curl::get($c_url);
                $c_obj=json_decode($c_ret);
                $c_status = $c_obj->status;
                if($c_status == 0){
                    $res = $c_obj->result;
                    $dataArray = $res->elements;
                    
                    $objDistance = $dataArray[count($dataArray)-1]->distance->value;
                    if ($objDistance < $closest_distance) {
                        $selected_store_id = $val['id'];
                        $closest_distance = $objDistance;
                    }
                }
            }
            
            return $selected_store_id;
        }
    }

?>