<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class TurnplateSwitch extends Model {


	/**
	 * 获取switch信息
	 */
	public function get_switch_info($order_id){
		return $this->Dao->select()->from(TABLE_TURNPLATE_SWITH)->where('order_id='.$order_id)->getOneRow();
	}


	/**
	 * 插入订单对应的开关数据
	 * @param type $verifed
	 * @return type
	 */
	public function insert_turnplate_switch($order_id){
		return $this->Dao->insert(TABLE_TURNPLATE_SWITH,'order_id,is_used')->values(array($order_id,0))->exec();
	}

	/**
	 * 关闭开关
	 */
	public function close_turnplate_switch($order_id){
		$data = array(
			'is_used'=>1
		);
		return $this->Dao->update(TABLE_TURNPLATE_SWITH)->set($data)->where('order_id='.$order_id)->exec(false);
	}



}