<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mAward extends Model {


    public function  getAwardDetail($amount){
        $now = time();
        $detail = $this->Db->getOneRow("SELECT * FROM `wshop_award` WHERE `min_amount` < $amount and `max_amount` >= $amount and `add_time` >= $now",false);
        return $detail;
    }



}