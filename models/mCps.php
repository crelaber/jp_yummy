<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mCps extends Model {


    public  function  all_cps_list(){
        return $this->Db->query("select * FROM `cps_log`");
    }

    public  function  page_cps_list($page,$pagesize){
        $sql_list="SELECT * FROM `cps_log` order by add_time desc LIMIT $page,$pagesize ";
        return $this->Db->query($sql_list,false);
    }

    public function  add_cps_log($uid,$order_id,$amount,$cps_amount,$type){

        return   $this->Dao->insert("cps_log", '`uid`,`order_id`,`amount`,`cps_amount`,`add_time`,`type`')->values(array($uid,$order_id,$amount,$cps_amount,time(),$type))->exec();

    }

    public function  getCpsDetail($id){
        return $this->Db->getOneRow("SELECT * FROM `cps_log` WHERE `id` = '$id';");

    }

    public function del_cps($id){


        return $this->Dao->delete ()->from ('cps_log' )->where ( "id =" . $id )->exec ();


    }
}