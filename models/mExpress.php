<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mExpress extends Model
{


    public function getExpressDetail($express_no) {
        return $this->Db->getOneRow("SELECT * FROM `j_express` WHERE `express_no` = '$express_no';");
    }

    public function  getExpressList(){

        return $this->Dao->select ()->from ('j_express' )->exec (false);

    }

    public function add_express($id,$name,$num,$tel,$url){

        if($id){
            return $this->Dao->update(j_express)->set(array(
                'name' => $name,
                'express_no' => $num,
                'tel' => $tel,
                'url' => $url
            ))->where("id=" . $id)->exec();
        }else{
            return   $this->Dao->insert("j_express", '`name`,`express_no`,`tel`,`url`')->values(array($name,$num,$tel,$url))->exec();

        }

    }


   public function get_express_detail($id){
       return $this->Db->getOneRow("SELECT * FROM `j_express` WHERE `id` = '$id';");

   }

    public function  del_Express($id){

        return $this->Dao->delete ()->from ( 'j_express' )->where ( "id =" . $id )->exec ();

    }


    //===================================================

    public function  add_package($order_id,$express_id,$express_no,$provider_id,$express_num){

        return   $this->Dao->insert("j_order_package", '`order_id`,`express_id`,`express_no`,`provider_id`,`express_num`')->values(array($order_id,$express_id,$express_no,$provider_id,$express_num))->exec();

    }

    public function  get_order_package($order_id,$provider_id){

        return $this->Db->getOneRow("SELECT * FROM `j_order_package` WHERE `order_id` = '$order_id' and `provider_id` = '$provider_id';");

    }

}