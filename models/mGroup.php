<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mGroup extends Model {




    public function  add_group($uid,$order_id,$product_id,$num,$product_time){
        $now = time();
        $time = strtotime("+24 hours");
        if($time >= strtotime($product_time)){
            $time = strtotime($product_time);
        }

        return   $this->Dao->insert("groups", '`uid`,`order_id`,`add_time`,`status`,`product_id`,`end_time`,`num`')->values(array($uid,$order_id,$now,0,$product_id,$time,$num))->exec(false);
    }


    public function  group_page_list($page,$pagesize){
        $sql_list="SELECT * FROM `groups` order by add_time desc LIMIT $page,$pagesize ";
        return $this->Db->query($sql_list,false);
    }

    public function group_list(){
        $sql_list="SELECT * FROM `groups`";
        return $this->Db->query($sql_list,false);
    }

     public function updateGroup($id,$data = array()){

         return $this->Dao->update('groups')->set($data)->where("id =".$id)->exec(false);
     }

    public function  getGroupingList(){
        $sql_list = "SELECT * FROM `groups` WHERE  status = 0";
        return $this->Db->query($sql_list,false);

        return $detail;
    }

    public function  getGroupDetail($uid,$product_id){
        $detail = $this->Db->getOneRow("SELECT * FROM `groups` WHERE `uid` = $uid and `product_id` = $product_id and status = 0",false);
        return $detail;
    }
    public function  getGroupDetailByOrderId($order_id){
        return $this->Db->getOneRow("SELECT * FROM `groups` WHERE `order_id` = '$order_id';",false);
    }


    public function  getGroupDetailById($id){
        return $this->Db->getOneRow("SELECT * FROM `groups` WHERE `id` = '$id';",false);

    }

    public function  add_group_join($uid,$order_id,$group_id){
        return   $this->Dao->insert("group_join", '`uid`,`order_id`,`add_time`,`group_id`')->values(array($uid,$order_id,time(),$group_id))->exec(false);

    }

    public function updateGroupJoin($id,$data = array()){

        return $this->Dao->update('group_join')->set($data)->where("id =".$id)->exec(false);
    }


    public function getGroupJoinByGroupId($group_id,$uid){
        return $this->Db->getOneRow("SELECT * FROM `group_join` WHERE `group_id` = '$group_id' and `uid` = '$uid';",false);

    }

    public function getGroupJoinByOrderId($order_id){
        return $this->Db->getOneRow("SELECT * FROM `group_join` WHERE `order_id` = '$order_id';",false);

    }

    public function  getGroupJoinList($group_id){
        $sql_list="SELECT * FROM `group_join` where group_id = $group_id ";
        return $this->Db->query($sql_list,false);
    }

    //group log
    public function  add_group_log($uid,$order_id,$group_id,$amount,$desc){
        return   $this->Dao->insert("groups_log", '`uid`,`order_id`,`add_time`,`group_id`,`amount`,`desc`')->values(array($uid,$order_id,time(),$group_id,$amount,$desc))->exec(false);

    }

    public function group_log_list(){
        $sql_list="SELECT * FROM `groups_log`";
        return $this->Db->query($sql_list,false);
    }

    public function  group_log_page_list($page,$pagesize){
        $sql_list="SELECT * FROM `groups_log` order by add_time desc LIMIT $page,$pagesize ";
        return $this->Db->query($sql_list,false);
    }


    public function  add_group_order_return($order_id,$return_id){
        return   $this->Dao->insert("group_order_return", '`order_id`,`return_id`')->values(array($order_id,$return_id))->exec(false);
    }

    public function getOrderReturn($order_id){
        return $this->Db->getOneRow("SELECT * FROM `group_order_return` WHERE `order_id` = '$order_id';");

    }



    public function groupFailMsg($groupId){
        global $config;
        $this->loadModel('WechatSdk');
        $this->loadModel('User');
        $this->loadModel('Product');

        $joinList = $this->getGroupJoinList($groupId);
        $group = $this->getGroupDetailById($groupId);
        $adminUser = $this->User->getUserInfo($group['uid']);

        $productInfo = $this->Product->getProductInfo($group['product_id']);

        $orderInfo = $this->mOrder->GetSimpleOrderInfo($group['order_id']);
        $amount = $productInfo['group_price']+$orderInfo[0]['order_yunfei'];
        Messager::sendTemplateMessage($config->messageTpl['pin_fail'], $adminUser['client_wechat_openid'], array(
            'first' => '您好，您参加的拼团由于团已过期，拼团失败',
            'keyword1' => $productInfo['product_name'],//价格
            'keyword2' => $amount,//订单号
            'keyword3' => $amount,
            'remark' => '您的退款已经提交微信审核，感谢您的参与！'
        ), $this->getBaseURI() . "?/Group/grouping/id=".$group['id']);
        if($joinList){
            foreach($joinList as $key => $val) {
                $user = $this->User->getUserInfo($val['uid']);
                $orderInfo = $this->mOrder->GetSimpleOrderInfo($val['order_id']);

                Messager::sendTemplateMessage($config->messageTpl['pin_fail'], $user['client_wechat_openid'], array(
                    'first' => '您好，您参加的拼团由于团已过期，拼团失败',
                    'keyword1' => $productInfo['product_name'],//价格
                    'keyword2' => $productInfo['group_price']+$orderInfo[0]['order_yunfei'],//订单号
                    'keyword3' => $productInfo['group_price']+$orderInfo[0]['order_yunfei'],
                    'remark' => '您的退款已经提交微信审核，感谢您的参与！'
                ), $this->getBaseURI() . "?/Group/grouping/id=".$group['id']);
            }

        }


    }
    //参团逻辑

    public function johnGroup($order_id,$uid,$group_id,$product){


        $this->loadModel('User');
        $this->loadModel('mOrder');
        $this->loadModel('WechatSdk');

        global $config;
        $isExistGroup = $this->getGroupDetail($uid,$product['product_id']);
        if(!$isExistGroup && $group_id == 0){
            $id= $this->add_group($uid,$order_id,$product['product_id'],$product['num'],$product['kill_time']);
        }else{

            if($group_id > 0){
                $isExistGroup = $this->getGroupDetailById($group_id);
            }
            if($uid== $isExistGroup['uid']){
                return $isExistGroup['id'];
            }else{
                $group = $this->getGroupDetailById($group_id);
                $joinList =  $this->getGroupJoinList($group_id);
                $joinNum = $group['num'] - count($joinList)-1;
                if($joinNum >0){
                    $this->add_group_join($uid,$order_id,$group['id']);
                }
                $joinNum = $group['num'] - count($joinList)-2;
                if($joinNum == 0){
                    if($group['status'] == 0){
                        $serArray = array('status'=>1);
                        $this->mGroup->updateGroup($group_id,$serArray);
                        $group['status'] = 1;
                        $joinList =  $this->mGroup->getGroupJoinList($group_id);
                        $adminInfo =  $this->User->getUserInfo($group['uid']);

                        foreach($joinList as $key => $val){
                            $user =   $this->User->getUserInfo($val['uid']);
                            $orderInfo = $this->mOrder->GetSimpleOrderInfo($val['order_id']);
                            Messager::sendTemplateMessage($config->messageTpl['pin_success'], $user['client_wechat_openid'], array(
                                'first' => '您参团的商品 '.$product['product_name'].' 已组团成功，预计将在1~2日发货，请注意查收！',
                                'keyword1' => $product['group_price'],//价格
                                'keyword2' => $orderInfo[0]['serial_number'],//订单号
                                'remark' => '点击详情 随时查看订单状态'
                            ), $this->getBaseURI() . "?/Order/expressDetail/order_id=".$val['order_id']);
                        }
                        $orderInfo = $this->mOrder->GetSimpleOrderInfo($group['order_id']);
                        Messager::sendTemplateMessage($config->messageTpl['pin_success'], $adminInfo['client_wechat_openid'], array(
                            'first' => '您参团的商品 '.$product['product_name'].' 已组团成功，预计将在1~2日发货，请注意查收！',
                            'keyword1' => $product['group_price'],//价格
                            'keyword2' => $orderInfo[0]['serial_number'],//订单号
                            'remark' => '点击详情 随时查看订单状态'
                        ), $this->getBaseURI() . "?/Order/expressDetail/order_id=".$group['order_id']);
                    }
                }

            }

        }

    }


    //二维码处理逻辑
    public function  add_qr_code($qr_value,$type,$add_time,$file_path,$one){
        return   $this->Dao->insert("group_qr_code", '`qr_value`,`type`,`add_time`,`file_path`,`one_info`')->values(array($qr_value,$type,$add_time,$file_path,$one))->exec(false);

    }

    public function  get_qr_code_by_value($qr_value,$type){
        return $this->Db->getOneRow("SELECT * FROM `group_qr_code` WHERE `qr_value` = '$qr_value' and `type` = $type;");

    }

    public function  get_qr_code_by_id($id){
        return $this->Db->getOneRow("SELECT * FROM `group_qr_code` WHERE `id` = 'id';");

    }




}