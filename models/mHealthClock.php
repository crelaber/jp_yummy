<?php

/**
 * 中英健康打卡信息类
 */
class mHealthClock extends Model {
    /**用户信息表*/
    const TABLE_NAME_USER_INFO = 'zy_hch_user_info';
    /**用户头衔表*/
    const TABLE_NAME_USER_TITLE = 'zy_hch_user_title';
    /*用户健康信息表*/
    const TABLE_NAME_USER_HEALTH_INFO = 'zy_hch_user_health_info';
    /**初级打卡记录表*/
    const TABLE_NAME_LOW_LEVEL_RECORD = 'zy_hch_low_level_record';
    /**高级打卡记录表*/
    const TABLE_NAME_HIGH_LEVEL_RECORD = 'zy_hch_high_level_record';
    /**方案详情表*/
    const TABLE_NAME_CASE_DETAIL = 'zy_hch_case_detail';
    /**模板消息记录表*/
    const TABLE_NAME_TPL_SEND_LOG = 'zy_hch_tpl_send_log';
    /**用户地址信息表*/
    const TABLE_NAME_USER_ADDRESS = 'zy_hch_user_address';
    /**兑换礼品表*/
    const TABLE_NAME_EXCHAGE_PRIZE = 'zy_hch_exchange_prize';
    /**兑换礼品表*/
    const TABLE_NAME_EXCHAGE_ORDER = 'zy_hch_exchange_order';
    /**图片表*/
    const TABLE_NAME_EXCHANGE_PRIZE_IMG = 'zy_hch_exchange_prize_img';
    /**黑名单表*/
    const TABLE_NAME_AWARD_BLACKLIST = 'zy_hch_user_blacklist';
    /**奖励资格表*/
    const TABLE_NAME_AWARD_QUALIFICATION = 'zy_hch_award_qualification';
    /**奖品兑换记录*/
    const TABLE_NAME_PRIZE_EXCHANGE_LOG = 'zy_hch_prize_exchange_log';


    const REDIS_PREFIX_HEALTH_CLOCK_USER = 'ZY_HEALTH_PUNCH_USER_INFO_';
    /**
     * 所有的方案数据
     */
    const REDIS_PREFIX_ALL_CASE_DATA = 'ZY_HEALTH_PUNCH_ALL_CASE_DATA';
    /**
     * 用户的最新的健康信息
     */
    const REDIS_PREFIX_USER_LATEST_HEALTH_INFO = 'ZY_HEALTH_PUNCH_USER_LATEST_HEALTH_INFO_';
    /**
     * 用户的最新的奖品信息
     */
    const REDIS_PREFIX_EXCHANGE_PRIZE_INFO = 'ZY_HEALTH_PUNCH_EXCHANGE_PRIZE_INFO_';
    /**
     * 订单信息
     */
    const REDIS_PREFIX_ORDER_INFO= 'ZY_HEALTH_PUNCH_ORDER_INFO_';
    /**
     * 用户的订单列表
     */
    const REDIS_PREFIX_USER_ADDRESS_LIST= 'ZY_HEALTH_PUNCH_USER_ADDRESS_LIST_';
    /**
     * 客户的奖品列表
     */
    const REDIS_PREFIX_ZY_CUSTOMER_PRIZE_LIST= 'ZY_HEALTH_PUNCH_ZY_CUSTOMER_PRIZE_LIST_';
    /**
     * 中英用户的的奖品列表
     */
    const REDIS_PREFIX_ZY_USER_PRIZE_LIST= 'ZY_HEALTH_PUNCH_ZY_USER_PRIZE_LIST_';
    /**
     * 用户的订单列表
     */
    const REDIS_PREFIX_PRIZE_LIST= 'ZY_HEALTH_PUNCH_PRIZE_LIST_';
    /**
     * 商品的库存列表
     */
    const REDIS_PREFIX_PRIZE_STOCK_QUEUE_LIST= 'ZY_HEALTH_PUNCH_PRIZE_STOCK_LIST_';
    /**
     * 用户的抢购资格列表
     */
    const REDIS_PREFIX_CURRENT_MONTH_QUALIFICATION= 'ZY_HEALTH_PUNCH_CURRENT_MONTH_QUALIFICATION_';
    /**
     * 用户的黑名单列表
     */
    const REDIS_PREFIX_USER_BLACKLIST= 'ZY_HEALTH_PUNCH_USER_BLACKLIST';
    /**
     * BMI的计算系数
     */
    const BMI_CAL_FACT = 100;
    /**
     * 体重的计算系数
     */
    const WEIGHT_CAL_FACT = 1000;
    /**
     * 高级打卡的周期
     */
    const HIGH_LEVEL_LAST_DAYS = 28;
    /**
     * 初级打卡的周期
     */
    const LOW_LEVEL_LAST_DAYS = 14;
    /**
     * 用户的头像的基数
     */
    const USER_TITLE_PUNCH_BASE_NUM = 7;
    /**
     * 默认数值
     */
    const DEFAULT_NUMERIC_VALUE = 0;
    /**
     * 默认字符值
     */
    const DEFAULT_STRING_VALUE = "";


    const IMAGE_UPLOAD_MARK = 'product_hpic2__';


    /**
     * 获取单个套餐详情
     */
    public function getPartnerUserInfo($partnerUid) {
        $redisKey = self::REDIS_PREFIX_HEALTH_CLOCK_USER.$partnerUid;
        $data = RedisUtil::get($redisKey);
        $info = '<======================================get user info from redis======================================>';
        if(!$data){
            $info = '<======================================get user info from db======================================>';
            if($data = $this->Dao->select()->from(self::TABLE_NAME_USER_INFO)->where("partner_uid = '$partnerUid'")->getOneRow()){
                //同时加载头衔信息
                $titles = $this->getUserAllTitles($partnerUid);
                $titles = $titles ? $titles : [];
                $data['titles'] = $titles;
                RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
            }
        }
        error_log($info);
        return $data;
    }


    public function getOriginUserInfo($partnerUid){
        $userInfo = $this->Dao->select()->from(self::TABLE_NAME_USER_INFO)->where("partner_uid = '$partnerUid'")->getOneRow();
        return $userInfo;
    }


    /**
     * 获取所有的用户列表
     */
    public function getAllUser($condition = ""){
        $query = $this->Dao->select()->from(self::TABLE_NAME_USER_INFO)->where('come_from = "zy_group"');
        if($condition){
            $query = $query->aw($condition);
        }
        return $query->exec(false);
    }


    /**
     * 自动注册第三方用户信息
     * @param $partnerUid 第三方用户编号
     * @param $partnerUname 第三方用户名
     * @param $from_platform 来源平台
     * @param $appid 对应的appid
     * @return array
     */
    public function autoRegPartnerUser($partnerUid,$partnerUname,$comeFrom = 'zy_group'){
        if(!$userInfo = $this->getPartnerUserInfo($partnerUid)){
            $partnerUname = urldecode($partnerUname);
            $time = time();
            $partnerLogId = $this->Dao->insert(self::TABLE_NAME_USER_INFO, 'partner_uid,partner_name,reg_time,come_from')
                ->values(array(
                    $partnerUid,
                    $partnerUname,
                    $time,
                    $comeFrom,
                ))->exec(false);
        }

        //增加自动更新客户编号的
        if(!$userInfo['cust_no']){
            $zyCustomCheckResult = ZyHealthClockHelper::checkZyCustomer($userInfo['partner_uid']);
            if($zyCustomCheckResult['cust_no']){
//                $updateData = [
//                    'cust_no' => $zyCustomCheckResult['cust_no']
//                ];
                $userInfo['cust_no'] = $zyCustomCheckResult['cust_no'];
                $this->updateUserInfo($userInfo,$userInfo['partner_uid'],true);
            }
        }

        return $partnerUid;
    }

    /**
     * 获取用户的所有头衔
     * @param $partnerUid 用户编号
     * @return array
     */
    public function getPartnerUserTitles($partnerUid){
        $redisKey = self::REDIS_PREFIX_HEALTH_CLOCK_USER.$partnerUid;
        $titles = [];
        if($userInfo = RedisUtil::get($redisKey)){
            if(!$userInfo['titles']){

               $userInfo['titles'] = $titles?$titles:[];
            }
            $titles = $userInfo['titles'];
        }
        return $titles;
    }


    /**
     * 更新用户信息
     * @param $data 要更新的数据
     * @param $uid 用户编号
     * @return boolean
     */
    public function updateUserInfo($data,$uid,$updateCache = true){
        $query = $this->Dao->update(self::TABLE_NAME_USER_INFO)->set($data)->where('partner_uid="'.$uid.'"');
        if($result = $query->exec(false)){
            if($updateCache){
                $userInfo = $this->getPartnerUserInfo($uid);
                foreach ($data as $key => $val){
                    $userInfo[$key] = $val;
                }
                $redisKey = self::REDIS_PREFIX_HEALTH_CLOCK_USER.$uid;
                RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
            }
        }
        return $result;
    }


    /**
     * 获取打卡的分页数据
     */
    public function getPunchRecordPageData($uid,$lastid,$isHighLevel = false,$pagesize = 10){
        $table = $isHighLevel ? self::TABLE_NAME_HIGH_LEVEL_RECORD : self::TABLE_NAME_LOW_LEVEL_RECORD;
//        $addtionalWhere = $isHighLevel ? '' : ' and answer_status = 1';
        $addtionalWhere = '';
        $sql_list="SELECT * from $table where uid = '$uid' and id < $lastid $addtionalWhere order by add_time desc,id desc LIMIT $pagesize ";
        $list = $this->Db->query($sql_list,false);
        $lastid = 0;
        if(count($list) > 0){
            foreach ($list as &$val){
                $day = date('m月d日',$val['add_time']);
                $time = date('H:i',$val['add_time']);
                $val['day'] = $day;
                $val['time'] = $time;
                $val['status'] = '打卡成功';
            }
            $ids = array_column($list,'id');
            $lastid = min($ids);
        }
        $list = $list ? $list : [];
        return array(
            'lastid' => (int)$lastid,
            'list' => $list
        );
    }

    /**
     * 插入用户的健康打卡数据
     * @param $uid 用户编号
     * @param $height 身高
     * @param $weight 体重
     * @param $sex 性别
     * @return int
     */
    public function doInputHealthInfo($uid,$height,$weight,$targetWeight,$sex){
        return $this->insertUserHealthInfo($uid,$height,$weight,$targetWeight,$sex);
    }


    /**
     * 初级打卡皆苦方法
     */
    public function doLowLevelPunch($uid,$challengeIndex,$spendTime,$questionData,$score = 0){
        $userName = "";
        $dayIndex = 1;
        if($userInfo = $this->getPartnerUserInfo($uid)){
            $timeData = $this->calLowLevelDay($uid,$userInfo);
            $dayIndex = $timeData['dayIndex'];
            $userName = $userInfo['partner_name'];
        }
//        //防止重复插入
        if(!$todayPunchInfo = $this->getCurrentDayPunchInfo($uid)){
            //插入答题记录,插入成功之后更新
            $questionData = urldecode($questionData);
            $questionDataList = json_decode($questionData,true);
            $userQuestion = $questionDataList[0];
            $answerStatus = intval($userQuestion['answer_status']);
            //如果答案正确，才会计入次数，否则只添加记录
            if($id = $this->insertLowLevelRecord($uid,$userName,$spendTime,$questionData,$dayIndex,$score,$challengeIndex,$answerStatus)){
//                if($answerStatus > 0){
                    //更新用户的打卡次数信息
                    $punchTimes = intval($userInfo['low_level_times']) + 1;
                    $data['low_level_times'] = $punchTimes;
                    $this->updateUserInfo($data,$uid,false);
                    //处理用户的头衔
                    $titles = $this->handleUserTitles($uid,$userInfo,false);

                    $userInfo['low_level_times'] = $punchTimes;
                    $userInfo['titles'] = $titles;
                    //更新用户的信息
                    $redisKey = self::REDIS_PREFIX_HEALTH_CLOCK_USER.$uid;
                    RedisUtil::set($redisKey,$userInfo,CACHE_TIME_TEN_DAY);

                    //完成一个周期自动的发送模板消息给中英
                    if($punchTimes % self::LOW_LEVEL_LAST_DAYS == 0){
                        $this->sendFinishRemindTplMsgToZy($uid,$userName,false);
                    }
//                }
            }
        }else{
            $id = 1;
        }
        $result = [
            'id' => $id,
            'status' => $answerStatus,
        ];
        return $result;
    }



    /**
     * 高级打卡接口
     */
    public function doHighLevelPunch($uid){
        $userName = "";
        $dayIndex = 1;
        $score = 0;
        if($userInfo = $this->getPartnerUserInfo($uid)){
            $timesData = $this->calHighLevelDay($uid,$userInfo);
            $dayIndex = $timesData['dayIndex'];
            $userName = $userInfo['partner_name'];
        }
        if(!$todayPunchInfo = $this->getCurrentDayPunchInfo($uid,true)){
            if($id = $this->insertHighLevelRecord($uid,$userName,$dayIndex,$score)){
                //更新用户的打卡次数信息
                $punchTimes = intval($userInfo['high_level_times']) + 1;
                $data['high_level_times'] = $punchTimes;
                $this->updateUserInfo($data,$uid,false);
                //用户头衔的列表
                $titles = $this->handleUserTitles($uid,$userInfo,true);
                $userInfo['high_level_times'] = $punchTimes;
                $userInfo['titles'] = $titles;

                //更新用户的信息
                $redisKey = self::REDIS_PREFIX_HEALTH_CLOCK_USER.$uid;
                RedisUtil::set($redisKey,$userInfo,CACHE_TIME_TEN_DAY);

                //完成一个周期自动的发送模板消息给中英
                if($punchTimes % self::HIGH_LEVEL_LAST_DAYS == 0){
                    $this->sendFinishRemindTplMsgToZy($uid,$userName,true);
                }
            }
        }else{
            $id = 1;
        }
        return $id;
    }

    /**
     * 处理用户的头衔
     */
    public function handleUserTitles($uid,$userInfo = null,$isHighLevel = false){
        $userInfo = $userInfo ? $userInfo : $this->getPartnerUserInfo($uid);
        $userTitlesData = $userInfo['titles'];
        if($isHighLevel){
            //计算总次数
            $totalPunchTimes =  $userInfo['high_level_times'] + 1;
            //取余
            $remainder = $totalPunchTimes % self::USER_TITLE_PUNCH_BASE_NUM ;
            //对应的头衔级别为除数
            $level = ($totalPunchTimes - $remainder) / self::USER_TITLE_PUNCH_BASE_NUM ;
            $level = $level + 2;
            //高级打卡只有3，4，5，6 四个称号
            $level = $level > 6 ? 6 : $level;
            $hasTitle = $this->containTitle($userTitlesData,$level);
            $canAddTitle = $hasTitle ? false : true;
        }else{
            //计算总次数
            $totalPunchTimes = $userInfo['low_level_times'] + 1;
            //取余
            $remainder = $totalPunchTimes % self::USER_TITLE_PUNCH_BASE_NUM ;
            //对应的头衔级别为除数
            $level = ($totalPunchTimes - $remainder) / self::USER_TITLE_PUNCH_BASE_NUM ;
//            $level = $level + 1;
            //初级打卡只有1，2两个称号
            $level = $level > 2 ? 2:$level;
            $hasTitle = $this->containTitle($userTitlesData,$level);
            $canAddTitle = $hasTitle ? false : true;
        }
//        //计算总次数
//        $totalPunchTimes = $userInfo['low_level_times'] + $userInfo['high_level_times'] + 1;
//        //取余
//        $remainder = $totalPunchTimes % self::USER_TITLE_PUNCH_BASE_NUM ;
//        //对应的头衔级别为除数
//        $level = ($totalPunchTimes - $remainder) / self::USER_TITLE_PUNCH_BASE_NUM ;
        //用户的头衔

        $userTitles = [];
        //表示头衔的级别,只有6个等级
        if($level > 0 && $level < 7 && $canAddTitle){
//            $userTitles = json_decode($userTitlesData,true);
            $userTitles = $userTitlesData ? $userTitlesData : [];
            $titleCount = count($userTitles);
            //如果头衔存在
            if($titleCount > 0){
                $titleExists = false;
                //用于判断是否存在对应的title
                foreach ($userTitles as $title){
                    if($title['level'] == $level){
                        $titleExists = true;
                        break;
                    }
                }
                //称号在称号列表中不存在，则插入
                if(!$titleExists){
                    $title = $this->addUserTitleByLevel($level,$uid);
                    array_push($userTitles,$title);
                }

            }else{
                //头像不存在则直接插入头衔
                $title = $this->addUserTitleByLevel($level,$uid);
                array_push($userTitles,$title);
            }
        }else if($level >= 7){
            $userTitles = $userTitlesData;
        }else{
            $userTitles = $userTitlesData;
        }
        return $userTitles;
    }


    public function containTitle($titles,$level){
        $levels = array_column($titles,'level');
        if(in_array($level,$levels)){
            return true;
        }
        return false;
    }

    /**
     * 根据头衔的级别给用户添加头衔
     * @param $level 头衔的级别
     * @param $uid 用户编号
     * @return array
     */
    public function addUserTitleByLevel($level,$uid){
        global $config;
        $systemTitles = $config->system_titles;
        //没有对应的头衔记录，则插入头衔记录
        $singleTitle = $systemTitles[$level];

        $userTitleObj = [
            'title' => $singleTitle['name'],
            'uid' => $uid,
            'level' => $singleTitle['level'],
            'add_time' => time()
        ];

        if($id = $this->insertUserTitle($userTitleObj['uid'],$userTitleObj['title'],$userTitleObj['level'])){
            $userTitleObj['id'] = $id;
        }
        return $userTitleObj;
    }


    /**
     * 获取用户所有的头衔
     * @param $uid 用户编号
     * @return array
     */
    public function getUserAllTitles($partnerUid){
        $titles = $this->Dao->select()->from(self::TABLE_NAME_USER_TITLE)->where('uid="'.$partnerUid.'"')->orderby('level')->exec(false);
        return $titles;
    }

    /**
     * 获取用户最新的健康信息
     * @param $uid 用户编号
     * @return array
     */
    public function getUserLatestHealthInfo($uid){
        $result = $this->Dao->select()->from(self::TABLE_NAME_USER_HEALTH_INFO)->where('uid="'.$uid.'"')->orderby('add_time')->desc()->getOneRow(false);
        return $result ? $result : [];
    }

    /**
     * 计算bmi
     * @param $weight 体重，单位为KG
     * @param $height 身高，单位为cm
     * @param $isConvert 是否进行单位转换
     * @return int
     */
    public function calBmi($weight , $height , $isConvert = false){
        //除数
        $divide = $weight;
        //被除数，因为身高的单位是厘米，实际的bmi中体重的计算单位为密
        $divided = ($height * $height) / 10000;
        $bmi =round($divide / $divided , 2);
        $bmi = $isConvert ? $bmi * self::BMI_CAL_FACT : $bmi;
        return $bmi;
    }

    /**
     * 根据升高和体重计算体重的类型
     * @param $weight 体重，单位为KG
     * @param $height 身高，单位为cm
     * @return int
     */
    public function calWeightType($weight,$height){
        $bmi = $this->calBmi($weight,$height,false);
        $mapping = $this->getBmiWeightTypeMapping();
        $weightType = 0;
        foreach ($mapping as $item){
            if($bmi >= $item['min'] && $bmi < $item['max']){
                $weightType = $item['type'];
            }
        }
        return $weightType;
    }

    /**
     * 根据BMI范围计算体重类型的映射关系
     */
    public function getBmiWeightTypeMapping(){
        return [
            //增肌
            [
                'min' => -1,
                'max' => 18.5,
                'type' => 2,
            ],
            //正常
            [
                'min' => 18.5,
                'max' => 24,
                'type' => 1,
            ],
            //减脂
            [
                'min' => 24,
                'max' => 10000,
                'type' => 3,
            ],
        ];
    }

    /**
     * 计算高级打卡的周期
     */
    public function calHighLevelDay($uid,$userInfo = null){
        $userInfo = $userInfo ? $userInfo : $this->getPartnerUserInfo($uid);
        $punchTimes = $userInfo['high_level_times'];
        $punchTimes = $punchTimes > 0 ? $punchTimes+1 : 1;
        $dayIndex = $punchTimes % self::HIGH_LEVEL_LAST_DAYS;
        $dayIndex = $dayIndex == 0 ? self::HIGH_LEVEL_LAST_DAYS:$dayIndex;
        $times = ( $punchTimes - $dayIndex ) /  self::HIGH_LEVEL_LAST_DAYS;
        return [
            'dayIndex' => $dayIndex,
            'times' => $times,
        ];
    }


    /**
     * 计算高级打卡的周期
     */
    public function calLowLevelDay($uid,$userInfo = null){
        $userInfo = $userInfo ? $userInfo : $this->getPartnerUserInfo($uid);
        $punchTimes = $userInfo['low_level_times'];
        $punchTimes = $punchTimes > 0 ? $punchTimes+1 : 1;
        $dayIndex = $punchTimes % self::LOW_LEVEL_LAST_DAYS;
        $dayIndex = $dayIndex == 0 ? self::LOW_LEVEL_LAST_DAYS:$dayIndex;
        $times = ( $punchTimes - $dayIndex ) /  self::LOW_LEVEL_LAST_DAYS;
        return [
            'dayIndex' => $dayIndex,
            'times' => $times,
        ];
    }


    /**
     * 发送中断模板数据给中英
     * @param $uid 用户编号
     * @param $dayIndex 天数对应的索引
     * @param $isHighLevel 是否为高级
     * @return array
     */
    public function sendInterruptTplMsgToZy($uid, $dayIndex, $isHighLevel = false){
        if($result = ZyHealthClockHelper::sendInterruptRemindTplMsg($uid,$dayIndex,$isHighLevel)){
            $insertData = [];
            foreach ($result as $key => $val){
                $newKey = ConvertHelper::humpToLine($key);
                $insertData[$newKey] = urldecode($val);
            }

            if($insertData){
                $this->insertData(self::TABLE_NAME_TPL_SEND_LOG,$insertData);
            }
        }
        return $result;
    }
    /**
     * 发送完成模板数据给中英
     * @param $uid 用户编号
     * @param $userName 天数对应的索引
     * @param $isHighLevel 是否为高级
     * @return array
     */
    public function sendFinishRemindTplMsgToZy($uid, $userName, $isHighLevel = false){
        if(!$userName){
            $userInfo = $this->getOriginUserInfo($uid);
            $userName = isset($userInfo['partner_name']) ? $userInfo['partner_name'] : $uid;
        }
        if($result = ZyHealthClockHelper::sendFinishPunchRemindTplMsg($uid,$userName,$isHighLevel)){
            $insertData = [];
            foreach ($result as $key => $val){
                $newKey = ConvertHelper::humpToLine($key);
                $insertData[$newKey] = urldecode($val);
            }

            if($insertData){
                $this->insertData(self::TABLE_NAME_TPL_SEND_LOG,$insertData);
            }
        }
        return $result;
    }

    /**
     * 发送抢奖品模板数据给中英
     * @param $uid 用户编号
     * @param $isHighLevel 是否为高级
     * @return array
     */
    public function sendAwardRemindTplMsgToZy($uid, $isHighLevel = false){
        if($result = ZyHealthClockHelper::sendAwardRemindTplMsg($uid,$isHighLevel)){
            $insertData = [];
            foreach ($result as $key => $val){
                $newKey = ConvertHelper::humpToLine($key);
                $insertData[$newKey] = urldecode($val);
            }
            if($insertData){
                $this->insertData(self::TABLE_NAME_TPL_SEND_LOG,$insertData);
            }
             //将用户添加到抢购资格列表中
            $this->insertAwardQualification($uid);

        }
        return $result;
    }


    /**
     * 中断提醒的定时任务
     */
    public function sendInterruptMsgCronJob($isHighLevel = false){
        $sendResult = [];
        if($isHighLevel){
            $condition = 'high_level_times >= 1 and high_level_times <'.self::HIGH_LEVEL_LAST_DAYS;
        }else{
            $condition = 'low_level_times >= 1 and low_level_times <'.self::LOW_LEVEL_LAST_DAYS;
        }

        if($userList = $this->getAllUser($condition)){
            $originUids = array_column($userList,'partner_uid');
            //获取当日所有已经打卡的用户记录
            $recordList = $this->getCurrentDayRecordList($isHighLevel);
            //先获取用户编号，再进行排重，因为再低级打卡中可能存在多条相同的用户记录的情况
            $puids = array_unique(array_column($recordList,'uid'));

            //取交集，这个是为了排除从茄子打卡的用户
            $currentDayPunchedZyUids = array_intersect($originUids,$puids);
            //取差集，用于获取未打卡的用户列表
            $currentDayUnpunchZyUids = array_diff($originUids,$currentDayPunchedZyUids);
            //未打卡的用户
            if($currentDayUnpunchZyUids){
                $baseUrl = $this->getBaseModuleRequestPath('sendInterruptTplMsg');
                foreach ($currentDayUnpunchZyUids as $uid){
                    $dayResult = $this->calLowLevelDay($uid);
                    //需要待定，是否使用异步来推送数据，如果数据量很大，则会造成网络堵塞
                    $dayIndex = $dayResult['dayIndex'];
                    $param = [
                        'uid' => $uid,
                        'day_index' => $dayIndex,
                        'is_high_level' => $isHighLevel,
                    ];
                    $url = $this->buildModuleRequestUrl($baseUrl,$param);
                    //异步调用http请求
                    HttpHelper::sendAsynRequest($url);
                    array_push($sendResult,$url);
                }
            }
        }
        return $sendResult;
    }

    /**
     * 抢领奖提醒的定时任务
     */
    public function sendAwardMsgCrondJob(){
        $sendResult = [];
        //只有完成过一次打卡的才有资格
//        if($userList = $this->getAllUser('low_level_times >= 1')){
//            $uids = array_column($userList,'partner_uid');
        if($userList = $this->getQualificationUserList()){
            $uids = array_column($userList,'openid');
            $baseUrl = $this->getBaseModuleRequestPath('sendAwardRemindTplMsg');
            foreach ($uids as $uid){
                $param = [
                    'uid' => $uid,
                    'is_high_level' => false,
                ];
                $url = $this->buildModuleRequestUrl($baseUrl,$param);
                //异步调用http请求
                HttpHelper::sendAsynRequest($url);
                array_push($sendResult,$url);
            }
        }
        return $sendResult;
    }


    public function getQualificationUserList(){
        global $config;
        $openDay = $config->exchange_open_day;
        $openTime = date('Y-m').'-'.$openDay;
        $startDay = date('Y-m-d',strtotime("$openTime -1 month"));
        $endDay = date('Y-m-d',strtotime("$openTime -1 day"));
        $beginDayTime = strtotime($startDay." 00:00:00");
        $endDayTime = strtotime($endDay." 23:59:59");
        $sql = "select *  from `zy_hch_tpl_send_log` where LEFT(`timestamp`,10) >=$beginDayTime and LEFT(`timestamp`,10)<=$endDayTime and `mark` = 'B' GROUP BY openid";
        $list = $this->Db->query($sql,false);
        return $list;
    }



    /**
     * 获取高级记录中的最大的编号
     * @param $uid  用户编号
     * @return string
     */
    public function getMaxHighLevelRecordId($uid){
        $offset  = $this->getOffsetTimeByTime();
//        $timeCondition = ' and add_time >= '.$offset['start'].' and add_time <= '.$offset['end'];
//        $sql = "select max(id)+1 id from ".self::TABLE_NAME_HIGH_LEVEL_RECORD." where uid = '$uid' $timeCondition";
        $sql = "select max(id)+1 id from ".self::TABLE_NAME_HIGH_LEVEL_RECORD." where uid = '$uid' ";
        $result = $this->Db->getOne($sql,false);
        $result = $result ? intval($result) : 0;
        return $result;
    }

    /**
     * 获取初级记录中的最大的编号
     * @param $uid  用户编号
     * @return int
     */
    public function getMaxLowLevelRecordId($uid){
        $sql = "select max(id)+1 id from ".self::TABLE_NAME_LOW_LEVEL_RECORD." where uid = '$uid'";
        $result = $this->Db->getOne($sql,false);
        $result = $result ? intval($result) : 0;
        return $result;
    }

    /**
     * 获取当天的最大的挑战记录序号
     * @return int
     */
    public function getLowLevelMaxChallengeIndex(){
        $offset = $this->getOffsetTimeByTime();
        $timeCondition = ' and add_time >='.$offset['start'].' and add_time <= '.$offset['end'];
        $sql = "select count(*)+1 from ".self::TABLE_NAME_LOW_LEVEL_RECORD ." where 1=1 $timeCondition"  ;
        $result = $this->Db->getOne($sql,false);
        $result = $result ? intval($result) : 1;
        $result = ZyHealthClockHelper::getRandomBaseChallengeIndex()+ $result;
        return $result;
    }

    /**
     * 获取当天的打卡记录，用于判断当天有没有打卡
     * @param $uid  用户编号
     * @param $isHighLevel  是否为高级打卡
     * @return array
     */
    public function getCurrentDayPunchInfo($uid,$isHighLevel=false){
        global $config;
        $checkoutDailyRecord = $config->check_daily_record;
        $data = false;
        if($checkoutDailyRecord){
            $time  = date('Y-m-d');
            $startTime = strtotime($time.' 00:00:00');
            $endTime = strtotime($time.' 23:59:59');
            $table = $isHighLevel ? self::TABLE_NAME_HIGH_LEVEL_RECORD : self::TABLE_NAME_LOW_LEVEL_RECORD;
            $data = $this->Dao->select()->from($table)
                ->where('uid="'.$uid.'"')
                ->aw('add_time >='.$startTime)
                ->aw('add_time <='.$endTime)
                ->orderby('add_time')
                ->desc()
                ->getOneRow(false);
        }
        return $data;
    }


    /**
     * 获取所有的方案
     */
    public function getAllCaseDetail(){
        $redisKey = self::REDIS_PREFIX_ALL_CASE_DATA;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            if($data = $this->Dao->select()->from(self::TABLE_NAME_CASE_DETAIL)->orderby('type')->desc()->exec(false)){
                $result = [];
                foreach ($data as $caseDetail){
                    $type = $caseDetail['type'];
                    if(!$result[$type]){
                        $result[$type] = [];
                    }
                    array_push($result[$type],$caseDetail);
                }
                RedisUtil::set($redisKey,$result);
            }
        }
        return $data;
    }




    /**
     * 根据方案类型以及日期获取方案详情
     * @param $type 方案的类型，1为正常，2位增肌，3位减脂
     * @param $day 日期
     * @return array
     */
    public function getCaseDetailByTypeAndDay($type,$day){
        $detail = [];
        if($list = $this->getAllCaseDetail()){
            $singleTypeList = $list[$type];
            if($singleTypeList){
                foreach ($singleTypeList as $item){
                    if($item['day_index'] == $day){
                        $detail = $item;
                        break;
                    }
                }
            }
        }
        if($detail){
            $detail['notice'] = str_replace("\n","<br/>",$detail['notice']);
        }
        return $detail;
    }

    /**
     * 获取当日的初级打卡记录列表
     * @param $isHighLevel 是否为高级打卡
     * @return array
     */
    public function getCurrentDayRecordList($isHighLevel = false){
        $offset = $this->getOffsetTimeByTime();
        $timeCondtion = 'add_time >= '.$offset['start'] . ' and add_time <= '.$offset['end'];
        $table = $isHighLevel ? self::TABLE_NAME_HIGH_LEVEL_RECORD : self::TABLE_NAME_LOW_LEVEL_RECORD;
        $query = $this->Dao->select()->from($table)->where("1=1")->aw($timeCondtion);
        $list = $query->exec(false);
        return $list ? $list : [];
    }

    /**
     * 创建兑换订单
     * @param $uid 用户编号
     * @param $prizeId 礼品编号
     * @return array
     */
    public function createExchageOrder($uid,$prizeId){
        //对于同一个奖品的订单增加一个5分钟创建一次的
        $latestOrder = $this->getUserLatestPrizeOrder($uid,$prizeId);
        if($latestOrder){
            $now = time();
            $orderTime = $latestOrder['order_time'];
            $diffTime = $now - $orderTime;
            if( $diffTime <= 60 * 5){
                return $latestOrder;
            }
        }
        $userInfo = $this->getPartnerUserInfo($uid);
        $prizeInfo = $this->getExchangePrizeInfoById($prizeId);
        $orderData = [
            'uid' => $uid,
            'uname' => $userInfo['partner_name'],
            'prize_id' => $prizeId,
            'prize_name' => $prizeInfo['prize_name']?$prizeInfo['prize_name']:'',
            'order_time' => time(),
            'update_time' => time(),
            'address_id' => self::DEFAULT_NUMERIC_VALUE,
            'receive_time' => self::DEFAULT_NUMERIC_VALUE,
            'status' => 'CHECKED',
        ];
        $orderId = $this->insertOrder($orderData);
        $orderData['id'] = $orderId;
        return $orderData;
    }

    /**
     * 创建用户对应的礼品订单
     */
    public function getUserLatestPrizeOrder($uid,$prizeId){
        $query = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_ORDER)
            ->where('uid = "'.$uid.'"')
            ->aw('prize_id = "'.$prizeId.'"')
            ->aw('status = "CHECKED"')
            ->orderby('order_time')->desc();
        $orderInfo = $query->getOneRow();
        return $orderInfo;
    }


    /**
     * 根据奖品编号获取奖品信息
     * @param $prizeId 奖品编号
     * @return array
     */
    public function getExchangePrizeInfoById($prizeId){
        $redisKey = self::REDIS_PREFIX_EXCHANGE_PRIZE_INFO.$prizeId;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            if($data = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_PRIZE)->where("id = '$prizeId'")->getOneRow()){
                $images = $this->getPrizeImages($prizeId);
                $data['images'] = $images ? $images :[];
                RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
            }
        }
        return $data;
    }

    /**
     * 获取用户可用的奖品列表
     * @param $uid 用户编号
     * @param $userInfo 用户信息
     * @return array
     */
    public function getAvailablePrizeList($uid,$userInfo = null){
        $userInfo = $userInfo ? $userInfo : $this->getPartnerUserInfo($uid);
        $prizeList = $userInfo['cust_no'] ? $this->getCustomerMonthPrizeList() : $this->getUserMonthPrizeList();
        return $prizeList;
    }

    /**
     * 中英客户的奖品列表
     * @param $year 年份
     * @param $month 月份
     * @return array
     */
    public function getCustomerMonthPrizeList($year = '',$month = ''){
        $results = [];
        $year = is_numeric($year) ? $year : date('Y');
        $month = is_numeric($month) ? (strlen($month) == 1 ? '0'.$month :$month) : date('m');
        $cacheKey = self::REDIS_PREFIX_ZY_CUSTOMER_PRIZE_LIST.$year.'_'.$month;
        if(!$results = RedisUtil::get($cacheKey)){
            $results = [];
            if($list = $this->getMonthPrizeList($year,$month)){
                foreach ($list as $prize){
                    if(!$prize['is_to_user']){
                        array_push($results,$prize);
                    }
                }
                RedisUtil::set($cacheKey,$results,CACHE_TIME_ONE_DAY);
            }
            $results = $results ? $results :[];
        }
        return $results;

    }


    /**
     * 中英用户的奖品列表
     * @param $year 年份
     * @param $month 月份
     * @return array
     */
    public function getUserMonthPrizeList($year = '',$month = ''){
        $year = is_numeric($year) ? $year : date('Y');
        $month = is_numeric($month) ? (strlen($month) == 1 ? '0'.$month :$month) : date('m');
        $cacheKey = self::REDIS_PREFIX_ZY_USER_PRIZE_LIST.$year.'_'.$month;
        if(!$results = RedisUtil::get($cacheKey)){
            $results = [];
            if($list = $this->getMonthPrizeList($year,$month)){
                foreach ($list as $prize){
                    if($prize['is_to_user']){
                        array_push($results,$prize);
                    }
                }
                RedisUtil::set($cacheKey,$results,CACHE_TIME_ONE_DAY);
            }
            $results = $results ? $results :[];
        }
        return $results;
    }

    /**
     * 获取月份的奖品列表
     * @param $year 年份
     * @param $month 月份
     * @return array
     */
    public function getMonthPrizeList($year = '',$month = ''){
        $year = is_numeric($year) ? $year : date('Y');
        $month = is_numeric($month) ? (strlen($month) == 1 ? '0'.$month :$month) : date('m');
        $belongTime = $year.$month;
        $redisKey = self::REDIS_PREFIX_PRIZE_LIST.$belongTime;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            if($data = $this->getMonthAvailablePrizeList($belongTime) ){
                global $config;
                $baseUrl = $config->domain.$config->zy_hch_prize_img_link;
                foreach ($data as &$prize){
                    $prize['cover_img'] = $baseUrl . $prize['cover_img'];
                }
                RedisUtil::set($redisKey,$data,CACHE_TIME_ONE_DAY * 3);
            }
            $data = $data ? $data : [];
        }
        return $data;
    }


    /**
     * 获取月份的有效的奖品列表
     * @param $belongTime 时间段格式为，201708
     * @return array
     */
    public function getMonthAvailablePrizeList($belongTime){
        $timeCondition = 'belong_time="'.$belongTime.'"';
        //上线的商品,同时该商品没有删除
        $otherCondtion = 'is_online = 1 and is_delete = 0';
        $query = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_PRIZE)
            ->where($timeCondition)->aw($otherCondtion)->orderby('sort')->desc();
        $list = $query->exec(false);
        return $list ? $list : [];
    }




    /**
     * 根据订单编号获取订单信息
     * @param $prizeId 奖品编号
     * @return array
     */
    public function getOrderInfoById($orderId){
        $redisKey = self::REDIS_PREFIX_ORDER_INFO.$orderId;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            if($data = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_ORDER)->where("id = '$orderId'")->getOneRow()){
                RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
            }
        }
        return $data;
    }

    public function updateOrderInfoByOrderId($orderId,$data){
        $query = $this->Dao->update(self::TABLE_NAME_EXCHAGE_ORDER)->set($data)->where('id="'.$orderId.'"');
        if($result = $query->exec(false)){
            $orderInfo = $this->getOrderInfoById($orderId);
            foreach ($data as $key => $val){
                $orderInfo[$key] = $val;
            }
            $redisKey = self::REDIS_PREFIX_ORDER_INFO.$orderId;
            RedisUtil::set($redisKey,$orderInfo,CACHE_TIME_TEN_DAY);
        }
        return $result;
    }

    /**
     * 根据编号更新奖品信息
     */
    public function updatePrizeInfoById($prizeId,$data,$prizeInfo = null){
        $query = $this->Dao->update(self::TABLE_NAME_EXCHAGE_PRIZE)->set($data)->where('id="'.$prizeId.'"');
        if($result = $query->exec(false)){
            $prizeInfo = $prizeInfo ? $prizeInfo : $this->getExchangePrizeInfoById($prizeId);
            foreach ($data as $key => $val){
                $prizeInfo[$key] = $val;
            }
            $redisKey = self::REDIS_PREFIX_EXCHANGE_PRIZE_INFO.$prizeId;
            RedisUtil::set($redisKey,$prizeInfo,CACHE_TIME_TEN_DAY);
        }
        return $result;
    }

    /**
     * 获取用户启用的地址
     */
    public function getUserEnableAddr($uid){
        $address = [];
        if($list = $this->getUserAddressList($uid)){
            foreach ($list as $addr){
                if($addr['enable'] > 0){
                    $address =  $addr;
                    break;
                }
            }
        }
        return $address;
    }


    public function getUserAddressList($uid){
        $redisKey = self::REDIS_PREFIX_USER_ADDRESS_LIST.$uid;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            if($data = $this->Dao->select()->from(self::TABLE_NAME_USER_ADDRESS)->where("uid = '$uid'")->exec(false)){
                RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
            }
        }
        return $data ? $data : [];
    }

    public function updateEnableAddress($uid,$newAddrId){
        $result = 0;
        $enableAddr = $this->getUserEnableAddr($uid);
        if($enableAddr['id'] != $newAddrId){
            $redisKey = self::REDIS_PREFIX_USER_ADDRESS_LIST.$uid;
            $otherAddressUpdateData = [
                'enable' => 0,
                'update_time' => time()
            ];
            $this->updateUserAddressByUid($uid,$otherAddressUpdateData);
            //更新enable地址
            $updateData = [
                'enable' => 1,
                'update_time' => time()
            ];
            $result = $this->updateUserAddressById($newAddrId,$updateData);
            RedisUtil::delete($redisKey);
        }
        return $result;
    }


    public function updateUserAddressByUid($uid,$updateData){
        $query = $this->Dao->update(self::TABLE_NAME_USER_ADDRESS)->set($updateData)->where('uid="'.$uid.'"');
        $result = $query->exec(false);
        return $result;
    }
    public function updateUserAddressById($id,$updateData){
        $query = $this->Dao->update(self::TABLE_NAME_USER_ADDRESS)->set($updateData)->where('id="'.$id.'"');
        $result = $query->exec(false);
        return $result;
    }


    /**
     * 套餐卡号列表
     */
    public function getPrizeList($where='',$orderby='add_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $query = $list = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_PRIZE)->where($where)->orderby($orderby)->limit("$offset,$pageSize");
            $list = $query->exec(false);
        }else{
            $query = $this->Dao->select()->from(self::TABLE_NAME_EXCHAGE_PRIZE)->where($where)->orderby($orderby);
            $list = $query->exec(false);
        }
        if(count($list) > 0){
            foreach ($list as &$val){
                $val['add_time'] = date('Y-m-d H:i',$val['add_time']);
                $val['update_time'] = date('Y-m-d H:i',$val['update_time']);
                $type = $val['prize_type'];
                $typeDesc = '';
                if($type == 1){
                    $typeDesc = '普通商品';
                }else if($type == 2){
                    $typeDesc = '满减优惠券<br>(满'.($val['satisfy_money']/100).'元减'.($val['coupon_money']/100).'元)';
                }else if($type == 3){
                    $typeDesc = '固定值优惠券<br>(面额：'.($val['coupon_value']/100).'元)';
                }else if($type == 4){
                    $typeDesc = '购物卡<br>(面额：'.($val['coupon_value']/100).'元)';
                }
                $val['prize_type'] = $typeDesc;

                $val['is_to_user'] = $val['is_to_user'] ? '中英用户' : '中英客户';
            }
        }
        return $list;
    }

    /**
     * @param type $postData
     * @return boolean
     */
    public function modifyPrize($postData) {
        $ret = false;
        $id = intval($postData['id']);
        if ($id == 0) {
            // 新建商品
            $id = 'NULL';
        }

        // 检查商品首图是否需要上传，并且加入上传列表
        foreach ($postData['product_infos'] as &$d) {
            if ($d['name'] == 'cover_img' && $d['value'] != '') {
                if (preg_match("/".self::IMAGE_UPLOAD_MARK."/is", $d['value'])) {
                    $postData['product_images'][-1] = $d['value'];
                    $d['value'] = str_replace(self::IMAGE_UPLOAD_MARK, '', $d['value']);
                }
            }
        }
        $prizeDesc = '';

        if ($id == 'NULL') {
            // 新建商品
            $field = array();
            $values = array();
            $year = date('Y');
            foreach ($postData['product_infos'] as &$d) {
                if ($d['name'] == 'product_desc') {
                    $prizeDesc = $d['value'];
                }
                if($d['name'] == 'belong_time'){
                    $d['value'] = $year.$d['value'];
                    $belongTime = $year.$d['value'];
                    $month = $d['value'];
                }
                if ($d['value'] != '') {
                    $field[] = "`$d[name]`";
                    $values[] = "'$d[value]'";
                }
            }

            $field[] = 'add_time';
            $values[] = time();
            $SQL = sprintf("INSERT INTO ".self::TABLE_NAME_EXCHAGE_PRIZE." (%s) VALUES (%s);", implode(',', $field), implode(',', $values));
            // 插入商品数据
            if($id = $this->Db->query($SQL)){
                $this->clearPrizeCache($id,$year,$month);
            };
            $ret = $id > 0;
        } else {

            $prize = $this->getExchangePrizeInfoById($id);
            $this->coverImg = $prize['cover_img'];
            $year = date('Y');
            $belongTime = '';
            $month = '';
            // 修改商品信息
            if ($id > 0) {
                $set = array();
                foreach ($postData['product_infos'] as &$d) {
                    if ($d['name'] == 'note') {
                        $note = $d['value'];
                    }

                    if($d['name'] == 'belong_time'){
                        if(strlen($d['value']) == 6){
                            $belongTime = $d['value'];
                            $month = substr($belongTime,4);
                        }else{
                            $belongTime = $year.$d['value'];
                            $month = $d['value'];
                            $d['value'] = $belongTime;
                        }

                    }

                    $set[] = sprintf("`$d[name]` = '%s'", addslashes($d['value']));
                }
                $set[] = sprintf("`update_time` = '%s'", time());

                $set = implode(',', $set);
                $SQL = "UPDATE ".self::TABLE_NAME_EXCHAGE_PRIZE." SET $set WHERE `id` = '$id';";
            }
            if($ret = $this->Db->query($SQL)){
                //清除缓存
                $this->clearPrizeCache($id,$year,$month);
            }
        }

        // 静态化商品描述html
        $stati_dir = dirname(__FILE__) . '/../html/zy_prize/';

        if (!is_dir($stati_dir)) {
            mkdir($stati_dir, 0755);
        }
        file_put_contents($stati_dir . $id . '.html', $note);
        $this->handleProductImages($id, $postData['product_images']);
        return $id;
    }

    /**
     * 商品上下架
     */
    public function switchOnlinePrize($prizeId,$isOnline,$prizeInfo = null){
        $updateData = [
            'is_online' => $isOnline
        ];
        $prizeInfo = $prizeInfo ? $prizeInfo : $this->getExchangePrizeInfoById($prizeId);
        $belongTime = $prizeInfo['belong_time'];
        $year = substr($belongTime,0,4);
        $month = substr($belongTime,4);
        if($result = $this->updatePrizeInfoById($prizeId,$updateData)){
            $this->clearPrizeCache($prizeId,$year,$month);
        }
        return $result;
    }

    /**
     * 删除奖品
     */
    public function deletePrize($prizeId,$prizeInfo = null){
        $prizeInfo = $prizeInfo ? $prizeInfo : $this->getExchangePrizeInfoById($prizeId);
        $updateData = [
            'is_delete' => 1
        ];
        if($result = $this->updatePrizeInfoById($prizeId,$updateData)){
            $belongTime = $prizeInfo['belong_time'];
            $year = substr($belongTime,0,4);
            $month = substr($belongTime,4);
            $this->clearPrizeCache($prizeId,$year,$month,false);
        }
    }

    /**
     * 清除商品的缓存，用于后台更新商品时候用
     */
    public function clearPrizeCache($prizeId,$year,$month,$clearDetail = true){
        $belongTime = $year.$month;
        $belongTimeRedisKey = self::REDIS_PREFIX_PRIZE_LIST.$belongTime;
        $detailRedisKey = self::REDIS_PREFIX_EXCHANGE_PRIZE_INFO.$prizeId;
        $customPrizeListKey = self::REDIS_PREFIX_ZY_CUSTOMER_PRIZE_LIST.$year.'_'.$month;
        $userPrizeListKey = self::REDIS_PREFIX_ZY_USER_PRIZE_LIST.$year.'_'.$month;
        RedisUtil::delete($belongTimeRedisKey);
        if($clearDetail){
            RedisUtil::delete($detailRedisKey);
        }
        RedisUtil::delete($customPrizeListKey);
        RedisUtil::delete($userPrizeListKey);
    }

    /**
     * 处理奖品图片数据
     * @global type $config
     * @param type $id
     * @param type $images
     */
    public function handleProductImages($id, $images) {
        global $config;
        error_log("======images==========".json_encode($images));
        // 目录检查
        if (!is_dir($config->zy_hch_prize_img_dir)) {
            mkdir($config->zy_hch_prize_img_dir, 644);
        }
        if (!is_dir($config->zy_hch_prize_img_tmp_dir)) {
            mkdir($config->zy_hch_prize_img_tmp_dir, 644);
        }
        //先删除数据
        $this->Dao->delete()->from(self::TABLE_NAME_EXCHANGE_PRIZE_IMG)->where("prize_id = '$id'")->exec(false);
        foreach ($images as $sort => $image) {
            $image = str_replace(self::IMAGE_UPLOAD_MARK, '', $image);
            $imagePath = $config->zy_hch_prize_img_dir . $image;
            $imageTmpPath = $config->zy_hch_prize_img_tmp_dir . $image;
            // 移动图片文件
            if (is_file($imageTmpPath)) {
                rename($imageTmpPath, $imagePath);
            }
            if ($sort != -1) {
                if($image != ""){
                    $this->Dao->insert(self::TABLE_NAME_EXCHANGE_PRIZE_IMG, '`prize_id`,`image_path`,`image_type`,`image_sort`')->values(array($id, $image, 0, $sort))->exec();
                }
            } else {
                // 如果是商品首图，而且重新上传了图片，则删除旧图片
                is_file($config->zy_hch_prize_img_dir . $this->coverImg) && @unlink($config->zy_hch_prize_img_dir . $this->coverImg);
            }

        }
    }


    /**
     * 获取商品图片列表
     * @param type $prizeId
     * @param type $limit
     */
    public function getPrizeImages($prizeId) {
        $query = $this->Dao->select()->from(self::TABLE_NAME_EXCHANGE_PRIZE_IMG)->where("prize_id = '$prizeId'")->orderby('image_sort');
        $list = $query->exec(false);
        return $list;
    }


    /**
     * 初始化奖品的库存缓存列表
     * @param $prizeId 奖品编号
     * @param $prizeInfo 奖品信息对象
     */
    public function initPrizeQueueCache($prizeId, $prizeInfo = null){
        if($prizeInfo = $prizeInfo ? $prizeInfo : $this->getExchangePrizeInfoById($prizeId)){
            $stock = $prizeInfo['stock'];
            //防止奖品抢完之后重新刷新redis信息
            if($stock > 0 && $this->getPrizeQueueLen($prizeId)==0){
                $cacheKey = $this->getPrizeStockCacheKey($prizeId);
                for($i=1;$i<=$stock;$i++){
                    RedisUtil::lPush($cacheKey,$i);
                }
            }
        }
    }

    /**
     * 礼品库存队列出栈
     */
    public function popPrizeQueue($prizeId){
        $key = $this->getPrizeStockCacheKey($prizeId);
        return RedisUtil::lPop($key);
    }

    /**
     * 获取奖品队列数据
     * @param $prizeId 奖品编号
     * @return array
     */
    public function getPrizeQueue($prizeId){
        $key = $this->getPrizeStockCacheKey($prizeId);
        return RedisUtil::lRange($key,0,-1);
    }

    /**
     * 获取奖品队列长度
     * @param $prizeId 奖品编号
     * @return integer
     */
    public function getPrizeQueueLen($prizeId){
        $key = $this->getPrizeStockCacheKey($prizeId);
        return RedisUtil::lLen($key);
    }

    /**
     * 获取奖品的缓存key
     * @param $prizeId 奖品编号
     * @return string
     */
    public function getPrizeStockCacheKey($prizeId){
        return self::REDIS_PREFIX_PRIZE_STOCK_QUEUE_LIST.$prizeId;
    }


    /**
     * 提交订单
     * @param $uid 用户编号
     * @param $prizeId 奖品编号
     * @param $orderId 订单编号
     * @param $orderId 订单编号
     */
    public function submitExchange($uid,$prizeId,$orderId,$topQueueValue,$addressId){
        //更改订单的状态为已经完成
        $time =  time();
        $orderUpdateData = [
            'status' => 'SUCC',
            'receive_time' => $time,
            'update_time' => $time,
            'fail_reason' => 'SUCCESS',
            'address_id' => $addressId,
        ];
        $prizeInfo = $this->getExchangePrizeInfoById($prizeId);
        $generatePrizeNoCouponTypes = [2,3,4];
        $prizeNo = '';
        $prizePwd = '';
        if(in_array($prizeInfo['prize_type'],$generatePrizeNoCouponTypes)){
            $prizeNo  = $this->generatePrizeNo($orderId);
            $orderUpdateData['prize_no']  = $prizeNo;
            $prizePwd  = $this->generatePrizePwd();
            $orderUpdateData['prize_pwd']  = $prizePwd;
        }
        //当队列的值为最后一个时候表示售罄
        if($topQueueValue && $topQueueValue == 1){
            $orderUpdateData['is_sold_out'] = 1;
        }

        if($result = $this->updateOrderInfoByOrderId($orderId,$orderUpdateData)){
            //减库存
            $stock = $prizeInfo['stock'];
            $updateStock = $stock - 1 > 0 ? ($stock -1) : 0;
            $prizeUpdateData = [
                'stock' => $updateStock
            ];
            $this->updatePrizeInfoById($prizeId,$prizeUpdateData,$prizeInfo);

            $prizeId = $prizeInfo['id'];
            $belongTime = $prizeInfo['belong_time'];
            $year = substr($belongTime,0,4);
            $month = substr($belongTime,4);
            $allPrizeListKey = self::REDIS_PREFIX_PRIZE_LIST.$belongTime;
            $isToUser = $prizeInfo['is_to_user'];
            $subPrizeListKey = $isToUser ? self::REDIS_PREFIX_ZY_USER_PRIZE_LIST.$year.'_'.$month:self::REDIS_PREFIX_ZY_CUSTOMER_PRIZE_LIST.$year.'_'.$month;
            if($allPrizeList = RedisUtil::get($allPrizeListKey)){
                foreach ($allPrizeList as &$allPrizeOjb){
                    if($allPrizeOjb['id'] == $prizeId){
                        $allPrizeOjb['stock'] = $updateStock;
                    }
                }
                RedisUtil::set($allPrizeListKey,$allPrizeList,CACHE_TIME_ONE_DAY*3);

                if($subPrizeList = RedisUtil::get($subPrizeListKey)){
                    foreach ($subPrizeList as &$subPrizeObj){
                        if($subPrizeObj['id'] == $prizeId){
                            $subPrizeObj['stock'] = $updateStock;
                        }
                    }
                    RedisUtil::set($subPrizeListKey,$subPrizeList,CACHE_TIME_ONE_DAY);
                }
            }

            //实物奖品没有有效期
            $validTime = in_array($prizeInfo['prize_type'],$generatePrizeNoCouponTypes) ? $time + 3600 * 24 * 365 : '';
            //设置有效期为一年
            $src = '健康打卡';
            $prizeGrade = 'vip';
            $isShow = '1';
            $user = $this->getPartnerUserInfo($uid);
            $response = ZyHealthClockHelper::sendExchangeData($uid, $prizeInfo['prize_name'], $time, $validTime, $prizeInfo['prize_desc'],$src, $prizeNo, $prizePwd,$prizeGrade,$isShow);
            //插入奖品日志
            $this->insertUserExchangeLog($orderId,$prizeId,$uid,$user['parter_name'],$prizeNo,$prizePwd,json_encode($response));
        }
        return $result;
    }


    /**
     * 检查用户是否有抢购的权限
     * @param $uid 用户编号
     * @return boolean
     */
    public function hasAwardPermission($uid){
        error_log('current uid is===========>'.$uid);
        //是否在黑名单中
        if($this->isUserInBlackList($uid)){
            return false;
        }
        //是否在抢购资格列表中
        if(!$this->isUserInQualificationList($uid)){
            return false;
        }
        return true;
    }

    /**
     * 判定用户是否在抢购资格列表中
     * @param $uid 用户编号
     * @return boolean
     */
    public function isUserInQualificationList($uid ,$year = 0,$month = 0){
        $year = $year ? $year : date('Y');
        $month = $month ? $month : date('m');
        $qualificationList = $this->getCurrentMonthQualificationCacheUserList($year,$month);
        error_log('qualificationList is==================>');
        error_log(json_encode($qualificationList));
        if(in_array($uid,$qualificationList)){
            error_log('user is in qualificationList==================>');
            return true;
        }
        return false;
    }


    /**
     * 判断是否是在黑名单中
     * @param $uid 用户编号
     * @return boolean
     */
    public function isUserInBlackList($uid){
        $blackList = $this->getUserBlackList();
        error_log('blacklist is==================>');
        error_log(json_encode($blackList));
        if(in_array($uid,$blackList)){
            error_log('user is in blacklist==================>');
            return true;
        }
        return false;
    }


    /**
     * 获取黑名单列表
     * @return array
     */
    public function getUserBlackList(){
        $cacheKey = self::REDIS_PREFIX_USER_BLACKLIST;
        if(!$list = RedisUtil::get($cacheKey)){
            if($list = $this->getAllBlackList()){
                $list = array_column($list,'uid');
            }
            $list = $list ? $list :[];
            RedisUtil::set($cacheKey,$list);
        }
        return $list;
    }

    /**
     * 获取黑名单列表
     * @return array
     */
    public function getAllBlackList(){
        return $this->Dao->select()->from(self::TABLE_NAME_AWARD_BLACKLIST)->exec(false);
    }


    /**
     * 获取当月有抢奖励资格的用户列表
     * @param $year 年份
     * @param $month 月份
     * @return array
     */
    public function getCurrentMonthQualificationCacheUserList($year,$month){
        $cacheKey = self::REDIS_PREFIX_CURRENT_MONTH_QUALIFICATION.$year.'_'.$month;
        if(!$list = RedisUtil::get($cacheKey)){
            $list = [];
            if($allList = $this->getCurrentMonthQualificationList($year,$month)){
                $list = array_column($allList,'uid');
                RedisUtil::set($cacheKey,$list);
            }
        }
        return $list;
    }


    /**
     * 判断是否能够重新兑换
     */
    public function canExchange($uid,$userInfo = null){
        $userInfo = $userInfo ? $userInfo :$this->getPartnerUserInfo($uid);
        global $config;
        $canExchangeTimes = $userInfo['cust_no'] ? $config->zy_hch_customer_can_exchange_times : $config->zy_hch_user_can_exchange_times;
        $totalExchangedTimes = $this->userExchangeSuccessTimes($uid);
        return $totalExchangedTimes >= $canExchangeTimes ? false : true;

    }

    /**
     * 用户兑换成功的次数
     */
    public function userExchangeSuccessTimes($uid,$prizeId = ''){
        $condition = 'uid = "'.$uid.'"';
        $beginDay = date('Y-m-01', strtotime(date("Y-m-d")));
        $endDay = date('Y-m-d', strtotime("$beginDay +1 month -1 day"));
        $startTime = strtotime($beginDay.' 00:00:00');
        $endTime =  strtotime($endDay.' 23:59:59');
        $timeCondition = "add_time between $startTime and $endTime ";
        $query = $this->Dao->select()->from(self::TABLE_NAME_PRIZE_EXCHANGE_LOG)
            ->where($condition)->aw($timeCondition);
        $list = $query->exec(false);
        $count = $list ? count($list) : 0;
        return $count;
    }


    /**
     * 获取当月有抢奖励资格的所有用户列表
     * @param $year 年份
     * @param $month 月份
     * @return array
     */
    public function getCurrentMonthQualificationList($year,$month){
        $list = $this->Dao->select()->from(self::TABLE_NAME_AWARD_QUALIFICATION)->where('year='.$year)->aw('month='.$month)->exec(false);
        return $list ? $list : [];
    }

    /**
     * 添加订单信息
     */
    public function insertOrder($data){
        $id = $this->insertData(self::TABLE_NAME_EXCHAGE_ORDER,$data);
        return $id;
    }

    /**
     * 插入用户的初级打卡历史记录
     * @param $uid 用户编号
     * @param $title 头衔名称
     * @param $level 头衔级别
     * @return int
     */
    public function insertUserTitle($uid,$title,$level){
        $data = [
            'uid' => $uid,
            'title' => $title,
            'level' => $level,
            'add_time' => time(),
        ];
        return $this->insertData(self::TABLE_NAME_USER_TITLE,$data);
    }

    /**
     * 插入用户的健康打卡数据
     * @param $uid 用户编号
     * @param $height 身高
     * @param $weight 体重
     * @param $sex 性别
     * @return int
     */
    public function insertUserHealthInfo($uid,$height,$weight,$targetWeight,$sex){
        $id = 0;
        if($userInfo = $this->getPartnerUserInfo($uid)){
            $punchTimes = $userInfo['high_level_times'];
            //计算轮次
            $remainder = $punchTimes % self::HIGH_LEVEL_LAST_DAYS;
            $timesIndex = ($punchTimes - $remainder) / self::HIGH_LEVEL_LAST_DAYS ;
            $timesIndex = $timesIndex + 1;
            $data = [
                'uid' => $uid,
                'height' => $height,
                'weight' => $weight * self::WEIGHT_CAL_FACT,
                'target_weight' => $targetWeight *  self::WEIGHT_CAL_FACT,
                'bmi' => $this->calBmi($weight,$height,true),
                'weight_type' => $this->calWeightType($weight,$height),
                'sex' => $sex,
                'times_index' => $timesIndex,
                'add_time' => time(),
            ];
            $id = $this->insertData(self::TABLE_NAME_USER_HEALTH_INFO,$data);
        }
        return $id;
    }
    /**
     * 插入用户的初级打卡历史记录
     */
    public function insertLowLevelRecord($uid,$userName,$spendTime,$questionData,$dayIndex,$score = 0,$challengeIndex = 0,$answerStatus = 0){
        $data = [
            'uid' => $uid,
            'user_name' => $userName,
            'spend_time' => $spendTime,
            'question_data' => $questionData,
            'add_time' => time(),
            'day_index' => $dayIndex,
            'score' => $score,
            'challenge_index' => $challengeIndex,
            'answer_status' => $answerStatus,
        ];
        return $this->insertData(self::TABLE_NAME_LOW_LEVEL_RECORD,$data);
    }
    /**
     * 插入用户的高级打卡历史记录
     */
    public function insertHighLevelRecord($uid,$userName,$dayIndex,$score){
        $data = [
            'uid' => $uid,
            'user_name' => $userName,
            'day_index' => $dayIndex,
            'score' => $score,
            'add_time' => time(),
        ];
        return $this->insertData(self::TABLE_NAME_HIGH_LEVEL_RECORD,$data);
    }

    /**
     * 插入用户的地址信息
     */
    public function insertUserAddress($uid,$userName,$province,$city,$area,$address,$phone){
        $data = [
            'uid' => $uid,
            'user_name' => $userName,
            'province' => $province,
            'city' => $city,
            'area' => $area,
            'address' => $address,
            'phone' => $phone,
            'add_time' => time(),
            'update_time' => time(),
        ];
        if($id = $this->insertData(self::TABLE_NAME_USER_ADDRESS,$data)){
            $redisKey = self::REDIS_PREFIX_USER_ADDRESS_LIST.$uid;
            RedisUtil::delete($redisKey);
        }
        return $id;

    }


    /**
     * 插入奖励资格表
     */
    public function insertAwardQualification($uid,$year = 0,$month =0){
        $data = [
            'uid' => $uid,
            'year' => $year ? $year :date('Y') ,
            'month' => $month ? $month :date('m') ,
            'add_time' => time() ,
        ];
        return $this->insertData(self::TABLE_NAME_AWARD_QUALIFICATION,$data);
    }
    /**
     * 插入用户黑名单表
     */
    public function insertUserBlackList($uid){
        $data = [
            'uid' => $uid,
        ];
        return $this->insertData(self::TABLE_NAME_AWARD_BLACKLIST,$data);
    }

    /**
     * 插入奖品兑换记录
     */
    public function insertUserExchangeLog($orderId, $prizeId, $uid, $username = '', $prizeNo = '', $prizePwd = '', $response=''){
        $data = [
            'uid' => $uid,
            'order_id' => $orderId,
            'prize_id' => $prizeId,
            'prize_no' => $prizeNo,
            'prize_pwd' => $prizePwd,
            'user_name' => $username,
            'response' => $response,
            'add_time' => time(),
        ];
        return $this->insertData(self::TABLE_NAME_PRIZE_EXCHANGE_LOG,$data);
    }


    /**
     * 公共的插入数据的方法
     */
    public function insertData($table,$data){
        $id = 0;
        if($data && count($data)){
            $keys = array_keys($data);
            $values = array_values($data);
            $field = implode(',',$keys);
            $id = $this->Dao->insert($table, $field)
                ->values($values)->exec(false);
        }
        return $id;
    }




    /**
     * 生成签名参数
     * @param $uid 用户编号
     * @param $partnerName 合作方用户名称
     * @return string
     */
    public function generateSign($uid,$partnerName,$token,$comeFrom='qiezi'){
        global $config;
        $encodeKey = $config->fruit_punch_url_encode_key;
        $partnerName = urldecode($partnerName);
        $encodeStr = $uid.$partnerName.$encodeKey.$token.$comeFrom;
        return md5($encodeStr);
    }

    /**
     * 生成签名环境所依赖的加密秘钥
     * @return string
     */
    public function generateUrlEncodeKey()
    {
        global $config;
        $environment = $config->environment;
        $encodeStr = 'zy_group#zhongliang#'.$environment;
        $encodeKey = md5($encodeStr);
        return $encodeKey;
    }


    /**
     * 根据输入的时间戳获取起始和截止时间
     * @param $time 时间戳
     * @return array
     */
    public function getOffsetTimeByTime($time = null){
        $time = $time ? $time :time();
        $day = date('Y-m-d',$time);
        $start = strtotime($day . " 00:00:00");
        $end = strtotime($day . " 23:59:59");
        return [
            'start' => $start,
            'end' => $end,
        ];
    }

    /**
     * 构建模块对应的url
     * @param $path 请求的路径
     * @param $params 请求的参数
     * @return string
     */
    public function buildModuleRequestUrl($url,$params = []){
        if($params){
            $param = http_build_query($params);
            $url = $url.'/'.$param;
        }
        return $url;
    }

    public function getBaseModuleRequestPath($path){
        global $config;
        $domain = $config->domain;
        $baseUrl = $domain.'?/vHealthClock/';
        $url = $baseUrl.$path;
        return $url;
    }


    /**
     * 说明： 生成奖品密码
     * 输出参数： true/false
     */
    public function generatePrizePwd(){
        $CODE_LEN = 18;
        $BYTE_LEN = 512 ;
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if (function_exists('openssl_random_pseudo_bytes')) {
            $random_string = openssl_random_pseudo_bytes($BYTE_LEN);
        }else{
            $random_string = substr(str_shuffle(str_repeat($pool, 5)), 0, $BYTE_LEN);
        }

        $md5 = md5($random_string);
        $sha1 = sha1($md5);
        $start = mt_rand(0, 25);
        $code = substr($sha1, $start, $CODE_LEN);
        return $code;
    }

    /**
     * 说明： 生成奖品编号
     * 输出参数： true/false
     */
    public function generatePrizeNo($orderId = null){
        $orderId = $orderId ? $orderId : rand(0,10000);
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $set_36 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $set_62 = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $current_time = time();
        $y = intval(date('y', $current_time));
        $m = intval(date('m', $current_time));
        $d = intval(date('j', $current_time));
        $h = intval(date('G', $current_time));
        $min = intval(date('i', $current_time));
        $s = intval(date('s', $current_time));
        $prefix = $chars[$m] . $set_36[$d] . $set_36[$h] . $set_62[$min] . $set_62[$s];
        $prizeNo = sprintf("%03d", $orderId);
        $prizeNo = $prefix . $prizeNo;
        return $prizeNo;
    }
}