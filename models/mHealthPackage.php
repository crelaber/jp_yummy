<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mHealthPackage extends Model {
    /**套餐信息表*/
    const TABLE_NAME = 'partner_health_package_info';
    /**套餐对应的订单表*/
    const TABLE_NAME_ORDER = 'partner_hp_order';
    /*合作伙伴的体检中心表*/
    const TABLE_NAME_HOSPITAL = 'partner_hp_hospital';
    /**合作伙伴用户表*/
    const TABLE_NAME_PARTNER_USER = 'partner_user';
    /**订单对应的卡号表*/
    const TABLE_NAME_PARTNER_ORDER_CARD = 'partner_hp_order_card';
    /**api接口调用表*/
    const TABLE_NAME_PARTNER_API_INVOKE_STATUS = 'partner_hp_api_invoke_status';


    const REDIS_PREFIX_PARTNER_USER = 'PARTNER_USER_INFO_';
    const REDIS_PREFIX_PLATFORM_USER = 'PLATFORM_USER_INFO_';

    /**
     * 获取单个套餐详情
     */
    public function get_package_info_by_id($id) {
        $redisKey = 'PACKAGE_INFO_'.$id;
        $data = RedisUtil::get($redisKey);
        if(!$data){
            $data = $this->Dao->select()->from(self::TABLE_NAME)->where("id = $id")->getOneRow();
            RedisUtil::set($redisKey,$data,CACHE_TIME_TEN_DAY);
        }
        return $data;
    }


    /**
     * 根据城市获取体检机构
     * @param $city string 城市
     * @param $excludeHospitalList array 待排除的医院列表
     * @return array
     */
    public function get_hospital_by_city($city, $exclude_list = []){
        $list = $this->get_hospital_list();
        $cityHospitalList = $list[$city]?$list[$city]:[];
        $finalHostpitalList = [];
        if(count($exclude_list) > 0 && count($cityHospitalList) > 0){
            foreach ($cityHospitalList as $hos){
                $obj = [];
                if(!in_array($hos['name'],$exclude_list)){
                    $obj['name'] = $hos['name'];
                    $obj['address'] = $hos['address'];
                    array_push($finalHostpitalList,$obj);
                }
            }
        }else{
            $finalHostpitalList = $cityHospitalList;
        }
        return $finalHostpitalList;
    }

    public function get_city_list($excludeCitys = []){
        $list = $this->get_hospital_list();
        $citys = $list ? array_keys($list) : [];
        $finalCitys =[];
        if(count($citys) > 0){
            if(count($excludeCitys) > 0){
                foreach ($citys as $city){
                    if(!in_array($city,$excludeCitys)){
                        array_push($finalCitys,$city);
                    }
                }
            }else{
                $finalCitys = $citys;
            }
        }
        return $finalCitys;
    }

    /**
     * 获取所有的医院列表
     */
    public function get_hospital_list(){

        $redis_key = 'ALL_HOSPITAL';
        $data = [];
        if(!$data = RedisUtil::get($redis_key)){
            if($list = $this->Dao->select()->from(self::TABLE_NAME_HOSPITAL)->exec(false)){
                foreach ($list as $val){
                    $city = $val['city'];
                    if(!$data[$city]){
                        $data[$city] = [];
                    }
                    array_push($data[$city],$val);
                }
            }
            RedisUtil::set($redis_key,$data);
        }
        return $data;

    }

    public function unicode_decode($name){
        $json = '{"str":"'.$name.'"}';
        $arr = json_decode($json,true);
        if(empty($arr)) return '';
        return $arr['str'];
    }



    /**
     * 创建订单并生成微信支付签名接口
     */
    public function createOrder($data,$orderMidFix = '',$is_produce = true,$openid = ''){
        $keys = array_keys($data);
        $values = array_values($data);
        $field = implode(',',$keys);
        error_log('createOrder keys=======>'.$keys);
        error_log('createOrder values=======>'.$keys);
        $orderId = $this->Dao->insert(self::TABLE_NAME_ORDER, $field)
            ->values($values)->exec(false);
        error_log('createOrder $orderId=======>'.$orderId);
        $outTradeNo = $this->generateOutTradeNo($orderId,$orderMidFix);
        $serArray = array('out_trade_no'=>$outTradeNo);
        error_log('createOrder openid=======>'.$openid);
        $this->updateOrder($orderId,$serArray);
        if($is_produce){
            $result = $this->wechatPay($data['total_amount'],$outTradeNo,$openid);
        }else{
            $result['orderId'] = $orderId;
            $result['outTradeNo'] = $outTradeNo;
        }

        return $result;
    }


    /**
     * 插入卡号信息
     */
    public function insertOrderCard($data){
        $logId = 0;
        if($data && count($data)){
            $keys = array_keys($data);
            $values = array_values($data);
            $field = implode(',',$keys);
            $logId = $this->Dao->insert(self::TABLE_NAME_PARTNER_ORDER_CARD, $field)
                ->values($values)->exec(false);
        }
        return $logId;
    }


    /**
     * 插入各个接口调用状态记录
     */
    public function insertInvokeStatus($orderId,$uid,$statusArr){
        $data = [
            'order_id'=> $orderId,
            'uid'=> $uid,
            'invoke_kk_card_api'=> $statusArr['invoke_kk_card_api'],
            'insert_card_log'=> $statusArr['insert_card_log'],
            'invoke_zy_callback_api'=> $statusArr['invoke_zy_callback_api'],
            'send_sms_to_user'=> $statusArr['send_sms_to_user'],
            'send_new_order_notify_msg'=> $statusArr['send_new_order_notify_msg'],
            'add_time'=> time(),
        ];
        return $this->insertData(self::TABLE_NAME_PARTNER_API_INVOKE_STATUS,$data);
    }

    /**
     * 公共的插入数据的方法
     */
    public function insertData($table,$data){
        $id = 0;
        if($data && count($data)){
            $keys = array_keys($data);
            $values = array_values($data);
            $field = implode(',',$keys);
            $id = $this->Dao->insert($table, $field)
                ->values($values)->exec(false);
        }
        return $id;
    }
    /**
     * 根据订单号获取订单id,不使用缓存
     */
    public function getOrderById($orderId){
        $data = $this->Dao->select()->from(self::TABLE_NAME_ORDER)->where("id = $orderId")->getOneRow();
        return $data;
    }

    /**
     * 使用微信支付获取签名以及信息
     * @param $amount 支付金额，单位为分
     * @param $outTradeNo 支付序列号
     * @return array
     */
    public function wechatPay($amount, $outTradeNo,$openid) {
        global $config;
        $orderId = $outTradeNo;
        // 订单总额
        $totalFee = $amount;
        $nonceStr = $this->Util->createNoncestr();
        $timeStamp = strval(time());
        $pack = array(
            'appid' => APPID,
            'body' => $config->zy_order_title,
            'timeStamp' => $timeStamp,
            'mch_id' => PARTNER,
            'nonce_str' => $nonceStr,
            'notify_url' => $config->order_wxpay_notify,
            'out_trade_no' => $config->out_trade_no_prefix . $orderId,
            'spbill_create_ip' => $this->getIp(),
            'total_fee' => $totalFee,
            'trade_type' => 'JSAPI',
            'openid' => $openid,
//            'goods_tag'=>$voucher
        );
        $pack['sign'] = $this->Util->paySign($pack);
        $xml = $this->Util->toXML($pack);
        $ret = Curl::post('https://api.mch.weixin.qq.com/pay/unifiedorder', $xml);
        $postObj = json_decode(json_encode(simplexml_load_string($ret, 'SimpleXMLElement', LIBXML_NOCDATA)));
        if (empty($postObj->prepay_id) || $postObj->return_code == "FAIL") {
            // 支付发起错误
//            $this->log('wepay_error:' . $postObj->return_msg);

        }
        $packJs = array(
            'appId' => APPID,
            'timeStamp' => $timeStamp,
            'nonceStr' => $nonceStr,
            'package' => "prepay_id=" . $postObj->prepay_id,
            'signType' => 'MD5'
        );

        $JsSign = $this->Util->paySign($packJs);
        $packJs['timestamp'] = $timeStamp;
        $packJs['paySign'] = $JsSign;
        return $packJs;
    }


    /**
     * 更新订单
     * @param $orderId 订单编号
     * @param $data 订单编号
     * @return array
     */
    public function updateOrder($orderId,$data = array()){
        return $this->Dao->update(self::TABLE_NAME_ORDER)->set($data)->where("id =".$orderId)->exec();
    }

    /**
     * 生成订单序列号
     */
    public function generateOutTradeNo($orderId, $prefix = ''){
        $len = strlen($orderId);
        $num = "000000";
        $orderNum =$prefix.date("Ymdhis").substr($num,$len).$orderId;
        return $orderNum ;

    }


    /**
     * 根据第三方用户编号获取用户信息
     */
    public function getPartnerUserByPartnerUid($partnerUid){
        $redisKey = self::REDIS_PREFIX_PARTNER_USER.$partnerUid;
        $user = RedisUtil::get($redisKey);
        if(!$user){
            if($user = $this->Dao->select()->from(self::TABLE_NAME_PARTNER_USER)->where("partner_uid = $partnerUid")->getOneRow()){
                RedisUtil::set($redisKey,$user,CACHE_TIME_TEN_DAY);
                $redisUserKey = self::REDIS_PREFIX_PLATFORM_USER.$user['uid'];
                RedisUtil::set($redisUserKey,$user,CACHE_TIME_TEN_DAY);
            }
        }
        return $user;

    }

    /**
     * 根据uid获取第三方用户信息
     */
    public function getPartnerUserByUid($uid){
        $redisKey = self::REDIS_PREFIX_PLATFORM_USER.$uid;
        $user = RedisUtil::get($redisKey);
        if(!$user){
            if($user = $this->Dao->select()->from(self::TABLE_NAME_PARTNER_USER)->where("uid = $uid")->getOneRow()){
                RedisUtil::set($redisKey,$user,CACHE_TIME_TEN_DAY);
            }
        }
        return $user;
    }


    /**
     * 自动注册第三方用户信息
     * @param $partnerUid 第三方用户编号
     * @param $partnerUname 第三方用户名
     * @param $from_platform 来源平台
     * @param $appid 对应的appid
     * @return array
     */
    public function autoRegPartnerUser($uid,$partnerUid,$partnerUname,$from_platform = 'zy_group',$appid = ''){
        $partnerUname = urldecode($partnerUname);
//        $uid = $this->Dao->insert('clients', 'client_nickname,client_name,client_head_lastmod,client_joindate,from_platform')
//            ->values(array(
//                $partnerUname,
//                $partnerUname,
//                'NOW()',
//                'NOW()',
//                $from_platform
//            ))->exec(false);

        $time = time();
        $partnerLogId = $this->Dao->insert(self::TABLE_NAME_PARTNER_USER, 'uid,partner_uid,partner_name,reg_time,partner_short_name,appid')
            ->values(array(
                $uid,
                $partnerUid,
                $partnerUname,
                $time,
                $from_platform,
                $appid
            ))->exec(false);
        return $uid;
    }

    /**
     * 微信支付成功之后的回调
     * @param $fullOutTradeNo 生产的商户订单号
     * @param $transactionId 微信的交易号
     */
    public function wxPayNotify($fullOutTradeNo,$wxPostObj,$isProduce = true){
        global $config;
        //输出微信支付的信息
        error_log('wx pay success obj====>'.json_encode($wxPostObj));
        $len = strlen($fullOutTradeNo);
        $orderNum = substr($fullOutTradeNo,$len-6,$len);
        $orderId =  preg_replace('/^0*/', '',$orderNum);
        $time = time();

        $updateOrderData = [
            'payment_source' => 'WXPAY',
//            'paid_time' => $wxPostObj->time_end,
            'paid_time' => $time,
            'bill_no' =>$wxPostObj->transaction_id,
            'pay_amount' =>$wxPostObj->total_fee, //实际支付的金额,因为实际上可能会包含代金券减掉的金额
            'status' =>'SUCC',
        ];
        $this->updateOrder($orderId,$updateOrderData);



        if($orderInfo = $this->getOrderById($orderId)){
            $this->loadModel('SmsApi');

            $phone = $orderInfo['phone'];
            $packageId = $orderInfo['package_id'];
            $partnerUid = $orderInfo['partner_uid'];
            $packageInfo = $this->get_package_info_by_id($packageId);
            $packageName = $packageInfo ? $packageInfo['name']:'test';
            $statusArr = [
                //调用kk的api接口
                'invoke_kk_card_api' => 'FAIL',
                //插入卡号信息
                'insert_card_log' => 'FAIL',
                //调用中英的回调接口
                'invoke_zy_callback_api' => 'FAIL',
                //发送短信给用户
                'send_sms_to_user' => 'FAIL',
                //发送订单通知
                'send_new_order_notify_msg' => 'FAIL',
            ];
            try{
                //1、调用康康的接口生成卡号
                $kkCard = TokenHelper::invokeKKCard($orderInfo);
                if($kkCard['errcode'] == 200){
                    $cardNo = $kkCard['card_no'];
                    $cardSecret = $kkCard['card_secret'];
                    $validTime = $kkCard['valid_time'];
                    //更改状态
                    $statusArr['invoke_kk_card_api'] = 'SUCCESS';
                    //2、将卡号插入到用户购买记录表中
                    $cardLogId = $this->synOrderCard($orderInfo,$kkCard);
                    if($cardLogId > 0){
                        $statusArr['insert_card_log'] = 'SUCCESS';
                    }

                    //中英要求的接口格式为2017-06-05这种字符类型
                    $validTimeStr = date('Y-m-d',$validTime);


                    //3、给中英发起回调请求
                    //排除掉不需要发送通知给中英的套餐
                    $packageType = $orderInfo['package_type'];
                    $notNotifyZyPackages = $config->not_send_notify_to_zy_package;
                    if(!in_array($packageType,$notNotifyZyPackages)){
                        $zyResult = TokenHelper::invokeZyApi($partnerUid, $cardNo, $cardSecret,$validTimeStr,$packageName);
                        if($zyResult['errcode'] == 0){
                            //更改状态
                            $statusArr['invoke_zy_callback_api'] = 'SUCCESS';
                        }else{
                            $statusArr['invoke_zy_callback_api'] = 'FAIL,'.$zyResult['errcode'].'，'.$zyResult['errmsg'];
                        }
                    }else{
                        $statusArr['invoke_zy_callback_api'] = 'OTHER,EMPLOYEE BENIFIT';
                    }


                    //4、给用户发送短信
                    $smsResult = $this->SmsApi->sendHealthPackagePaySuccessSMS($phone,$packageId,$cardNo,$cardSecret,$validTimeStr);
                    if($smsResult['errcode'] == 200){
                        $statusArr['send_sms_to_user'] = 'SUCCESS';
                    }else{
                        $statusArr['send_sms_to_user'] = 'FAIL,'.$smsResult['msg'];
                    }


                    //发送订单通知
                    if($isProduce){
                        //5、发送新订单消息通知
                        $this->comeNewOrderNotify($orderInfo);
                    }
                    $statusArr['send_new_order_notify_msg'] = 'SUCCESS';
                    $this->insertInvokeStatus($orderId,$orderInfo['uid'],$statusArr);
                }
            }catch (Exception $e){

            }finally{

            }

        }


    }

    public function synOrderCard($orderInfo,$kkCardInfo){
        $data = [
            'order_id' => $orderInfo['id'],
            'card_no' => $kkCardInfo['card_no'],
            'card_secret' => $kkCardInfo['card_secret'],
            'uid' => $orderInfo['uid'],
            'partner_uid' => $orderInfo['partner_uid'],
            'partner_user_name' => $orderInfo['partner_user_name'],
            'package_code' => $orderInfo['package_code'],
            'package_type' => $orderInfo['package_type'],
            'add_time' => $kkCardInfo['add_time'],
            'valid_time' => $kkCardInfo['valid_time'],
            'remote_ip' => $kkCardInfo['ip'],
        ];
        $id =  $this->insertOrderCard($data);
        return $id;
    }


    /**
     * 来新的订单时候的消息通知
     */
    public function comeNewOrderNotify($orderInfo){
        global $config;
        if (isset($config->messageTpl['new_order_notify']) && $config->messageTpl['new_order_notify'] != '') {
            // 查找通知openid列表
            $openIds = explode(',', $this->getSetting('order_notify_openid'));
            if (!is_array($openIds) && count($openIds) <= 0) {
                return false;
            }
            $totalAmount = $orderInfo['total_amount'] / 100;
            $packageInfo = $this->get_package_info_by_id($orderInfo['package_id']);

            foreach ($openIds as $openid) {
                //Messager::sendNotification(WechatSdk::getServiceAccessToken(), $openid, $content, $this->getBaseURI() . "?/Order/expressDetail/order_id=$orderId");
                // 批量通知商户
                if ($orderInfo) {
                    Messager::sendTemplateMessage($config->messageTpl['new_order_notify'], $openid, array(
                        'first' => '有一位顾客购买了一份价值￥'.$totalAmount.'元的体检套餐',
                        'keyword1' => $orderInfo['partner_user_name'],
                        'keyword2' => $orderInfo['out_trade_no'],
                        'keyword3' => '¥' .$totalAmount,
                        'keyword4' => $packageInfo['name'],
                        'remark' => '点击详情 随时查看订单状态'
                    ));
                }

            }
        }
    }



    /**
     * 获取所有的配送单
     * @param string $where
     * @param string $orderby
     **/
    public function get_hp_order_list($where='',$orderby='order_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $list = $this->Dao->select()->from(TABLE_HP_ORDER)->where($where)->orderby($orderby)->limit("$offset,$pageSize")->exec(false);
        }else{
            $list = $this->Dao->select()->from(TABLE_HP_ORDER)->where($where)->orderby($orderby)->exec(false);
        }
        foreach ($list as $key => &$val){
            if(!$package = $this->get_package_info_by_id($val['package_id'])){
                $package['name'] = '';
            }
            $sex = $val['user_sex'] == 1 ? '男' : '女';
            $val['user_sex'] = $sex;
            $married_status = $val['user_married_status'] == 1 ? '未婚' : '已婚';
            $val['user_married_status'] = $married_status;
            $val['order_time'] = date('Y-m-d H:i',$val['order_time']);
            $val['paid_time'] = date('Y-m-d H:i',$val['paid_time']);
            $val['package_info'] = $package;

            $status = $val['status'];
            if($status == 'UNPAY'){
                $status = '未支付';
            }else if($status == 'SUCC'){
                $status = '支付成功';
            }else if($status == 'REFUND'){
                $status = '已退款';
            }
            $val['total_amount'] = $val['total_amount'] / 100;
            $val['status'] = $status;
        }
        return $list;
    }

    /**
     * 套餐卡号列表
     */
    public function get_hp_order_card_list($where='',$orderby='add_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $list = $this->Dao->select()->from(TABLE_HP_ORDER_CARD)->where($where)->orderby($orderby)->limit("$offset,$pageSize")->exec(false);
        }else{
            $list = $this->Dao->select()->from(TABLE_HP_ORDER_CARD)->where($where)->orderby($orderby)->exec(false);
        }

        if(count($list) > 0){
            foreach ($list as &$val){
                $val['add_time'] = date('Y-m-d H:i',$val['add_time']);
                $val['valid_time'] = date('Y-m-d H:i',$val['valid_time']);
            }
        }
        return $list;
    }

    /**
     * 套餐列表
     */
    public function get_hp_package_list($where='',$orderby='add_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $list = $this->Dao->select()->from(TABLE_HP_PACKAGE_INFO)->where($where)->orderby($orderby)->limit("$offset,$pageSize")->exec(false);
        }else{
            $list = $this->Dao->select()->from(TABLE_HP_PACKAGE_INFO)->where($where)->orderby($orderby)->exec(false);
        }

        if(count($list) > 0){
            foreach ($list as &$val){
                $val['add_time'] = date('Y-m-d H:i',$val['add_time']);
                $val['sale_price'] =  $val['sale_price'] / 100;
                $val['market_price'] =  $val['market_price'] / 100;
            }
        }
        return $list;
    }

    /**
     * 体检中心列表
     */
    public function get_hp_hospital_list($where='',$orderby='add_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $list = $this->Dao->select()->from(TABLE_HP_HOSPITAL)->where($where)->orderby($orderby)->limit("$offset,$pageSize")->exec(false);
        }else{
            $list = $this->Dao->select()->from(TABLE_HP_HOSPITAL)->where($where)->orderby($orderby)->exec(false);
        }

        if(count($list) > 0){
            foreach ($list as &$val){
                $val['add_time'] = date('Y-m-d H:i',$val['add_time']);
            }
        }
        return $list;
    }

    /**
     * 接口调用状态
     */
    public function get_hp_api_invoke_list($where='',$orderby='add_time desc',$offset,$pageSize){
        $list = array();
        if($offset>=0){
            $list = $this->Dao->select()->from(TABLE_HP_API_INVOKE_STATUS)->where($where)->orderby($orderby)->limit("$offset,$pageSize")->exec(false);
        }else{
            $list = $this->Dao->select()->from(TABLE_HP_API_INVOKE_STATUS)->where($where)->orderby($orderby)->exec(false);
        }

        if(count($list) > 0){
            foreach ($list as &$val){
                $val['add_time'] = date('Y-m-d H:i',$val['add_time']);
            }
        }
        return $list;
    }



}