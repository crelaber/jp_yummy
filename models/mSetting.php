<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class mSetting extends Model{

    public function getSettingByKey($key){
        $data = $this->Dao->select("value")->from('wshop_settings')->where("`key` = '".$key."'")->getOne();
        $data = json_decode($data,true);
        return $data;
    }

}