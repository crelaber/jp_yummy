<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mSupplier extends Model {



    public function  supplier_list(){

        return $this->Dao->select ()->from ( 'product_supplier' )->exec (false);

    }

    public function  get_detail_supplier($id){
        return $this->Db->getOneRow("SELECT * FROM `product_supplier` WHERE `id` = '$id';");

    }





    public function  del_supplier($id){
        return $this->Dao->delete ()->from ( 'product_supplier' )->where ( "id =" . $id )->exec ();

    }

    public function add_supplier($id,$name,$city,$admin_name){

        if($id){
            return $this->Dao->update(product_supplier)->set(array(
                'name' => $name,
                'city' => $city,
                'admin_name'=> $admin_name
            ))->where("id=" . $id)->exec();
        }else{
            return   $this->Dao->insert("product_supplier", '`name`,`city`,`admin_name`')->values(array($name,$city,$admin_name))->exec();

        }

    }

    public function  get_detail_supplier_by_name($name){
        return $this->Db->getOneRow("SELECT * FROM `product_supplier` WHERE `admin_name` = '$name';");

    }
    
}
