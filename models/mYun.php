<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mYun extends Model
{


    public function getYunCost($yun_no,$cityname) {
        return $this->Db->getOneRow("SELECT * FROM `yun_cost` WHERE `yun_no` = '$yun_no' and `city_name` = '$cityname';");
    }

    public function  getYunCostById($id){
        return $this->Db->getOneRow("SELECT * FROM `yun_cost` WHERE `id` = '$id';");

    }


    public function getYunSysList(){

        return $this->Dao->select ()->from ('yun_sys' )->exec (false);

    }
    public function  get_detail_yun_sys($yun_no){
        return $this->Db->getOneRow("SELECT * FROM `yun_sys` WHERE `yun_no` = '$yun_no';");

    }



    public function  del_yun_cost($id){
        return $this->Dao->delete ()->from ( 'yun_cost' )->where ( "id =" . $id )->exec ();

    }

    public function  del_yun_cost_by_yunno($yun_no){
        return $this->Dao->delete ()->from ( 'yun_cost' )->where ( "yun_no =" . $yun_no )->exec ();

    }

    public function del_yun($id){


        return $this->Dao->delete ()->from ( 'yun_sys' )->where ( "yun_no =" . $id )->exec ();


    }

    public function add_yun_sys($id,$name,$express_id,$express_name){

        if($id){
            return $this->Dao->update(yun_sys)->set(array(
                'yun_name' => $name,
                'express_id'=>$express_id,
                'express_name'=>$express_name
            ))->where("yun_no=" . $id)->exec();
        }else{
            return   $this->Dao->insert("yun_sys", '`yun_name`,`express_id`,`express_name`')->values(array($name,$express_id,$express_name))->exec();

        }

    }

    public function getYunCostList($yun_no){

        return $this->Dao->select ()->from ('yun_cost' )->where("yun_no = '$yun_no'")->exec (false);

    }

    public function add_yun_cost($id,$first_p,$first_w,$second_p,$second_w,$city_name,$yun_no){

        if($id){
            return $this->Dao->update(yun_cost)->set(array(
                'first_p' => $first_p,
                'first_w' => $first_w,
                'second_p' => $second_p,
                'second_w' => $second_w,
                'city_name' => $city_name,
            ))->where("id=" . $id)->exec();
        }else{
            return   $this->Dao->insert("yun_cost", '`city_name`,`first_p`,`first_w`,`second_p`,`second_w`,yun_no')->values(array($city_name,$first_p,$first_w,$second_p,$second_w,$yun_no))->exec();

        }
    }

    public function getCityList(){

        return $this->Dao->select ()->from ('citys' )->exec (false);

    }


    //================订单快递编号查询==========================

    public function getOrderExpress($order_id,$provider_id){
        return $this->Db->getOneRow("SELECT * FROM `j_order_package` WHERE `provider_id` = '$provider_id' and `order_id` = '$order_id';");

    }



}