require(['config'], function (config) {
require(['util', 'jquery'], function (util, $) {
var datalist;
var lastid1=$("#lastid1").val();
var lastid2=$("#lastid2").val();
var low_is_loaded = false;
var high_is_loaded = false;
var high_can_load = true;
var low_can_load = true;
var can_scroll=true;
var rend_data=true;
var num=0;
var lnum=0;
var hnum=0;
    close_note();
    init_click_event();
    init_active_click();
    high_clock();
    init_scroll_window();
function close_note(){
  $("#closeicon").click(function(){
    $(".pop_content").hide();
  });
}

 /**
 * 加载初始对应的列表数据
 */
function init_active_click(){
    $(".type_tab ul li").each(function () {
        if($(this).find('.my_clock').hasClass('active')){
            if($(this).hasClass('low')){
                load_low_data();
            }else if($(this).hasClass('high')){
                load_high_data();
            };
        }
    })
}

/**
 * 加载点击事件
 */
function init_click_event() {
    $(".type_tab ul li").click(function(){//切换cps和分销tab
        can_scroll=false;
        $(".my_clock").removeClass('active');
        $(this).find(".my_clock").addClass('active');
        if($(this).hasClass('low')){
            load_low_data();
        }else if($(this).hasClass('high')){
            load_high_data();
        }
    });
}

/**
 * 加载cps对应的列表数据
 */
function load_low_data() {
    $("#senior_clock").hide();
    $("#primary_clock").show();
    if(!low_is_loaded){
        ajaxclockList('low');
    }else{
      can_scroll=true;
    }
}

/**
 * 加载粉丝对应的列表数据
 */
function load_high_data() {
    $("#primary_clock").hide();
    $("#senior_clock").show();
    if(!high_is_loaded){
        ajaxclockList('high');
    }else{
      can_scroll=true;
    }
}
/**
 * 滚动事件的处理
 */
function init_scroll_window(){
  if ($('.clock_list ul').length > 0) {
        $(window).scroll(function () {//滑动到底部加载数据
            var scrollTop = $(this).scrollTop(); //滚动条距离顶部的高度
            var scrollHeight = $(document).height(); //当前页面的总高度
            var windowHeight = $(this).height(); //当前可视的页面高度
            if ((scrollTop + windowHeight >= scrollHeight) && can_scroll) {
                //加载数据
                if($('#pri_clock').hasClass('active') && low_can_load){
                    ajaxclockList('low');
                }else if($('#sen_clock').hasClass('active') && high_can_load){
                    ajaxclockList('high');
                }
            }
        });
    }
}
/**
 * 请求ajax数据
 */
function ajaxclockList(type){//ajax加载数据
    $("#list-loading").show();
    var lastid = type=='low'? lastid1 : lastid2;
    var uid = $('#uid').val();
    can_scroll=false;
    if(lastid > 0){
        var url = '/?/vHealthClock/ajaxGetRecordList/last_id='+lastid+'&uid='+uid+'&type='+type;
        $.get(url,function(res){
          if(res.errcode==200){
              $("#list-loading").hide();
              lastid=res.data.lastid;
              if(type == 'low'){
                  lastid1=lastid;
                  
              }else{
                  lastid2=lastid;
                  
              }
              datalist = res.data.list;
              var i=0;
              if(null != datalist) {                 
                    render_data(datalist,type);
              }
          }
        });
    }else{
        $("#list-loading").hide();
    }
}
function render_data(datalist,type){
  var len =datalist.length;
  var html = '';
  var obj;
  var source='';
  var msgtype='';
  for(var i=0;i<len;i++) {
      obj = datalist[i];
      if(type=="low"){
        lnum=lnum+1;
        num=lnum;
      }else if(type=="high"){
        hnum=hnum+1;
        num=hnum;
      }
      var reg = new RegExp("\\[([^\\[\\]]*?)\\]", 'igm'); //i g m是指分别用于指定区分大小写的匹配、全局匹配和多行匹配。
      html = $('#clock_list').html();
      if (type=="low"){
        msgtype='水果营养知识挑战打卡第'
      }else{
        msgtype='体重管理计划打卡第'
      }
      source = source+html.replace(reg, function (node, key) {
          return {
              'num':num,
              'msgtype':msgtype,
              'date': obj.day,
              'index':obj.day_index,
              'state': obj.status,
              'time':obj.time
              }[key];
      });
  }
  if(type=='low'){
      $("#primary_clock ul").append(source);
      
      low_is_loaded = !low_is_loaded;
      if(len < 10 ){
          low_can_load=false;
      }
      can_scroll=true;
      rend_data=true;
  }else if(type='high'){
      if(len > 0){
          $("#senior_clock ul").find('img').remove();
      }
      if(source.length > 0){
          // $("#senior_clock ul").html('');
          $("#senior_clock ul").append(source);
          
      }
      high_is_loaded = !high_is_loaded;
      if(len < 10 ){
          high_can_load=false;
      }
      can_scroll=true;
      rend_data=true;
  }
}


/*高级打卡*/
function high_clock(){
  $("#complete_read").click(function(){
    var url='/?/vHealthClock/doHighLevelPunch';
    $.get(url,function(res){
      if(res.errcode==200){
        $(".complete_read").addClass('done');
        $(".complete_read").html('<span id="clock_done">已打卡</span>');
        $("#clock_done").on('click',function(){
          layer.open({
             content: '您已打卡！',
             time: 3 //3秒后自动关闭
              });
        });
      }
      else{
        alert('参数错误！');
      }
    });
  });
  
}

});
});