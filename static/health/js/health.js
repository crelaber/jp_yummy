$(function(){
	'use strict';
  	var value=['iPhone 4', 'iPhone 4S', 'iPhone 5', 'iPhone 5S', 'iPhone 6', 'iPhone 6 Plus', 'iPad 2', 'iPad Retina', 'iPad Air', 'iPad mini', 'iPad mini 2', 'iPad mini 3'];
 	$(document).on("pageInit", "#page-picker", function(e, id, page) {
    $("#city_select").picker({
      toolbarTemplate: '<header class="bar bar-nav">\
      <button class="button button-link pull-right close-picker">\
      确定\
      </button>\
      <h1 class="title">选择城市</h1>\
      </header>',
      cols: [
        {
          textAlign: 'center',
          values:value ,
          cssClass: 'picker-items-col-normal'
        }
      ],
      onClose:function(){
        
        var pickerval=$("#city_select").val();
        for(var i=0;i<value.length;i++){
           if(pickerval==value[i]){
           	var val=i;
            $(".center_list").hide();
	  		$(".center_list").eq(val).show();
           }
          }
      }
    });

  });
 
  $.init();
	var pay=parseInt($(".price").text());
	$(".amount").html('￥'+pay);
	$(".check_con").eq(0).show();
	$(".center_list").eq(0).show();			
	initstate();  
	

});

function initstate(){
		$(".project_tab ul li").click(function(){
	  	$(".project_tab ul li").removeClass('active');
	  	$(this).addClass('active');
	    var num =$(".project_tab ul li").index(this);
	    $(".check_con").hide();
	    $(".check_con").eq(num).show();
	  })
		//数量减1
	  $(".minus").click(function(){
	  	var pay=parseInt($(".price").text());
	  	var m=parseInt($(".cart_num").text());
	  		if(m>1){
	  			m=m-1;
	  			var money=m*pay;
	  			$(".cart_num").html(m);
	  			$(".amount").html('￥'+money);
	  		}
	  });
	  //数量加1
	  $(".plus").click(function(){
	  	var pay=parseInt($(".price").text());
	  	var n=parseInt($(".cart_num").text());
	  		n=n+1;
	  		var money=n*pay;
	  	$(".cart_num").html(n);
	  	$(".amount").html('￥'+money);
	  });
	  //城市选择
	  $(".check_city select").click(function(){
	  	var val=$(".check_city").find("option:selected").val();
	  	$(".center_list").hide();
	  	$(".center_list").eq(val).show();
	  })
	  //显示弹窗信息
	  $(".submit_pay").click(function(){
	  	$(".fill_infor").show();
	  });
	  //取消弹窗信息填写
	  $(".submit_health .cancel").click(function(){
	  	$(".fill_infor").hide();
	  });

	  //确认提交信息
	  $("#submit_infor").click(function(){
	  	var sex=$("#sex").find("option:selected").val();
	  	var marry=$("#marry").find("option:selected").val();
	  	var phone=$("#phone").val();
	  	var code=$("#code").val();
	  	if(sex.length==0||marry.length==0||phone.length==0||code.length==0){
	  		layer.open({
                 content: '请填写完整信息！',
                 time: 3 //3秒后自动关闭
                  });
            return false;	
	  	}
	  	alert("提交成功");
	  });
	}
	var wait = 60;
    var code_state = "IDLE";
function count_down(){
	//获取验证码
	  if(wait <= 0){
            // reset state
            $(".code_btn").removeClass("active")
            $(".code_btn").attr("onclick", "send_code()");
            $(".code_btn").html("获取验证码");
            wait = 60;
            code_state = "IDLE";
        } else {
            $(".code_btn").addClass("active")
            $(".code_btn").attr("onclick", "");
            $(".code_btn").html( "剩余" + wait + "秒");
            wait--;
            setTimeout(count_down, 1000);
        
        }
}
function send_code()
        {
        if (code_state != "IDLE") return false;
        var tel = $.trim($("input[name='mobile']").val());
        var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
        
        if(false == reg.test(tel)){
			layer.open({
                 content: '请输入正确的手机号！',
                 time: 3 //3秒后自动关闭
                  });
            return false;
        }
        code_state = "SENDING";
        // start count down
        count_down();
        }