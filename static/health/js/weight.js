$(function(){
    $("#plan_select").picker({
        toolbarTemplate: $('#picker').html(),
        cols: [
            {
                textAlign: 'center',
                values: ['-5','-4','-3','-2','-1','0','1','2','3','4','5']
            }
        ]
    });
});

$("#submit_weight").click(function () {
    var height = $("#height").val();
    var weight = $("#weight").val();
    var plan = $("#plan_select").val();
    if (height.length == 0) {
        layer_box('身高不能为空！');
        return false;
    }
    if (weight.length == 0) {
        layer_box('体重不能为空！');
        return false;
    }
    if (plan.length == 0) {
        layer_box('目标不能为空！');
        return false;
    }
    var url = '/?/vHealthClock/doInputHealthInfo/';
    $.get(url, {
        height:height,
        weight:weight,
        target_weight:plan,
        sex:1
    }, function (res) {
        if (res.errcode == 200) {
            layer_box('提交成功');
            var redirectPage = $('#redirectPage').val();
            window.location.href = redirectPage;
            // $(window).attr('location',redirectPage);
        }
        else {
            layer_box('提交失败');
        }
    });
});


function layer_box(msg) {
    layer.open({
        content: msg,
        time: 3 //3秒后自动关闭
    });
}
