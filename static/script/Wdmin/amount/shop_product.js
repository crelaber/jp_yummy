requirejs(['jquery', 'util', 'datatables', 'Spinner'], function ($, util,  dataTables, Spinner) {



    $('#express_del_btn').click(function(){

        var id = $('#express_id').val();
        var url = "?/WdminAjax/ajaxDelExpress";
        var resultUrl = "?/WdminPage/express_list";
        $.post(url,{id:id},function(data){

            window.location.href= resultUrl;
        });
    });

    $('#express_save').click(function(){

        var id = $('#express_id').val();
        var name = $('#name').val();
        var num = $('#num').val();
        var tel = $('#tel').val();
        var iconUrl = $('#url').val();
        var url = "?/WdminAjax/ajaxUpdateExpress/";
        $.post(url,{id:id,name:name,num:num,tel:tel,iconUrl:iconUrl},function(data){

            window.location.href="?/WdminPage/express_list";

        });
        //var id = $('#client_id').val();


    });



    $('#supplier_save').click(function(){

       var id = $('#supplier_id').val();
       var name = $('#name').val();
        var city = $('#city').val();
        var admin_name = $('#admin_name').val();


        var url = "?/WdminAjax/ajaxUpdateSupplier/";
        $.post(url,{id:id,name:name,city:city,admin_name:admin_name},function(data){

            window.location.href="?/WdminPage/supplier_list";

        });
        //var id = $('#client_id').val();


    });

    $('#yun_save').click(function(){

        var id = $('#yun_id').val();
        var name = $('#yun_name').val();
        var express = $('#express').val();
        var url = "?/WdminAjax/ajaxUpdateYunSys/";
        $.post(url,{id:id,name:name,express:express},function(data){

            window.location.href="?/WdminPage/yun_sys_list";

        });
    });

    $('#yun_cost_save').click(function(){

        var id = $('#yun_id').val();
        var yun_no = $('#yun_no').val();
        var first_w = $('#first_w').val();
        var first_p = $('#first_p').val();
        var second_w = $('#second_w').val();
        var second_p = $('#second_p').val();
        var city_name = $('#city_name').val();
        var url = "?/WdminAjax/ajaxUpdateYunCost/";
        var resultUrl = "?/WdminPage/yun_cost_list/id="+yun_no;
        $.post(url,{id:id,yun_no:yun_no,first_w:first_w,first_p:first_p,second_w:second_w,second_p:second_p,city_name:city_name},function(data){

            window.location.href=resultUrl;

        });
    });

    $('#do_success').click(function(){
        var id = $('#do_id').val();
        var url = "?/WdminAjax/ajaxUpdateRedraw";
        var resultUrl = "?/WdminPage/pay_account";
        $.post(url,{id:id},function(data){

            window.location.href= resultUrl;
        });

    });

    $('#sup_del_btn').click(function(){
        var id = $('#supplier_id').val();
        var url = "?/WdminAjax/ajaxDelSupplier";
        var resultUrl = "?/WdminPage/supplier_list";
        $.post(url,{id:id},function(data){

            window.location.href= resultUrl;
        });

    });

    $('#yun_cost_del').click(function(){

        var id = $('#yun_id').val();
        var yun_no = $('#yun_no').val();

        var url = "?/WdminAjax/ajaxDelYunCost";
        var resultUrl = "?/WdminPage/yun_cost_list/id="+yun_no;
        $.post(url,{id:id},function(data){

            window.location.href= resultUrl;
        });
    });

    $('#yun_del').click(function(){
        var id = $('#yun_id').val();
        var url = "?/WdminAjax/ajaxDelYun";

        $.post(url,{id:id},function(data){

            window.location.href="?/WdminPage/yun_sys_list";
        });
    });

});