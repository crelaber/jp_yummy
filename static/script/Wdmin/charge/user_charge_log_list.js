requirejs(['jquery', 'util', 'fancyBox', 'Spinner','layer','laytpl','laypage','laydate'], function ($, util, fancyBox, Spinner,layer,laytpl,laypage,laydate ) {

    $(function() {
        //dt = $('.dTable').dataTable(DataTableConfig).api();
//        $('#DataTables_Table_0_filter').hide();
//        $('.chargeLogDel').click(function() {
//            var node = $(this);
//            if (confirm('要删除该充值记录吗？')) {
//                $.post('?/ChargeManage/delete_charge_log/', {id: $(this).attr('data-id')}, function(r) {
//                    if (r > 0) {
//                        dt.row(node.parents('tr')).remove().draw();
//                        util.Alert('删除成功！');
//                    } else {
//                        util.Alert('删除失败！', true);
//                    }
//                });
//            }
//        });

		//父类容器
		var parent_container = $('.dTable tbody');
		//laytpl模版中的数据
		var laytpl_data_template = $('#data_render').html();
		var base_url = '?/WdminAjax/ajax_user_charge_log_list/';
		//异步加载数据的url
		var purl = util.build_laytpl_url(base_url);
		//页码
		var page = 1;
		//加载数据
		util.ajax_load_data_by_layer(purl,page,util.get_default_page_size(),parent_container,laytpl_data_template,layer,laytpl,laypage);


		//制卡函数
	    fnFancyBox('.chargeLogDel',function(){
	     	$('#ok').on('click', function(){
	     		var id = $(this).attr('data-id');
	     		$.post('?/ChargeManage/delete_charge_log/', {id: id}, function(r) {
	     			$.fancybox.close();
	     			if (r > 0) {
                        util.Alert('删除成功,2s后将自动刷新！',false,null,1000);
                    } else {
                        util.Alert('删除失败！', true);
                    }
	     			
	     			util.delay_refresh('?/WdminPage/user_charge_log_list/');
                });
	     	});
	     	
	     	$('#close').on('click', function () {
	     		$.fancybox.close();
	     	})
	     	
	     });
        
        
    });
});