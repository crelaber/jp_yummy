
DataTableConfig.order = [[0, 'desc']];

requirejs(['jquery', 'util', 'fancyBox', 'datatables', 'Spinner'], function($, util, fancyBox, dataTables, Spinner) {
    $(function() {

        dt = false;

        // 加载完毕隐藏loading
        window.frameOnload = function() {
            window.setTimeout(function() {
                Spinner.stop();
                $('#iframe_loading').fadeOut();
            }, 500);
        };

        /**
         * 编辑分组名
         * @returns {undefined}
         */
        fnFancyBox('.list-user-group-item .del', function() {
            $('#save_btn').on('click', function() {
                $(this).html('处理中...');
                // HttpPost
                $.post('?/WdminAjax/ajaxAlterUserGroup/', {
                    id: $('#gid').val(),
                    name: $('#gname').val()
                }, function(res) {
                    if (res.errcode === 0) {
                        util.Alert('保存成功');
                        $.fancybox.close();
                    } else {
                        util.Alert('操作失败!', true);
                        $(this).html('保存');
                    }
                });
            });
        });

        fnFancyBox('#addGroup', function() {
            $('#save_btn').on('click', function() {
                $(this).html('提交中...');
                // HttpPost
                $.post('?/WdminAjax/ajaxAddUserGroup/', {
                    name: $('#gname').val()
                }, function(res) {
                    if (res.errcode === undefined) {
                        util.Alert('添加成功');
                        $.fancybox.close();
                        location.reload();
                    } else {
                        util.Alert('操作失败!', true);
                        $(this).html('提交');
                    }
                });
            });
        });

        /**
         * 加载用户列表
         * @param {type} gid
         * @returns {undefined}
         */
        function ajaxLoadUserList(gid) {
            $('#iframe_loading').show();
            Spinner.spin($('#iframe_loading').get(0));
            $('#iframe_customer').attr('src', '?/WdminPage/list_agent');
        }

        /**
         * 注册resize函数
         */
        util.onresize(function() {
            $('#categroys').css('height', $(window).height());
            $('#iframe_customer').css('height', $(window).height());
        });

        ajaxLoadUserList('');

    });
});