/* global shoproot */

var pdCatimg = false;
var pdImages = ['', '', '', '', ''];
var prizeId = false;
var pdImageWidth = false;
requirejs(['jquery','util', 'fancyBox', 'ueditor', 'jUploader','datetimepicker'], function ($,util, fancyBox, ueditor, jUploader) {

    loadingLock = false;
    
    fnFancyBox('#catimgPv');
    fnFancyBox('.pd-image-view');
    fnFancyBox('#fetch_product_btn');//抓取数据按钮fancybox
    $('input[name="prize_type"]').change();
    $('#prize_type_sub_item_2').hide();
    $('#prize_type_sub_item_3').hide();
    $('#prize_type_sub_item_4').hide();

    $('input[name="prize_type"]').change(function(){
        var value = $(this).val();
        $('.prize_type').each(function () {
            var className = 'prize_type_'+value;
            $(this).hide();
            if($(this).hasClass(className)){
                $(this).show();
            }
        })
    });

        $.datetimepicker.setLocale('zh');
    $('#pd-form-msexp').datetimepicker({
        format: 'Y-m-d H:i:s'
    });
    // 产品首图
    pdCatimg = $('#pd-catimg').val() === '' ? false : $('#pd-catimg').val();

    if ($('#mod').val() === 'edit') {
        // 编辑模式 存入图片列表
        $('.pd-images').each(function (i, node) {
            if ($(node).val() && $(node).val() !== '' && i < 5) {
                pdImages[$(node).attr('data-sort')] = $(node).val();
                $('.pd-image-sec').eq(parseInt($(node).attr('data-sort')) + 1).append('<img src="' + shoproot + 'uploads/zy_hch/prize_img/' + $(node).val() + '" width="' + pdImageWidth + 'px" /><a href="' + shoproot + 'uploads/zy_hch/prize_img/' + $(node).val() + '" class="pd-image-view"> </a><i data-id=' + $(node).attr('data-sort') + ' class="pd-image-delete"> </i>').addClass('ove');
            }
        });
        fnPdimageDelete();
        prizeId = parseInt($('#pid').val());
        fnFancyBox('.pd-image-view');
    } else {
        prizeId = false;
    }


    var pdImageHeight = false;

    // 产品图片
    $('.pd-image-sec').each(function () {
        var btn = this;
        if (!pdImageHeight) {
            pdImageHeight = $(this).width();
        }
        $(this).height(pdImageHeight);
        $.jUploader({
            button: $(btn),
            action: shoproot + '?/HealthClockManage/updatePrizeImg',
            onUpload: function (fileName) {
                util.Alert('图片上传中');
            },
            onComplete: function (fileName, response) {
                var Btn = $(this.button[0]);
                if (response.s > 0) {
                    var iid = parseInt(Btn.attr('data-id'));
                    var src = decodeURIComponent(response.link);
                    Btn.addClass('ove').removeClass('hover');
                    util.Alert('图片上传成功');
                    if (Btn.find('img').length > 0) {
                        Btn.find('img').attr('src', src);
                        Btn.find('.pd-image-view').attr('href', src);
                    } else {
                        Btn.append('<img src="' + src + '" /><a href="' + src + '" class="pd-image-view"> </a><i data-id=' + (iid - 1) + ' class="pd-image-delete"> </i>');
                    }
                    // 奖品首图
                    if (!pdCatimg || iid === 0) {
                        pdCatimg = 'product_hpic2__' + response.img_name;
                        $('#pd-catimg').val(pdCatimg);
                        $('#catimgPv').attr('href', shoproot + 'uploads/zy_hch/prize_img/' + response.img_name);
                    }
                    if (iid !== 0) {
                        pdImages[iid - 1] = 'product_hpic2__' + response.img_name;
                    }
                    fnFancyBox('.pd-image-view');
                    fnPdimageDelete();
                } else {
                    util.Alert('上传图片失败，请确保/uploads/zy_hch/prize_img/具有写权限');
                }
            }
        });
        $(this).hover(function () {
            if (!$(this).hasClass('ove')) {
                $(this).addClass('hover');
            }
        }, function () {
            if (!$(this).hasClass('ove')) {
                $(this).removeClass('hover');
            }
        });
    });

    // 产品首图
    if (pdCatimg) {
        $('.pd-image-sec').eq(0).addClass('ove').append('<img src="' + shoproot + 'uploads/zy_hch/prize_img/' + pdCatimg + '" />');
        $('#catimgPv').attr('href', shoproot + 'uploads/zy_hch/prize_img/' + pdCatimg);
    }

    $('body').css('overflow-x', 'hidden');

    uep = UM.getEditor('ueditorp', {
        autoHeight: true
    });
    uep.ready(function () {
        ueploaded = true;
    });

    // 图片已经上传过了。
    $('#save_product_btn').unbind('click').click(prizeEditFinish);

    // 删除图片--
    function fnPdimageDelete() {
        $('.pd-image-delete').unbind('click').on('click', function () {
            var nP = $(this).parent();
            // 删除图集数据
            pdImages[parseInt($(this).attr('data-id'))] = '';
            // 删除标记
            nP.removeClass('ove').find('i,img,.pd-image-view').remove();
            nP = null;
        });
    }

    /**
     * 奖品编辑结束
     * @returns {undefined}
     */
    function prizeEditFinish() {
        if (!loadingLock) {
            debugger;
            var postData = $('#pd-baseinfo').serializeArray();
            var prizeName = $('#pd-form-title').val();
            var prizeBrand = $('#brand_info').val();
            var place = $('#place').val();
            var tips  = $('#tips').val();
            var stock = parseInt($('input[name="stock"]').val());
            var originStock = parseInt($('input[name="origin_stock"]').val());

            var prizeType = $('input[name="prize_type"]:checked').val();

            if(null == prizeName || '' == prizeName ){
                util.Alert("奖品名称不能为空");
                return ;
            }

            if(!pdCatimg){
                util.Alert("请上传产品主图");
                return ;
            }



            var prize_desc =  $('textarea[name="prize_desc"]').val();
            if(prize_desc == '' || undefined == prize_desc){
                util.Alert("请填写奖品简介");
                return ;
            }

            if(prizeType == '' || undefined == prizeType){
                util.Alert("请选择奖品类型");
                return ;
            }

            if(prizeType == 2){
                var satisfyMoney = $('input[name="satisfy_money"]').val();
                if(!$.isNumeric(satisfyMoney) && satisfyMoney <= 0){
                    util.Alert("请填写正确的满减值");
                    return ;
                }
                var couponMoney = $('input[name="coupon_money"]').val();
                if(!$.isNumeric(couponMoney)&& couponMoney <= 0){
                    util.Alert("请填写正确的满减值");
                    return ;
                }
            }
            if(prizeType == 3){
                var couponValue = $('input[name="coupon_value"]').val();
                if(!$.isNumeric(couponValue) && couponValue <= 0){
                    util.Alert("请填写正确的优惠券面额");
                    return ;
                }
            }
            if(prizeType == 4){
                var shopCardValue = $('input[name="shop_card_value"]').val();
                if(!$.isNumeric(shopCardValue) && shopCardValue <= 0){
                    util.Alert("请填写正确的优惠券面额");
                    return ;
                }
            }


            if(originStock <= 0){
                util.Alert("请输入正确的最大奖品数量");
                return ;
            }
            if(stock <= 0){
                util.Alert("请输入正确的奖品数量");
                return ;
            }
            if(stock > originStock){
                util.Alert("现有奖品数量不能大于最大奖品数量");
                return ;
            }

            if(null == prizeBrand || '' == prizeBrand){
                util.Alert("请填写品牌信息");
                return ;
            }

            if(null == place || '' == place){
                util.Alert("请填写产地");
                return ;
            }

            if(null == tips || '' == tips){
                util.Alert("请填写提示信息");
                return ;
            }

            if(pdImages == null || pdImages == '' || pdImages[0] == ''){
                util.Alert("请上传图片集");
                return ;
            }

            var node = $(this);
            //loadingLock = true;
            node.html('数据处理中');
            $.post(shoproot + '?/HealthClockManage/savePrize', {
                id: !prizeId ? 0 : prizeId,
                product_infos: postData,
                product_images: pdImages,
            }, function (r) {
                if (r > 0) {
                    loadingLock = false;
                    if (!prizeId) {
                        util.Alert('添加成功', false, function () {
                            // 返回列表
                            history.go(-1);
                        });
                    } else {
                        util.Alert('保存成功', false, function () {
                            // 返回列表
                            history.go(-1);
                        });
                    }
                } else {
                    util.Alert('保存失败');
                }
                node.html('保存');
            });

        }
    }

    /**
     * 奖品删除监听
     */
    util.pdDeleteListen();

    /**
     * window resize 监听
     */
    util.onresize(function () {
        $('.pd-image-sec').each(function () {
            pdImageWidth = $(this).width();
            $(this).height(pdImageWidth);
        });
    });

});