requirejs(['jquery', 'util', 'fancyBox', 'datatables', 'Spinner','WdatePicker','layer','laytpl','laypage','laydate'], function($, util, fancyBox, dataTables, Spinner,WdatePicker,layer,laytpl,laypage,laydate) {
    $(function () {
        //要渲染的容器对象
        var parent_container = $('.dTable tbody');
        //laytpl模版中的数据
        var template_container = $('#data_render').html();
        //异步加载数据的url
        var url = '?/HealthClockManage/ajaxLoadPrizeList/page=__PAGE&pageSize=__PAGE_SIZE';
        //每页显示的数目
        var pageSize = 10;
        //页码
        var page = 1;
        //起始页
        var start_page = 1;

        ajax_load_data(page);

        /**
         * 异步加载数据
         * status 状态码
         * current_page 当前页码
         */
        function ajax_load_data(current_page){
            layer.load(0);
            if(current_page){
                page = current_page;
            }
            purl =url.replace('__PAGE',page).replace('__PAGE_SIZE',pageSize);
            console.log(purl);
            $.get(purl,function(data){
                $('.date_btn').each(function(){
                    $(this).attr('data-status',data.status);
                });

                //解析模版组装数据
                laytpl(template_container).render(data,function(html){
                    layer.closeAll('loading');
                    parent_container.html(html);
                    $('.pd-switchonline').click(switchonline);
                    // 删除按钮监听
                    $('.pd-del-btn').unbind('click').click(deletePrize);

                });

                //分页插件laypage的数据组装
                var reminder = data.total%pageSize;
                var devided = data.total/pageSize;
                var totalPage =  reminder==0?devided:devided+1;
                laypage({
                    cont: 'page', //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
                    pages: totalPage , //通过后台拿到的总页数
                    curr: current_page || 1, //当前页
                    skip: true, //是否开启跳页
                    skin: '#AF0000',
                    groups: 3, //连续显示分页数
                    jump: function(obj, first){ //触发分页后的回调
                        if(!first){ //点击跳页触发函数自身，并传递当前页：obj.curr
                            ajax_load_data(obj.curr);
                        }
                    }
                });
            });
        }

        /**
         * 上下架操作
         * @returns {undefined}
         */
        function switchonline() {
            debugger;
            var node = $(this);
            var onLineStr = ['上架', '下架'];
            var isOnline = parseInt(node.attr('data-prize-online')) === 1;
            var prizeId = node.attr('data-prize-id');
            var pImg = $('#pdlist-image' + prizeId);
            pImg.parent().css({
                width: pImg.width(),
                height: pImg.width() + 10
            });
            pImg.hide();
            Spinner.spin(pImg.parent().get(0));
            // [HttpPost]
            $.post('?/HealthClockManage/switchOnline/', {
                prize_id: prizeId,
                is_online: isOnline ? 0 : 1
            }, function (result) {
                pImg.show();
                Spinner.stop();
                if (result.errcode == 200) {
                    util.Alert('商品已' + onLineStr[Number(isOnline)]);
                    node.attr('data-prize-online', isOnline ? 0 : 1);
                    node.html(onLineStr[Number(!isOnline)]).toggleClass('tip');
                } else {
                    util.Alert(result.msg, true);
                }
            });
        }


        function deletePrize() {
            debugger;
            var tR = $(this).parent().parent();
            var node = $(this);
            var prizeId = node.attr('data-prize-id');
            if (confirm('你确定要删除这个奖品吗')) {
                $.post(shoproot + '?/HealthClockManage/deletePrize', {
                    prize_id: prizeId
                }, function (result) {
                    if (result.errcode == 200) {
                        tR.fadeOut('normal');
                        util.Alert('商品已加入回收站');
                    } else {
                        util.Alert(result.msg, true);
                    }
                });
            }
        }
        

        util.keyEnter('.searchbox', function (key) {
            param.key = key;
            util.removeEmptyTip();
            fnAjaxLoadPds();
        });

        

        fnFancyBox('.pd-qrcodebtn');

    });
});

