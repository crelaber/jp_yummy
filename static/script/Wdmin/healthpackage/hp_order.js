requirejs(['jquery', 'util', 'fancyBox', 'datatables', 'Spinner','WdatePicker','layer','laytpl','laypage','laydate'], function($, util, fancyBox, dataTables, Spinner,WdatePicker,layer,laytpl,laypage,laydate) {
	$(function(){
		//要渲染的容器对象
		var parent_container = $('.dTable tbody');
		//laytpl模版中的数据
		var template_container = $('#data_render').html();
		//异步加载数据的url
		var url = '?/HealthPackageManage/ajax_load_hp_order_list/status=__STATUS&day=__DAY&page=__PAGE&pageSize=__PAGE_SIZE';
		//每页显示的数目
		var pageSize = 10;
		//页码
		var page = 1;
		//起始页
		var start_page = 1;
		
		//导航Tab的点击事件
    	$(".nav-tabs>li>a").click(function() {
			if(!$(this).hasClass("active")) {
				var status = $(this).attr('data-status');
				var day = $("#day").val();
				$(this).parent().siblings().removeClass("active");
				$(this).parent().addClass('active');
				ajax_load_data(status,start_page);
			}
		});
    	//初始化Tab的点击事件，只有当有active样式时候才进行加载
    	$(".nav-tabs>li.active>a").click();
    	//前一天、今天、后一天等日期按钮的点击事件
    	$('.date_btn').click(function(){
    		if(!$(this).hasClass('primary')){
    			$(this).addClass('primary')
    			$(this).siblings().removeClass('primary');
    			$('#day').val($(this).attr('data-diff'));
    			ajax_load_data($(this).attr('data-status'),start_page);
    		}
    	});
    	
		/**
		 * 异步加载数据
		 * status 状态码
		 * current_page 当前页码
		 */
    	function ajax_load_data(status,current_page){
    		layer.load(0);
    		if(current_page){
    			page = current_page;
    		}
    		var day = $("#day").val();
    		purl =url.replace('__STATUS',status).replace('__DAY',day).replace('__PAGE',page).replace('__PAGE_SIZE',pageSize); 
    		console.log(purl);
    		$.get(purl,function(data){
    			$('#total').html(data.total);
    			$('#day').html(data.day);
    			$('#status').val(data.status);
    			$('.date_btn').each(function(){
    				$(this).attr('data-status',data.status);
    			});
    			
    			//解析模版组装数据
    			laytpl(template_container).render(data,function(html){
    			    layer.closeAll('loading');
    			    parent_container.html(html);
        		});
    			
    			//分页插件laypage的数据组装
    			var reminder = data.total%pageSize;
    			var devided = data.total/pageSize;
    			var totalPage =  reminder==0?devided:devided+1;
    			laypage({
    	            cont: 'page', //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
    	            pages: totalPage , //通过后台拿到的总页数
    	            curr: current_page || 1, //当前页
    	            skip: true, //是否开启跳页
    	            skin: '#AF0000',
    	            groups: 3, //连续显示分页数
    	            jump: function(obj, first){ //触发分页后的回调
    	                if(!first){ //点击跳页触发函数自身，并传递当前页：obj.curr
    	                	ajax_load_data(status,obj.curr);
    	                }
    	            }
    	        });
    		});
    	}
    	

        
        
        /**
         * 已送达
         */
        fnFancyBox('.reached', function () {
            $('#ok').on('click', function () {
            	//$(this).attr('data-id') 为弹出的内容框中ok按钮的data-id属性
            	$.get('?/Distribute/delievery_reached/', {id: $(this).attr('data-id')}, function(data) {
            		$.fancybox.close();
            		
            		if (data.ret_code > 0) {
    					util.Alert('成功送达！');
    				} else {
    					util.Alert(data.ret_msg, true);
    				}
//            		window.location.href = '?/WdminPage/distribute_list/status=delievered';
            		var status = $('#status').val();
            		ajax_load_data(status,start_page);
        		});
            })
            
            $('#close').on('click', function () {
            	$.fancybox.close();
            })
        });
    });
});