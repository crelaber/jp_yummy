
DataTableConfig.order = [[0, 'desc']];
DataTableConfig.searching = true;
requirejs(['jquery', 'util', 'fancyBox', 'datatables'], function ($, util, fancyBox, dataTables) {
    $(function () {
        window.util = util;
        util.loadOrderStatNums();
        var d = new Date();
        var d1 = d.getFullYear() + '-' + (d.getMonth() + 1);
        $('#month-select option[value=' + d1 + ']').get(0).selected = true;
        $('#month-select').on('change', function () {
	            ajaxLoadOrderlist(util);
        });
        ajaxLoadOrderlist(util);
    });


});



function ajaxLoadOrderlist(util) {
    $('#orderlist').load('?/Wdmin/ajaxLoadOrderlist/page=0&status=delivering&month=' + $('#month-select').val(), function (r) {
        if (r === '0') {
            util.listEmptyTip();
        } else {
            $('.wshop-empty-tip').remove();
            $('.dTable').dataTable(DataTableConfig).api();
            //必须要加上这句，否则会在列表中第一行出现空格
            util.remove_margin_top();
            fnFancyBox('.pd-list-viewExp,.od-list-pdinfo');

            fnFancyBox('.notes', function () {
                $('#status_btn').unbind('click').bind('click',function(){
                    $order_id = $('#order_id').val();

                    $status = $('#status').val();

                    $url = '?/WdminAjax/updateStatus';
                    $.post($url,{
                        'order_id' : $order_id,
                        'status' : $status
                    },function(data){
                        if(data.ret_code >= 0){
                            util.Alert('修改成功');
                            $.fancybox.close();
                        }else{
                            util.Alert(data.ret_msg,true);
                            window.location.reload();
                        }
                    });


                });
            });

        }
    });
}