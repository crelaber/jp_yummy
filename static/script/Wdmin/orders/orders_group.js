
/* global shoproot, DataTableConfig */

var dT;

DataTableConfig.order = [[0, 'desc']];
DataTableConfig.searching = true;
requirejs(['jquery', 'util', 'fancyBox', 'datatables', 'Spinner', 'jUploader', 'jpaginate','jPrintArea'], function ($, util, fancyBox, dataTables, Spinner, jUploader, jPrint) {
    $(function () {
        window.util = util;

        fnFancyBox('.various');

        fnFancyBox('.queryRefund');

        fnFancyBox('.orderRefund', function () {
            $('#refundBtn').on('click', function () {
                var groupId = $('#groupId').val();
                var pay_amount =  parseFloat($('#pay_amount').val());

                var balance_amount = parseFloat($('#balance_amount').val());
                var posting = false;
                // 发起退款
                if (!posting && confirm('你确认要退款 吗?')) {
                    var orderId = $(this).attr('data-id');
                    util.Alert('正在处理中...');
                    $('#refundBtn').html('正在处理中...');
                    posting = true;
                    // [HttpPost]
                    $.post(shoproot + '?/Order/orderRefund/', {
                        id: orderId,
                        groupId:groupId,
                        pay_amount:pay_amount,
                        balance_amount: balance_amount
                    }, function (r) {
                        $('#refundBtn').html('确认退款');
                        posting = false;
                        if (r !== '0') {
                            util.Alert('退款请求已成功提交，请等待微信商户处理', false);
                            $.fancybox.close();
                            window.location.reload();
                        } else {
                            util.Alert('退款处理出错，请联系技术支持', true);
                        }
                    });

                }
            });
        });

        //添加备注
        fnFancyBox('.notes', function () {
            $('#save_btn').unbind('click').bind('click',function(){
                $order_id = $('#order_id').val();
                $notes = $('#notes').val();

                if($notes.length > 100){
                    util.Alert('备注长度不能大于100',true);
                    return;
                }

                $url = '?/WdminAjax/updateNotes';
                $.post($url,{
                    'order_id' : $order_id,
                    'notes' : $notes
                },function(data){
                    if(data.ret_code >= 0){
                        util.Alert('修改成功');
                        $.fancybox.close();
                    }else{
                        util.Alert(data.ret_msg,true);
                    }
                });


            });


            $('#status_btn').bind('click',function(){

                $order_id = $('#order_id').val();
                $status = $('#status').val();
                $url = '?/WdminAjax/updateStatus';
                $.post($url,{
                    'order_id' : $order_id,
                    'status' : $status
                },function(data){
                    if(data.ret_code >= 0){
                        util.Alert('修改成功');
                        window.location.reload();
                        $.fancybox.close();
                    }else{
                        util.Alert(data.ret_msg,true);
                        window.location.reload();
                    }
                });

            });
        });

    });

});


