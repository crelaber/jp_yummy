
requirejs(['jquery', 'util', 'fancyBox'], function($, util, fancyBox) {
    $(function() {
        window.util = util;
        //添加备注
        fnFancyBox('.notes', function () {
            $('#save_btn').unbind('click').bind('click',function(){
                $order_id = $('#order_id').val();
                $notes = $('#notes').val();

                if($notes.length > 100){
                    util.Alert('备注长度不能大于100',true);
                    return;
                }

                $url = '?/WdminAjax/updateNotes';
                $.post($url,{
                    'order_id' : $order_id,
                    'notes' : $notes
                },function(data){
                    if(data.ret_code >= 0){
                        util.Alert('修改成功');
                        $.fancybox.close();
                    }else{
                        util.Alert(data.ret_msg,true);
                    }
                });


            });
        });
    });
});

