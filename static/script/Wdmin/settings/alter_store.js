/* global shoproot */


requirejs(['jquery', 'util', 'fancyBox', 'datatables', 'Spinner', 'jUploader', 'ztree', 'ztree_loader', 'baiduTemplate'], function ($, util, fancyBox, dataTables, Spinner, jUploader, ztree, treeLoader,baiduTemplate) {

    var openids = [];
    var relId = $('#pids').val();

    $('#saveBtn').click(function () {
    	var name = $('#name').val();
    	if(null == name || '' == name){
    		util.Alert("店铺名称不能为空",true);
    		return;
    	}
                        
        var address = $('#address').val();
        if(null == address || '' == address){
            util.Alert("店铺地址不能为空",true);
            return;
        }
    	
    	var range = $('#range').val();
    	if(null == range || '' == range){
    		util.Alert("服务半径不能为空",true);
    		return;
    	}
        var admin_openids = [];
        $('#admin_ids .usrItem').each(function () {
                if (!$(this).hasClass('add')) {
                    admin_openids.push($(this).attr('data-openid'));
                }
        });
        $('input[name="admin_openids"]').val(admin_openids.join());
        var id = $(this).attr('data-id');
        $.post('?/wSettings/alterStore/', $('form').serialize(), function (r) {
            if (r > 0) {
                if (id > 0) {
                    $('#saveBtn').attr('data-id', r);
                    util.Alert('保存成功');
                } else {
                    util.Alert('添加成功');
                }
                window.setTimeout(function () {
                                 location.href = '?/WdminPage/settings_stores';
                                 }, 2000);
            } else {
                util.Alert('操作失败', true);
            }
        });
    });

          function fnGetOpenids(Wrap) {
                $(Wrap + ' .usrItem').each(function () {
                        this_openid = $(this).attr('data-openid');
                        if (this_openid !== '' && this_openid !== undefined) {
                            openids.push(this_openid);
                        }
                });
          }
          
          fnGetOpenids('#admin_ids');
          fnFancyBox('#add-admin', function () {
                fnFancyLis();
          });
          /**
           *
           * @param {type} type
           * @returns {undefined}
           */
          function fnFancyLis() {
                $('.ztree li').eq(0).click();
                $('.fancybox-skin').css('background', '#fff');
                // 目录树点击回调函数
                treeLoader.setting.callback.onClick = function (event, treeId, treeNode) {
                    $('#pds-pdright #inlists').html('');
                    Spinner.spin($('#pds-pdright #inlists').get(0));
                    $.get('?/WdminAjax/ajax_customer_select_in/id=' + treeNode.dataId, function (html) {
                      $('#pds-pdright #inlists').html(html);
                      $('.pdBlock').bind('click', pdBlockLis);
                      $('#okSProduct').bind('click', confirmUser);
                    });
                };
                // 初始化目录树
                treeLoader.init('#pds-catLeft', '?/Uc/getAllGroup/r=' + (new Date()).getTime(), function () {
                                $('.ztree li').eq(0).click();
                });
          }
          
          /**
           * fnRemoveUsr
           * @returns {undefined}
           */
          function fnRemoveUsr() {
                $('.usrItem b').unbind('click').bind('click', function () {
                        var openid = $(this).parents('.usrItem').attr('data-openid');
                        for (i=0; i <openids.length; i++) {
                            if (openids[i] === openid) {
                                $(this).parents('.usrItem').remove();
                                delete openids[i];
                            }
                        }
                });
          }
          
          function confirmUser() {
            var list = [];
            $('.pdBlock.selected').each(function () {
                    if ($.inArray($(this).attr('data-openid'), openids) === -1) {
                        list.push({
                            src: $(this).find('img').attr('src'),
                            uname: $(this).find('.title').html(),
                            openid: $(this).attr('data-openid')
                        });
                        openids.push($(this).attr('data-openid'));
                    }
            });
            var html = baidu.template('t:usrlist', {list: list});
            $('#admin_ids').prepend(html);
            fnRemoveUsr();
            $.fancybox.close();
          }
          
          function pdBlockLis() {
            $(this).toggleClass('selected');
            $(this).find('.sel').toggleClass('hov');
          }
          
          fnRemoveUsr();
          
});