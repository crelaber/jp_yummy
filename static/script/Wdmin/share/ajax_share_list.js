
/* global shoproot, DataTableConfig */

requirejs(['jquery', 'util', 'fancyBox', 'Spinner','layer','laytpl','laypage','laydate'], function ($, util, fancyBox, Spinner,layer,laytpl,laypage,laydate ) {
    
    //$(function () {
    //    window.util = util;
    //
    //    // 加载产品库存列表
    //    ajaxShareList(util, callback);
    //    $('#month-select').on('change', function () {
    //    	ajaxShareList(util, listLoadCallback);
    //    });
    //});


    //父类容器
    var parent_container = $('.dTable tbody');
    //laytpl模版中的数据
    var laytpl_data_template = $('#data_render').html();
    var base_url = '?/WdminAjax/ajaxShareList/';
    //异步加载数据的url
    var purl = util.build_laytpl_url(base_url);
    //页码
    var page = 1;
    //加载数据
    util.ajax_load_data_by_layer(purl,page,util.get_default_page_size(),parent_container,laytpl_data_template,layer,laytpl,laypage);


});



/*
function ajaxShareList(util, callback) {
    $('.dTable tbody').load('?/WdminAjax/ajaxShareList/page=0&month=' + $('#month-select').val(), function (r) {
        if (r === '0') {
            util.listEmptyTip();
        } else {
            if (callback !== undefined) {
                callback();
            }
        }
    });
}

function callback(){
	
	
}

function dataExp() {
    DataTableSelect = true;
    DataTableMuli = true;
    $('.od-exp-check').show();
    $('.wd-btn.hidden').removeClass('hidden');
    $('#data-exp-trans').hide();
    $(this).hide();
    dataTableLis();
}


function dataExpCancel() {
    DataTableSelect = false;
    DataTableMuli = false;
    $('.od-exp-check').hide();
    $('#data-exp').show();
    $('#data-exp-confirm').addClass('hidden');
    $('#data-exp-trans').show();
    $(this).addClass('hidden');
    location.reload();
}*/
