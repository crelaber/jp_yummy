/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http=>//www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http=>//www.iwshop.cn
 */
requirejs(['jquery', 'util', 'echarts', 'echarts/chart/line', 'echarts/chart/pie', 'echarts/chart/bar'], function($, util, echarts) {

    var  currentMonthDayNums= function(){
        var  day = new Date();
        var year = day.getFullYear();
        var month = day.getMonth()+1;
        return MonthOfDays(year,month);
    }

    var MonthOfDays = function(year,month){
        var d = new Date(year,month,0);
        return d.getDate();
    }
    var getDays = function(str, day_count, format) {
        if (typeof str === "number") {
            format = day_count;
            day_count = str;
            str = new Date();
        }
        var date = new Date();
        //var date = new Date(2015,11,28);
        var month = 0;
        var month_arr_30_days = [4,6,9,11];
        var month_arr_28_days = [2];
        var dates = [];
        var totalDays = 0;
        for (var i = 0; i < day_count + 1; i++) {
            var d = null;
            if (format) {
                var fmt = format;
                fmt = fmt.replace(/y{4}/, date.getFullYear());
                fmt = fmt.replace(/M{2}/, date.getMonth()%12==0?12:date.getMonth()%12);
                fmt = fmt.replace(/d{2}/, date.getDate() >= 10 ? date.getDate() : '0' + date.getDate());
                d = fmt;
            } else {
                d = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
            }
            month = date.getMonth();
            if($.inArray(month,month_arr_30_days)>-1){  //只有30天的情况
                if(date.getDate()!= 31){
                    dates.push(d);
                }
            }else if($.inArray(month,month_arr_28_days)>-1){ //只有28天的情况
                if(date.getDate()<=28){
                    dates.push(d);
                }
            }else{
                dates.push(d);
            }
            date.setDate(date.getDate() + 1);
        }
        return dates;
    };

    util.onresize(function() {
        var h = $(window).height() - 2;
        $('.stat-h-50').height(h / 2);
        $('#stat-wrap').height(h);
        $('.fLeft50').height((h / 2) - 3);
    });

    var Days = getDays(currentMonthDayNums(), "MM-dd");
    console.log(Days);

    $.post(shoproot + '?/WdminStat/getOrderStat/', {}, function(r) {
        var data = [];
        for (var i in Days) {
            if (r[Days[i]] !== undefined) {
                data.push(r[Days[i]]);
            } else {
                data.push(0);
            }
        }
        // 销售情况面积图
        var option = {
            title: {
                text: '近30天订单',
                //subtext: '纯属虚构',
                x: 'center',
                padding: 10,
                textStyle: {
                    fontFamily: 'Verdana, Microsoft YaHei,Helvetica',
                    align: 'center',
                    baseline: 'middle',
                    fontWeight: 'normal'
                }
            },
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                show: false
            },
            calculable: false,
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: Days,
                    axisLine: {
                        show: false
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} 单'
                    }
                }
            ],
            series: [
                {
                    name: '单数',
                    type: 'line',
                    data: data,
                    markLine: {
                        data: [
                            {type: 'average', name: '平均值'}
                        ]
                    }
                }
            ]
        };

        echarts.init($('.stat-h-50').get(0)).setOption(option);
    });



    $.post(shoproot + '?/WdminStat/getClientsStat/', {}, function(r) {
        var data = [];
        for (var i in Days) {
            if (r[Days[i]] !== undefined) {
                data.push(r[Days[i]]);
            } else {
                data.push(0);
            }
        }
        // 销售情况面积图
        var option = {
            title: {
                text: '近30天会员',
                //subtext: '纯属虚构',
                x: 'center',
                padding: 10,
                textStyle: {
                    fontFamily: 'Verdana, Microsoft YaHei,Helvetica',
                    align: 'center',
                    baseline: 'middle',
                    fontWeight: 'normal'
                }
            },
            tooltip: {
                trigger: 'axis'
            },
            toolbox: {
                show: false
            },
            calculable: false,
            xAxis: [
                {
                    type: 'category',
                    boundaryGap: false,
                    data: Days,
                    axisLine: {
                        show: false
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value} 人'
                    }
                }
            ],
            series: [
                {
                    name: '人数',
                    type: 'line',
                    data: data,
                    markLine: {
                        data: [
                            {type: 'average', name: '平均值'}
                        ]
                    }
                }
            ]
        };

        echarts.init($('.stat-h-50').get(1)).setOption(option);
    });
});