/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */

var priceHashId = 0;

require(['config'], function(config) {

	require(['util', 'jquery', 'Spinner', 'Cart', 'Slider', 'Tiping'],
		function(util, $, Spinner, Cart, Slider, Tiping) {

			$('#select_back').click(function() {

				
				var couponId = $('#couponId').val();
			    var time = $('#time').val();
			    var isbalance =$('#isbalance').val();
                var groupId = $('#groupId').val();
                var isTuan = $('#isTuan').val();
                var pid  = $('#pid').val();

                var url = shoproot + "?/Cart/index_order/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
                window.location.href = url;

			});
			
			$('#back').click(function() {

                var groupId = $('#groupId').val();
                var isTuan = $('#isTuan').val();
				var couponId = $('#couponId').val();
			    var time = $('#time').val();
			    var isbalance =$('#isbalance').val();
                var pid  = $('#pid').val();

                var url = shoproot + "?/Cart/index_order/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
                window.location.href = url;

			});

            $('#update').click(function(){
                var provincetxt = $('#provincetxt option:selected').text();
                var city = $('#citytxt option:selected').text();
                var areatxt = $('#areatxt option:selected').text();
                var address = $('#fillarea').val();
                var name = $('#fillname').val();
                var phone = $('#fillphone').val();
                var fillid = $('#fillid').val();
                var id = $('#id').val();
                var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
                if(provincetxt=="请选择"||city=="请选择"||areatxt=="请选择"||address.trim()==""||name.trim()==""||phone.trim()==""){
                    layer.open({
                        content: '请完善地址信息！',
                        time: 2
                    });
                    return;
                }
                if (!reg.test(phone)) {
                    layer.open({
                        content: '请填写正确的手机号！',
                        time: 3 //3秒后自动关闭
                    });
                    return;
                }
                if(fillid.trim().length !=0 &&!isIdCard(fillid)){
                    layer.open({
                        content: '您填写的身份证有误！',
                        time: 3 //3秒后自动关闭
                    });
                    return;
                }
                if(address.trim().length<5){
                    layer.open({
                        content: '详细地址不得少于5个字！',
                        time: 3 //3秒后自动关闭
                    });
                    return;
                }


                var url = shoproot + "?/UserAddress/ajaxUpdateAddress";
                $.post(url, {
                    id:id,
                    user_name: name,
                    phone: phone,
                    city: city,
                    area: areatxt,
                    province:provincetxt,
                    identity:fillid,
                    address: address

                }, updateAddressResult);

            });

            function updateAddressResult(data){

                var groupId = $('#groupId').val();
                var isTuan = $('#isTuan').val();
               var url = shoproot + "?/Cart/index_order/isTuan="+isTuan+"&groupId="+groupId;
                window.location.href = url;
            }
			
			$('#submit').click(
				function() {

                    var provincetxt = $('#provincetxt option:selected').text();
                    var city = $('#citytxt option:selected').text();
                    var areatxt = $('#areatxt option:selected').text();
                    var address = $('#fillarea').val();
                    var name = $('#fillname').val();
                    var phone = $('#fillphone').val();
                    var fillid = $('#fillid').val();


                    var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
					if(provincetxt=="请选择"||city=="请选择"||areatxt=="请选择"||address.trim()==""||name.trim()==""||phone.trim()==""){
					layer.open({
						    content: '请完善地址信息！',
						    time: 2
						});
					return;
					}
                    if (!reg.test(phone)) {
						layer.open({
                				content: '请填写正确的手机号！',
                 				time: 3 //3秒后自动关闭
          			  });
						return;
					}
					if(fillid.trim()!=""&&!isIdCard(fillid)){
						layer.open({
                				content: '您填写的身份证有误！',
                 				time: 3 //3秒后自动关闭
          			  });
						return;
					}
					if(address.trim().length<5){
					layer.open({
                				content: '详细地址不得少于5个字！',
                 				time: 3 //3秒后自动关闭
          			  });
						return;	
					}
					var url = shoproot + "?/UserAddress/add_address";
					$.post(url, {
						user_name: name,
						phone: phone,
						city: city,
						area: areatxt,
                        province:provincetxt,
                        identity:fillid,
						address: address

					}, addAddressResult);
				});
			function isIdCard(person_id) {
                    var person_id = person_id;
                    
                        //身份证的地区代码对照  
                        var aCity = {
                            11 : "北京",
                            12 : "天津",
                            13 : "河北",
                            14 : "山西",
                            15 : "内蒙古",
                            21 : "辽宁",
                            22 : "吉林",
                            23 : "黑龙江",
                            31 : "上海",
                            32 : "江苏",
                            33 : "浙江",
                            34 : "安徽",
                            35 : "福建",
                            36 : "江西",
                            37 : "山东",
                            41 : "河南",
                            42 : "湖北",
                            43 : "湖南",
                            44 : "广东",
                            45 : "广西",
                            46 : "海南",
                            50 : "重庆",
                            51 : "四川",
                            52 : "贵州",
                            53 : "云南",
                            54 : "西藏",
                            61 : "陕西",
                            62 : "甘肃",
                            63 : "青海",
                            64 : "宁夏",
                            65 : "新疆",
                            71 : "台湾",
                            81 : "香港",
                            82 : "澳门",
                            91 : "国外"
                        };
                        //获取证件号码  
                        //  var person_id=person_id.value();  
                        //合法性验证  
                        var sum = 0;
                        //出生日期  
                        var birthday;
                        //验证长度与格式规范性的正则  
                        var pattern = new RegExp(
                                /(^\d{15}$)|(^\d{17}(\d|x|X)$)/i);
                        if (pattern.exec(person_id)) {
                            //验证身份证的合法性的正则  
                            pattern = new RegExp(
                                    /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/);
                            if (pattern.exec(person_id)) {
                                //获取15位证件号中的出生日期并转位正常日期       
                                birthday = "19" + person_id.substring(6, 8)
                                        + "-" + person_id.substring(8, 10)
                                        + "-" + person_id.substring(10, 12);
                            } else {
                                person_id = person_id.replace(/x|X$/i, "a");
                                //获取18位证件号中的出生日期  
                                birthday = person_id.substring(6, 10) + "-"
                                        + person_id.substring(10, 12) + "-"
                                        + person_id.substring(12, 14);

                                //校验18位身份证号码的合法性  
                                for ( var i = 17; i >= 0; i--) {
                                    sum += (Math.pow(2, i) % 11)
                                            * parseInt(
                                                    person_id.charAt(17 - i),
                                                    11);
                                }
                                if (sum % 11 != 1) {
                                    $(this).addClass("txtRequired");
                                    //alert("身份证号码不符合国定标准，请核对！");                               
                                    //$("#person_id").val("");  
                                    $("#birthday").val("")
                                    return false;
                                }
                            }
                            //检测证件地区的合法性                                  
                            if (aCity[parseInt(person_id.substring(0, 2))] == null) {
                                $(this).addClass("txtRequired");
                                //  alert("证件地区未知，请核对！");                             
                                //$("#person_id").val("");  
                                $("#birthday").val("");
                                return false;
                            }
                            var dateStr = new Date(birthday.replace(/-/g, "/"));

                            //alert(birthday +":"+(dateStr.getFullYear()+"-"+ Append_zore(dateStr.getMonth()+1)+"-"+ Append_zore(dateStr.getDate())))  
                            if (birthday != (dateStr.getFullYear() + "-"
                                    + Append_zore(dateStr.getMonth() + 1) + "-" + Append_zore(dateStr
                                    .getDate()))) {
                                $(this).addClass("txtRequired");
                                //  alert("证件出生日期非法！");                           
                                //$("#person_id").val("");  
                                $("#birthday").val("");
                                return false;
                            }
                            $(this).removeClass("txtRequired");
                            $("#birthday").val(birthday);
                            
                        } else {
                            $(this).addClass("txtRequired");
                            // alert("证件号码格式非法！");                           
                            //$("#person_id").val("");  
                            $("#birthday").val("")
                            return false;
                        }
                       
                        return true;
                }

                function Append_zore(temp) {
                    if (temp < 10) {
                        return "0" + temp;
                    } else {
                        return temp;
                    }
                }
			function addAddressResult(data) {

				if (data.ret_code < 0) {

					layer.open({
                 content: data.ret_msg,
                 time: 3 //3秒后自动关闭
                 });
					return;
				}
				var couponId = $('#couponId').val();
			    var time = $('#time').val();
			    var isbalance =$('#isbalance').val()
                var groupId = $('#groupId').val();
                var isTuan = $('#isTuan').val();
                var pid  = $('#pid').val();
				var url = shoproot + "?/Cart/index_order/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
				window.location.href = url;
			}

			//增加地址
			$('#add').click(function() {
				var couponId = $('#couponId').val();
			    var time = $('#time').val();
			    var isbalance =$('#isbalance').val();
                var groupId = $('#groupId').val();
                var isTuan = $('#isTuan').val();
                var pid = $('#pid').val();
				var url = shoproot + "?/UserAddress/edit_address/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
				window.location.href = url;
			});
			
			//删除地址
			function delete_address() {
				$(".delete-btn").click(function() {
					var value = $(this).parents('li').find(
						'.address-detail').attr('data-id');
                        	layer.open({
                            content: '确认删除地址？',
                            btn: ['确认', '取消'],
                            shadeClose: false,
                            yes: function(){
                            $.post('?/UserAddress/remove_address',{

                            id:value
                            },function(data){
                                if(data.ret_code > 0){
                                    $(this).parents("li").remove();

                                }

                                var couponId = $('#couponId').val();
                			    var time = $('#time').val();
                			    var isbalance =$('#isbalance').val();
                                var groupId = $('#groupId').val();
                                var isTuan = $('#isTuan').val();
                                var pid  = $('#pid').val();
                				var url = shoproot + "?/UserAddress/list_address/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
                                window.location.href = url;


                            });

    }
});

                    })
                }

			function modify_address() {
				$(".modify-add").click(
					function() {
						if ($(this).text() == "编辑") {
							$(".select-btn").hide();
							$(".delete-btn").show();
							$(this).html("完成")

						} else {
							$(".select-btn").show();
							$(".delete-btn").hide();
							$(this).html("编辑")
							var address_list = $(".address-list ul li").find(".select-btn");
							if (address_list.hasClass('active')) {} else {
								$(".address-list ul li:first").find(
									".select-btn").addClass('active');
							}
						}
					})
			}

			function select_address() {
				$(".address-list ul li").click(
					function() {

                        var couponId = $('#couponId').val();
                        var time = $('#time').val();
                        var isbalance =$('#isbalance').val();

						var value = $(this).find(
							'.address-detail').attr('data-id');
						var url = shoproot + "?/UserAddress/ajaxEditAddress";

						$.post(url, {
							id: value

						}, function(data) {
                            // var url = shoproot + "?/Cart/index_order/couponId="+couponId+"&time="+time+"&isbalance="+isbalance;
                            // window.location.href = url;
						});

                        var groupId = $('#groupId').val();
                        var isTuan = $('#isTuan').val();
                        var pid = $('#pid').val();

                        if ($(this).find('.select-btn').hasClass('active')) {} else {
							$(".address-list ul li .select-btn")
								.removeClass('active');
							$(this).find('.select-btn').addClass('active');
						}
						if($(".delete-btn").is(":hidden")) {
                                    var url = shoproot + "?/Cart/index_order/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
                                window.location.href = url;
						}
					})
			}
			$(document).ready(function() {
				select_address();
				delete_address();
				modify_address();
			})

		});
});