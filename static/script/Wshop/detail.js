/* global wx, shoproot, parseFloat */

/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */

require(['config'], function(config) {

    require(['util', 'jquery', 'Spinner', 'Cart', 'Tiping', 'touchSlider'], function(util, $, Spinner, Cart, Tiping, touchSlider) {

        Cart.init();
        var storesec=$(".secstores");
        $('#buy').click(function() {
            var count = window.localStorage.getItem("totalCount");
            var fromUid = $('#from_uid').val();

            if (count == 0) {

                layer.open({
                    content: '购物车空空的！',
                    time: 3 //3秒后自动关闭
                });
                return;
            }

            window.location.href = shoproot + "?/Cart/index_order/fromUid="+fromUid;


        });

        function doCallback(data) {

            if (data != "") {
                // alert(JSON.stringify(data));
                // val total = data.total;
                // alert(data.total);


                var total = data.total;
                var yun = data.totalYun;
                if(data.total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }
                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);
                if (data.topCats != "") {

                }

            }
            $(".number").html(Cart.count());
        }

        function empty_cart() {
            $(".header-right").click(function() {
                layer.open({
                    content: '确认清空购物车？',
                    btn: ['确认', '取消'],
                    shadeClose: false,
                    yes: function(){
                        $.post(shoproot + '?/Cart/ajaxDelCart/', function(Res) {

                            //$(".cart-list ul").remove();
                            Cart.clear();
                            layer.closeAll();
                            $(".number").html(0);
                            $(".cart-list").removeClass('active');
                            window.localStorage.setItem("total","0.00");
                            window.localStorage.setItem("yun","0.00");
                            window.localStorage.setItem("totalCount","0");

                            $(".pro_price ").html("￥0.00");
                            $(".freight b").html("￥0.00");
                            window.location.reload();



                        });
                    }
                });
            });
        }

        function cart_list_show() {
            $(".cart-list img").click(function() {
                $(".cart-list").hide();
            });
            $(".shopping-cart").click(function() {
                var count = window.localStorage.getItem("totalCount");
                if ($(".cart-list").hasClass('active')) {
                    $(".cart-list").hide();
                    $(".cart-list").removeClass('active');
                    fnLoadCatlist(cat);

                } else if (count > 0) {
                    $(".cart-list").show();
                    $(".cart-list").addClass('active');
                    $.post(shoproot + '?/Index/ajaxGetCartProducts/', clearResult);
                    ajaxSyncCart();
                }
            })
        }

        function ajaxSyncCart(){

            $.post(shoproot + '?/Cart/ajaxSyncCart/', syncResult);

        }

        function syncResult(data){

            var totalCount = data.totalCount;
            var total = data.total;
            var yun = data.yun;

            window.localStorage.setItem("totalCount",totalCount);


            if(total > 0){

                window.localStorage.setItem("total",total.toFixed(2));
                if(yun > 0){
                    window.localStorage.setItem("yun",yun.toFixed(2));
                }else{
                    window.localStorage.setItem("yun","0.00");
                    yun = "0.00";
                }

            }else{
                window.localStorage.setItem("total","0.00");
                window.localStorage.setItem("yun","0.00");
                total = "0.00";
                yun = "0.00";
            }
            check_cart();
            $(".pro_price ").html("&yen;" + total);
            $(".freight b").html("&yen;" + yun);


        }

        function clearResult(data) {

            $('#cart_list').html(data);
            cart_proname_wd();
            cart_plus_click();
            cart_minus_click();

        }

        // $(".number").html(Cart.count());

        function cart_proname_wd() {
            var win_wd = $(window).width();
            var win_hg = $(window).height();
            $(".title-pro").css({
                'max-width': win_wd - 175
            });
            $(".cart-list ul").css({
                'max-height': win_hg / 2
            });
        }

        function cart_plus_click() {

            $('.prolist #cart_plus').click(function() {
                var hash = $(this).parents('li').find(".pro-buy-num").attr('data-hash');
                var p = $(this).parents('li').find(".pro-buy-num").attr('data-p');
                var spid = $(this).parents('li').find(".pro-buy-num").attr('data-sp');

                var numNode = $(this).parents('li').find('.num');


                var instock = $(".commodity-body").attr('data-instock');
                if(parseInt(numNode.text()) + 1 > parseInt(instock)){
                    layer.open({
                        content:'无法选择更多',
                        time: 3 //3秒后自动关闭
                    });
                    return;
                }

                var num= parseInt(numNode.text()) + 1;
                ajax_do_cart_product_cart($(this).parents('li'),p,spid,num);
            });
        }

        function ajax_do_cart_product_cart(node,pid,spid,count){



            $.post('?/Cart/ajaxDoCartProduct/', {
                pid: pid,
                spid: spid,
                pcount:count
            }, function(result){

                var code =  result.ret_code;
                var data = result.ret_msg;
                //alert(code);
                if(code == -1){
                    //产品已下架

                    layer.open({
                        content: '产品已下架',
                        time: 3 //3秒后自动关闭
                    });
                    $('.add_pro').hide();
                    var localNode = $('.add_pro .hidden').find('.num');
                    localNode.html(0);
                    node.remove();
                }else if(code == -2){
                    layer.open({
                        content: '产品秒杀已结束',
                        time: 3 //3秒后自动关闭
                    });
                    $('.add_pro').hide();
                    var localNode = $('.add_pro .hidden').find('.num');
                    localNode.html(0);
                    node.remove();
                }else if(code == 1){
                    //产品为0已移除
                    node.remove();
                    if($(".proprice").attr("data-p")==pid){
                        $('.add_pro .hidden').hide();
                        $('.add_pro .hidden').find('.num').html('0');
                        $('.add_pro .addcart').show();
                    }
                    

                }else if(code == 2){
                    //产品已经成功处理
                    node.find(".add_pro .hidden").show();
                    $('.add_pro .hidden').show();

                }

                var p = node.find(".pro-buy-num").attr('data-p')
                if (p == $('#iproductId').val()) {

                    var localNode = $('.add_pro .hidden').find('.num');

                    localNode.html(count);
                    //alert(num);

                }
                node.find('.num').html(count);
                var totalCount = data.totalCount;
                var total = data.total;
                var yun = data.yun;
                window.localStorage.setItem("totalCount",totalCount);

                if(total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }

                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                check_cart();

                if(totalCount == 0){

                    // $(".cart-list").removeClass('active');
                    $(".cart-list").hide();
                    $(".cart-list").removeClass('active');

                }

                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);

            });
        }

        function cart_minus_click() {
            var cartcount=Cart.count();
            $('.prolist #cart_minus').click(function() {
                var hash = $(this).parents('li').find(".pro-buy-num").attr('data-hash');
                var numNode = $(this).parents('li').find('.num');
                var p = $(this).parents('li').find(".pro-buy-num").attr('data-p');
                var sp = $(this).parents('li').find(".pro-buy-num").attr('data-sp');
                var num = parseInt(numNode.text()) - 1;


                ajax_do_cart_product_cart($(this).parents('li'),p,sp,num);

            });
        }

        function detail_plus() {
            $(".icon-plus").click(function() {
                var hash = $(".commodity-body").attr('data-hash');
                var num = parseInt($(".add_pro .hidden .num").text()) + 1;

                var pid = $(".commodity-body").attr('data-p');
                var spid = $(".commodity-body").attr('data-sp');

                var instock = $(".commodity-body").attr('data-instock');
                if(num > parseInt(instock)){
                    layer.open({
                        content:'无法选择更多',
                        time: 3 //3秒后自动关闭
                    });
                    return;
                }

                ajax_do_product_cart(pid,spid,num);

            });


            $(".icon-minus").click(function() {

                var num_minus = $(".add_pro .hidden .num").text();
                var hash = $(".commodity-body").attr('data-hash');
                var p = $(".commodity-body").attr('data-p');
                var sp = $(".commodity-body").attr('data-sp');
                var num = parseInt(num_minus) - 1;
                ajax_do_product_cart(p,sp,num);

            });

            $(".addcart").click(function(){
                var hash = $(".commodity-body").attr('data-hash');
                var num = parseInt($(".add_pro .hidden .num").text()) + 1;

                var pid = $(".commodity-body").attr('data-p');
                var spid = $(".commodity-body").attr('data-sp');

                var instock = $(".commodity-body").attr('data-instock'); 
                ajax_do_product_cart(pid,spid,num);
            });

        }


        function ajax_do_product_cart(pid,spid,count){



            $.post('?/Cart/ajaxDoCartProduct/', {
                pid: pid,
                spid: spid,
                pcount:count
            }, function(result){

                var code =  result.ret_code;
                var data = result.ret_msg;

                if(code == -1){
                    //产品已下架

                    layer.open({
                        content: '产品已下架',
                        time: 3 //3秒后自动关闭
                    });
                    $(".add_pro").hide();
                    $(".add_pro .hidden .num").html("0");


                }else if(code == -2){

                    layer.open({
                        content: '产品秒杀已结束',
                        time: 3 //3秒后自动关闭
                    });
                    $(".add_pro").hide();
                    $(".add_pro .hidden .num").html("0");
                }else if(code == 1){
                    //产品为0已移除
                    $(".add_pro .hidden").hide();
                    $(".add_pro .hidden .num").html("0");
                    $(".addcart").show()
 

                }else if(code == 2){
                    //产品已经成功处理
                    $(".add_pro .hidden").show();
                    $(".add_pro .hidden .num").html(count);
                    $(".addcart").hide();

                }

                var totalCount = data.totalCount;
                var total = data.total;
                var yun = data.yun;
                window.localStorage.setItem("totalCount",totalCount);

                if(total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }

                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                check_cart();

                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);

            });
        }

        function totopbtn(){
            $(window).scroll(function () {
                var topheight=$(window).scrollTop();
                if(topheight>50){
                    $(".to_top").removeClass('hidden');
                }
                else{
                    $(".to_top").addClass('hidden');
                }
            });
            $(".to_top").click(function(){
                $('html,body').animate({scrollTop: '0px'}, 500);
            });
        }

        $(document).ready(function() {
            detail_plus();
            cart_list_show();
            empty_cart();
            totopbtn();
            ajaxGetContent();
            ajaxSyncCart();
            check_cart();
            miaosha();

        });
        function miaosha(){
            var time = $(".panic_buying").attr('data-time');
            //refreshTime(time,$(".pro_list ul li").eq(i));
            //setInterval(refreshTime(time,$(".pro_list ul li").eq(i)),1000);
            var a= setInterval(function(){
                var endtime=new Date(time);
                var nowtime = new Date();
                var leftsecond=parseInt((endtime.getTime()-nowtime.getTime())/1000);
                d=parseInt(leftsecond/3600/24);
                h=parseInt((leftsecond/3600)%24);
                m=parseInt((leftsecond/60)%60);
                s=parseInt(leftsecond%60);
                if(h < 10){

                    if(d > 0){
                        h = 24*d +h;
                    }else{
                        h = "0"+h;
                    }

                }else{
                    if(d > 0){
                        h = 24*d +h;
                    }
                }
                if(m<10){
                    m = "0"+m;
                }
                if(s<10){
                    s = "0"+s;
                }
                if(leftsecond>0){
                    var countdown = $(".detail_proinfor").find('.time_end').html("<b>"+h+"</b>:<b>"+m+"</b>:<b>"+s+"</b>");
                }else{
                    var countdown = $(".detail_proinfor").find('.timeend').html("秒杀已结束");
                    $(".detail_proinfor").find(".msadd_pro ").hide();
                }

            }, 1000);

            //if(cat != 119){
            //  clearInterval(a);
            //}
        }
        function check_cart(){
            var count = window.localStorage.getItem("totalCount");
            if(count==0){
                $(".body-footer.empty").show();
                $(".body-footer.cartlist").hide();
            }
            else{
                $(".body-footer.empty").hide();
                $(".body-footer.cartlist").show();
            }
        }

        function ajaxGetContent() {

            $('#vpd-content').html('');
            Spinner.spin($('#vpd-content').get(0));
            $.ajax({
                url: '/html/products/' + $('#iproductId').val() + '.html',
                success: function(data) {
                    Spinner.stop();
                    $('#vpd-content').html(data);
                    contentLoaded = true;
                    $('#vpd-detail-header').show();
                    $('.notload').removeClass('notload');
                    $('#vpd-content').fadeIn();
                    // 调整图片
                    $('#vpd-content img').each(function() {
                        $(this).on('load', function() {
                            if ($(this).width() >= document.body.clientWidth) {
                                $(this).css('display', 'block');
                            }
                            $(this).height('auto');
                        });
                    });
                    $('#vpd-content').find('div').width('auto');
                },
                error: function() {
                    $('#vpd-content').load('?/vProduct/ajaxGetContent/id=' + $('#iproductId').val(), function() {
                        Spinner.stop();
                        contentLoaded = true;
                        $('#vpd-detail-header').show();
                        $('.notload').removeClass('notload');
                        $('#vpd-content').fadeIn();
                        // 调整图片
                        $('#vpd-content img').each(function() {
                            $(this).on('load', function() {

                                if ($(this).width() >= document.body.clientWidth) {
                                    $(this).css('display', 'block');
                                }
                                $(this).height('auto');
                            });
                        });
                        $('#vpd-content').find('div').width('auto');
                    });
                }
            });
        }

    });

});