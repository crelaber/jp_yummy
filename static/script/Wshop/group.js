/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */
var priceHashId = 0;
require(['config'], function (config) {
    require(['util', 'jquery', 'Spinner', 'Slider', 'Tiping'], function (util, $, Spinner, Slider, Tiping) {

        group_time();

        function group_time(){

            $(".group_list ul .group").each(function (i) {
                var time = $(".group_list ul .group").eq(i).find(".proInfor").attr('data-time');
                //refreshTime(time,$(".pro_list ul li").eq(i));
                //setInterval(refreshTime(time,$(".pro_list ul li").eq(i)),1000);
                var a= setInterval(function(){
                    var endtime=new Date(time);
                    var nowtime = new Date();
                    var leftsecond=parseInt((endtime.getTime()-nowtime.getTime())/1000);
                    d=parseInt(leftsecond/3600/24);
                    h=parseInt((leftsecond/3600)%24);
                    m=parseInt((leftsecond/60)%60);
                    s=parseInt(leftsecond%60);

                    if(h < 10){

                        if(d > 0){
                            h = 24*d +h;
                        }else{
                            h = "0"+h;
                        }

                    }else{
                        if(d > 0){
                            h = 24*d +h;
                        }
                    }
                    if(m<10){
                        m = "0"+m;
                    }
                    if(s<10){
                        s = "0"+s;
                    }
                    if(leftsecond>0){
                        var countdown = $(".group_list ul .group").eq(i).find('.time_end').html("<b>"+h+"</b>时<b>"+m+"</b>分<b>"+s+"</b>秒");
                    }else{
                        var countdown = $(".group_list ul .group").eq(i).find('.timeend').html("秒杀已结束");
                        $(".group_list ul .group").eq(i).find(".add_pro ").hide();
                    }

                }, 1000);


            });
        }

    });
});
