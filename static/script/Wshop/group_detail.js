/* global wx, shoproot, parseFloat */

/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */

require(['config'], function(config) {

    require(['util', 'jquery', 'Spinner', 'Tiping', 'touchSlider'], function(util, $, Spinner, Tiping, touchSlider) {



        $(document).ready(function() {
            ajaxGetContent();
            miaosha();
            tuaning();
            $(".attencode").hide();
           $(".closecode img").click(function(){

               $(".attencode").hide();

           });
        });

        $('#back_list').click(function(){




            var url = "?/Group/group_list/id="+$('#catId').val();
            window.location.href=url;

        });


        $('#buy').click(function(){

                var pid =  $('#iproductId').val();
            var url = "?/Cart/index_order/isTuan=2&pid="+pid;
            window.location.href=url;

        });


        $("#ct").click(function(){

            var isJoin = $('#isJoin').val();
            var id = $('#groupId').val();
            var pid = $('#pid').val();
            var result = $('#result').val();
            var isSubscribed =  $('#isSubscribed').val();

            if(isSubscribed == 0){
                $(".attencode").show();
                return;
            }
            if(isJoin == 0){


                if(result == 1 || result == -1){
                    var url = "?/Cart/index_order/isTuan=1&pid="+pid;
                }else{
                    var url = "?/Cart/index_order/isTuan=1&pid="+pid+"&groupId="+id;

                }
                window.location.href=url;
            }else{



                if(result == 1 || result == -1){
                    var url = "?/Cart/index_order/isTuan=1&pid="+pid;
                    window.location.href=url;
                }else{
                    $(".maskshare").show();
                }

            }

        });


        $('#kt').click(function(){


            var pid = $('#iproductId').val();
            var group = $('#group').val();
            var isEnd = $('#isEnd').val();
            var isSubscribed =  $('#isSubscribed').val();
            var is_active =  $('#is_active').val();

            if(is_active == 0){
                layer.open({
                    content:"亲 还在预热中 暂不能参团!",
                    time: 3 //3秒后自动关闭
                });
                return;
            }
            if(isSubscribed == 0){
                $(".attencode").show();
                return;
            }
            if(isEnd == 1){

                layer.open({
                    content:"亲 活动已结束，下次早点哦!",
                    time: 3 //3秒后自动关闭
                });
                return;
            }
            if(group == 1){
                var url = "?/Group/grouping/pid="+pid;

            }else{
                var url = "?/Cart/index_order/isTuan=1&pid="+pid;

            }

            window.location.href=url;

        });

        function miaosha(){
            var time = $(".proprice").attr('data-time');
            //refreshTime(time,$(".pro_list ul li").eq(i));
            //setInterval(refreshTime(time,$(".pro_list ul li").eq(i)),1000);
            var a= setInterval(function(){
                var endtime=new Date(time);
                var nowtime = new Date();
                var leftsecond=parseInt((endtime.getTime()-nowtime.getTime())/1000);
                d=parseInt(leftsecond/3600/24);
                h=parseInt((leftsecond/3600)%24);
                m=parseInt((leftsecond/60)%60);
                s=parseInt(leftsecond%60);
                if(h < 10){

                    if(d > 0){
                        h = 24*d +h;
                    }else{
                        h = "0"+h;
                    }

                }else{
                    if(d > 0){
                        h = 24*d +h;
                    }
                }
                if(m<10){
                    m = "0"+m;
                }
                if(s<10){
                    s = "0"+s;
                }
                if(leftsecond>0){
                    var countdown = $(".detail_proinfor").find('.time_end').html("<b>"+h+"</b>时<b>"+m+"</b>分<b>"+s+"</b>秒");
                }else{
                    var countdown = $(".detail_proinfor").find('.timeend').html("秒杀已结束");
                    $(".detail_proinfor").find(".msadd_pro ").hide();
                }

            }, 1000);

            //if(cat != 119){
            //  clearInterval(a);
            //}
        }

        //参团中的页面
        function tuaning(){
            var time = $(".timedown").attr('data-time');
            //refreshTime(time,$(".pro_list ul li").eq(i));
            //setInterval(refreshTime(time,$(".pro_list ul li").eq(i)),1000);
            var a= setInterval(function(){
                var endtime=new Date(time);
                var nowtime = new Date();
                var leftsecond=parseInt((endtime.getTime()-nowtime.getTime())/1000);
                d=parseInt(leftsecond/3600/24);
                h=parseInt((leftsecond/3600)%24);
                m=parseInt((leftsecond/60)%60);
                s=parseInt(leftsecond%60);
                if(h < 10){

                    if(d > 0){
                        h = 24*d +h;
                    }else{
                        h = "0"+h;
                    }

                }else{
                    if(d > 0){
                        h = 24*d +h;
                    }
                }

                if(m<10){
                    m = "0"+m;
                }
                if(s<10){
                    s = "0"+s;
                }
                if(leftsecond>0){
                    var countdown = $(".count_down").find('.time_end').html("<b>"+h+"</b>时<b>"+m+"</b>分<b>"+s+"</b>秒");
                }else{
                    var countdown = $(".count_down").find('.time_end').html("秒杀已结束");
                    //$(".detail_proinfor").find(".msadd_pro ").hide();
                }

            }, 1000);

            //if(cat != 119){
            //  clearInterval(a);
            //}
        }


        function ajaxGetContent() {

            $('#vpd-content').html('');
            Spinner.spin($('#vpd-content').get(0));
            $.ajax({
                url: '/html/products/' + $('#iproductId').val() + '.html',
                success: function(data) {
                    Spinner.stop();
                    $('#vpd-content').html(data);
                    contentLoaded = true;
                    $('#vpd-detail-header').show();
                    $('.notload').removeClass('notload');
                    $('#vpd-content').fadeIn();
                    // 调整图片
                    $('#vpd-content img').each(function() {
                        $(this).on('load', function() {
                            if ($(this).width() >= document.body.clientWidth) {
                                $(this).css('display', 'block');
                            }
                            $(this).height('auto');
                        });
                    });
                    $('#vpd-content').find('div').width('auto');
                },
                error: function() {
                    $('#vpd-content').load('?/vProduct/ajaxGetContent/id=' + $('#iproductId').val(), function() {
                        Spinner.stop();
                        contentLoaded = true;
                        $('#vpd-detail-header').show();
                        $('.notload').removeClass('notload');
                        $('#vpd-content').fadeIn();
                        // 调整图片
                        $('#vpd-content img').each(function() {
                            $(this).on('load', function() {

                                if ($(this).width() >= document.body.clientWidth) {
                                    $(this).css('display', 'block');
                                }
                                $(this).height('auto');
                            });
                        });
                        $('#vpd-content').find('div').width('auto');
                    });
                }
            });
        }

    });

});