/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */
var priceHashId = 0;
require(['config'], function (config) {
    require(['util', 'jquery', 'Spinner', 'Cart', 'Slider', 'Tiping'], function (util, $, Spinner, Cart, Slider, Tiping) {
        $('#list-loading').show();
        check_cart();
        initCheck();
        ajaxSyncCart();
        var cat = $('#cat').val();
        var storesec=$(".secstores");
        plus_click();
        cart_list_show();
        select_pro();
        empty_cart();
        prolist_scroll();
        fnLoadCatlist(cat);
        mask_height();


        function initCheck(){
            var total = window.localStorage.getItem("total");
            var yun = window.localStorage.getItem("yun");
            if(total != null){

                if(yun == 0){
                    yun = "0.00";
                }
                if(total == 0){
                    total = "0.00";
                }
                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);
            }

            var totalCount = window.localStorage.getItem("totalCount");
            if(totalCount == 0){
                $(".cart-list").removeClass('active');

            }

        }




        function mask_height() {
            var mask_top_h = parseInt($(".top-mask").height());
            $(".header-time").css({
                'line-height': mask_top_h + 'px'
            });
            $(".header-time").css({
                'height': mask_top_h + 'px'
            });
            $(".header-empty").css({'height': mask_top_h + 'px'});
            $(".mid-mask").css('top', mask_top_h);
            $(".product-body").click(function () {
                $(".mid-mask").hide();
                $(".store-mask").hide();
                $(".top-mask").hide();
            })
        }



        function select_pro() {
            $(".toptab ul li").click(function () {
                var catId = $(this).find('.tab').attr('data-catid');
                cat = catId;
                $("#cat").attr("value",cat);
                fnLoadCatlist(catId);
                $(".toptab ul li .tab.active").removeClass('active');
                $(this).find('.tab').addClass('active');
                $(window).scrollTop(0);
            });
        }

        function prolist_scroll() {
            var left_height = $(window).height();
            $(".left-cat").css('height', left_height + 'px');
            $(".right-pro").css('height', left_height + 'px');
        }

        function empty_cart() {
            $(".header-right").click(function () {
                layer.open({
                    content: '确认清空购物车？',
                    btn: ['确认', '取消'],
                    shadeClose: false,
                    yes: function () {

                        $.post(shoproot + '?/Cart/ajaxDelCart/', function (Res) {
                            //$(".cart-list ul").remove();
                            Cart.clear();
                            $(".number").html(Cart.count());
                            $(".num").html("0");
                            $(".hidden").hide();
                            $('.cat-num').hide();
                            $("#cart_list").html("");
                            $(".cart-list").hide();
                            $(".cart-price").html("￥0.00");
                            layer.closeAll();
                            window.localStorage.setItem("total","0.00");
                            window.localStorage.setItem("yun","0");
                            $(".pro_price ").html( "￥0.00");
                            $(".freight b").html("￥0.00");
                            $(".addcart").show()
                            $(".cart-list").removeClass('active');
                            $(".body-footer.cartlist").hide();
                            $(".body-footer.empty").show();
                        });
                    }
                });

            });
        }

        function cart_list_show() {
            $(".cart-list img").click(function () {
                $(".cart-list").hide();
                fnLoadCatlist(cat);
            });
            $(".shopping-cart").click(function () {
                var count = window.localStorage.getItem("totalCount");
                if ($(".cart-list").hasClass('active')) {
                    $(".cart-list").hide();
                    $(".cart-list").removeClass('active');
                    fnLoadCatlist(cat);
                } else if (count > 0) {
                    $(".cart-list").show();
                    $(".cart-list").addClass('active');
                    $.post(shoproot + '?/Index/ajaxGetCartProducts/', clearResult);
                    ajaxSyncCart();
                }
            })
        }



        function ajaxSyncCart(){

            $.post(shoproot + '?/Cart/ajaxSyncCart/', syncResult);

        }

        function syncResult(data){

            var totalCount = data.totalCount;
            var total = data.total;
            var yun = data.yun;

            window.localStorage.setItem("totalCount",totalCount);


            if(total > 0){

                window.localStorage.setItem("total",total.toFixed(2));
                if(yun > 0){
                    window.localStorage.setItem("yun",yun.toFixed(2));
                }else{
                    window.localStorage.setItem("yun","0.00");
                    yun = "0.00";
                }

            }else{
                window.localStorage.setItem("total","0.00");
                window.localStorage.setItem("yun","0.00");
                total = "0.00";
                yun = "0.00";
            }
            check_cart();
            $(".pro_price ").html("&yen;" + total);
            $(".freight b").html("&yen;" + yun);
        }

        function clearResult(data) {
            $('#cart_list').html(data);

            cart_proname_wd();
            cart_plus_click();
            cart_minus_click();
        }

        // $(".number").html(Cart.count());
        function cart_proname_wd() {
            var win_wd = $(window).width();
            var win_hg = $(window).height();
            $(".title-pro").css({
                'max-width': win_wd - 175
            });
            $(".cart-list ul").css({
                'max-height': win_hg / 2
            });
        }
        function plus_click() {
            $(".icon-plus").click(function () {
                var hash = $(this).parents('li').find(".proInfor").attr('data-hash');
                var numNode = $(this).parents('li').find('.num');
                var num = parseInt($(this).parents('li').find('.num').html());
                var stock = parseInt($(this).parents('li').attr('data-stock'));

                var pid =  $(this).parents('li').find(".proInfor").attr('data-p');
                var spid =  $(this).parents('li').find(".proInfor").attr('data-sp');

                if ((num + 1) > stock) {
                    layer.open({
                        content: '无法选择更多',
                        time: 3 //3秒后自动关闭
                    });
                    return false;
                }

                num = num+1;
                ajax_do_product_cart(0,$(this),$(this).parents('li'),pid,spid,num);

                //if(num==0){
                //$(".body-footer.cartlist").show();
                //  $(".body-footer.empty").hide();
                //}
                //$(this).parents(".add_pro").find(".hidden").show();
                // Cart.set(hash, parseInt(numNode.text()) + 1, doCallback);
                // numNode.html(parseInt(numNode.text()) + 1);
            });

            $(".addcart").click(function(){
                var hash = $(this).parents('li').find(".proInfor").attr('data-hash');
                var numNode = $(this).parents('li').find('.num');
                var num = parseInt($(this).parents('li').find('.num').html());
                var stock = parseInt($(this).parents('li').attr('data-stock'));

                var pid =  $(this).parents('li').find(".proInfor").attr('data-p');
                var spid =  $(this).parents('li').find(".proInfor").attr('data-sp');
                num = num+1;
                ajax_do_product_cart(0,$(this),$(this).parents('li'),pid,spid,num);
                
            });

        }
        function ajax_do_product_cart(type,parent,node,pid,spid,count){
            var domain = $('#domain').val();
            var url = domain + '?/Cart/ajaxDoCartProduct/'
            $.post(url, {
                pid: pid,
                spid: spid,
                pcount:count
            }, function(result){

                var code =  result.ret_code;
                var data = result.ret_msg;
                if(code == -1){

                    parent.parents(".add_pro").find(".hidden").hide();
                    parent.hide()
                    // node.find(".hidden").show();
                    node.find('.num').html("0");
                    //产品已下架
                    layer.open({
                        content: '产品已下架',
                        time: 3 //3秒后自动关闭
                    });


                }else if(code == -2){
                    parent.parents(".add_pro").find(".hidden").hide();
                    parent.hide()
                    // node.find(".hidden").show();
                    node.find('.num').html("0");
                    //产品已下架
                    layer.open({
                        content: '产品秒杀已结束',
                        time: 3 //3秒后自动关闭
                    });
                }

                else if(code == 1){
                    //产品为0已移除
                    parent.parents(".add_pro").find(".hidden").hide();
                    parent.parents(".add_pro").find(".addcart").show()
                    node.find('.num').html(count+"");

                }else if(code == 2){
                    //产品已经成功处理
                    node.find(".add_pro .hidden").show();
                    node.find(".add_pro .addcart").hide()
                    node.find('.num').html(count+"");

                }

                var totalCount = data.totalCount;
                var total = data.total;
                var yun = data.yun;
                window.localStorage.setItem("totalCount",totalCount);

                if(total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }

                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                check_cart();

                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);

            });
        }




        function minus_click() {
            $(".icon-minus").click(function () {
                var hash = $(this).parents('li').find(".proInfor").attr('data-hash');
                var numNode = $(this).parents('li').find('.num');
                var p = $(this).parents('li').find(".proInfor").attr('data-p');
                var sp = $(this).parents('li').find(".proInfor").attr('data-sp');
                var num = parseInt(numNode.text()) - 1;

                ajax_do_product_cart(0, $(this),$(this).parents('li'),p,sp,num);

            })
        }

        function doCallback(data) {
            if (data != "") {
                // alert(JSON.stringify(data));
                //val total = data.total;
                //alert(data.total);
                var total = data.total;
                var yun = data.totalYun;
                if(data.total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }

                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);

                if (data.topCats != "") {
                    var cat = data.topCats;
                    for (var i = 0; i < cat.length; i++) {
                        $(".left-cat li").each(function () {
                            var v = $(this).find('.cat-btn').attr('data-catid');
                            if (cat[i].cat_id == v) {
                                if (typeof(cat[i].count) != 'undefined') {
                                    $(this).find('.cat-num').html(cat[i].count);
                                    $(this).find(".cat-num").show();
                                } else {
                                    $(this).find('.cat-num').hide();
                                }
                            }
                        });
                    }
                }
                $(".number").html(Cart.count());
            }
        }

        // 购买
        $('#buy').click(function () {
            $.post(shoproot + "?/Cart/add_product_to_cart", {
                cartData: window.localStorage.getItem('cart')
            }, function result(data) {
                window.location.href = shoproot + "?/Cart/index_order";
            });


        });

        function cart_plus_click() {
            $('.prolist #cart_plus').click(function () {
                var clicked_li = $(this).closest('li');
                var p = clicked_li.find(".pro-buy-num").attr('data-p');
                var sp = clicked_li.find(".pro-buy-num").attr('data-sp');
                var hash = clicked_li.find(".pro-buy-num").attr('data-hash');
                var numNode = clicked_li.find('.num');
                var num = parseInt(clicked_li.find('.num').html());
                var stock = parseInt(clicked_li.attr('data-stock'));
                if ((num + 1) > stock) {
                    layer.open({
                        content: '无法选择更多',
                        time: 3 //3秒后自动关闭
                    });
                    return false;
                }
                num = num+1;
                ajax_do_cart_product_cart($(this),$(this).parents('li'),p,sp,num);

            });
        }

        function ajax_do_cart_product_cart(parent,node,pid,spid,count){
            var domain = $('#domain').val();
            var url = domain + '?/Cart/ajaxDoCartProduct/'
            $.post(url, {
                pid: pid,
                spid: spid,
                pcount:count
            }, function(result){

                var code =  result.ret_code;
                var data = result.ret_msg;
                if(code == -1){
                    //产品已下架
                    // node.find(".hidden").show();
                    layer.open({
                        content: '产品已经下架',
                        time: 3 //3秒后自动关闭
                    });
                    node.remove();
                    // parent.parents(".add_pro").find(".hidden").hide()
                    // parent.hide();

                } else if(code == -2){

                    layer.open({
                        content: '产品秒杀已结束',
                        time: 3 //3秒后自动关闭
                    });
                    node.remove();
                }else if(code == 1){
                    //产品为0已移除
                    node.remove();

                }else if(code == 2){
                    //产品已经成功处理

                }

                if(code == 1 || code == 2){
                    $(".pro_list ul li").each(function (i) {
                        var pro_p = $(".pro_list ul li").eq(i).find(".proInfor").attr('data-p');
                        var plus_num = $(".pro_list ul li").eq(i).find(".num");
                        var p = node.find(".pro-buy-num").attr('data-p')
                        if (pro_p == p) {
                            plus_num.html(count);
                        }

                        if(count == 0){
                            if($(".pro_list ul li").eq(i).find(".proInfor").attr("data-p")==p){
                                $(".pro_list ul li").eq(i).find('.add_pro .hidden').hide();
                                $(".pro_list ul li").eq(i).find('.add_pro .addcart').show();
                            }
                        }

                    });
                    node.find('.num').html(count+"");
                }

                if(code == -1){
                    $(".pro_list ul li").each(function (i) {
                        var pro_p = $(".pro_list ul li").eq(i).find(".proInfor").attr('data-p');
                        var plus_num = $(".pro_list ul li").eq(i).find(".num");
                        var p = node.find(".pro-buy-num").attr('data-p')
                        if (pro_p == p) {
                            plus_num.html(count);
                        }


                        $(".pro_list ul li").eq(i).find('.add_pro').hide();


                    });
                }


                var totalCount = data.totalCount;
                var total = data.total;
                var yun = data.yun;
                window.localStorage.setItem("totalCount",totalCount);

                if(total > 0){

                    window.localStorage.setItem("total",total.toFixed(2));
                    if(yun > 0){
                        window.localStorage.setItem("yun",yun.toFixed(2));
                    }else{
                        window.localStorage.setItem("yun","0.00");
                        yun = "0.00";
                    }

                }else{
                    window.localStorage.setItem("total","0.00");
                    window.localStorage.setItem("yun","0.00");
                    total = "0.00";
                    yun = "0.00";
                }
                check_cart();
                if(totalCount == 0){
                   $(".addcart").show();
                    $(".cart-list").removeClass('active');
                    $(".body-footer.cartlist").hide();
                    $(".body-footer.empty").show();
                    $("#cart_list").html("");
                    $(".cart-list").hide();
                }   

                $(".pro_price ").html("&yen;" + total);
                $(".freight b").html("&yen;" + yun);

            });
        }

        function cart_minus_click() {
            $('.prolist #cart_minus').click(function () {
                var hash = $(this).parents('li').find(".pro-buy-num").attr('data-hash');
                var numNode = $(this).parents('li').find('.num');
                var p = $(this).parents('li').find(".pro-buy-num").attr('data-p');
                var sp = $(this).parents('li').find(".pro-buy-num").attr('data-sp');
                var num = parseInt(numNode.text()) - 1;
                ajax_do_cart_product_cart($(this),$(this).parents('li'),p,sp,num);
            });
        }


        function check_cart(){
            var count = window.localStorage.getItem("totalCount");
            if(count==0){
                $(".body-footer.empty").show();
                $(".body-footer.cartlist").hide();
                $(".cart-list").removeClass('active');

            }
            else{
                $(".body-footer.empty").hide();
                $(".body-footer.cartlist").show();
            }
        }



        function fnLoadCatlist(cat) {
            var domain = $('#domain').val();
            var url = domain+ '?/Index/ajax_list_item/id=' + cat;
            $('#rightContainer').load(url, function () {
                plus_click();
                minus_click();
                echo.init({
                    offset: 100,
                    unload: false

                });
                $(".pro_list ul .miaosha").each(function (i) {
                    var time = $(".pro_list ul .miaosha").eq(i).find(".proInfor").attr('data-time');
                    //refreshTime(time,$(".pro_list ul li").eq(i));
                    //setInterval(refreshTime(time,$(".pro_list ul li").eq(i)),1000);
                    var a= setInterval(function(){
                        var endtime=new Date(time);
                        var nowtime = new Date();
                        var leftsecond=parseInt((endtime.getTime()-nowtime.getTime())/1000);
                        d=parseInt(leftsecond/3600/24);
                        h=parseInt((leftsecond/3600)%24);
                        m=parseInt((leftsecond/60)%60);
                        s=parseInt(leftsecond%60);
                        if(h < 10){

                            if(d > 0){
                                h = 24*d +h;
                            }else{
                                h = "0"+h;
                            }

                        }else{
                            if(d > 0){
                                h = 24*d +h;
                            }
                        }
                        if(m<10){
                            m = "0"+m;
                        }
                        if(s<10){
                            s = "0"+s;
                        }
                        if(leftsecond>0){
                            // var countdown = $(".pro_list ul .miaosha").eq(i).find('.time_end').html("<b>"+h+"</b>:<b>"+m+"</b>:<b>"+s+"</b>");
                            var mstime=$(".ms_title .mstime").find('.time_end').html("<b>"+h+"</b>:<b>"+m+"</b>:<b>"+s+"</b>");
                        }else{
                            // var countdown = $(".pro_list ul .miaosha").eq(i).find('.timeend').html("秒杀已结束");
                            var mstime=$(".ms_title .mstime").find('.timeend').html("秒杀已结束");
                            $(".pro_list ul .miaosha").eq(i).find(".add_pro ").hide();
                        }

                    }, 1000);

                });

            });
        }
    });
});
