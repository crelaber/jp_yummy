
require([ 'config' ], function(config) {

    require([ 'util', 'jquery', 'Spinner', 'Cart' ], function(util, $, Spinner,
                                                              Cart) {


        $('#send').click(function(){

            var orderId = $('#orderId').val();
            var providerId = $('#providerId').val();
            var expressId = $('#express').val();
            var num = $('#num').val();

            var url = "?/Order/sendExpress";
            var resultUrl = $('#url').val();
            layer.open({
                content: '确认发货？',
                btn: ['确认', '取消'],
                shadeClose: false,
                yes: function(){
                    layer.closeAll();
                    $.post(url,{orderId:orderId,providerId:providerId,expressId:expressId,code:num},function(data){

                        if(data == -1){
                            alert('请填写快递单号');
                        }else{
                            window.location.href=resultUrl;
                        }
                    });
                },
                no: function(){
                    layer.closeAll();
                }

            });

        });


        var isbalance= 0;
        var b = $('#isbalance').val();
        if(b != ''){
            isbalance = b;

        }
        $('#back').click(function() {

            var url = "?/Index/index";
            window.location.href = url;
        });

        $('#discount_money').click(function(){

            var deliver_time = $('#time').val();
            var couponId = $('#userCouponId').val();
            var url = "?/Coupon/coupon_list/couponId="+couponId+"&time="+deliver_time+"&isbalance="+isbalance;
            window.location.href = url;
        });

        var orderId;
        var isPaying = false;
        var isTuan = 0;
        var groupId = "";
        $('#update-address').click(function() {
            var addressId = $('#addressId').val();
            var deliver_time = $('#time').val();
            var couponId = $('#userCouponId').val();
            isTuan =  $('#isTuan').val();
            groupId = $('#groupId').val();
            var pid =  $('#pid').val();

            var url = shoproot + "?/UserAddress/list_address/couponId="+couponId+"&time="+deliver_time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;

            if(addressId == ''){

                url =  shoproot + "?/UserAddress/edit_address/couponId="+couponId+"&time="+deliver_time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
            }
            window.location.href = url;

        });

        function payOrder() {
            if(isPaying){
                return;
            }


            var addressId = $('#addressId').val();

            if (addressId == '') {
                layer.open({
                    content: '请选择要派送的地址！',
                    time: 3 //3秒后自动关闭
                });
                return;
            }

            // check deliver time
            var deliver_time = '尽快';


            var reciTex = '';
            var rcHead = '';
            var couponId = $('#orderCouponId').val()+","+$('#userCouponId').val();
            var user_note = $('#user_note').val();
            var fromUid = $('#fromUid').val();
            var pid =  $('#pid').val();
            isTuan =  $('#isTuan').val();
            groupId = $('#groupId').val();

            var url = shoproot + "?/Order/createOrder";
            isPaying = true;
            $.post(url, {
                cartData : "",
                addrData : addressId,
                reciHead : rcHead,
                reciTex : reciTex,
                time : deliver_time,
                coupon : couponId,
                isbalance:	isbalance,
                user_note : user_note,
                from_uid:fromUid,
                isTuan:isTuan,
                pid:pid,
                groupId:groupId

            }, orderGenhandleReq);
        }

        function orderGenhandleReq(id) {

            if(id.ret_code < 0){
                layer.open({
                    content:id.ret_msg,
                    time: 3 //3秒后自动关闭
                });

                isPaying = false;
                return;
            }
            if(id.ret_code == 2){
                isPaying = false;
                layer.open({
                            content: '海外商品需要填写身份证信息哦！',
                            btn: ['确认', '取消'],
                            shadeClose: false,
                            yes: function(){
                                var url = shoproot+"?/UserAddress/update_address/id="+id.ret_msg;
                                window.location.href=url;
                            },
                            no: function(){
                                layer.closeAll();
                                }

                            });
                return;
            }


            // alert(JSON.stringify(id));
            orderId = parseInt(id.ret_msg);

            window.localStorage.setItem("total","0.00");
            window.localStorage.setItem("yun","0.00");
            window.localStorage.setItem("totalCount","0")

            if (orderId > 0) {
                $.post(shoproot + "?/Order/ajaxOrderPay/", {
                    orderId : orderId
                }, function(bizPackage) {
                    isPaying = false;

                    var state = bizPackage.ret_code;
                    var msg = bizPackage.ret_msg;


                    if (state == 1){
                        // 全额支付
                        if(isTuan == 1){
                            history.replaceState(null, "CheersLife 下午茶", "?/Group/group_list");

                        }else{
                            history.replaceState(null, "CheersLife 下午茶", "?/Index/index");
                        }
                        var now = new Date();

                        if(isTuan == 1){
                            window.location.href = shoproot + '?/Group/grouping/order_id=' + orderId+"&id="+groupId;
                        }else{
                            // location.href = '?/Uc/orderlist/';
                            location.href = '?/Order/scan_code';
                        }

                    }
                    else if(state == -1)
                    {
                        layer.open({
                            content:msg,
                            time: 3 //3秒后自动关闭
                        });
                    } else {
                        // alert(JSON.stringify(bizPackage));
                        // 订单映射
                        msg.success = wepayCallback;
                        msg.cancel = wepayCancelCallback;
                        // 发起微信支付
                        wx.chooseWXPay(msg);

                    }

                });

            } else {
                layer.open({
                    content:'订单无效或者已过期',
                    time: 3 //3秒后自动关闭
                });
                isPaying = false;
            }
        }
        /**
         * 微信支付回调
         *
         * @param {type}
         *            res
         * @returns {undefined}
         */
        function wepayCallback(res) {
            if(isTuan == 1){
                history.replaceState(null, "CheersLife 下午茶", "?/Group/group_list");

            }else{
                history.replaceState(null, "CheersLife 下午茶", "?/Index/index");
            }
            window.payed = true;

            if(isTuan == 1){
                window.location.href = shoproot + '?/Group/grouping/order_id=' + orderId+"&id="+groupId;

            }else{
                // window.location.href = shoproot + '?/Order/expressDetail/?order_id=' + orderId;
                window.location.href = shoproot + '?/Order/scan_code';
                // window.location.href = shoproot + '?/Order/expressDetail/?order_id=' + orderId;
            }

            $('#wechat-payment-btn').removeClass('disable').html('微信安全支付');
        }

        function wepayCancelCallback(res) {
            if(isTuan == 1){
                history.replaceState(null, "CheersLife 下午茶", "?/Group/group_list");

            }else{
                history.replaceState(null, "CheersLife 下午茶", "?/Index/index");
            }
            window.location.href = shoproot + '?/Order/expressDetail/?order_id=' + orderId;
        }

        $('#pay').click(payOrder);

        function balance_change() {
            $(".yes-no").click(
                function() {
                    var yes = $(".yes-no img").attr("src").indexOf("yes");
                    if (yes > 0) {
                        var replace_yes = $(".yes-no img").attr("src")
                            .replace('yes', 'no');
                        $(".yes-no img").attr("src", replace_yes);
                        isbalance = 0;
                    } else if (yes <= 0) {
                        var replace_no = $(".yes-no img").attr("src")
                            .replace('no', 'yes');
                        $(".yes-no img").attr("src", replace_no);

                        isbalance = 1;
                    }
                })
        }

        function order_title_wd(){
            var web_win_wd=$(window).width();
            order_name_wd=web_win_wd-103;
            $(".pro-name").css({'max-width':order_name_wd});
        }
        function express_title_wd(){
            var web_win_wd=$(window).width();
            order_name_wd=web_win_wd-103;
            $(".order-pro-list p .title-pro").css({'max-width':order_name_wd});
        }

        var time_data;
        $(document).ready(function() {

            balance_change();
            order_title_wd();
            express_title_wd();

            // FastClick.attach(document.body);
            var selectArea = new MobileSelectArea();
            selectArea.init({trigger:'#time',value:$('#hd_time').val(),data:'deliverTime.php',level:2});
        });


    });

});

