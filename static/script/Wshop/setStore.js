/**
 * Desc
 *
 * @description Holp You Do Good But Not Evil
 * @copyright Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author Chenyong Cai <ycchen@iwshop.cn>
 * @package Wshop
 * @link http://www.iwshop.cn
 */

require(['config'], function (config) {
    require(['util', 'jquery', ], function (util, $) {
		select_stores();
		
		function select_stores(){
            $(".store-item").click(function(){
                $(this).find(".select-btn").addClass('active');
                var store_name = $(this).attr('data-name');
                var store_id = $(this).attr('data-val');
                // reload stock info of given date
                $.get('?/Index/setDeliverStore/id=' + store_id, function (data) {
                    var result = $.parseJSON(data);
                    if (result.err == 0) {
                      window.location.href="?/Index/";
                    }
                });
            });
        }
    });
});
