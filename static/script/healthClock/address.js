/**
 * Desc
 */

$(function () {
    var shoproot = '/';
    var prizeId = $('#prize_id').val();
    var orderId = $('#order_id').val();

    //选择地址
    $(".address-list ul li").click(function() {
        var couponId = $('#couponId').val();
        var time = $('#time').val();
        var isbalance =$('#isbalance').val();

        var value = $(this).find('.address-detail').attr('data-id');
        var url = shoproot + "?/vHealthClock/updateEnableAddress";
        $.post(url, {
            id: value
        }, function(data) {
        });


        if ($(this).find('.select-btn').hasClass('active')) {} else {
            $(".address-list ul li .select-btn").removeClass('active');
            $(this).find('.select-btn').addClass('active');
        }
        if($(".delete-btn").is(":hidden")) {
            var url = shoproot + "?/vHealthClock/exchangeOrder/order_id="+orderId+"&prize_id="+prizeId;
            window.location.href = url;
        }
    })

    //选择返回
    $('#select_back').click(function() {
        var pid  = $('#pid').val();
        var url = shoproot + "?/vHealthClock/exchangeOrder/order_id="+orderId+"&prize_id="+prizeId;
        window.location.href = url;
    });


    $('#back').click(function() {
        var pid  = $('#pid').val();
        var url = shoproot + "?/vHealthClock/exchangeOrder/order_id="+orderId+"&prize_id="+prizeId;
        window.location.href = url;

    });

    $('#update').click(function(){
        var provincetxt = $('#provincetxt option:selected').text();
        var city = $('#citytxt option:selected').text();
        var areatxt = $('#areatxt option:selected').text();
        var address = $('#fillarea').val();
        var name = $('#fillname').val();
        var phone = $('#fillphone').val();
        var fillid = $('#fillid').val();
        var id = $('#id').val();
        var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
        if(provincetxt=="请选择"||city=="请选择"||areatxt=="请选择"||address.trim()==""||name.trim()==""||phone.trim()==""){
            layer.open({
                content: '请完善地址信息！',
                time: 2
            });
            return;
        }
        if (!reg.test(phone)) {
            layer.open({
                content: '请填写正确的手机号！',
                time: 3 //3秒后自动关闭
            });
            return;
        }
        if(fillid.trim().length !=0 &&!isIdCard(fillid)){
            layer.open({
                content: '您填写的身份证有误！',
                time: 3 //3秒后自动关闭
            });
            return;
        }
        if(address.trim().length<5){
            layer.open({
                content: '详细地址不得少于5个字！',
                time: 3 //3秒后自动关闭
            });
            return;
        }


        var url = shoproot + "?/UserAddress/ajaxUpdateAddress";
        $.post(url, {
            id:id,
            user_name: name,
            phone: phone,
            city: city,
            area: areatxt,
            province:provincetxt,
            identity:fillid,
            address: address

        }, updateAddressResult);

    });



    //增加地址
    $('#add').click(function() {
        var url = shoproot + "?/vHealthClock/editAddress/order_id="+orderId+"&prize_id="+prizeId;
        window.location.href = url;
    });


    $('#submit').click(function() {
        var provincetxt = $('#provincetxt option:selected').text();
        var city = $('#citytxt option:selected').text();
        var areatxt = $('#areatxt option:selected').text();
        var address = $('#fillarea').val();
        var name = $('#fillname').val();
        var phone = $('#fillphone').val();
        var fillid = $('#fillid').val();


        var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
        if(provincetxt=="请选择"||city=="请选择"||areatxt=="请选择"||address.trim()==""||name.trim()==""||phone.trim()==""){
            layer.open({
                content: '请完善地址信息！',
                time: 2
            });
            return;
        }
        if (!reg.test(phone)) {
            layer.open({
                content: '请填写正确的手机号！',
                time: 3 //3秒后自动关闭
            });
            return;
        }
        if(address.trim().length<5){
            layer.open({
                content: '详细地址不得少于5个字！',
                time: 3 //3秒后自动关闭
            });
            return;
        }
        var url = shoproot + "?/vHealthClock/saveAddress";
        $.post(url, {
            user_name: name,
            phone: phone,
            city: city,
            area: areatxt,
            province:provincetxt,
            identity:fillid,
            address: address

        }, addAddressResult);
    });

    $(".delete-btn").click(function() {
        var value = $(this).parents('li').find('.address-detail').attr('data-id');
        layer.open({
            content: '确认删除地址？',
            btn: ['确认', '取消'],
            shadeClose: false,
            yes: function(){
                $.post('?/UserAddress/remove_address',{
                    id:value
                },function(data){
                    if(data.ret_code > 0){
                        $(this).parents("li").remove();
                    }
                    var couponId = $('#couponId').val();
                    var time = $('#time').val();
                    var isbalance =$('#isbalance').val();
                    var groupId = $('#groupId').val();
                    var isTuan = $('#isTuan').val();
                    var pid  = $('#pid').val();
                    var url = shoproot + "?/UserAddress/list_address/couponId="+couponId+"&time="+time+"&isbalance="+isbalance+"&isTuan="+isTuan+"&groupId="+groupId+"&pid="+pid;
                    window.location.href = url;
                });
            }
        });
    });

    $(".modify-add").click(function() {
        if ($(this).text() == "编辑") {
            $(".select-btn").hide();
            $(".delete-btn").show();
            $(this).html("完成")

        }else {
            $(".select-btn").show();
            $(".delete-btn").hide();
            $(this).html("编辑")
            var address_list = $(".address-list ul li").find(".select-btn");
            if (!address_list.hasClass('active')) {
                $(".address-list ul li:first").find(".select-btn").addClass('active');
            }
        }
    })

    function addAddressResult(data) {
        console.log(data);
        if (data.errcode != 200) {
            layer.open({
                content: data.msg,
                time: 3 //3秒后自动关闭
            });
            return;
        }
        var url = shoproot + "?/vHealthClock/selectAddress/order_id="+orderId+"&prize_id="+prizeId;
        window.location.href = url;
    }

    function updateAddressResult(data){
        var groupId = $('#groupId').val();
        var isTuan = $('#isTuan').val();
        var url = shoproot + "?/Cart/index_order/isTuan="+isTuan+"&groupId="+groupId;
        window.location.href = url;
    }

});
