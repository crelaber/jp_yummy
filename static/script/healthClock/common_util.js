/**
 * Created by Justfly on 2016/2/22.
 */

var CommonUtil = {
    getScoreResult : function(s){
        switch(s)
        {

            case 0:
                t = '我在NQ大挑战里得了0分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 1:
                t = '我在NQ大挑战里得了10分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 2:
                t = '我在NQ大挑战里得了20分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 3:
                t = '我在NQ大挑战里得了30分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 4:
                t = '我在NQ大挑战里得了40分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 5:
                t = '我在NQ大挑战里得了50分，营养智商很捉急，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 6:
                t = '我在NQ大挑战里得了60分！营养智商达标，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 7:
                t = '我在NQ大挑战里得了70分！营养天分突显，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 8:
                t = '我在NQ大挑战里得了80分！营养天分显著，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 9:
                t = '我在NQ大挑战里得了90分！营养天分极高，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;
            case 10:
                t = '我在NQ大挑战里得了100分！营养智商堪称完美，要不你也来试试~';
                d = '2015年“营养科学”流言榜权威发布，测一测你的营养智商有多高';
                break;

        }

    },
    getArrayItems : function(arr, num) {

        var temp_array = new Array();
        for (var index in arr) {
            temp_array.push(arr[index]);
        }

        var return_array = new Array();
        for (var i = 0; i<num; i++) {

            if (temp_array.length>0) {

                var arrIndex = Math.floor(Math.random()*temp_array.length);

                return_array[i] = temp_array[arrIndex];

                temp_array.splice(arrIndex, 1);
            } else {

                break;
            }
        }
        return return_array;
    },

    getUrlParams : function(a) {
        var b = location.search.slice(1), c = b.split("&"), d = 0, e = 0, f = /(^.+)=(.+)$/, g = {}, h = null;
        if (!h) {
            for (d = 0, e = c.length; e > d; d++)f.test(c[d]) && (g[c[d].match(f)[1]] = c[d].match(f)[2]);
            h = g
        }
        return "string" == typeof a ? h[a] : h
    },

    changeOpenLink : function(a){
        $(a).each(function (a, b) {
            var c = $(b), d = c.data("link");
            c.removeAttr("schema"), d && c.attr("href", d)
        })
    },
    getDesc : function (score){
        var key = 'SCORE_'+score;
        var descArr = DESC_LIB_DATA[key];
        return this.getRandomResultText(descArr);
    },

    getRandomResultText : function(textArr){
        var len = textArr.length;
        if(len==1){
            return textArr[0];
        }
        var randomNum = Math.floor(Math.random()*len);
        return textArr[randomNum];
    },

    link_to_qrcode:function () {
        $('#qr_code').click(function () {
            var url = BASE_URL + "qr_code.html";
            window.location.href = url;
        });
    },

    set_user_info : function (){
        if(UserUtil.getCurrentUser()==null){
            var uid = $('#session_user_id').val();
            var user_name = $('#session_user_name').val();
            var avatar = $('#session_user_avatar').val();

            var user = {
                'uid':uid,
                'user_name':user_name,
                'avatar':avatar
            }
            UserUtil.setCurrentUser(user)
        }
    },

    get_pic_lib_share_score_title:function(score){
        var title = '';
        switch (score){
            case 100 :
                title = '我简直就是猜蔬菜界的小诸葛，不服来战？！';
                break;
            case 90 :
                title = '差一点满分，我猜错了哪个蔬菜呢...你也来试试吧？';
                break;
            case 80 :
                title = '看来我还要多去菜场啊...你敢来挑战么？';
                break;
            case 70 :
                title = '不错不错，蔬菜我认的差不多了~你想来试试么？';
                break;
            case 60 :
                title = '好险，总算及格了，去菜场不会被老板蒙啦~~~你也来试试吧？';
                break;
            case 50 :
                title = '羞愧中...猜错一半蔬菜，我要重新做一次，你想来试试么？';
                break;
            case 40 :
                title = '我真的尽力了，但我真的不认识几个蔬菜啊…你要来试试不？';
                break;
            case 30 :
                title = '耻辱啊，常去菜场常被蒙，你敢来挑战下不？';
                break;
            case 20 :
                title = '这是真的么？！这些蔬菜真难猜，呜呜呜...，你也来试试吧…';
                break;
            case 10 :
                title = '呃...我真的...去过菜场么？你敢来试试不？';
                break;
            case 0 :
                title = '我真的不是地球人！我真的不认识地球上的蔬菜！你来试试？';
                break;
            default :
                title = '我真的不是地球人！我真的不认识地球上的蔬菜！你来试试？';
                break;
        }
        return title;

    }


}

