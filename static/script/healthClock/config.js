var ENVIRONMENT = 'produce';

var BASE_URL = 'http://cheerslife.dev.com';


var shoproot = BASE_URL;
var RANK_PAGE_SIZE = 10;

/**
 * 构建url
 * */
function build_get_url(path, params){
    var url = BASE_URL+path;
    var url = BASE_URL+path;

    if(params){
        url = url + "?";
        for(var key in params){
            url = url+ key+"="+params[key]+'&';
        }
    }
    var last = url.substr(url.length-1);
    if(last == '&'){
        url = url.substr(0,url.length-1);
    }
    return url;
}


function parse_url_params(url){
    var purl = url;
    var rtn_arr = [];
    var url_arr = [];
    var result = {};
    var param_str;
    if(url.indexOf('?')>0){
        url_arr = url.split('?');
        purl = url_arr[0];
        param_str = url_arr[1];
        var kvArr;
        if(param_str.indexOf('&') > 0){
            var params = param_str.split('&');

            var kv;
            for(var i= 0,len = params.length;i<len ;i++){
                kvArr = params[i].split('=');
                kv = {
                    'key' : kvArr[0],
                    'value' : kvArr[1]
                }
                rtn_arr.push(kv);
            }
        }else{
            kvArr = param_str.split('=');
            kv = {
                'key' :kvArr[0],
                'value' : kvArr[1]
            }
            rtn_arr.push(kv);
        }
    }
    result = {
        'url' : purl,
        'params' : rtn_arr
    }
    return result;
}


function get_url_param_value(input_key ,params){
    if(params.length <= 0){
        return '';
    }
    var vallue = '';
    for(var i= 0,len=params.length;i<len;i++){
        if(input_key == params[i]['key']){
            vallue =  params[i]['value'];
            break;
        }
    }

    return vallue;

}

