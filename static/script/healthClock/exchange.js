$(function () {
    initBanner();
    ajaxGetContent();
    $('.cartlist').click(function(){
        var prizeId = $('#prize_id').val();
        window.location.href = '/?/vHealthClock/exchangeOrder/prize_id='+prizeId;
    });


    /**
     * 初始化banner
     */
    function initBanner() {
        var showImg = $('#show_img').val();
        if(showImg == 1){
            var bullets = document.getElementById('position').getElementsByTagName('li');
            var banner = Swipe(document.getElementById('mySwipe'), {
                auto: 3000,
                continuous: true,
                disableScroll:false,
                callback: function(pos) {
                    var i = bullets.length;
                    while (i--) {
                        bullets[i].className = ' ';
                    }
                    bullets[pos].className = 'cur';
                }
            });
        }
    }

    function ajaxGetContent() {
        $('#vpd-content').html('');
        $.ajax({
            url: '/html/zy_prize/' + $('#prize_id').val() + '.html',
            success: function(data) {
                $('#vpd-content').html(data);
                contentLoaded = true;
                $('#vpd-detail-header').show();
                $('.notload').removeClass('notload');
                $('#vpd-content').fadeIn();
                // 调整图片
                $('#vpd-content img').each(function() {
                    $(this).on('load', function() {
                        if ($(this).width() >= document.body.clientWidth) {
                            $(this).css('display', 'block');
                        }
                        $(this).height('auto');
                    });
                });
                $('#vpd-content').find('div').width('auto');
            },
            error: function() {
                $('#vpd-content').load('?/vHealthClock/ajaxGetPrizeNote/id=' + $('#prize_id').val(), function() {
                    contentLoaded = true;
                    $('#vpd-detail-header').show();
                    $('.notload').removeClass('notload');
                    $('#vpd-content').fadeIn();
                    // 调整图片
                    $('#vpd-content img').each(function() {
                        $(this).on('load', function() {

                            if ($(this).width() >= document.body.clientWidth) {
                                $(this).css('display', 'block');
                            }
                            $(this).height('auto');
                        });
                    });
                    $('#vpd-content').find('div').width('auto');
                });
            }
        });
    }
});
