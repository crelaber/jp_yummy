$(function () {
    var orderId = $('#order_id').val();
    var addressId = $('#address_id').val();
    var prizeId =  $('#prize_id').val();
    $('#update-address').click(function() {
        var url = "/?/vHealthClock/selectAddress/prize_id="+prizeId+"&order_id="+orderId;
        if(addressId == ''){
            url =  "/?/vHealthClock/editAddress/prize_id="+prizeId+"&order_id="+orderId;
        }
        window.location.href = url;
    });


    /**
     * 提交订单
     * */
    $('#submit').click(function () {
        $(this).attr('disabled','disabled');
        var buttonName = $(this).text();
        $(this).text('订单提交中，请稍后.....')
        var that = $(this);
        var url = '/?/vHealthClock/submitExchange'
        var param = {
            'order_id' : orderId,
            'address_id' : addressId,
            'prize_id' : prizeId,
        };
        $.post(url,param,function(result){
             that.removeAttr('disabled');
            $(this).text(buttonName)
            var errcode = result.errcode;
            console.log('result===>'+result);
            var redirectUrl = errcode == 200 ? '/?/vHealthClock/exchangeSuccess' : '/?/vHealthClock/exchangeFail'
            window.location.href = redirectUrl;
        });
    });
});




