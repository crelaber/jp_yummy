var indexPageLink = BASE_URL + 'index.php?platform=';
var rankPageLink = BASE_URL + 'rank_redirect.php?platform=';
var DEFAULT_COVER =  BASE_URL+'challenge_files/image/share_cover.jpg';

var PAGE_LINK = {

    'link' : indexPageLink,
    'qiezi' : {
        'index' : indexPageLink + 'qiezi',
        'rank' : rankPageLink + 'qiezi'
    }
}


var SHARE_DATA = {
    'qiezi':{
        'home_page_title': '懂不懂营养，来挑战过才知道！',
        'home_page_desc': '快来参加“茄子营养师”营养智商大挑战！',
        'home_page_link': PAGE_LINK.qiezi.index,
        'home_page_cover' :DEFAULT_COVER,
        //结果页分享的文案
        'result_page_title' : '我在营养智商大挑战中得了[x]分，耗时[z]，排名第[y]，你敢来试试么？',
        'result_page_desc' : '懂不懂营养，来测测就知道，快来参加“茄子营养师”营养智商大挑战',
        'result_page_link' : PAGE_LINK.qiezi.index,
        'result_page_cover' : DEFAULT_COVER,

        //排行榜分享的文案
        'rank_page_title': '我在营养智商大挑战中最高分[x]分，排名第[y]，你敢来试试么？',
        'rank_page_desc' : '懂不懂营养，来测测就知道，快来参加“茄子营养师”营养智商大挑战',
        'rank_page_link' : PAGE_LINK.qiezi.rank,
        'rank_page_cover' :DEFAULT_COVER
    }

}
