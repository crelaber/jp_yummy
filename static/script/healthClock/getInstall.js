/**
 * WEB 内调起 APP
 *
 * @ depend on jQuery | Zepto
 *
 * HTML
 * <!-- <a href="" schema="" class="">open</a> -->
 *
 * date: 2014.12.17
 * author: jianjian
 *
 * V1.2
 * 1. add button
 */


(function(root, factory){
	'use strict';
	if(typeof define === 'function' && define.amd){
		// AMD environment
		define('getInstall',[],function(){
			return factory(root, document);
		});
	}else if(typeof exports === 'object'){
		// CommonJS environment
		module.exports = factory(root, document);
	}else{
		// Browser environment
		root.getInstall = factory(root, document);
	}
})(window, function(w, d){
	'use strict';

	// 应用白名单
	var WhiteList = ['drugs','medtime','aspirin','idxyer','keflex'];

	var browser={
		versions:function(){
			var u = navigator.userAgent, app = navigator.appVersion;
			return {
				ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
				android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
				weixin: u.indexOf('MicroMessenger') > -1
			};
		}()
	};
	var android={
		types:function(){
			var u = navigator.userAgent;
			return {
				uc: u.indexOf('UCBrowser') > -1,
				xiaomi: u.indexOf('MiuiBrowser') > -1,
				firefox: u.indexOf('Firefox') > -1
			};
		}()
	};

	var PACKAGENAME, IOSLink, webLink, ardWebSite;

	var getInstall = function(packageName, config){
		if( WhiteList.indexOf(packageName) === -1 ){
			console.log('抱歉，我们暂时不支持该应用的调用');
			return false;
		}
		this.packageName = packageName;

		this.config = $.extend({
			btnEl: $('.J-dxy-open-app'),
			unInstallTip: '您的设备未安装该应用，是否马上安装？',
			isWebDownload: false,
			androidLink: false,
			IOSLink: false,
			checkInstall: false,
			installedTip: '启动应用'
		}, config);

		this.getVarName(packageName);

		this.bindUI();
	};
	getInstall.prototype = {
		bindUI: function(){
			var _self = this,
				el = this.config.btnEl;
			if(el.length){
				el.bind('click', function(e){
					var schema = $(this).attr('schema') || '',
						et = $(this),
						id = '',
						isForm = ($(this).get(0).nodeName === 'BUTTON');  // 如果是 button 则默认为 表单
					if( schema !== '' ){
						id = schema.split('://')[1];
						_self.getVarName(_self.packageName, id);
					}

					var bv = browser.versions;
					// 微信浏览器
					if( bv.weixin ){
						WeixinJSBridge.invoke("getInstallState",{
							"packageUrl": schema,
							"packageName": PACKAGENAME
						},function(res){
							// 未安装
							if( res.err_msg == 'get_install_state:no' ){
								if(bv.ios){
									d.location = IOSLink;
								}else if(bv.android){
									d.location = webLink;
								}
							}else{
								// @fix form 表单的下载
								if(isForm){
									et.closest('form').submit();
								}
							}
						});





						if(!isForm){
							$(this).attr('target','open_get_install');
							var iframe = '<iframe style="display: none;" src="" name="open_get_install"></iframe>';
							$('body').append(iframe);
							$(this).attr('href',scheme);
						}

						return false;
					}else if(bv.ios){
						if (schema) d.location = schema;

						// @REVIEW; 未解决已安装 app 的提醒
						var timer = setTimeout(function(){
							if (confirm(_self.config.unInstallTip)){
								d.location = IOSLink;
							}
						}, 1000);
					}else if(bv.android){

						// @TODO; isWebDownload 依赖 app.dxy.cn 对应用做了链接匹配
						if(_self.config.isWebDownload){
							if(android.types.xiaomi){// 小米浏览器
								$(this).attr('href', 'intent://app.dxy.cn/'+ardWebSite+'#Intent;package='+PACKAGENAME+';scheme=http;end');
							}else if(android.types.uc || android.types.firefox){// uc fiefox浏览器
							    $(this).attr('href', 'http://app.dxy.cn/'+ardWebSite+'.htm');
							}else{ //其他浏览器
							    $(this).attr('href', 'intent://app.dxy.cn/'+ardWebSite+'#Intent;action=android.intent.action.VIEW;category=android.intent.category.DEFAULT;category=android.intent.category.BROWSABLE;scheme=http;end');
							}
						}else{
							d.location = webLink;
							return false;
						}

					}
					// return false;
				});

				if( browser.versions.weixin && this.config.checkInstall){
					this.checkInstall(el);
				}

			}else{
				console.log('未找到绑定元素');
			}
		},


		checkInstall: function(el){
			var _self = this;
			var schema = el.attr('schema') || '';

			// 延时执行，避免 WebView 没有准备好
			setTimeout(function(){
				WeixinJSBridge.invoke("getInstallState",{
					"packageUrl": schema,
					"packageName": PACKAGENAME
				},function(res){
					if(res.err_msg == 'get_install_state:yes'){
						el.html(_self.config.installedTip);
					}
				});
			}, 300);

		},

/**
		 * 获取变量参数
		 * 赋值全局变量
		 * @param {String} // package name
		 * @param {String} // sub schemd
		 *//*
*/


		getVarName: function(name, id){
			switch (name){
				case 'drugs':
				PACKAGENAME = 'cn.dxy.medicinehelper';
				IOSLink = 'https://itunes.apple.com/cn/app/id540999305';
				webLink = 'http://app.dxy.cn/drugs.htm';
				ardWebSite = 'drugs' + (id ? '/'+id : '');
				break;
				case 'medtime':
				PACKAGENAME = 'cn.dxy.medtime';
				IOSLink = 'https://itunes.apple.com/cn/app/yi-xue-shi-jian-ding-xiang/id538302463';
				webLink = 'http://app.dxy.cn/medtime';
				ardWebSite = 'medtime' + (id ? '/'+id : '');
				break;
				case 'aspirin':
				PACKAGENAME = 'cn.dxy.android.aspirin';
				IOSLink = 'https://itunes.apple.com/us/app/id521635095?mt=8';
				webLink = 'http://app.dxy.cn/aspirin/drugs';
				ardWebSite = 'aspirin/drugs' + (id ? '/'+id : '');
				break;
				case 'idxyer':
				PACKAGENAME = 'cn.dxy.idxyer';
				IOSLink = 'http://itunes.apple.com/cn/app/id493466318';
				webLink = 'http://app.dxy.cn/dxyer.htm';
				ardWebSite = 'dxyer' + (id ? '/'+id : '');
				break;
				case 'keflex':
				PACKAGENAME = 'cn.dxy.keflex';
				IOSLink = 'https://itunes.apple.com/cn/app/yi-xue-shen-zao-yuan-ming/id637437027';
				webLink = 'http://app.dxy.cn/keflex.htm';
				ardWebSite = 'keflex' + (id ? '/'+id : '');
				break;
			}

			// 外部参数覆盖
			!!this.config.androidLink && (webLink = this.config.androidLink);
			!!this.config.IOSLink && (IOSLink = this.config.IOSLink);
		}
	}

	return getInstall;
});
