var score = 0;
var debug = 0;
$(function(){
    // //加载GA
    complete_clock();
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-34399670-7', 'auto');
    ga('send', 'pageview');


    if (quizUtil.gameInit(), $(".view-btn").on("click", function () {
            ga("send", "event", "sleep2015", "clickView")
        }), $(".start-btn").on("click", function () {
            //增加开始的时间
            quizUtil.app.startTime = new Date().getTime();
            ga("send", "event", "sleep2015", "clickStart")
        }), "DxysApp" === CommonUtil.getUrlParams("TAG"))console.log("in dxys app"), changeOpenLink(".view-btn"); else {
        new getInstall("aspirin", {btnEl: $(".view-btn"), isWebDownload: !0})
    }
    // CommonUtil.link_to_qrcode();

});





var Quiz = function () {
	
    function init(questionData) {
        var randomData = CommonUtil.getArrayItems(questionData.qData,1), resultData = RESULT_DATA, version = questionData.version,userResult = {
            hp: 0,
            full: 0,
            passed: 0,
            num: 0,
            selected: [],
            answered: !0,
            ended: !1
        };
        // return userResult.hp = 0 === questionData.totalScore ? questionData.qData.reduce(function (a, b) {
        //     return a + b.score
        // }, 0) : questionData.totalScore, userResult.full = userResult.hp,
        // {
        return userResult.hp = 0 === questionData.totalScore ? 0 : questionData.totalScore, userResult.full = userResult.hp,
        {
            reset: function () {
                userResult.hp = userResult.full, userResult.passed = 0, userResult.num = 0, userResult.selected = [], userResult.answered = !0, userResult.ended = !1
            },

            getAnswers: function () {
                return randomData[userResult.num].answers
            },

            getNext: function () {
                var questionOpt = {
                    num: userResult.num,
                    question_id : randomData[userResult.num].question_id,
                    desc: randomData[userResult.num].question,
                    opt: randomData[userResult.num].options,
                    score: randomData[userResult.num].score,
                    text: randomData[userResult.num].text,
                    answer: randomData[userResult.num].answers[0],
                    total: randomData.length
                }
                if(quizUtil.question_type == 'pic'){
                    questionOpt.questionImg = randomData[userResult.num].question_img;
                }


                return !userResult.ended && userResult.num < randomData.length && userResult.answered ? (userResult.num++, userResult.selected = [], userResult.answered = !1,questionOpt) : null
            },

            getStat: function () {
                return {
                    full: userResult.full,
                    num: userResult.num,
                    hp: userResult.hp,
                    ended: userResult.ended,
                    passed: userResult.passed,
                    answered: userResult.answered,
                    selected: userResult.selected,
                    total: randomData.length
                }
            },

            inputAnswer: function (inputAnswerArr) {
                return inputAnswerArr.length && "" !== inputAnswerArr[0] && inputAnswerArr.forEach(function (a) {
                    -1 === userResult.selected.indexOf(+a) && userResult.selected.push(+a)
                }),console.log(userResult.selected), userResult.selected
            },

            removeAnswer: function (a) {
                var b = -1;
                return a ? a.length && a.forEach(function (a) {
                    b = userResult.selected.indexOf(+a), -1 !== b && userResult.selected.splice(b, 1)
                }) : userResult.selected = [], userResult.selected
            },

            submitAnswer: function (inputAnswerIndexArr,userInputAnswer) {
                var type = quizUtil.question_type;
                var isRight = null;
                var userSelected = this.inputAnswer(inputAnswerIndexArr);
                var flag = inputAnswerIndexArr && userSelected ;
                //判断输入的答案和当前的答案是否一样
                var checkResult = quizUtil.app.currentQuestion.answer == userInputAnswer;
                return flag, userResult.ended || 0 === userResult.num || userResult.answered ? void 0 : ( checkResult ? (userResult.passed++, isRight = !0) :
                    (userResult.hp -= randomData[userResult.num - 1].score, isRight = !1), userResult.answered = !0, (userResult.hp <= 0 || userResult.num >= randomData.length) && (userResult.ended = !0), isRight)
            },

            getResult: function () {
                var index = 0, len = 0, result = {passed: userResult.passed, hp: userResult.hp, num: userResult.num};
                if (userResult.ended === !0) {
                    for (index = 0, len = resultData.length; len > index; index++)
                        if (!(
                            "" !== resultData[index].num &&
                            userResult.num > resultData[index].num ||
                            "" !== resultData[index].passed &&
                            userResult.passed > resultData[index].passed) &&
                            (!resultData[index].score || userResult.hp <= resultData[index].score)
                        ) {
                        result.desc = resultData[index].desc ;
                        result.pic_desc = resultData[index].desc_pic ;
                        break
                    }
                    return result ;
                }
            },

            getTimeStamp: function () {
                return version
            },


            buildNextQuestion: function (childTemplate) {
                var html = "", parentTemplate = {
                    template: "<li><span>$A</span>$O</li>",
                    questionId:"",
                    optNode: "",
                    descNode: "",
                    textNode: ""
                }, nextQestion = this.getNext();
                return nextQestion ? (childTemplate = $.extend({}, parentTemplate, childTemplate), html = nextQestion.opt.reduce(function (previousValue, currentValue, currentIndex) {
                    return previousValue + childTemplate.template.replace(/\$A/g, String.fromCharCode(65 + currentIndex)).replace(/\$O/g, currentValue)
                }, ""), childTemplate.optNode && $(childTemplate.optNode).html(html), childTemplate.descNode && $(childTemplate.descNode).html(nextQestion.desc), childTemplate.textNode && $(childTemplate.textNode).html(nextQestion.text), childTemplate.questionId && $(childTemplate.questionId).html(nextQestion.question_id), nextQestion) : !1
            },
            

            buildResult: function (childTemplate) {
                var resultData = "", d = null, parentTemplate = {template: "答对了$PASSED题，死在第$NUM题", resNode: ""}, result = this.getResult();
                return result ? (childTemplate = $.extend({}, parentTemplate, childTemplate), resultData = childTemplate.template.replace(/\$PASSED/g, result.passed).replace(/\$TOTAL/g, randomData.length).replace(/\$NUM/g, result.num).replace(/\$HP/g, result.hp), result.desc.length && result.desc.forEach(function (currentValue, index) {
                    d = new RegExp("\\$" + index, "g"), resultData = resultData.replace(d, currentValue)
                }), childTemplate.resNode && $(childTemplate.resNode).html(resultData), result) : void 0
            }
        }
    }

    var b;
    return {
        getInstance: function (questionData) {
            return b || (b = init(questionData)), b
        }
    }
}();

var quizUtil = {
    app: {},
    question_type : '',
    nodes: {
        startBtn: ".start-btn",
        nextBtn: ".next-btn",
        container: ".container",
        quizNum: ".quiz-num",
        quizTotal: ".quiz-total",
        quizDesc: ".quiz-desc",
        quizText: ".quiz-text",
        rightBtn: ".right-btn",
        wrongBtn: ".wrong-btn",
        cBtn: ".c-btn",
        dBtn: ".d-btn",
        processBlk: ".process",
        resultHeading: ".result-heading",
        resultText: ".result-text",
        testBtn: ".test-btn",
        testInput: ".test-num",
        testBox: ".test-box",
        promHeading:".prom-heading",
        optionBtn:".option-btn",
        questionImg:".questionImg"
    },
    getData: function () {
        var platform = $('#platform').val();
        var thiz = this;
        var data = QUESTION_DATA;
        //动态的加载题库
        var dayIndex = $('#day_index').val();
        var questionLib = QUESTIONS;
        var dailyKey = 'day'+ dayIndex;
        var questionList = questionLib[dailyKey];
        data.qData = questionList;
        console.log('data======>');
        console.log(data.qData);
        thiz.app = Quiz.getInstance(data)
        console.log(thiz.app);
    },
    gameInit: function () {
        var thiz = this;
        var question_type = $('#question_type').val();
        thiz.question_type = question_type;
        //问题集合
        thiz.app.question_data_arr = [];

        //初始化全局变量app
        thiz.getData(),
            $(thiz.nodes.container).on("click", thiz.nodes.startBtn + "," + thiz.nodes.nextBtn, function () {
                if($(this).hasClass('start-btn')){
                    quizUtil.app.question_data_arr = [];
                }
                thiz.buildNextQuestion({
                    textNode: thiz.nodes.quizText,
                    descNode: thiz.nodes.quizDesc,
                    numNode: thiz.nodes.quizNum,
                    totalNode: thiz.nodes.quizTotal,
                    optionNode : thiz.nodes.optionBtn,
                    questionImgNode : thiz.nodes.questionImg
                }),
                $(thiz.nodes.container).removeClass("pos-start").addClass("pos-process")
        }).on("click", thiz.nodes.rightBtn, function () {
            thiz.showAnswer(1)
        }).on("click", thiz.nodes.wrongBtn, function () {
            thiz.showAnswer(0)
        }).on("click", thiz.nodes.cBtn, function () {
            thiz.showAnswer(2)
        }).on("click", thiz.nodes.dBtn, function () {
            thiz.showAnswer(3)
        })
    },

    //waiting add question info,
    //这里需要加入题目的array的逻辑
    showAnswer: function (answerIndex) {
        var b = null, thiz = this;
        thiz.app.currentQuestion.user_answer = answerIndex;
        //答案的文字结果
        var answer_text = thiz.app.currentQuestion.opt[thiz.app.currentQuestion.answer]
        if(answerIndex == thiz.app.currentQuestion.answer){
            thiz.app.currentQuestion.answer_status = 1;
        }else{
            thiz.app.currentQuestion.answer_status = 0;
        }
        thiz.app.question_data_arr.push(thiz.app.currentQuestion);

        //图片替换答案中的结果
        var answer_tpl = this.pic_answer_template();
        var answer_html = answer_tpl.replace('{{answer}}',answer_text);
        $('.process').find('.right_ans').each(function(){
            $(this).html(answer_html);
        });

        var answer_desc = thiz.app.currentQuestion.text;
        var answer_desc_tpl = this.pic_answer_desc_template();
        var answer_desc_html =answer_desc_tpl.replace('{{text}}',answer_desc);
        $('.process').find('.ans_describe').each(function(){
            $(this).html(answer_desc_html);
        });
        b = this.app.submitAnswer([answerIndex],answerIndex), $(this.nodes.processBlk).addClass("answered").addClass(b ? "right" : "wrong"), this.app.getStat().ended && ($(this.nodes.container).off("click"), $(this.nodes.nextBtn).on("click", function () {
            ga("send", "event", "sleep2015", "clickResult"), thiz.buildResult()
        }).text("查看结果"));
		set_answer_heigth();
    },

    pic_answer_template :function(){
        return '正确答案是“{{answer}}”';
    },
    pic_answer_desc_template :function(){
        return '{{text}}';
    },

    buildNextQuestion: function (defaultConfig) {
        var nextQuestionConfig = this.app.getNext();
        this.app.currentQuestion = nextQuestionConfig;
        return nextQuestionConfig ?this.buildOptionNode(defaultConfig,nextQuestionConfig): !1
    },

    buildOptionNode:function(defaultConfig,nextQuestionConfig){
        var options = '';
        if(this.question_type != 'pic'){ //文字题库的返回类型
            options = defaultConfig.descNode &&$(defaultConfig.descNode).html(nextQuestionConfig.desc),
            defaultConfig.textNode && $(defaultConfig.textNode).html(nextQuestionConfig.text),
            defaultConfig.numNode && $(defaultConfig.numNode).html(nextQuestionConfig.num+1),
            defaultConfig.totalNode &&$(defaultConfig.totalNode).html(nextQuestionConfig.total),
                void $(this.nodes.processBlk).removeClass("answered wrong right");
        }else{ //图片题库
            var tpl = $(defaultConfig.questionImgNode).attr('data-template');
            tpl = tpl.replace('{{img_path}}',nextQuestionConfig.questionImg);
            options = defaultConfig.numNode && $(defaultConfig.numNode).html(nextQuestionConfig.num+1),
            defaultConfig.totalNode &&$(defaultConfig.totalNode).html(nextQuestionConfig.total),
            defaultConfig.optionNode && $(defaultConfig.optionNode).each(function(index){
                var that = $(this);
                var questionOpt =  nextQuestionConfig.opt;
                if(that.hasClass('option_a')){
                    that.html('A.'+questionOpt[1]);
                }else if(that.hasClass('option_b')){
                    that.html('B.'+questionOpt[0]);
                }else if(that.hasClass('option_c')){
                    that.html('C.'+questionOpt[2]);
                }else if(that.hasClass('option_d')){
                    that.html('D.'+questionOpt[3]);
                }
            }),
            defaultConfig.questionImgNode && $(defaultConfig.questionImgNode).attr('src',tpl),
            void $(this.nodes.processBlk).removeClass("answered wrong right");
        }


    },

    getHeading:function(){
        var head = "想要了解更多健康知识";
        if(this.question_type == 'pic'){
            head = "想要了解更多健康知识";
        }
        return head;
    },
    getResultImg:function(score){
        var img = "word_high_score.png";
        if(this.question_type == 'pic'){
            if(score>=60){
                img = 'pic_high_score.png';
            }else{
                img = 'pic_low_score.png';
            }
        }else{
            if(score>=60){
                img = 'word_high_score.png';
            }else{
                img = 'word_low_score.png';
            }

        }
        return img;
    },

    buildResult: function () {
        var result = this.app.getResult(), b = "";
        console.log(this.app.question_data_arr);
        //结束的时间
        var endTime = new Date();
        this.app.endTime =endTime.getTime();
        score =result.passed;

        //描述
        var promHeading = this.getHeading();

        var platform = $('#platform').val();
        //获取随机的评语


        //获取结果的内容
        var descHtml = result.desc[0];
        //组装问题数据,添加入历史数据表中
        var question_arr = this.app.question_data_arr;
        var question_data_arr = [];
        for(var key in question_arr){
            var obj = {};
            obj.question_id = question_arr[key]['question_id'];
            obj.answer = question_arr[key]['answer'];
            obj.user_answer = question_arr[key]['user_answer'];
            obj.answer_status = question_arr[key]['answer_status'];
            question_data_arr.push(obj);
        }
        console.log(question_data_arr);
        var challengeIndex = $('#challengeIndex').val();
        //计算答题时间
        var diff_time = Math.round((this.app.endTime-quizUtil.app.startTime)/1000);
        var url='/?/vHealthClock/doLowLevelPunch/challenge_index='+challengeIndex+'&spend_time='+diff_time+'&question_data='+JSON.stringify(question_data_arr);
        $.get(url,function(json){
            if(json.errcode==200){
                var redirectUrl = redirectUrl = $('#resultPage').val();
                window.location.href =redirectUrl;
                // if(json.data.status > 0){
                //     redirectUrl = $('#resultPage').val();
                // }else{
                //     redirectUrl = $('#homePage').val();
                // }
                // redirectUrl = $('#resultPage').val();
                // window.location.href =redirectUrl;
            }
        });
    },
    test: function (a) {
        var b = this.app.getStat().total, c = 0, d = null;
        for ($(this.nodes.startBtn).trigger("click"); b - a > c; c++)d = this.app.getAnswers()[0], 0 === d ? $(this.nodes.wrongBtn).trigger("click") : 1 === d && $(this.nodes.rightBtn).trigger("click"), $(this.nodes.nextBtn).trigger("click");
        for (c = 0; a > c; c++)d = this.app.getAnswers()[0], 1 === d ? $(this.nodes.wrongBtn).trigger("click") : 0 === d && $(this.nodes.rightBtn).trigger("click"), $(this.nodes.nextBtn).trigger("click");
        $(this.nodes.testBox).css("display", "none")
    }
};


function set_answer_heigth(){
	// var answer_h=$(".answer-blk").height()-$(".sign").height();
	// answer_h=answer_h-75;
    $(".pop_box").show(); 
}
/*点击叉号
提交打卡信息*/
function complete_clock(){
    $("#complete_clock").click(function(){
        quizUtil.buildResult();
    });
}
function share_wx(shareData){
    wx.ready(function(){
        wx.checkJsApi({
            jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo"],
            success: function() {
                // 朋友圈
                wx.onMenuShareTimeline(shareData);
                // 朋友
                wx.onMenuShareAppMessage(shareData);

                // QQ
                wx.onMenuShareQQ(shareData);

                // 腾讯微博
                wx.onMenuShareWeibo(shareData);
            }
        });
    });
}