var QUESTIONS = {
    'day1':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "香蕉适合各种体质的人食用",
            answers: [0],
            text: "香蕉性寒，过量食用会引起胃肠功能紊乱，并造成体内钾、钠、钙、镁等元素的比例失调，对健康产生危害。体质偏虚寒者，如胃胀、腹泻、肾炎者，最好少吃。有明显水肿和需要禁盐的病人也不宜多吃。温热病、口烦渴、大便秘结、痔疮出血者和减肥者可以适量食用。"
        },
    ],
    'day2':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "常吃火龙果能抑制痴呆症",
            answers: [1],
            text: "火龙果富含维生素E和花青素。它们都具有抗氧化、抗自由基、抗衰老的作用，还能提高对脑细胞变性的预防，抑制痴呆症的作用。"
        },
    ],
    'day3':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "糖尿病患者不能吃石榴",
            answers: [0],
            text: "石榴中含有铬元素，铬在糖和脂肪的新陈代谢中起着重要作用，是帮助胰岛素作用的重要成分，有益于糖尿病患者。吃完石榴后，将石榴皮洗净、晒干，自制成石榴茶。对糖尿病患者来说，石榴茶是一种非常理想的饮品。"
        },
    ],
    'day4':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "经常食用鲜枣会导致胆结石",
            answers: [0],
            text: "鲜枣中丰富的维生素C，可以使体内多余的胆固醇转变为胆汁酸。胆固醇少了，结石形成的概率也就随之降低。"
        },
    ],
    'day5':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "孕妇可以吃杏",
            answers: [0],
            text: "杏味酸性大热，且有滑胎作用，由于妊娠胎气胎热较重，故一般应遵循“产前宜清”的药食原则，而杏的热性及其滑胎特性，为孕妇之大忌，不宜食用。还有其他一些水果，如荔枝——易引起大便干燥;西瓜、白兰瓜、哈密瓜、桃子——易引起腹泻;菠萝蜜、榴莲——易引起食欲不振;新鲜杨梅——易引起胃酸过多，均不宜食用。"
        },
    ],
    'day6':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "食用猕猴桃后不可立即喝牛奶或其他乳制品",
            answers: [1],
            text: "猕猴桃中含有丰富的维生素C，维生素C易与奶制品中的蛋白质凝结成块，不但影响消化吸收，还会使人出现腹胀、腹痛、腹泻，所以食用猕猴桃后，一定不要马上喝牛奶或吃其他乳制品。另外，猕猴桃性寒，不宜多食,脾胃虚寒者应慎食，腹泻者不宜食用，先兆性流产、月经过多和尿频者忌食。"
        },
    ],
    'day7':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "荔枝可以大量食用",
            answers: [0],
            text: "大量食用鲜荔枝，会导致人体胰岛素分泌过多，出现反应性低血糖，症状有口渴、出汗、头晕、腹泻，甚至出现昏迷和循坏衰竭等症，医学上称为“荔枝病“，即低血糖症。上火的人也不要吃荔枝，以免加重上火症状。"
        },
    ],
    'day8':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "草莓有助于减肥",
            answers: [1],
            text: "草莓是一种营养价值很高的水果，含有大量的蛋白质，糖类，有机酸和果胶。有助于消化，也预防便秘，可以满足人体的营养需求。两餐之间吃点草莓会增加胃饱和感，从而能减少吃的主食，所以说适量的服用草莓也是可以有一点减肥的功效的。"
        },
    ],
    'day9':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "西瓜有助于肾炎和高血压治疗",
            answers: [1],
            text: "西瓜所含的无机盐和糖能利尿并消除肾脏的炎症；蛋白酶能把不溶性蛋白质转化为可溶性蛋白质，增加肾炎病人的营养；它还含有是血压降低的物质。"
        },
    ],
    'day10':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "煮熟的梨没有药用价值",
            answers: [0],
            text: "煮熟的梨去除了寒性，梨皮会变得略苦，去燥润肺的功效完全被释放出来。梨籽中的木质素本来属于不可溶纤维，但在加热后会在肠道中被溶解，将有害的胆固醇揪出体外。"
        },
    ],
    'day11':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "榴莲不可多吃",
            answers: [1],
            text: "榴莲具有丰富的营养，肠胃无法完全吸收时会上火；且榴莲富含纤维素，但它在肠胃中会吸水膨胀，过多食用反而会阻塞肠道，引起便秘。榴莲含热量及糖分比较高，因此肥胖病人应少吃，糖尿病者更不应该进食。它亦含较高的钾，肾病及心脏病人应少吃。"
        },
    ],
    'day12':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "长期吸烟的人应多吃葡萄",
            answers: [1],
            text: "长期吸烟的肺部积聚大量毒素，功能受损。葡萄中所含有效成分能提高细胞新陈代谢率，帮助肺部细胞排毒。另外，葡萄还具有祛痰作用，并能缓解因吸烟引起的呼吸道发炎、痒痛等不适症状。"
        },
    ],
    'day13':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "芒果没有美容养颜作用",
            answers: [0],
            text: "皮肤中的胶原蛋白若弹性不足就容易出现皱纹。芒果是预防皱纹的最佳水果，因为含有丰富的β-胡萝卜素和独一无二的酶，能激发肌肤细胞活力，促进废弃物排出，有助于保持胶原蛋白弹性，有效延缓皱纹出现。"
        },
    ],
    'day14':[
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "蒸吃带皮苹果，止泻易消化",
            answers: [1],
            text: "苹果中富含的果胶等有助排便，还具有降低血浆胆固醇水平、刺激肠内益生菌群的生长、消炎等功效，因此，利用熟苹果治疗腹泻效果是非常明显的。研究发现，苹果加热后，苹果内所含的多酚类天然抗氧化物质含量会大幅增加。这类物质不仅能降血糖血脂、抑制自由基而抗氧化、抗炎杀菌，还能抑制血浆胆固醇升高。由此可见，吃煮熟的苹果是大有益处的。把苹果带皮切成小片，放到小碗里面，隔水蒸5分钟就可以了，稍微冷却之后，就可以吃了。"
        },
    ],

}

