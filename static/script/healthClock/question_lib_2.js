var QUESTIONS = {
    day1: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "木瓜有健脾消食的作用",
            answers: [1],
            text: "木瓜中的木瓜蛋白酶，可将脂肪分解为脂肪酸。木瓜中含有一种酵素，能消化蛋白质，有利于人体对食物进行消化和吸收，因此有健脾消食之功。"
        }
    ],
    day2: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "无花果容易使人便秘",
            answers: "[0]",
            text: "无花果含有苹果酸、柠檬酸、脂肪酶，蛋白酶、水解酶等，能帮助人体对食物的消化，促进食欲，又因其含有多种脂类，故具有润肠通便的效果。 "
        }
    ],
    day3: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "吃山竹容易上火",
            answers: "[0]",
            text: "山竹含有一种特殊物质，具有降燥、清凉解热的作用，这使山竹能克榴莲之燥热。它含有丰富的蛋白质和脂类，对机体有很好的补养作用，对体弱、营养不良、病后都有很好的调养作用。"
        }
    ],
    day4: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "橘子不能与牛奶一起食用",
            answers: [1],
            text: "橘子前后1小时内不要喝牛奶，因为牛奶中的蛋白质遇到果酸会凝固，影响消化吸收。"
        }
    ],
    day5: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "孕妇不可食用榴莲",
            answers: "[0]",
            text: "一般健康人都可食用。病后及妇女产后可用之来补养身体。 榴莲含有丰富的蛋白质和脂类，对机体有很好的补养作用，是良好的果品类营养来源。榴莲虽富含纤维素，但它在肠胃中会吸水膨胀，过多食用反而会阻塞肠道，引起便秘。榴莲含热量及糖分较高，因此肥胖人宜少吃，糖尿病者更不应该进食。"
        }
    ],
    day6: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "糖尿病患者不宜吃葡萄",
            answers: [1],
            text: "葡萄含糖量较多，便秘者不宜多食，糖尿病人禁食葡萄。葡萄具有降血脂，扩张血管，增加冠脉血流量，降低血压和胆固醇，软化血管等作用，能阻止血栓形成。"
        }
    ],
    day7: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "杨桃能预防心血管疾病",
            answers: [1],
            text: "杨桃含有多种营养成分，并含有大量的挥发性成分，带有一股清香。在茶余酒后吃几片杨桃，会感到口爽神怡，另有一番风味。 功效：杨桃能减少机体对脂肪的吸收，有降低血脂、胆固醇的作用，对高血压、动脉硬化等心血管疾病有预防作用。"
        }
    ],
    day8: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "用水果代替每餐约1/3等量的米饭，有助于瘦身吗？",
            answers: [1],
            text: "因为水果含有抑制淀粉酶活性的成分，消化速度比米饭慢，血糖指数比米饭低，胰岛素反应小，有利于减少脂肪合成。"
        }
    ],
    day9: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "一杯无花果汁的含钙含量和一杯牛奶的含钙含量相近吗？",
            answers: "[0]",
            text: "无花果富含钙和铁，但是和牛奶相比，等量的无花果的含钙的量大约为牛奶的一半。"
        }
    ],
    day10: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "换牙阶段的儿童适合吃山楂吗？",
            answers: "[0]",
            text: "山楂中的酸性物质对牙齿具有一定的腐蚀性，食用后要注意及时漱口、刷牙，正处在牙齿更替期的儿童更应格外注意。"
        }
    ],
    day11: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "杨桃对于女士美容有效吗？",
            answers: [1],
            text: "杨桃里含有特别多的果酸，能够抑制黑色素的沉淀，能有效地去除或淡化黑斑，并且有保湿的作用，可以让肌肤变得滋润、有光泽，对改善干性或油性肌肤组织也有显著的功效。"
        }
    ],
    day12: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "用淘米水洗水果对于去除农药科学吗？",
            answers: [1],
            text: "淘米水可以中和酸性的有机磷，达到去除残留农药的目的。"
        }
    ],
    day13: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "饭前吃水果科学吗？",
            answers: "[0]",
            text: "人的胃容积是有限的,饭前吃水果会影响正常的饮食，饭后马上吃水果会对食物的消化起干扰的作用,最好在饭后2-3小时内吃水果,对水果和食物的消化都比较好。"
        }
    ],
    day14: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "感冒病人吃荔枝可以吗？",
            answers: "[0]",
            text: "荔枝性温，多吃易“上火”，伤风感冒或患有急性炎症的人最好别吃荔枝，否则会加重病症。"
        }
    ]
}
