var QUESTIONS = {
    day1: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "吃石榴对于保护视力有作用吗？",
            answers: [1],
            text: "石榴含有花青素，有保护微血管的作用，可改善眼部供血。同时，花青素还有利于加速视紫红质蛋白再生，从而在一定程度上减轻眼睛疲劳感和对弱光的视力敏感性。所以多吃石榴能够保护我们的眼睛。"
        }
    ],
    day2: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "低血糖人群吃荔枝管用吗？",
            answers: [0],
            text: "虽然荔枝的糖分是果糖，但当人体摄入果糖后，要由转化酶把果糖转化为葡萄糖后才能吸收。荔枝吃得太多，转化酶供不应求，不但不能把果糖转化，反倒会让血液内葡萄糖不足，"
        }
    ],
    day3: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "吃西瓜撒盐会更甜吗",
            answers: [1],
            text: "利用了味觉的对比规律，加盐后反而变甜。"
        }
    ],
    day4: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "榴莲的储存方式，冷藏优于冷冻吗？",
            answers: [0],
            text: "冷冻的榴莲保质期更长，而且其质感和口味上也比冷藏的方式更佳。"
        }
    ],
    day5: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "皮肤易过敏的人适合吃芒果吗？",
            answers: [0],
            text: "芒果中含有易引起皮肤过敏反应的化学成分，如二羟基苯，食用后易引发皮肤瘙痒、潮红等过敏现象。"
        }
    ],
    day6: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "孕妇适合吃龙眼吗？",
            answers: [0],
            text: "孕妇容易产生内热，出现大便干燥、胎热及早产、肝经郁热的症状，因此应避免吃龙眼。"
        }
    ],
    day7: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "红枣和牛奶适合一起食用吗？",
            answers: [0],
            text: "红枣中维生素C含量较高，容易和牛奶中的蛋白发生凝结反应生成块状物。"
        }
    ],
    day8: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "柚子是心脑血管疾病最佳的食疗水果",
            answers: [1],
            text: "高血压患者常利用药物来排除体内多余的钠，柚子正好含有这些患者必需的天然矿物质——钾，却几乎不含钠，因此是患有心脑血管病及肾脏病患者最佳的食疗水果。"
        }
    ],
    day9: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "菠萝能消除炎症和水肿",
            answers: [1],
            text: "菠萝含有一种叫“菠萝朊酶”的物质，它能分解蛋白质。在食肉类或油腻食物后，吃些菠萝对身体大有好处。“菠萝朊酶”还有溶解阻塞于组织中的纤维蛋白质和血凝块的作用，能改善局部的血液循环，消除炎症和水肿。"
        }
    ],
    day10: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "樱桃含铁量很低",
            answers: [0],
            text: "樱桃的含铁量特别高，位于各种水果之首。常食樱桃可补充体内对铁元素的需求，促进血红蛋白的再生，既可防治缺铁性贫血，又可增强体质，健脑益智。"
        }
    ],
    day11: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "大枣能提高人体免疫力，抑制癌细胞",
            answers: [1],
            text: "药理研究发现，红枣能促进白细胞的生成，降低血清胆固醇，提高血清白蛋白，保护肝脏。红枣还含有抑制癌细胞，甚至可使癌细胞向正常细胞转化的物质。"
        }
    ],
    day12: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "草莓具有明目养肝的作用",
            answers: [1],
            text: "草莓中所含的胡萝卜素是合成维生素A的重要物质，具有明日养肝作用。它还含有果胶和丰富的膳食纤维，可以帮助消化、通畅大便。草莓对胃肠道和贫血均有一定的滋补调理作用。"
        }
    ],
    day13: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "肝病患者最好少食用李子",
            answers: [0],
            text: "李子对肝病有较好的保养作用。唐代名医孙思邈评价李子时曾说：“肝病宜食之。”李子有促进血红蛋白再生的作用，贫血者适度食用李子对健康大有益处。"
        }
    ],
    day14: [
        {
            question_id: 1,
            score: 10,
            options: ["错", "对"],
            question: "空腹不宜吃橙子",
            answers: [1],
            text: "饭前或空腹时不宜食用，否则橙子所含的有机酸会刺激胃黏膜，对胃不利。吃橙子前后1小时内不要喝牛奶，因为牛奶中的蛋白质遇到果酸会凝固，影响消化吸收。"
        }
    ]
}

