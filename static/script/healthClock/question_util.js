/**
 * Created by Justfly on 2016/2/22.
 */

var QUESTION_DATA = {
    version: 2015031114,
    totalScore: 0

}


var RESULT_DATA = [{
    num: "",
    passed: 0,
    score: "",
    desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
    "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
    "<div class='result-result'>" +
    '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
    '<p class="height"></p>'+
    '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
    '<p class="height"></p>' +
    '<div class="result-btn">' +
    '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
    '</div>' +
    '</div>'
    ],
    desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
    "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
    "<div class='result-result'>" +
    '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
    '<p class="height"></p>'+
    '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
    '<p class="height"></p>' +
    '<div class="result-btn">' +
    '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
    '</div>' +
    '</div>'
    ]
},
    {
        num: "",
        passed: 1,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 2,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 3,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 4,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 5,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 6,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 7,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 8,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]

    },
    {
        num: "",
        passed: 9,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    },
    {
        num: "",
        passed: 10,
        score: "",
        desc: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中排名第<b>${{myRank}}</b>名</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ],
        desc_pic: [" <div class='padding-top'></div><div class='result-text'><p>${{desc}}</p></div>"+
        "<div class='result-img'><img src='./challenge_files/image/${{result_img}}'/></div>"+
        "<div class='result-result'>" +
        '<p class="result-source"><span class="score_num">${{score}}</span><span class="fen">分</span><span class="use_time">耗时${{spend_time}}</span></p>' +
        '<p class="height"></p>'+
        '<p class="result-content">你在<b>${{totalChallenge}}</b>个挑战者中，打败了<b>${{myRank}}</b>的玩家</p>' +
        '<p class="height"></p>' +
        '<div class="result-btn">' +
        '<span class="link_rank"><a href="${{rank_page}}"><img src="challenge_files/image/rank_link.png" /></a></span><span class="challenge_again"><a href="${{start_page}}"><img src="challenge_files/image/again.png" /></a></span>' +
        '</div>' +
        '</div>'
        ]
    }
]