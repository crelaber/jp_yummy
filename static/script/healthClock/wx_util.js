var WxUtil = {
    share_data : null,

    setShareConfig : function (config) {
        wx.onMenuShareTimeline(config), wx.onMenuShareAppMessage(config), wx.onMenuShareQQ(config), wx.onMenuShareWeibo(config)
    },

    share_wx_data : function (shareData){
        var shareDataConfig = {
            title: shareData.title,
            //摘要
            desc: shareData.desc,
            //链接,可以换主页
            link: shareData.link,
            //缩略图
            imgUrl: shareData.cover,
        }

        wx.ready(function(){
            wx.checkJsApi({
                jsApiList: ["onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo"],
                success: function() {
                    // 朋友圈
                    wx.onMenuShareTimeline(shareDataConfig);
                    // 朋友
                    wx.onMenuShareAppMessage(shareDataConfig);

                    // QQ
                    wx.onMenuShareQQ(shareDataConfig);

                    // 腾讯微博
                    wx.onMenuShareWeibo(shareDataConfig);
                }
            });
        });
    },

    shareDataConfig : function(platform){
        var shareData = SHARE_DATA[platform];
        shareData.homePage = {
            'title' : shareData['home_page_title'] ,
            'desc' : shareData['home_page_desc']  ,
            'link' : shareData['home_page_link']  ,
            'cover' : shareData['home_page_cover']  ,
        };

        shareData.resultPage = {
            'title' : shareData['result_page_title']  ,
            'desc' : shareData['result_page_desc']  ,
            'link' : shareData['result_page_link']  ,
            'cover' : shareData['result_page_cover']  ,
        };

        shareData.rankPage = {
            'title' : shareData['rank_page_title']  ,
            'desc' : shareData['rank_page_desc']  ,
            'link' : shareData['rank_page_link'] ,
            'cover' : shareData['rank_page_cover']  ,
        };
        return shareData;
    },

    getShareData : function(platform,currentPage){
        if(!this.share_data){
            this.share_data = this.shareDataConfig(platform);

        }
        var result = {};
        switch(currentPage){
            case 'homePage':
                result = this.share_data.homePage;
                break;
            case 'resultPage':
                result = this.share_data.resultPage;
                break;
            case 'rankPage':
                result = this.share_data.rankPage;
                break;
            default :
                result = this.share_data.homePage;
                break;
        }
        return result;
    }

}
