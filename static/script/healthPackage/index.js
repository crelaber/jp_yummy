$(function(){
    'use strict';
    localStorage.setItem('url',window.location.href);
    $(document).on("pageInit", "#page-picker", function(e, id, page) {
        init_city_picker();
    });
    $.init();
    init_price();
    initstate();
});

function init_price() {
    var pay=parseFloat($(".price").text());
    $(".amount").html('￥'+pay);
    $(".check_con").eq(0).show();
    $(".center_list").eq(0).show();
}

function init_city_picker() {
    var cityslist=$("#citys").val();
    var value=cityslist.split(",")
    var host = $('#domain').val();
    var packageType = $('#package').val();
    $("#city_select").picker({
        toolbarTemplate: $('#city_picker_header').html(),
        cols: [{
            textAlign: 'center',
            values:value ,
            cssClass: 'picker-items-col-normal'
        }],
        onClose:function(){
            var cityTpl = $('#city_item').html();
            var pickerval=$("#city_select").val();
            $(".center_list").css('max-height','150px');
            $.get(host+"?/vHealthPackage/getHospital/city="+pickerval+"&package="+packageType, function(result){
                var citylist = [];
                if(result.errcode == 200){
                    citylist = result.data;
                    $(".center_list ul").html("");
                    if(citylist.length > 0){
                        var tmpContnent = '';
                        for(var i=0;i<citylist.length;i++){
                            tmpContnent = cityTpl.replace('__CENTER_NAME__',citylist[i].name).replace('__CENTER_ADDRESS__',citylist[i].address);
                            $(".center_list ul").append(tmpContnent);
                            var add_height=$(".center_list").height();
                            if(add_height>=150){
                                $(".open_all").show();
                            }else{
                               $(".open_all").hide(); 
                            }
                        }
                    }
                }
            });
        }
    });

}

/**
 * 初始化
 */
function initstate(){
    $(".project_tab ul li").click(function(){
        $(".project_tab ul li").removeClass('active');
        $(this).addClass('active');
        var num =$(".project_tab ul li").index(this);
        $(".check_con").hide();
        $(".check_con").eq(num).show();
    });
    $(".open_all").click(function(){
        $(this).hide();
        $(".center_list").css('max-height','none');
    });
    //数量减1
    // $(".minus").click(function(){
    //     var pay=parseFloat($(".price").text());
    //     var m=parseInt($(".cart_num").text());
    //     if(m>1){
    //         m=m-1;
    //         var money=m*pay;
    //         $(".cart_num").html(m);
    //         $(".amount").html('￥'+money);
    //     }
    // });
    //数量加1
    // $(".plus").click(function(){
    //     var pay=parseFloat($(".price").text());
    //     var n=parseInt($(".cart_num").text());
    //     n=n+1;
    //     var money=n*pay;
    //     $(".cart_num").html(n);
    //     $(".amount").html('￥'+money);
    // });
    //城市选择
    $(".check_city select").click(function(){
        var val=$(".check_city").find("option:selected").val();
        $(".center_list").hide();
        $(".center_list").eq(val).show();
    })
    //显示弹窗信息
    $(".submit_pay").click(function(){
        var city=$("#city_select").val();
        if(null == city || '' == city || "" == city){
            showMsg('请选择城市');
            return ;
        }
        $(".fill_infor").show();
    });
    //取消弹窗信息填写
    $(".submit_health .cancel").click(function(){
        $(".fill_infor").hide();
    });
    //取消弹窗信息填写
    $(".submit_health .sure").click(function(){
        //數量
        var city=$("#city_select").val();
        var quanlity = 1;
        var sex = $('#sex').val();
        if(null == sex || '' == sex || "" == sex){
            showMsg('请填写性别');
            return ;
        }
        var isMarried = $('#marry').val();
        if(null == isMarried || '' == isMarried || "" == isMarried){
            showMsg('请填写婚姻状况');
            return ;
        }


        var phone = $.trim($("input[name='phone']").val());
        var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
        if (phone.length == 0 || !reg.test(phone)) {
            showMsg('请填写正确的手机号码');
            return ;
        }


        var code = $.trim($("input[name='verify_code']").val());
        if(code.length == 0){
            showMsg('请填写验证码');
            return ;
        }

        submitOrder(quanlity,sex,isMarried,phone,code,city);

    });
}


function enableSubmit() {
    $(".submit_health .sure").removeAttr('disabled');
}

function disableSubmit() {
    $(".submit_health .sure").attr('disabled','diabled');
}

/**
 * 提交订单
 */
function submitOrder(quanlity,sex,isMarried,phone,code,city) {
    disableSubmit();
    var domain = $('#domain').val();
    var url = domain +'?/vHealthPackage/submitOrder';
    var param = {
        'quanlity' : quanlity,
        'package_id' : $('#package_id').val(),
        'user_sex' : sex,
        'user_married_status' : isMarried,
        'phone' : phone,
        'code' : code,
        'package' : $('#package').val(),
        'city':city
    };
    $.post(url,param,function (result) {
        enableSubmit();
        if(result.errcode == 200){
            var biz = result.data.biz
            biz.success = paySuccess;
            biz.cancel = payFailed;
            // 发起微信支付
            wx.chooseWXPay(biz);
        }else{
            showMsg(result.msg);
        }
    });
}

function payFailed() {
    // window.location.reload();
    var domain = $('#domain').val();
    window.location.href = domain + '?/vHealthPackage/payFail'
}



function paySuccess() {
    var domain = $('#domain').val();
    window.location.href = domain + '?/vHealthPackage/paySuccess'
}


/**
 * 显示消息
 */
function showMsg(msg,time) {
    layer.open({
        content: msg ,
        time: time ? time : 2 //3秒后自动关闭
    });
}

var wait = 60;
var code_state = "IDLE";

/**
 * 倒计时
 */
function count_down(){
    if(wait <= 0){
        $(".code_btn").removeClass("active")
        $(".code_btn").attr("onclick", "send_code()");
        $(".code_btn").html("获取验证码");
        wait = 60;
        code_state = "IDLE";
    } else {
        $(".code_btn").addClass("active")
        $(".code_btn").attr("onclick", "");
        $(".code_btn").html( "剩余" + wait + "秒");
        wait--;
        setTimeout(count_down, 1000);
    }
}

/**
 * 发送短信验证码
 */
function send_code(){
    if (code_state != "IDLE"){
        return;
    }
    var tel = $.trim($("input[name='phone']").val());
    var reg = /^1[3|4|5|7|8][0-9]\d{8}$/;
    var domain = $('#domain').val();

    if(false == reg.test(tel)){
        showMsg('请输入正确的手机号');
        return ;
    }
    code_state = "SENDING";

    $.post(domain+'?/vHealthPackage/ajaxSendCode', {
        phone : tel
    }, function(data) {
        if(data.errcode == 200){
            count_down();
        }
        showMsg(data.msg);
    });

}