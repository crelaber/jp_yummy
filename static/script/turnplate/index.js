$(function () {
    rotateimg_position();
    init_game();
});


/**
 * 初始化游戏
 */
function init_game(){
    var order_id = $('#order_id').val();
    var url = '?/Turnplate/init_game/order_id='+order_id;
    do_get(url,function(data){
        $('#turnplate_angle').val(data.prize_info.angle);
        $('#times').val(data.draw_times);
        init_turnplate();
        render_winner_list(data.winner_list)
    });
}

/**
 * 游戏结束时候的调用
 */
function game_end(num){
    var order_id = $('#order_id').val();
    var url = '?/Turnplate/game_end/coupon_num='+num+'&order_id='+order_id;
    do_get(url,function(data){
        $('#turnplate_angle').val(data.prize_info.angle);
        var times = parseInt($('#times').val());
        times = times - 1;
        if(times < 0){
            times = 0;
        }
        $('#times').val(times);
        //layer.open({
        //    content: '恭喜获得'+num+'元优惠券',
        //    time: 3
        //})
        set_win_result(data);
        reset_turnplate();

    });
}

/**设置结果*/
function set_win_result(data){
    $('.draw_result').html('');
    var html = get_win_result_html();
    html = html.replace('${{coupon_num}}',data.coupon_num)
        .replace('${{coupon_num}}',data.coupon_num)
        .replace('${{coupon_effective_end_time}}',data.effective_end_desc)
        .replace('${{user_mobile}}',data.mobile)
        .replace('${{coupon_id}}',data.coupon_id)
        .replace('${{link}}',data.link);
    $('.draw_result').append(html);
    $('.winning_list').hide();
}


function render_winner_list(winner_list){
    if(winner_list.length <= 0){
        $('#winner_list').html('');
    }else{
        var item_html = get_winner_list_item_html();
        var html = '';
        var count = 0;
        var winner ;
        for(var i= 0,len=winner_list.length;i<len;i++){
            winner = winner_list[i];
            html = html+item_html.replace('${{winner_header}}',winner.user_info.client_head+'/64')
                .replace('${{winner_name}}',winner.user_info.client_nickname)
                .replace('${{coupon_num}}',winner.coupon_num)
                .replace('${{get_coupon_day}}',winner.get_coupon_day)
                .replace('${{get_coupon_hours}}',winner.get_coupon_hours)
                .replace('${{desc}}',winner.desc);
        }
        $('#winner_list').append(html);
    }
}

/**
 * 初始化转盘
 */
function init_turnplate(){
    $(".rotate-con-zhen").rotate({
        bind: {
            click: function () {
                var times = parseInt($('#times').val());
                if(times <= 0){
                    layer.open({
                        time : 2,
                        content:'本次抽奖机会已使用'
                    })
                    return;
                }
                var angle = gen_angle();
                $(this).rotate({
                    duration: 5000,               //转动时间
                    angle: 270,                    //起始角度
                    animateTo: 2160 + angle,      //结束的角度
                    easing: $.easing.easeOutSine,//动画效果，需加载jquery.easing.min.js
                    callback: function () {
                        rnzp();//简单的弹出获奖信息
                    }
                });
            }
        }
    });


    function rnzp() {
        var angle = gen_angle();
        var times = parseInt($("#times").val());
        if (times > 0) {
            if (300 < angle && angle < 360) {
                game_end(3);
            } else if (240 < angle && angle < 300) {
                game_end(4);
            } else if (0 < angle && angle < 60) {
                game_end(5);
            } else if (180 < angle && angle < 240) {
                game_end(6);
            }else if (60 < angle && angle < 120) {
                game_end(8);
            }else{
                game_end(10);
            }

        } else {
            //没有抽奖次数
            game_end(-1);
        }
    }
}

/*将转盘复位*/
function reset_turnplate(){
    $(".rotate-con-zhen").rotate({
        duration: 1000,               //转动时间
        animateTo: 2160,      //结束的角度
        callback: function () {
        }
    });
}

/**
 * 中奖结果
 */
function get_win_result_html(){
    var html = '<div class="win_money">' +
                '<div class="win_coupon"><div class="win_container">'+
                    '<div class="coupon-amount">'+
                        ' <span class="amount-money">${{coupon_num}}</span><span class="yuan">元</span>'+
                    '</div>'+
                    '<div class="coupon-detail">'+
                        '<span class="condition">${{coupon_num}}元优惠券</span>'+
                        '<span class="superposition">不可与同类优惠券共享</span>'+
                        '<span class="expire-time">有效期至：${{coupon_effective_end_time}}</span></div>'+
                        '<span class="select-btn" data-id="${{counpon_id}}">'+
                            '<i class="proicon icon-select"></i>'+
                        '</span>'+
                    '</div></div>'+
                    '<a href="${{link}}"><div class="midify_phone">券已放入你的账户<span class="user_link">立即使用></span><div class="clear" style="clear: both;"></div></a></div>';
                '</div>';
    return html;
}


/**
 * 中奖名单列表item项
 */
function get_winner_list_item_html(){
    var tpl =   '<li>'+
                    '<div class="winning_user">'+
                        '<div class="winner_header">' +
                            '<img src="${{winner_header}}"/>' +
                        '</div>'+
                        '<div class="winner_infor">'+
                            '<div class="friend-top">'+
                                '<span class="f-name">${{winner_name}} </span>'+
                                '<span class="f-day">${{get_coupon_day}} </span>'+
                                '<span class="f-time">${{get_coupon_hours}} </span>'+
                            '</div>'+
                        '<div class="friend-com">${{desc}}</div>'+
                        '</div>'+
                        '<div class="friend-money">${{coupon_num}} 元</div>'+
                    '</div>'+
                '</li>';
    return tpl
}



/**
 * 调整转盘的位置
 */
function rotateimg_position() {
    var win_wd = $(window).width();
    var turn = win_wd / 1.29;
    $(".rotate-con-zhen").css({'height': turn});
    $(".rotate-con-zhen img").css({'height': turn});
}



/**
 * 生成随机的角度参数
 **/
function gen_angle(){
    var angle = parseInt($('#turnplate_angle').val());
    return angle;
}



function do_get(url,callback,is_full_data){
    layer.open({type: 2});
    $.get(url,function(json){
        layer.closeAll('loading');
        var result = json;
        if(is_full_data){
            callback(result);
        }else{
            if(result.error_code == 200){
                callback(result.data);
            }else{
                console.log(result.msg);
            }
        }
    });
}