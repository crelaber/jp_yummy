<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <link rel="stylesheet" href="../../static/css/bootstrap.css" />
    <link rel="stylesheet" href="../../static/css/home.css" />
    <link rel="stylesheet" href="../../static/css/group.css" />
    <link rel="stylesheet" href="../../static/layer/layer.css" />
    <link rel="stylesheet" href="../../static/font/fonticon.css" />
    <link rel="stylesheet" href="../../static/font/fonticon_new.css" />
    <link rel="stylesheet" href="../../static/css/commodity.css" />
    <link rel="stylesheet" href="../../static/css/mobile.css" />
    <script src='../../static/script/Wshop/swipe.js' type="text/javascript"></script>
    <script src="../../static/script/jquery-2.1.1.min.js"></script>
    <script src="../../static/layer/layer.js"></script>
    <title>CheersLife健康减脂拼团</title>
</head>
    <style>body{ background-color: #f0f1ee; }</style>
<body>
    <input type="hidden" value="{$productid}" id="iproductId" />
    <input type="hidden" value="{$group}" id="group" />
    <input type="hidden" value="{$isEnd}" id="isEnd" />
    <input type="hidden" value="{$catId}" id="catId" />
    <input type="hidden" value="{$isSubscribed}" id="isSubscribed" />
    <input type="hidden" value="{$productInfo.is_active}" id="is_active" />


    <div class="comm-detail">
        <div class="pintuanicon"><img src="../../static/img/home/pinicon.png" alt=""></div>
        <div class="addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    {foreach from=$images item=img}
                        {if $img['image_id'] != ''}
                            <div>

                                <img  class="img-responsive" src="/uploads/product_hpic/{$img.image_path}" alt="">

                            </div>
                        {/if}
                    {/foreach}
                </div>
                <ul id="position">

                    {foreach from=$images item=img}
                        {if $img['image_id'] != ''}
                            <li class="cur"></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="commodity-body" data-instock="1000000"  data-p="" data-sp=""  data-hash = "">
            <div class="feedify">
                <div class="feedify-item">
                    <div class="proInfor detail_proinfor feedify-item-header" >
                        <div class="pintuan panic_buying"  data-time="">
                            <span class="msicon">
                                <img class="pin" src="{$docroot}static/img/home/pin.png" alt="">{$productInfo.num}人拼团</span>
                            <span class="timeend" >

                                   {if $productInfo.is_active == 0}
                                       <em>距离活动开始还有：</em>
                                {else}
                                    <em>距离活动结束还有：</em>
                                   {/if}


                                <i class="time_end"><b></b>
                                    时<b></b>
                                    分
                                    <b></b>秒</i>
                            </span>
                        </div>
                        <div class="prointroduction">{$productInfo.product_name}</div>
                        <div class="proprice" data-p="" data-time="{$productInfo.kill_time}">
                            <span class="leftprice">
                                <b class="n">{$productInfo.group_price}</b>
                                <b class="y">元/件</b>
                                <s>￥{$productInfo['pinfo'].sale_price|string_format:"%.2f"}</s>
                            </span>
                        </div>

                        <div class="clear"></div>
                    </div>
                    <div class="group_note">
                    <div class="rule_link"><b>拼团玩法</b>*{$productInfo.num}人团，人数不足自动退款<a class="rulelink" href="http://www.icheerslife.com/?/Gmess/view/id=5">拼团规则</a></span></div>
                    <div class="ruleimg"><img src="{$docroot}static/img/home/rule.png" alt=""></div>
                    </div>
                    <div class="feedify-item-body">
                        <div class="pro_introduction">
                            <div class="c_code">
                                <img src="{$docroot}static/img/cheerslife_c.jpg" alt=""></div>
                            <div class="rightintro">
                                <div class="introText">
                                    <span class="intronote">商品介绍：</span>
                                    <span class="introcon">{$productInfo.product_subtitle}</span>
                                </div>
                                <div class="introText">
                                    <span class="intronote">适宜人群：</span>
                                    <span class="introcon">{$productInfo.people}</span>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="pro-information">
                            <div class="inforhead">商品信息</div>
                            <div class="infor_list">
                                <ul>
                                    <li>
                                        <span class="leftinfor">【品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牌】</span>
                                        <span class="rightinfor">{$productInfo.brand_info}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【产&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地】</span>
                                        <span class="rightinfor">{$productInfo.place}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【储存方法】</span>
                                        <span class="rightinfor">{$productInfo.save}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【规&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格】</span>
                                        <span class="rightinfor">{$productInfo.spec_attr}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【保&nbsp;&nbsp;质&nbsp;&nbsp;期】</span>
                                        <span class="rightinfor">{$productInfo.is_valid_time}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【使用方法】</span>
                                        <span class="rightinfor">{$productInfo.user_method}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【快递信息】</span>
                                        <span class="rightinfor">{$productInfo.kd_attr}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【服务信息】</span>
                                        <span class="rightinfor">{$productInfo.service_info}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【温馨提示】</span>
                                        <span class="rightinfor">{$productInfo.tips}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="advantage">
                            <img src="../../static/img/home/advantage.jpg" alt=""></div>
                        <div class="commodity-infor" id="vpd-content"></div>
                    </div>
                </div>

            </div>

            <div class="bottom_tab">
                <ul>
                    <li class="left">
                    <button class="othergroup" id="buy">￥{$specs[0].sale_price|string_format:"%.2f"}单买</button>

                    </li>
                    <li class="right {if $productInfo.is_active == 0}yure{/if}">

                    <button class="rightbutton sharebtn {if $productInfo.is_active == 0}yure{/if}" id="kt">
                        {if $group == 1}
                            查看团
                        {else}
                            {if $isEnd == 1}
                                 活动结束
                            {/if}
                            {if $isEnd == 0}
                                {$productInfo.num}人团￥{$productInfo.group_price}开团

                            {/if}

                        {/if}
                    </button>
                    </li>
            </ul>
        </div>
    </div>


        <div class="backtohome" id="back_list">
            <img src="../../static/images/backcircle.png" alt="">
        </div>

    <div class="to_top hidden"></div>

    <div class="attencode" style="display: none">
        <span class="codeimg"><img src="../../static/img/home/attcode.png" alt=""></span>
        <span class="closecode"><img src="../../static/img/home/cha.png" alt=""></span>
    </div>

    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
     <script type="text/javascript">
         var name = "超值! 只需{$productInfo.group_price}元，团购【{$productInfo.num}人团】{$productInfo.product_name} 拿回家";
         var name2 = "超值! 只需{$productInfo.group_price}元，团购【{$productInfo.num}人团】{$productInfo.product_name} 拿回家 ";
         var imgUrl = "{$base_url}{$config.productPicLink}{$productInfo.catimg}";


         wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: {$signPackage.timestamp},
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
            });
            wx.ready(function () {
                // 在这里调用 API


                wx.onMenuShareTimeline({
                    title: name2, // 分享标题
                    link: '{$base_url}?/Group/group_detail/id='+{$productInfo['product_id']}, // 分享链接
                    imgUrl: imgUrl, // 分享图标
                    success: function () {

                    }
                });
                wx.onMenuShareAppMessage({
                    title: '健康减脂拼团', // 分享标题
                    desc: name, // 分享描述
                    link: '{$base_url}?/Group/group_detail/id='+{$productInfo['product_id']}, // 分享链接
                    imgUrl: imgUrl, // 分享图标
                    success: function () {

                    }
                });
            });
</script>

<script type="text/javascript">
    var bullets = document.getElementById('position').getElementsByTagName('li');

    var banner = Swipe(document.getElementById('mySwipe'), {
                       auto: 3000,
                       continuous: true,
                       disableScroll:false,
                       callback: function(pos) {
                       var i = bullets.length;
                       while (i--) {
                       bullets[i].className = ' ';
                       }
                       bullets[pos].className = 'cur';
                       }
                       });

</script>
<script data-main="../../static/script/Wshop/group_detail.js" src="{$docroot}static/script/require.min.js"></script>
</body>
</html>