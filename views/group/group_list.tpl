<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>CheersLife健康减脂拼团</title>
	<link rel="stylesheet" href="../../static/css/home.css">
	<link rel="stylesheet" href="../../static/css/group.css">
	<script src="../../static/script/Wshop/jquery-2.1.1.min.js"></script>
	<link  rel="stylesheet" herf=="../../static/css/echo.css">
	<script src="../../static/script/echo.min.js"></script>
	<script>
		echo.init({
                    offset: 100,
                    unload: false

                });
	</script>
	<style>body{ background-color: #f0f1ee; }</style>
</head>
<body>
	<div class="group_list">
		<ul>
            {foreach from=$products item=pd}
			<li class="group">
                {if $pd.is_active == 0}
                <div class="labelimg">{$pd.start}点开始</div>
                {/if}
				<div class="proDetail">
                    <div class="probanner"  onclick="location = '{$docroot}?/Group/group_detail/id={$pd.product_id}';">
                        <img  src="{$docroot}static/img/home/blank.jpg" data-echo="/uploads/product_hpic/{$pd.catimg}" alt=""></div>
                    <div class="proInfor" data-time="{$pd.kill_time}">
                        <div class="pintuan panic_buying">
							<span class="msicon"><img class="pin" src="../../static/img/home/pin.png" alt="">{$pd.num}人拼团
							</span>
							<span class="timeend" >
                                {if $pd.is_active == 0}
								<em>距离活动开始还有：</em>
                                {else}
                                    <em>距离活动结束还有：</em>
                                {/if}

                                 <i class="time_end"><b>{$pd.h}</b>时<b>{$pd.m}</b>分<b>{$pd.s}</b>秒</i>
							</span>
                        </div>
                        <div class="prointroduction intro_list">{$pd.product_name}</div>
                        <div class="proprice">
							<span class="leftprice">
								<b class="n">{$pd.group_price}</b>
								<b class="y">元/件</b>
								<s>￥{$pd['pinfo'].sale_price|string_format:"%.2f"}</s>
							</span>

							<span class="open_group {if $pd.is_active == 0}preheat{/if}" onclick="location = '{$docroot}?/Group/group_detail/id={$pd.product_id}';">{if $pd.is_active == 0}即将开团{else}立即开团{/if}</span>
                        </div>
                    </div>
                </div>
			</li>
        {/foreach}
		</ul>
	</div>
</body>

<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    var name2 = "健康减脂拼团，优质商品直供，快来一起拼团吧";
    var imgUrl = "{$base_url}/static/img/site_icon.jpg";

    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: {$signPackage.timestamp},
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API


        wx.onMenuShareTimeline({
            title: name2, // 分享标题
            link: '{$base_url}?/Group/group_list/id='+{$catId}, // 分享链接
            imgUrl: imgUrl, // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: '健康减脂拼团', // 分享标题
            desc: name2, // 分享描述
            link: '{$base_url}?/Group/group_list/id='+{$catId}, // 分享链接
            imgUrl: imgUrl, // 分享图标
            success: function () {

            }
        });
    });
</script>

<script data-main="{$docroot}static/script/Wshop/group.js?v={$cssversion}" src="{$docroot}static/script/require.min.js"></script>
</html>