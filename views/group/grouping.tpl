<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<meta content="telephone=no" name="format-detection" />
	<title>CheersLife健康减脂拼团</title>
	<link rel="stylesheet" href="../../static/css/home.css">
	<link rel="stylesheet" href="../../static/css/group.css">
	<link rel="stylesheet" href="../../static/font/fonticon_new.css" />
	<script src="../../static/script/Wshop/jquery-2.1.1.min.js"></script>
</head>
<style>body{ background-color: #f0f1ee; }
</style>
<body>
   <input type="hidden" value="{$id}" id="groupId">
   <input type="hidden" value="{$pid}" id="pid">
   <input type="hidden" value="{$isJoin}" id="isJoin">
   <input type="hidden" value="{$result}" id="result">
   <input type="hidden" value="{$catId}" id="catId">
   <input type="hidden" value="{$isSubscribed}" id="isSubscribed" />

   <div class="group_infor">
		<!-- <div class="successicon"><img src="../../static/img/home/success.png" alt=""></div> --><!-- 拼团成功-->
        {if $result == -1}
            <div class="failicon"><img src="../../static/img/home/failicon.png" alt=""></div>
        {/if}
        {if $result == 1}
            <div class="successicon"><img src="../../static/img/home/success.png" alt=""></div>
        {/if}
        <div class="group_img" style="background-image: url(/uploads/product_hpic/{$productInfo.catimg}); background-position: center; background-size: cover;" onclick="location = '{$docroot}?/Group/group_detail/id={$productInfo.product_id}';"></div>
		<div class="group_detail">
			<div class="groupNum"><img class="pin" src="../../static/img/home/pin.png" alt="">{$productInfo.num}人拼团</div>
			<div class="grouptitle">{$productInfo.product_name}</div>
			<div class="Num_price"><span class="leftN">{$productInfo.num}人团价：</span><span class="leftprice"><b class="n">{$productInfo.group_price}</b><b class="y">元/件</b><s>￥{$spec.sale_price|string_format:"%.2f"}</s></span></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="groupstate">
		<div class="groupheader">
			<ul>
				<li>
					<div class="headerimg"><img class="header_img" src="{$adminUinfo.client_head}/0" alt="">
					<span class="leader"><img src="../../static/img/home/leader.png" alt=""></span></div>
				</li>
                {foreach $joinList as $key => $val}
				<li>
					<div class="headerimg"><img class="header_img" src="{$val['user']['client_head']}/0" alt=""></div>
				</li>
                {/foreach}

                {foreach $defaultArray as $key => $val}

                <li>
                    <div class="headerimg"><img class="header_img" src="../../static/img/home/default.png" alt=""></div>
                </li>
                {/foreach}
				<div class="clear"></div>
			</ul>
		</div>
        {if $result == 0}<p class="congratulations">还差<b>{$joinNum}</b>人，盼你如南方人盼暖气</p>{/if}
        {if $result == 1}<p class="congratulations">恭喜小伙伴们，拼团成功啦！</p>{/if}
        {if $result == -1}
        <p class="congratulations">人数不足开团失败了，还有机会，不要灰心哦</p>{/if}
		<!-- <p class="congratulations"></p> -->
		<div class="timedown" data-time="{$endTime}" >
            {if $result == 0}
            	<p>距离拼团活动结束还有：</p>
				<div class="count_down"><i class="time_end"><b>00</b>
					时<b>00</b>分
					<b>00</b>秒</i>
				</div>
            {/if}
		</div>
		<div class="people_list">
			<p class="openlist">点击查看更多信息<i class="proicon icon-spread active"></i></p>
			<ul class="leaderlist">
				<li>
				<span class="centericon"> <img src="../../static/img/home/totop.png" alt=""></span>
				<div class="groupleader">
						<span class="leaderimg"><img src="{$adminUinfo.client_head}/0" alt=""></span>
						<span class="leadername">团长&bull;{$adminUinfo.client_name}</span>
						<span class="starttime">{$startTime} 开团</span>
					</div>	
				</li>
			</ul>
			<ul  class="memberlist">

                {foreach $joinList as $key => $val}
				<li>
					<div class="members">
					 	<i></i>
						<div class="membersimg"><img src="{$val['user']['client_head']}/0" alt=""></div>
						<div class="memberinfor">
						<span class="membername">{$val['user']['client_name']}</span>
						<span class="membertime">{$val['add_time']}</span>
						</div>
						<div class="clear"></div>
					</div>
				</li>
                {/foreach}
			</ul>
		</div>
	</div>
	<div class="bottom_tab">
		<ul>
                    {if $isJoin == 1}
                        {if $result == 1 || $result == -1}
            <li class="left">
				<button class="othergroup" onclick="location = '{$docroot}?/Group/group_list/id={$catId}';">更多团购</button>
			</li>
            <li class="right">
				<button class="rightbutton sharebtn" id="ct">
                            重新开团
                </button>
			</li>
                        {/if}

                        {if $result == 0}
            <li class="left groupingl">
				<button class="othergroup" onclick="location = '{$docroot}?/Group/group_list/id={$catId}';">更多团购</button>
			</li> 
            <li class="right groupingr">
				<button class="rightbutton sharebtn" id="ct">
                            邀请好友参团
               	</button>
			</li>
                        {/if}

                    {/if}

                    {if $isJoin == 0}
                        {if $result == 1 || $result == -1}
           <li class="left">
				<button class="othergroup" onclick="location = '{$docroot}?/Group/group_list/id={$catId}';">更多团购</button>
			</li>
            <li class="right">
                <button class="rightbutton sharebtn" id="ct">重新开团</button>
            </li>
                        {/if}
                        {if $result == 0}
            <li class="left  groupingl">
				<button class="othergroup" onclick="location = '{$docroot}?/Group/group_list/id={$catId}';">更多团购</button>
			</li>
            <li class="right  groupingr">
				<button class="rightbutton sharebtn" id="ct">
                            立即参团
                </button>
			</li>
                        {/if}

                    {/if}
                
		</ul>
	</div>

	<div class="maskshare" style="display: none;">
		<img src="../../static/img/home/share.png" alt="">
		<span class="knowbtn">我知道了</span>
	</div>

	<div class="attencode" style="display: none">
		<span class="codeimg"><img src="{$code}" alt=""></span>
		<span class="closecode"><img src="../../static/img/home/cha.png" alt=""></span>
	</div>

	<script>
	$(function(){
		init();
	});
	function init(){
		$(".openlist").click(function(){
			if($(this).find(".icon-spread").hasClass("active")){
				$(this).find(".icon-spread").removeClass("active");
				$(".people_list ul").hide();
			}
			else{
				$(this).find(".icon-spread").addClass("active");
				$(".people_list ul").show();
			}
		});

		$(".knowbtn").click(function(){
			$(".maskshare").hide();
		});
		$(".closecode img").click(function(){
			$(".attencode").hide();
		});

	}
	</script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript">
        var name = "超值! 只需{$productInfo.group_price}元，团购【{$productInfo.num}人团】{$productInfo.product_name} 拿回家";
        var name2 = "超值! 只需{$productInfo.group_price}元，团购【{$productInfo.num}人团】{$productInfo.product_name} 拿回家 ";
        var imgUrl = "{$base_url}{$config.productPicLink}{$productInfo.catimg}";

        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: {$signPackage.timestamp},
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
        });
        wx.ready(function () {
            // 在这里调用 API


            wx.onMenuShareTimeline({
                title: name2, // 分享标题
                link: '{$base_url}?/Group/grouping/id={$id}&showwxpaytitle=1', // 分享链接
                imgUrl: imgUrl, // 分享图标
                success: function () {

                }
            });
            wx.onMenuShareAppMessage({
                title: 'CheersLife健康商城', // 分享标题
                desc: name, // 分享描述
                link: '{$base_url}?/Group/grouping/id={$id}&showwxpaytitle=1', // 分享链接
                imgUrl: imgUrl, // 分享图标
                success: function () {

                }
            });
        });
    </script>

    <script data-main="../../static/script/Wshop/group_detail.js" src="{$docroot}static/script/require.min.js"></script>

</body>
</html>