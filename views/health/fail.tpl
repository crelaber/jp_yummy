<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>健康体检</title>
	<link rel="stylesheet" href="../../static/health/css/health.css">
	<script type='text/javascript' src='http://g.alicdn.com/sj/lib/zepto/zepto.js' charset='utf-8'></script>
</head>

<style>
	html,body{ height: 100%; background: #ffffff; }
</style>
<body>
	<div class="successicon">
		<img src="../../static/health/img/fail.png" alt="">
	</div>
	<div class="btn_con">
	<a  href="{$rtnUrl}">
		<div class="backhome">
			<span>返回首页</span>
		</div>
	</a>
	<a  id="btnBack">
		<div class="buyagain">
			<span>重新购买</span>
		</div>
	</a>
	</div>
<script type="text/javascript">
	var url = localStorage.getItem('url');
	$('#btnBack').attr('href',url);
</script>

</body>
</html>