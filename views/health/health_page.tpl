<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>{$package}套餐详情</title>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
	<script src="{$docroot}static/layer/layer.js"></script>
	<link rel="stylesheet" href="{$docroot}static/health/css/sm.css">
	<link rel="stylesheet" href="{$docroot}static/health/css/health.css">
	<script type='text/javascript' src='{$docroot}static/script/healthPackage/zepto.js' charset='utf-8'></script>
    <script src="{$docroot}static/script/healthPackage/config.js"></script>
</head>
<body>
<input type="hidden" id="package_id" value="{$package_info['id']}">
<input type="hidden" id="package" value="{$package}">
<input type="hidden" id="citys" value="{$citys}">
<input type="hidden" id="domain" value="{$docroot}">
	<div class="health_main">
		<div class="banner">
			<img src="{$docroot}static/health/img/health_banner.jpg" alt=""></div>
		<div class="describe">
			<div class="des1">
				<span class="des_tt">【{$package_info['name']}】</span>
				<span class="check_num">{$package_info['name_desc']}</span>
			</div>
			<div class="des2">{$package_info['note']}</div>
			<div class="des3">
				<span class="price">{$package_info['sale_price']/100}元</span>
				<span class="orig_price"><s>￥{$package_info['market_price']/100}</s></span>
			</div>
		</div>
		<div class="check_city page" id="page-picker" >
			<span class="lefttxt">体检城市：</span>
			<input type="text" placeholder="城市" id='city_select' readonly/>
			<span class="notetxt">（请选择您体检所在城市）</span>
			<span class="openicon"></span>
		</div>
		<div class="detail_address">
			<div class="center-header">【体检中心】</div>
			<div class="center_list">
				<ul>
					<li>
						<div class="center_name">请选择城市获取地址</div>
					</li>	
				</ul>
			</div>
			<div class="open_all">查看全部</div>
		</div>
		<div class="package_details">
			<div class="center-header">【套餐详情】</div>
			<div class="detail_list">
				<ul>
					<li>
						<div class="infor_detail">
							<span class="left-tt">【套餐名称】</span>
							<span class="right-dt">{$package_info['name']}</span>
						</div>
					</li>
					<li>
						<div class="infor_detail">
							<span class="left-tt">【体验项目】</span>
							<span class="right-dt">{$package_info['package_item_desc']}</span>
						</div>
					</li>
					<li>
						<div class="infor_detail">
							<span class="left-tt">【有效期】</span>
							<span class="right-dt">{$package_info['expired_time_desc']}</span>
						</div>
					</li>
					<li>
						<div class="infor_detail">
							<span class="left-tt">【发票类型】</span>
							<span class="right-dt">健康服务费</span>
						</div>
					</li>
					<li>
						<div class="infor_detail">
							<span class="left-tt">【开票单位】</span>
							<span class="right-dt">中粮数字健康科技（北京）有限公司</span>
						</div>
					</li>
				</ul>
			</div>
			<div class="reminder">
				(温馨提示：优惠价低至五折，因各城市体检机构不同会有差异，请选择体检城市后显示支付价格。)
			</div>
		</div>
		<div class="check_project">
			<div class="project_tab">
				<ul>
					<li class="active">
						<div class="check_cat">科室检查</div>
					</li>
					<li>
						<div class="check_cat">实验室检查</div>
					</li>
					<li>
						<div class="check_cat">医技检查</div>
					</li>
					<li>
						<div class="check_cat">其他</div>
					</li>
				</ul>
			</div>
			<div class="check_con">
				<img src="{$package_info['item_img_1']}" alt="">
			</div>
			<div class="check_con">
				<img src="{$package_info['item_img_2']}" alt="">
			</div>
			<div class="check_con">
				<img src="{$package_info['item_img_3']}" alt="">
			</div>
			<div class="check_con">
				<img src="{$package_info['item_img_4']}" alt="">
			</div>
		</div>
	</div>
	<div class="pay_footer">
		<div class="pay_amount">待支付<span class="amount"></span></div>
		<div class="submit_pay">确认支付</div>
	</div>
	<div class="fill_infor" style="display: none;">
		<div class="shadebg"></div>
		<div class="fill_main">
			<div class="fill_section">
				<div class="fill_child">
					<div class="fill-tt">填写信息</div>
					<div class="fill_input">
						<div class="input_dt">
							<span class="lefticon"><img src="../../static/health/img/sexicon.png" alt=""></span>
							<div class="rightinput">
							<span class="inputtxt">
								<select name="sex" id="sex">
									<option value="">请选择性别</option>
									<option value="1">男</option>
									<option value="2">女</option>
								</select>
							</span>
							<span class="openicon"></span>
							</div>
						</div>
						<div class="input_dt">
							<span class="lefticon"><img src="../../static/health/img/marryicon.png" alt=""></span>
							<div class="rightinput">
							<span class="inputtxt">
								<select name="marry" id="marry">
									<option value="">婚姻状况</option>
									<option value="0">已婚</option>
									<option value="1">未婚</option>
								</select>
							</span>
							<span class="openicon"></span>
							</div>
						</div>
						<div class="input_dt">
							<span class="lefticon"><img src="../../static/health/img/phoneicon.png" alt=""></span>
							<div class="rightinput">
							<span class="inputtxt"><input type="text" placeholder="请输入手机号" id="phone" maxlength="11" name="phone"></span>
							</div>
						</div>
						<div class="input_dt">
							<span class="lefticon"><img src="../../static/health/img/codeicon.png" alt=""></span>
							<div class="rightinput">
							<span class="inputcode"><input type="text" id="code" placeholder="请输入验证码" name="verify_code"></span>
							<span class="code_btn"  onclick="send_code();">获取验证码</span>
							</div>
						</div>
					</div>
					<div class="ps">注：体检卡号和密码我们将以短信的形式发送给您，请凭此短信预约体检</div>

					<div class="submit_health">
						<div class="btn cancel">取消</div>
						<div class="btn sure">确定</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="city_picker_header">
		<header class="bar bar-nav">
			<button class="button button-link pull-right close-picker">确定</button>
			<h1 class="title">城市</h1>
		</header>
	</script>
	<script type="text/html" id="city_item">
		<li>
			<div class="center_name">__CENTER_NAME__</div>
			<div class="list_address">__CENTER_ADDRESS__</div>
		</li>
	</script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
		wx.config({
			debug: false,
			appId: '{$signPackage.appId}',
			timestamp: {$signPackage.timestamp},
			nonceStr: '{$signPackage.nonceStr}',
			signature: '{$signPackage.signature}',
			jsApiList: ['chooseWXPay']
		});
	</script>
	<script type='text/javascript' src='{$docroot}static/script/healthPackage/sm.js' charset='utf-8'></script>
	<script data-main="{$docroot}static/script/healthPackage/index.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script>
</body>
</html>