<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>健康体检</title>
	<link rel="stylesheet" href="../../static/health/css/health.css">
</head>
<style>
	html,body{ height: 100%; background: #ffffff; }
</style>
<body>
	<div class="banner">
		<img src="../../static/health/img/health_banner.jpg" alt="">
	</div>
	<div class="the_method">
		<div class="the_tt">
			预约方式
		</div>
		<div class="the_website">
			登录体检网http://y.kktijian.com
		</div>
		<div class="phonenum">
			或拨打中英人寿体检客服电话010-56989772，预约电话的服务时间为周一至周五（9:00-18:00）
		</div>
	</div>
</body>
</html>