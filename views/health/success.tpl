<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>健康体检</title>
	<link rel="stylesheet" href="../../static/health/css/health.css">
</head>
<style>
	html,body{ height: 100%; background: #ffffff; }
</style>
<body>
	<div class="successicon">
		<img src="../../static/health/img/success.png" alt="">
	</div>
	<a href="{$rtnUrl}">
		<div class="backbtn">
			<span>返回</span>
		</div>
	</a>

</body>
</html>