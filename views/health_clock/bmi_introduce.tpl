<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<title>健康打卡</title>
</head>
<body>
	<div class="health_banner">
		<img src="/static/img/clock/banner.jpg" alt="">
	</div>
	<div class="bmi_main">
		<div class="main_msg">
			你的<b>BMI</b>值为
		</div>
		<div class="bmi_number">
			<span><b>23</b></span>
		</div>
		<div class="main_msg">
			你的体重已超标，建议你执行减重计划
		</div>
	</div>
	<div class="bmi_content">
		<div class="content_tt">
			<b>BMI</b>值范围<br>
		</div>
		<div class="content_intro">
			身体质量指数（BMI，Body Mass Index）是国际上常用的衡量人体肥胖程度和是否健康的重要标准，主要用于统计分析。肥胖的程度的判断不能采用体重的绝对值，它天然与身高有关。
		</div>
	</div>
	<div class="button_clock">
		<a href=""><span>开启计划</span></a>
	</div>
</body>
</html>