<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="format-detection" content="email=no" />
        <link rel="stylesheet" href="/static/font/fonticon.css" />
        <link rel="stylesheet" href="/static/css/mobile.css" />
        <link rel="stylesheet" href="/static/layer/layer.css" />
        <link rel="stylesheet" href="/static/css/bootstrap.css" />
        <title>Cheerslife健康商品</title>
    </head>

    <body style="background-color:#eeeeee;">
        <input type="hidden" value="{$orderId}" id="order_id"/>
        <input type="hidden" value="{$prizeId}" id="prize_id"/>
        <div class="select-add-body">
            <div class="detail-header">
                <span class="backicon" id="select_back"><i class="proicon icon-back"></i></span>
                <span class="title-text">收货地址</span>
                {if {$address}}
                <span class="modify-add">编辑</span>
                {/if}
            </div>
            <div class="empty-header" style="height:51px;"></div>
            <div class="address-list">
                <ul>
                    {foreach from=$addressList item=ad}
                        <li>
                            {if {$ad.enable} == '1'}
                                <div class="address-detail" data-id={$ad.id}><span class="full-name">{$ad.user_name}&nbsp;&nbsp;{$ad.phone}</span><span class="full-address">{$ad.province}{$ad.city}{$ad.area}{$ad.address}</span><span class="full-address">{$ad.identity}</span></div>
                                <span class="select-btn active"><i class="proicon icon-select"></i></span>
                                <span class="delete-btn" style="display:none;"><i class="proicon icon-empty"></i></span>
                            {else}
                                <div class="address-detail" data-id={$ad.id}><span class="full-name">{$ad.user_name}&nbsp;&nbsp;{$ad.phone}</span><span class="full-address">{$ad.province}{$ad.city}{$ad.area}{$ad.address}</span><span class="full-address">{$ad.identity}</span></div>
                                <span class="select-btn"><i class="proicon icon-select"></i></span>
                                <span class="delete-btn" style="display:none;"><i class="proicon icon-empty"></i></span>
                            {/if}
                        </li>
                    {/foreach}
                </ul>
            </div>
            <div class="add_new_add" id="add" ><i class="proicon icon-add" ></i></div>
        </div>

        <script language="javascript" src="/static/script/jquery-2.1.1.min.js"></script>
        <script src="/static/layer/layer.js"></script>
        <script language="javascript" src="/static/script/healthClock/address.js?v={$smarty.now}"></script>
        <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
        <script type="text/javascript">
            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: '{$signPackage.timestamp}',
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: [
                    'hideMenuItems',
                    'hideOptionMenu',
                    'hideAllNonBaseMenuItem'
                ]
            });
            wx.ready(function () {
                wx.hideMenuItems({
                    menuList: [
                        'menuItem:share:timeline',
                        'menuItem:favorite'
                    ],
                });
                wx.hideOptionMenu();
                wx.hideAllNonBaseMenuItem();
            });
        </script>
    </body>
</html>
