<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/yummy.css" />
	<link rel="stylesheet" href="/static/css/mobile.css" />
	<link rel="stylesheet" href="/static/font/fonticon.css" />
	<link rel="stylesheet" href="/static/layer/layer.css" />

	<title>添加地址</title>
</head>

<body style="background-color: #eeeeee;">
	<input type="hidden" value="{$orderId}" id="order_id"/>
	<input type="hidden" value="{$prizeId}" id="prize_id"/>
    <div class="filladdbody">
		<div class="address_sec">
			<div class="add_province">
				<span class="leftPrompt">省份</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillprovince" id="provincetxt" onChange="changeComplexProvince(this.value, sub_array, 'citytxt', 'areatxt');"></select>
				</span>
				<span class="rightarrow"> <i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_city">
				<span class="leftPrompt">城市</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillcity" id="citytxt" onChange="changeCity(this.value,'areatxt','areatxt');"></select>
				</span>
				<span class="rightarrow"> <i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_area">
				<span class="leftPrompt">区域</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillarea" id="areatxt"></select>
				</span>
				<span class="rightarrow">
					<i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_detail">
				<span class="leftPrompt">地址</span>
				<span class="fillinput">
					<textarea  type="text" name="fillarea" id="fillarea" placeholder="请填写详细地址，不少于5个字"></textarea>
				</span>
			</div>
		</div>
		<div class="fill_infor">
			<div class="fillname">
				<span class="leftPrompt">姓名</span>
				<span class="fillinput">
					<input type="text" name="fillname" id="fillname"></span>
			</div>
			<div class="fillphone">
				<span class="leftPrompt">手机号</span>
				<span class="fillinput">
					<input type="text" name="fillphone" id="fillphone"></span>
			</div>
		</div>
		<div class="add-address" id="submit">添加地址</div>
	</div>
	<script src="/static/script/jquery-1.7.2.min.js"></script>
	<script language="javascript" src="/static/script/healthClock/address.js?v={$smarty.now}"></script>
	<script src="/static/layer/layer.js"></script>
	<script src="/static/citypicker/area.js" type="text/javascript"></script>
	<script src="/static/citypicker/AreaData_min.js" type="text/javascript"></script>
	<script type="text/javascript">
        $(function (){
            initComplexArea('provincetxt', 'citytxt', 'areatxt', area_array, sub_array, '0', '0', '0');
        });
	</script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: [
                'hideMenuItems',
                'hideOptionMenu',
                'hideAllNonBaseMenuItem'
            ]
        });
        wx.ready(function () {
            wx.hideMenuItems({
                menuList: [
                    'menuItem:share:timeline',
                    'menuItem:favorite'
                ],
            });
            wx.hideOptionMenu();
            wx.hideAllNonBaseMenuItem();
        });
	</script>
</body>

</html>