<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />  
    <meta name="format-detection" content="email=no" /> 
<link rel="stylesheet" type="text/css" href="/static/city_select/mobile-select-area.css">
<link rel="stylesheet" type="text/css" href="/static/city_select/dialog.css">
<link rel="stylesheet" href="/static/layer/layer.css" />
<link rel="stylesheet" href="/static/font/fonticon.css" />
<link rel="stylesheet" href="/static/font/fonticon_new.css" />
<link rel="stylesheet" href="/static/css/mobile.css" />
<link rel="stylesheet" href="/static/css/commodity.css" />
<link rel="stylesheet" href="/static/css/bootstrap.css" />
<link rel="stylesheet" href="/static/css/clock.css" />
<style type="text/css">
    .order-detail {
         padding-bottom: 0px;
    }
</style>
<title>健康打卡礼品抢购</title>
</head>
<body style="background-color:#eeeeee;">
<input type="hidden" id="order_id" value="{$order.id}" />
<input type="hidden" value="{$prizeInfo.id}" id="prize_id"/>
<input type="hidden" value="{$address.id}" id="address_id"/>
<div class="order-detail-body">
    <div class="order-detail" >
        <div class="orderinfor">
            {if $address}
                <div class="order-address" id="update-address">
                    <div class="addressleft"><i class="iconfont icon-local"></i></div>
                    <div class="addresslist">
                        <p>收货人：{$address.user_name}&nbsp;{$address.phone}</p>
                        <p>收货地址：{$address.province}{$address.city}{$address.area}{$address.address}</p>
                        <p><span class="modify-icon"><i class="proicon icon-right"></i></span>
                    </div>
                </div>
            {else}
                <div class="link_add" id="update-address" style="text-align:center;" > <span class="modify-icon"><i class="proicon icon-right"></i></span><span style="color: #CAB68C; font-size:18px;">点击添加地址</span></div>
            {/if}
        </div>
        <div class="order-list">
            <ul class="express">
                <li class="expressN">
                    {*<div class="toptag">
                        <span class="package">包裹</span>
                        <span class="expressmode">{$expressCity}发货</span>
                    </div>*}
                    {foreach $prizeList as $k => $v}
                        <div class="order_list">
                            <div class="proimgleft">
                                <img src="/uploads/zy_hch/prize_img/{$v['cover_img']}" alt="" width="40px" height="40px" />
                            </div>
                            <div class="rightpro">
                                <p class="pro-name">{$v.prize_name}</p>
                                <p class="proprice">{$v.prize_desc}</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    {/foreach}
                </li>
            </ul>
        </div>
    </div>
    {if $address}
        <div class="order-detail1">
            <div class="detail-modify discount">
                <span class="detail-modify-left">领取时间</span>
                <span class="modify-icon"></span><span class="detail-modify-right">{$order.order_time}</span>
            </div>
            <div class="detail-modify discount">
                <span class="detail-modify-left">联系人</span>
                <span class="modify-icon"></span><span class="detail-modify-right">{$address.user_name}</span>
            </div>
            <div class="detail-modify discount">
                <span class="detail-modify-left">联系电话</span>
                <span class="modify-icon"></span><span class="detail-modify-right">{$address.phone}</span>
            </div>
            <div class="detail-modify discount">
                <span class="detail-modify-left">联系地址</span>
                <span class="modify-icon"></span><span class="detail-modify-right">{$address.province}{$address.city}{$address.area}{$address.address}</span>
            </div>
        </div>
    {/if}
    {*<div class="order_note">
        <input id="user_note" maxlength="100" placeholder="如您还有其他要求，请在此添加备注（100字内）" />
    </div>*}
        <button class="submit_sure" id="submit">确定</button>
</div>
<script type="text/javascript" src="/static/city_select/zepto.min.js"></script>
<script type="text/javascript" src="/static/city_select/dialog.js"></script>
<script type="text/javascript" src="/static/city_select/mobile-select-area.js"></script>
<script language="javascript" src="/static/layer/layer.js"></script>
<script src="/static/script/healthClock/exchangeOrder.js?v={$smarty.now}"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: '{$signPackage.timestamp}',
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: [
            'hideMenuItems',
            'hideOptionMenu',
            'hideAllNonBaseMenuItem'
        ]
    });
    wx.ready(function () {
        wx.hideMenuItems({
            menuList: [
                'menuItem:share:timeline',
                'menuItem:favorite'
            ],
        });
        wx.hideOptionMenu();
        wx.hideAllNonBaseMenuItem();
    });
</script>
</body>
</html>


