<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<title>领取失败</title>
</head>
<body>
	<div class="health_banner">
		<img style="display: block;" src="/static/img/clock/fail.png" alt="" >
	</div>
	<div class="exchange_result">
		<p>{$msg}</p>
	</div>

	<div class="button_foot">
		<a href="{$redirectUrl}">
			<span>继续打卡</span>
		</a>
	</div>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: [
                'hideMenuItems',
                'hideOptionMenu',
                'hideAllNonBaseMenuItem'
            ]
        });
        wx.ready(function () {
            wx.hideMenuItems({
                menuList: [
                    'menuItem:share:timeline',
                    'menuItem:favorite'
                ],
            });
            wx.hideOptionMenu();
            wx.hideAllNonBaseMenuItem();
        });
	</script>
</body>
</html>