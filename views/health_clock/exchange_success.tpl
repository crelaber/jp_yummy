<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<title>领取成功</title>
</head>
<body>
	<div class="health_banner">
		<img style="display: block;" src="/static/img/clock/success.png" alt="" >
	</div>
	<div class="exchange_result">
		<p>恭喜您，领取成功！</p>
		{if $isCoupon}
			<p class="result_note">券号和密码将在24小时之内通过手机短信发送给您，敬请留意</p>
        {else}
            <p class="result_note">礼品信息可点击“查看我的奖品”，选择“我的卡券/奖品”查看（实物礼品将尽快为您寄出，敬请期待）</p>
		{/if}
	</div>

	<div class="button_foot">

        {if $isCoupon}
			<a href="{$redirectUrl}">
				<span>返回首页</span>
			</a>
        {else}
			<a href="{$redirectUrl}">
				<span>查看我的奖品</span>
			</a>
        {/if}
	</div>

	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: [
                'hideMenuItems',
                'hideOptionMenu',
                'hideAllNonBaseMenuItem'
            ]
        });
        wx.ready(function () {
            wx.hideMenuItems({
                menuList: [
                    'menuItem:share:timeline',
                    'menuItem:favorite'
                ],
            });
            wx.hideOptionMenu();
            wx.hideAllNonBaseMenuItem();
        });
	</script>
</body>
</html>