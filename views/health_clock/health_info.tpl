<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">

	<link rel="stylesheet" href="/static/css/sm.css">
	<link rel="stylesheet" href="/static/layer/layer.css" />
	<title>健康打卡</title>
</head>
<style>
	.picker-item{ font-size: 1.6rem; }
	.picker-columns{ height: 14rem; }
</style>
<body style="background: #ffffff;">
	<input type="hidden" value="{$redirectPage}" id="redirectPage">
	<div class="health_banner">
		<img src="/static/img/clock/banner.jpg" alt="">
	</div>
	<div class="input_info" style="font-size: 1.2rem;">
		<span><b>请填写你的基本信息</b>（制订体重减五公斤到增五公斤的健康打卡计划）</span>
	</div>
	<div class="select_info">
		<span class="left_icon"><img src="/static/img/clock/hicon.png" alt=""></span>
		<div class="right_info">
			<span class="info_msg">身高（CM）</span>
			<span class="info_input"><input type="number" id="height" placeholder="180"></span>
		</div>
		<div class="clear"></div>
	</div>
	<div class="select_info">
		<span class="left_icon"><img src="/static/img/clock/kgicon.png" alt=""></span>
		<div class="right_info">
			<span class="info_msg">体重（KG）</span>
			<span class="info_input"><input type="number" id="weight" placeholder="50"></span>
		</div>
		<div class="clear"></div>
	</div>
	<div class="select_info"  id="plan-picker">
		<span class="left_icon"><img src="/static/img/clock/planicon.png" alt=""></span>
		<div class="right_info">
			<span class="info_msg">计划目标（KG）</span>
			<span class="info_input"><input type="text" placeholder="5" id='plan_select' readonly/></span>
		</div>
		<div class="clear"></div>
	</div>
	<div class="button_submit active" id="submit_weight">
		<span>提交</span>
	</div>
</body>
<script type="text/html" id="picker">
	<header class="bar bar-nav" style="    height: 4rem;   line-height: 4rem;">
		<button class="button button-link pull-left"></button>
		<button class="button button-link pull-right close-picker" style=" height: 4rem;   line-height: 4rem;   font-size: 16px;">确定</button>
		<h1 class="title" style="    line-height: 4rem; height: 4rem;    font-size: 16px;">管理目标</h1>
	</header>
</script>

<script src="/static/layer/layer.js"></script>
<script type='text/javascript' src='http://g.alicdn.com/sj/lib/zepto/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='http://g.alicdn.com/msui/sm/0.6.2/js/sm.js' charset='utf-8'></script>
<script src="/static/health/js/weight.js?{$smarty.now}"></script>
<script>

</script>
</html>