<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<link rel="stylesheet" href="/static/layer/layer.css" />
	<script src="/static/layer/layer.js"></script>
	<title>健康打卡</title>
</head>
<body>
	<div class="health_banner">
		<img src="/static/health/img/{$caseDetail.pic_path}" alt="">
	</div>
	<div class="program_tp">
		<div class="programme_date">
			<div class="date"><b>Day{$dayIndex}</b></div>
			<div class="date_msg">{$caseDetail.profile}</div>
		</div>
		<div class="famous_quotes">
			{$caseDetail.dictum}
			<br>
			<div class="author_name"><span class="author">{$caseDetail.dictum_author}</span></div>
		</div>		
	</div>
	<div class="diet_detail">
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">早餐</span>
				<span class="diet_time"><img src="/static/img/clock/time.png" alt="">{$caseDetail.breakfast_time_range}</span>
			</div>
			<div class="diet_message">
                {$caseDetail.breakfast_content}
			</div>
		</div>
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">加餐</span>
				<span class="diet_time"><img src="/static/img/clock/time.png" alt="">{$caseDetail.breakfast_extra_time_range}</span>
			</div>
			<div class="diet_message">
                {$caseDetail.breakfast_extra_content}
			</div>
		</div>
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">午餐</span>
				<span class="diet_time"><img src="/static/img/clock/time.png" alt="">{$caseDetail.lunch_time_range}</span>
			</div>
			<div class="diet_message">
                {$caseDetail.lunch_content}
			</div>
		</div>
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">加餐</span>
				<span class="diet_time"><img src="/static/img/clock/time.png" alt="">{$caseDetail.lunch_extra_time_range}</span>
			</div>
			<div class="diet_message">
                {$caseDetail.lunch_extra_content}
			</div>
		</div>
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">晚餐</span>
				<span class="diet_time"><img src="/static/img/clock/time.png" alt="">{$caseDetail.supper_time_range}</span>
			</div>
			<div class="diet_message">
                {$caseDetail.supper_content}
			</div>
		</div>

        {if $caseDetail.drink_content}
			<div class="diet_list">
				<div class="diet_type">
					<span class="type_msg">饮水</span>
				</div>
				<div class="diet_message">
                    {$caseDetail.drink_content}
				</div>
			</div>
        {/if}

        {if $caseDetail.sleep_content}
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">睡眠</span>
			</div>
			<div class="diet_message">
                {$caseDetail.sleep_content}
			</div>
		</div>
        {/if}
        {if $caseDetail.sport_content}
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">运动</span>
			</div>
			<div class="diet_message">
                {$caseDetail.sport_content}
			</div>
		</div>
        {/if}
        {if $caseDetail.notice}
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg">注意事项</span>
			</div>
			<div class="diet_message">
                {$caseDetail.notice}
			</div>
		</div>
        {/if}

        {if $caseDetail.service}
		<div class="diet_list">
			<div class="diet_type">
				<span class="type_msg type_service">健康服务推荐</span>
			</div>
			<div class="diet_message">
                {$caseDetail.service}
			</div>
		</div>
		{/if}
	</div>
	<div class="complete_read {if $status == 1}done{/if} ">
		{if $status == 0}
			<span id="complete_read">已阅读，进行打卡</span>
		{else}
			<span id="clock_done">已打卡</span>
		{/if}
	</div>
	{if $showHint > 0 }
	<div class="pop_content">
		<div class="pop_shade"></div>
		<div class="pop_main">
			<div class="pop_section">
				<div class="pop_child">
					<img src="/static/img/clock/popplan.png" alt="">
					<div class="closeicon" id="closeicon">
						<img src="/static/img/clock/closeicon.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	{/if}
</body>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script data-main="/static/health/js/clock.js" src="/static/script/require.min.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: '{$signPackage.timestamp}',
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: '{$shareInfo.title}', // 分享标题
            link: '{$shareInfo.link}', // 分享链接
            imgUrl: '{$shareInfo.icon}', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: '{$shareInfo.title}', // 分享标题
            desc: '{$shareInfo.desc}', // 分享描述
            link: '{$shareInfo.link}', // 分享链接
            imgUrl: '{$shareInfo.icon}', // 分享图标
            success: function () {

            }
        });
    });
</script>
</html>