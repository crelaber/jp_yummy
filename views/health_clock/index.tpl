<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<script src="/static/script/jquery-2.1.1.min.js"></script>
	<title>健康打卡</title>
</head>
<body>
	<input type="hidden" value="{$lowLevelLastId}" id="lastid1">
	<input type="hidden" value="{$highLevelMaxLastId}" id="lastid2">
	<input type="hidden" value="{$uid}" id="uid">
	<div class="health_banner">
		<a href="{$exchangePage}">
			<img src="/static/img/clock/clock_banner.jpg" alt="" >
		</a>
	</div>
	<div class="clock_type"><span class="type_msg">健康打卡</span></div>
	<div class="clock_link">
		<div class="clock_img">
			<a href="{$lowLevelPage}"><img src="/static/img/clock/primary.png" alt=""></a>
			<!--判断当天是否初级打卡--><a href="?/vHealthClock/result" style="display: none;"><img src="/static/img/clock/primary.png" alt=""></a>
		</div>
		<div class="clock_img">
			<a href="{$highLevelPage}"><img src="/static/img/clock/senior.png" alt=""></a>
		</div>
	</div>
	<div class="clock_type"><span class="type_msg">我的称号</span></div>
	<div class="low_name">
        {foreach $titles as $key => $title}
		<div class="clockname">
			<div class="v_grade v{$title.level}">
				<span>v{$title.level}</span>
			</div>
			<div class="v_name">
                {$title.title}
			</div>
		</div>
        {/foreach}
	</div>

	<div class="note_msg">
		<span>通过积极参与，才能快速获得称号哦~</span>
	</div>
	<div class="clock_type"><span class="type_msg">打卡记录</span></div>
	<div class="type_tab">
		<ul>
			<li class="low">
				<span id="pri_clock" class="my_clock active">初级打卡</span>
			</li>
			<li class="high">
				<span id="sen_clock" class="my_clock">高级打卡</span>
			</li>
		</ul>
	</div>
	<div class="clock_content">
		<div class="clock_list" id="primary_clock">
			<ul>		
			</ul>
		</div>
		<div class="clock_list hidden" id="senior_clock">
			<ul>
				{if $currentLevel < 2}
				<img src='/static/img/clock/no_open.png' alt=''>
				{/if}
			</ul>
		</div>
		<div id="list-loading" style="display: none;"><img src="/static/img/clock/ajax-loader.gif" width="30"></div>
		</div>

	<script type="text/html" id="clock_list">
		<li>
			<div class="list_num">
				<span class="">[num]</span>
				<!-- <span class="no_grade">[num]</span> -->  <!--未打卡需包含no_grade的class-->
			</div>
			<div class="list_detail">
				<span class="date">[date]</span>
				<span class="clock_dt">[msgtype][index]天</span>
			</div>
			<div class="list_state">
				<span class="state">[state]</span>
				<span class="time">[time]</span>
			</div>
			<div class="clear"></div>
		</li>
	</script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script data-main="/static/health/js/clock.js?v={$smarty.now}" src="/static/script/require.min.js"></script>
	<script type="text/javascript">
        var config = {
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
        };

        wx.config(config);
        var shareInfo = {
            title: '{$shareInfo.title}', // 分享标题
            link: '{$shareInfo.link}', // 分享链接
            imgUrl: '{$shareInfo.icon}', // 分享图标
		};
		wx.ready(function () {
			// 在这里调用 API
            wx.onMenuShareTimeline({
                title: '{$shareInfo.title}', // 分享标题
                link: '{$shareInfo.link}', // 分享链接
                imgUrl: '{$shareInfo.icon}', // 分享图标
                success: function () {

                }
            });
            wx.onMenuShareAppMessage({
                title: '{$shareInfo.title}', // 分享标题
                desc: '{$shareInfo.desc}', // 分享描述
                link: '{$shareInfo.link}', // 分享链接
                imgUrl: '{$shareInfo.icon}', // 分享图标
                success: function () {

                }
            });
		});
	</script>
</body>
</html>