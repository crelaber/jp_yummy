<html lang="zh-CN">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="/static/layer/layer.css" />
    <script src="/static/layer/layer.js"></script>
    <title>初级打卡</title>
    <style>
        html {
            overflow: hidden;
        }
    </style>
    <!--动态的加载css-->
    <link rel="stylesheet" href="/static/css/health_clock/low_level.css">

</head>
<body style="overflow: hidden;">
    <input type="hidden" id="day_index" value="{$dayIndex}">
    <input type="hidden" id="platform" value="qiezi">
    <input type="hidden" id="base_url" value="{$docroot}">
    <input type="hidden" id="question_type" value="question">
    <input type="hidden" value="{$resultPage}" id="resultPage">
    <input type="hidden" value="{$homePage}" id="homePage">
    <input type="hidden" value="{$challengeIndex}" id="challengeIndex">
    <div class="container pos-start" style="height:100%;">
        <div class="start_content">
            <div class="start_top">
                <img src="../../static/img/clock/start.png" alt="">
            </div>
            <div class="bottom_body">
                <div class="bottom_num">
                    你是今天第<b>{$challengeIndex}</b>个挑战者
                </div>
                <div class="start-btn btn"><img src="../../static/img/clock/start-btnlv.png"/></div>
                <div class="partner">
                    本挑战由中英人寿联合中粮数字健康科技（北京）有限公司提供
                </div>
            </div>
        </div>
        <div class="process blk">
            <div class="quiz-blk">
                <div class="center-box quiz-top-box">
                    <p class="quiz-top"><span>第{$showDay}天水果打卡</span></p>
                </div>
                <div class="center-box quiz-desc-box">
                    <p class="quiz-desc"></p>
                </div>
            </div>
            <div class="opt-blk sub-blk">
                <a href="javascript:;" class="right-btn opt-btn">正确</a>
                <a href="javascript:;" class="wrong-btn opt-btn">错误</a>
            </div>

        </div>

        
    </div>
    <div class="process blk">
    <div class="pop_box answer-blk" style="display: none;">
        <div class="pop_shade"></div>
        <div class="pop_main">
            <div class="pop_section">
                <div class="pop_child">
                    <div class="close" id="complete_clock"><img src="../../static/img/clock/close_box.png" alt=""></div>
                    <div class="answer_title">
                        <p class="right-sign sign show">回答正确</p>
                        <p class="wrong-sign sign">回答错误</p>
                    </div>
                    <div class="anser_content">
                        <p class="quiz-text"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script href="/static/script/Wshop/jquery-2.1.1.min.js"></script>
    <script src="/static/script/healthClock/config.js"></script>
    <script src="/static/script/healthClock/share_data.js"></script>
    <!--动态的加载题库-->
    <script src="/static/script/healthClock/question_lib_{$circle}.js"></script>
    <!--加载结果页的配置js-->
    <script src="/static/script/healthClock/common_util.js"></script>
    <script src="/static/script/healthClock/getInstall.js"></script>
    <script src="/static/script/healthClock/core.js"></script>
    <script src="/static/script/healthClock/question_util.js"></script>
    <script src="/static/script/healthClock/wx_util.js"></script>
    <script src="/static/script/healthClock/main.js"></script>
   <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: '{$signPackage.timestamp}',
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: '{$shareInfo.title}', // 分享标题
            link: '{$shareInfo.link}', // 分享链接
            imgUrl: '{$shareInfo.icon}', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: '{$shareInfo.title}', // 分享标题
            desc: '{$shareInfo.desc}', // 分享描述
            link: '{$shareInfo.link}', // 分享链接
            imgUrl: '{$shareInfo.icon}', // 分享图标
            success: function () {

            }
        });
    });
</script>
</body>
</html>