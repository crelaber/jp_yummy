<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="/static/css/health_clock/low_level.css">
	<title>打卡结果</title>
</head>
<body style=" background: #94cc14;">
	<div class="result blk">
            <div class="result-blk">
                <div class="result-heading">
                    <div class="result_img">
                        <img src="../../static/img/clock/resimg.png" alt="">
                    </div>
                    <div class="res_msg">
                        恭喜你
                    </div>
                    <div class="res_note">
                        {if $finishStatus}
                            完成整个水果营养挑战打卡计划，获得中英健康日（每月15日）抢领奖品的资格!
                        {else}
                            完成今天的挑战，打卡成功!<br/>您还有{$diffDay}天任务待完成，记得坚持打卡哦！酷炫礼品等你拿！
                        {/if}
                    </div>
                    <div class="result_sure">
                        <a href="{$homePage}"><img src="../../static/img/clock/res_sure.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>

    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript">
        var config = {
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
        };
        wx.config(config);
        wx.ready(function () {
            // 在这里调用 API
            wx.onMenuShareTimeline({
                title: '{$shareInfo.title}', // 分享标题
                link: '{$shareInfo.link}', // 分享链接
                imgUrl: '{$shareInfo.icon}', // 分享图标
                success: function () {

                }
            });
            wx.onMenuShareAppMessage({
                title: '{$shareInfo.title}', // 分享标题
                desc: '{$shareInfo.desc}', // 分享描述
                link: '{$shareInfo.link}', // 分享链接
                imgUrl: '{$shareInfo.icon}', // 分享图标
                success: function () {

                }
            });
        });
    </script>
</body>
</html>