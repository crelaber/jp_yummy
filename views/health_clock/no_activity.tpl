<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<title>活动未开始</title>
</head>
<body>
	<style>
		.activity_note p b{ color: #000000; font-weight: normal; }
	</style>
	<div class="health_banner">
		<img style="display: block;" src="/static/img/clock/banner.jpg" alt="" >
	</div>
	<div class="activity_note">
		<p><b>•	参与规则</b><br><br>为了帮助您了解并养成健康的生活习惯，坚持合理、科学的饮食及运动习惯 ，我们邀请您坚持完成打卡任务，完成任务后您将有希望达成健康控重目标，并有机会在每月一次的中英健康日抢领到各种酷炫的健康礼品！</p>
		<p><b>•	礼品开放时间</b><br><br>每月的15日中午12:30 正式开始！<br>
		限时抢领，先抢先得！</p>
		<p><b>•	参与对象</b><br><br>只要您完成任意一项健康打卡计划，即有资格参加中英健康日抢领礼品</p>
	</div>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: [
                'hideMenuItems',
                'hideOptionMenu',
                'hideAllNonBaseMenuItem'
            ]
        });
        wx.ready(function () {
            wx.hideMenuItems({
                menuList: [
                    'menuItem:share:timeline',
                    'menuItem:favorite'
                ],
            });
            wx.hideOptionMenu();
            wx.hideAllNonBaseMenuItem();
        });
	</script>
</body>
</html>