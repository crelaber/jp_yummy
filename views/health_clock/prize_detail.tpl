<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <link rel="stylesheet" href="/static/css/home.css" />
    <link rel="stylesheet" href="/static/layer/layer.css" />
    <link rel="stylesheet" href="/static/css/commodity.css" />
    <link rel="stylesheet" href="/static/css/mobile.css" />
    <link rel="stylesheet" href="/static/css/clock.css">
    <style type="text/css">
        .shopping-cart {
            width: 100%;
            float: left;
            background-color: #000000;
            box-sizing: border-box;
            padding: 0 10px;
        }
    </style>
    <title>{$prizeInfo.prize_name}</title>
</head>
<style>body{ background-color: #f0f1ee; }</style>
<body>
    <input type="hidden" value="{$prizeInfo.id}" id="prize_id" />
    <input type="hidden" value="{$showImg}" id="show_img" />
    <div class="comm-detail">
        {if $showImg}
        <div class="addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    {foreach from=$prizeInfo.images item=img}
                        {if $img['image_id'] != ''}
                            <div><img  class="img-responsive" src="/uploads/zy_hch/prize_img/{$img.image_path}" alt=""></div>
                        {/if}
                    {/foreach}
                </div>
                <ul id="position">
                    {foreach from=$images item=img}
                        {if $img['image_id'] != ''}<li class="cur"></li>{/if}
                    {/foreach}
                </ul>
            </div>
        </div>
        {/if}
        <div class="commodity-body" data-instock="1000000"  data-p="{$prizeInfo.id}" data-sp="{$specs[0].id}"  data-hash = "p{$prizeInfo.id}m{$specs[0].id}">
            <div class="feedify">
                <div class="feedify-item">
                    <div class="proInfor detail_proinfor feedify-item-header" >
                        <div class="prointroduction">{$prizeInfo.prize_name}</div>
                        <div class="proprice" data-p="{$prizeInfo.id}">
                            <span class="leftprice">
                                <b class="n">{$prizeInfo.prize_desc}</b>
                            </span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="feedify-item-body">
                        <div class="pro-information">
                            <div class="inforhead">商品信息</div>
                            <div class="infor_list">
                                <ul>
                                    <li>
                                        <span class="leftinfor">【品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牌】</span>
                                        <span class="rightinfor">{$prizeInfo.prize_brand}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【产&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地】</span>
                                        <span class="rightinfor">{$prizeInfo.place}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【规&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格】</span>
                                        <span class="rightinfor">{$prizeInfo.specification}</span>
                                    </li>
                                    <li>
                                        <span class="leftinfor">【温馨提示】</span>
                                        <span class="rightinfor">{$prizeInfo.tips}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="commodity-infor" id="vpd-content"></div>
                    </div>
                </div>
            </div>
            <!--购物车不为空 -->
            {if $hasPermission && $isPrizeValid > 0}
            <div class="body-footer cartlist">
                <div class="exchange_pro">
                    <span class="pro_price"></span>
                    <span class="freight"><b>立即领取</b></span>
                </div>
            </div>
            {/if}
        </div>
        {if $from!='preview'}
        <a href="/?/vHealthClock/exchange"><div class="backtohome"><img src="/static/images/backcircle.png" alt=""></div></a>
        {/if}
        <div class="to_top hidden"></div>
        <script src='/static/script/Wshop/swipe.js' type="text/javascript"></script>
        <script src="/static/script/jquery-2.1.1.min.js"></script>
        <script src="/static/layer/layer.js"></script>
        <script type="text/javascript" src="/static/script/healthClock/exchange.js?v={$smarty.now}"></script>
        <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
        <script type="text/javascript">
            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: '{$signPackage.timestamp}',
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: [
                    'hideMenuItems',
                    'hideOptionMenu',
                    'hideAllNonBaseMenuItem'
                ]
            });
            wx.ready(function () {
                wx.hideMenuItems({
                    menuList: [
                        'menuItem:share:timeline',
                        'menuItem:favorite'
                    ],
                });
                wx.hideOptionMenu();
                wx.hideAllNonBaseMenuItem();
            });
        </script>
</body>
</html>