<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="../../static/css/clock.css">
	<title>限时领取</title>
</head>
<body style="background: #f7f7f7;">
	<div class="pro_list">
		<ul>
            {foreach from=$prizeList item=prize}
				{if $prize.prize_type == 1}
				<li data-prize-id="{$prize.id}" class="{if $prize.stock <= 0}sold-out{/if}">
					<div class="pro_sale">
						<div class="pro_image" style="background-image: url('{$prize.cover_img}')">
							<div class="pro_main">
							<span class="pro_name">{$prize.prize_name}</span>
							</div>
						</div>
						<div class="sale_now">
							{if $prize.stock <= 0}已抢光{else}立即领取{/if}
						</div>
					</div>
				</li>
				{elseif $prize.prize_type == 2}
					<li data-prize-id="{$prize.id}" class="{if $prize.stock <= 0}sold-out{/if}">
						<div class="pro_sale">
							<div class="pro_image" style="background-image:url(/static/img/clock/coupon1.png)">
								<div class="conpon_con">
									<div class="coupon_type">满减券</div>
									<div class="coupon_detail">满<b>{$prize.satisfy_money/100}</b>减<b>{$prize.coupon_money/100}</b></div>
									{*<div class="coupon_condition">(中粮数字健康官微商城)</div>*}
								</div>
							</div>
							<div class="sale_now">
                                {if $prize.stock <= 0}已抢光{else}立即领取{/if}
							</div>
						</div>
					</li>
				{elseif $prize.prize_type == 3}
					<li data-prize-id="{$prize.id}" class="{if $prize.stock <= 0}sold-out{/if}">
						<div class="pro_sale">
							<div class="pro_image" style="background-image:url(/static/img/clock/coupon2.png)">
								<div class="conpon_con">
									<div class="coupon_type">{$prize.prize_name}</div>
									<div class="coupon_detail"><b>{$prize.coupon_value/100}</b>元折扣券</div>
									<div class="coupon_condition">(全场通用，特价商品除外)</div>
								</div>
							</div>
							<div class="sale_now">
                                {if $prize.stock <= 0}已抢光{else}立即领取{/if}
							</div>
						</div>
					</li>
				{elseif $prize.prize_type == 4}
					<li data-prize-id="{$prize.id}" class="{if $prize.stock <= 0}sold-out{/if}">
						<div class="pro_sale">
							<div class="pro_image" style="background-image: url('/static/img/clock/coupon3.png'">
								<div class="coupon_one">
									<div class="coupon_num">￥<b>{$prize.shop_card_value/100}</b>购物卡</div>
									<div class="coupon_use">无门槛使用</div>
								</div>
							</div>
							<div class="sale_now">
                                {if $prize.stock <= 0}已抢光{else}立即领取{/if}
							</div>
						</div>
					</li>
				{else}
				{/if}
			{/foreach}
			{*<li>
				<div class="pro_sale">
					<div class="pro_image" style="background-image: url(/static/img/clock/coupon3.png)">
						<div class="conpon_con">
							<div class="coupon_type">满减券</div>
							<div class="coupon_detail">满<b>99</b>减<b>20</b></div>
							<div class="coupon_condition">(中粮我买网)</div>
						</div>
					</div>
					<div class="sale_now">
                        立即领取
					</div>
				</div>
			</li>*}

			<div class="clear"></div>
		</ul>
	</div>
	{if $showMore == 1}
	<div class="user_form">如您是中英客户，进行身份绑定后可领取更多奖品</div>
	<div class="more_button">

		<a href="{$bindUrl}"><span>点击绑定</span></a>
	</div>
	{/if}
	<script src="/static/script/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="/static/script/healthClock/prizeList.js"></script>
	<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script type="text/javascript">
        wx.config({
            debug: false,
            appId: '{$signPackage.appId}',
            timestamp: '{$signPackage.timestamp}',
            nonceStr: '{$signPackage.nonceStr}',
            signature: '{$signPackage.signature}',
            jsApiList: [
                'hideMenuItems',
                'hideOptionMenu',
                'hideAllNonBaseMenuItem'
            ]
        });
        wx.ready(function () {
            wx.hideMenuItems({
                menuList: [
                    'menuItem:share:timeline',
                    'menuItem:favorite'
                ],
            });
            wx.hideOptionMenu();
            wx.hideAllNonBaseMenuItem();
        });
	</script>
</body>
</html>