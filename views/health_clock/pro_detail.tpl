<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<link rel="stylesheet" href="/static/layer/layer.css" />
	<script src="/static/layer/layer.js"></script>
	<title>商品详情</title>
</head>
<body>
	<div class="comm-detail">
<!--         <div class="commodity-img">
            <img src="{if $config.usecdn}{$config.imagesPrefix}product_hpic/{$images[0].image_path}_x500{else}{$config.productPicLink}{$images[0].image_path}{/if}"></div> -->
        <div class="addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    <div>
                        <img  class="img-responsive" src="/uploads/product_hpic/{$img.image_path}" alt="">

                    </div>
                </div>
                <ul id="position">
                	<li class="cur"></li>
            	</ul>
            </div>
        </div>    
        <div class="commodity-body">
        <div class="feedify">
            <div class="feedify-item">
            <div class="proInfor detail_proinfor feedify-item-header" >
                <div class="prointroduction">中粮代餐粉</div>
                <div class="proprice" data-p="{$productInfo.product_id}">
                    <span class="leftprice">
                        <b class="n">78</b>
                        <b class="y">元/件</b>
                        <s>￥98</s>
                    </span>
                        <div class="add_pro msadd_pro">
                            <div class="hidden" style="display: none;">
                            <i class="proicon icon-minus"></i>
                            <b class="num">0</b>
                             <i class="proicon icon-plus"></i>
                            </div>
                           
                            <div class="addcart {$cartCss} {if $productInfo.sale_tips }cartred{/if}">加入购物车</div>
                        </div>
                    </div>
                <div class="clear"></div>
            </div>
            <div class="feedify-item-body">
            <div class="pro_introduction">
                <div class="c_code"><img src="{$docroot}static/img/cheerslife_c.jpg" alt=""></div>
                <div class="rightintro">
                <div class="introText"><span class="intronote">商品介绍：</span><span class="introcon">{$productInfo.product_subtitle}</span></div>
                <div class="introText"><span class="intronote">适宜人群：</span><span class="introcon">{$productInfo.people}</span></div>
                </div>
                <div class="clear"></div>
            </div>
             <div class="pro-information">
                <div class="inforhead">商品信息</div>
                <div class="infor_list">
                    <ul>
                        <li>
                            <span class="leftinfor">【品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牌】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【产&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【储存方法】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【规&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【保&nbsp;&nbsp;质&nbsp;&nbsp;期】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【使用方法】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【快递信息】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【服务信息】</span>
                            <span class="rightinfor"></span>
                        </li>
                        <li>
                            <span class="leftinfor">【温馨提示】</span>
                            <span class="rightinfor"></span>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="advantage"><img src="/static/img/home/advantage.jpg" alt=""></div>
            <div class="commodity-infor" id="vpd-content"></div>
            </div>
            </div>
            
           
        </div>
        <!--购物车不为空 -->
            <div class="body-footer cartlist">
                <div class="shopping-cart">
                    <span class="carticon">
                    </span>
                    <div class="price_total">
                        <span class="pro_price">200元</span>
                        <span class="freight">
                            运费: <b>1元</b>
                        </span>
                    </div>
                </div>
                <div id="buy" class="settlement">立即结算</div>
            </div>
            <!--购物车为空 -->
            <!-- <div class="body-footer empty" >
                <div class="shopping-cart empty">
                    <span class="carticon">
                    </span>
                    <div class="price_total empty">
                        <span class="pro_price">0.00元</span>
                        <span class="freight">
                            运费: <b>0.00元</b>
                        </span>
                    </div>
                </div>
                <div  class="settlement empty">立即结算</div>
            </div>   -->              

        <div class="cart-list" style="display:none;">
            <img src="/static/img/mouse-ground.png" class="cartlist-bg"/>
            <div class="list-header" >
                <span class="header-left">商品列表</span>
                <span class="header-right">
                    <i class="proicon icon-empty"></i>
                    清空购物车
                </span>
            </div>
            <ul id="cart_list"></ul>
        </div>
    </div>
    <a href=""><div class="backtohome"><img src="/static/images/backcircle.png" alt=""></div></a>
    <div class="to_top hidden"></div>
</body>
</html>