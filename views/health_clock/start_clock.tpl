<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="/static/css/clock.css">
	<title>健康打卡</title>
</head>
<body>
	<div class="start_img">
		<img src="/static/img/clock/start_img.jpg" alt="">
	</div>
	<div class="start_button">
		<a href="{$startPage}"><span>开始打卡</span></a>
	</div>
	<div class="partner">
		本挑战由中英人寿联合中粮数字健康科技（北京）有限公司提供
	</div>
</body>
</html>