<ul>

    {if $isFirst == 1}
        {if $isHaveMs == 1}
    <div class="ms_title">
        <p class="toptitle"><span class="cattitle">每日秒杀</span></p>
        <p class="mstime"><span class="timeend" >
                                 距离结束时间：
                                <i class="time_end"></i>
                            </span></p>
                            </div>
        {/if}
        {foreach from=$products2 item=pd}

            <li data-stock=100000 class="miaosha">
                <div class="proDetail">
                    <div class="probanner"  onclick="location = '{$docroot}?/vProduct/view/id={$pd.product_id}&catId={$cat_id}showwxpaytitle=1';">
                        <img src="{$docroot}static/img/home/blank.jpg" data-echo="/uploads/product_hpic/{$pd.catimg}" alt=""></div>
                    <div class="proInfor" data-p="{$pd.product_id}" data-time="{$pd.kill_time}" data-sp="{$pd['pinfo'].id}"  data-hash = "p{$pd.product_id}m{$pd['pinfo'].id}">
                        <div class="panic_buying"> <!--秒杀包邮-->
							<span class="msicon"> <i class="proicon icon-msicon"></i>
								秒杀包邮
							</span>

                        </div>
                        <div class="prointroduction">{$pd.product_name}</div>
                        <div class="proprice">
							<span class="leftprice">
								<b class="n">{$pd['pinfo'].sale_price|string_format:"%.2f"}</b>
								<b class="y">元/件</b>
								<s>￥{$pd['pinfo'].market_price}</s>
							</span>
                            {if $pd.is_kill != '1' &&  $pd.product_online != '0'}
                            <div class="add_pro">
                                {if $pd.product_quantity == '0'}
                                <div class="hidden" style="display: none;">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>

                                <div class="addcart {$cartCss}">加入购物车</div>

                                {else}

                                <div class="hidden">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss}"  style="display: none;">加入购物车</div>
                                {/if}
                            </div>
                            
                        </div>
                        {/if}
                    </div>
                </div>
            </li>
        {/foreach}
    <div class="ms_title">
        <p class="toptitle"><span class="cattitle">精选推荐</span></p>
        <p class="mstime">100%正品，来自全球直采</p>
    </div>

    {foreach from=$products item=pd}

        <li  data-stock=100000>
            <div class="proDetail">
                <div class="probanner"  onclick="location = '{$docroot}?/vProduct/view/id={$pd.product_id}&catId={$cat_id}&showwxpaytitle=1';">
                    <img  src="{$docroot}static/img/home/blank.jpg" data-echo="/uploads/product_hpic/{$pd.catimg}" alt=""></div>
                <div class="proInfor" data-p="{$pd.product_id}" data-sp="{$pd['pinfo'].id}"  data-hash = "p{$pd.product_id}m{$pd['pinfo'].id}">
                    <div class="abroadbuy">
                        <span class="spot_red"><img src="{$docroot}uploads/banner/{$pd['cat']['cat_image']}" alt=""></span>
                        <span class="abroadcart">{$pd.yun_attr}</span>
                        <span class="abroaddetail">{$pd.shop_attr}</span>
                    </div>
                    <div class="prointroduction">{$pd.product_name}</div>
                    <div class="proprice">
							<span class="leftprice">
								<b class="n">{$pd['pinfo'].sale_price|string_format:"%.2f"}</b>
								<b class="y">元/件</b>
								<s>￥{$pd['pinfo'].market_price}</s>
							</span>
                        <div class="add_pro">
                            {if $pd.product_quantity == '0'}
                                <div class="hidden" style="display: none;">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>

                                <div class="addcart {$cartCss}">加入购物车</div>
                            {else}
                                <div class="hidden">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss}"  style="display: none;">加入购物车</div>

                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </li>

    {/foreach}

    {else}
    {foreach from=$products item=pd}

        <li  data-stock=100000>
            <div class="proDetail">
                <div class="probanner"  onclick="location = '{$docroot}?/vProduct/view/id={$pd.product_id}&catId={$cat_id}&showwxpaytitle=1';">
                    <img  src="{$docroot}static/img/home/blank.jpg" data-echo="/uploads/product_hpic/{$pd.catimg}" alt=""></div>
                <div class="proInfor" data-p="{$pd.product_id}" data-sp="{$pd['pinfo'].id}"  data-hash = "p{$pd.product_id}m{$pd['pinfo'].id}">
                    <div class="abroadbuy">
                        <span class="spot_red"><img src="{$docroot}uploads/banner/{$pd['cat']['cat_image']}" alt=""></span>
                        <span class="abroadcart">{$pd.yun_attr}</span>
                        <span class="abroaddetail">{$pd.shop_attr}</span>
                    </div>
                    <div class="prointroduction">{$pd.product_name}</div>
                    <div class="proprice">
							<span class="leftprice">
								<b class="n">{$pd['pinfo'].sale_price|string_format:"%.2f"}</b>
								<b class="y">元/件</b>
								<s>￥{$pd['pinfo'].market_price}</s>
							</span>
                        <div class="add_pro">
                            {if $pd.product_quantity == '0'}
                                <div class="hidden" style="display: none;">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>

                                <div class="addcart {$cartCss}">加入购物车</div>
                            {else}
                                <div class="hidden">
                                    <i class="proicon icon-minus"></i>
                                    <b class="num">{$pd.product_quantity}</b>
                                    <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss}"  style="display: none;">加入购物车</div>

                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </li>

    {/foreach}
    {/if}

</ul>

