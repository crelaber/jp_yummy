<div class="sendtime_note">国内满200包邮，国外满300包邮</div>
<ul>
    {foreach from=$products item=pd}
    <li cat-data="cat-03" data-stock=100000 >
        <div class="pro-img" onclick="location = '{$docroot}?/vProduct/view/id={$pd.product_id}&showwxpaytitle=1';">
            <img src="/uploads/product_hpic/{$pd.catimg}" />
        </div>
        <div class="pro-detail" onclick="location = '{$docroot}?/vProduct/view/id={$pd.product_id}&showwxpaytitle=1';">
            <div class="pro-title">
                <div class="titleleft">
                    <span class="title-text" id="product_name" >{$pd.product_name}</span>
                    {if $pd['brand'].brand_name != ''}
                    <span class="pro-limit">{$prefix}{$pd['brand'].brand_name}
                    </span>
                    {/if}
                    <span class="express">
                        {if $pd.yun_fa == 1}
                            日本直邮
                        {else if $pd.yun_fa == 2}
                            保税直发
                        {else if $pd.yun_fa == 3}
                            国内快递
                        {/if}
                    </span>

                   <!--
                    <span class="sold-out">{$prefix}已售罄</span>

                    -->
                </div>
            </div>
            <div class="pro-intru" data-p="{$pd.product_id}" data-sp="{$pd['pinfo'].id}"  data-hash = "p{$pd.product_id}m{$pd['pinfo'].id}" >{$pd.product_subname}</div>
           <!--
            <div class="effects">
                <div class="lefteff">功效</div>
                <span class="effectdetail">美容养颜</span>
            </div>

            -->
        </div>
        <div class="pro-price">
            <span class="price-num" >
                {if $pd['pinfo'].sale_price != $pd['pinfo'].market_price}
                    {if $pd.product_cat == '119'}
                        <span style="color:#fd0000">秒杀价:<b>&yen;</b>{$pd['pinfo'].sale_price|string_format:"%.2f"}</span>
                {else}
    <b>&yen;</b>{$pd['pinfo'].sale_price|string_format:"%.2f"}
                    {/if}

                <s style="color: #9a9a9a;font-size: 16px;"><b>&yen;</b>{$pd['pinfo'].market_price|string_format:"%.2f"}</s>
                {else}
               {if $pd.product_cat == '119'}
                    <span style="color:#fd0000">秒杀价:<b>&yen;</b>{$pd['pinfo'].sale_price|string_format:"%.2f"}</span>
                {else}
    <b>&yen;</b>{$pd['pinfo'].sale_price|string_format:"%.2f"}
                {/if}
                {/if}
            </span>
            <div class="buy-num">
            <!-- 未点单隐藏style-->

                {if $pd.product_quantity == '0'}
                <div class="hidden" style="display:none;"><i class="proicon icon-minus"></i><span class="num">{$pd.product_quantity}</span><i class="proicon icon-plus" ></i></div><div class="addtocart"><img src="{$docroot}static/img/addcarticon.png" alt=""></div>
                {else}
                <div class="hidden" ><i class="proicon icon-minus"></i><span class="num">{$pd.product_quantity}</span><i class="proicon icon-plus" ></i></div><div class="addtocart"  style="display:none;"><img src="{$docroot}static/img/addcarticon.png" alt=""></div>
                {/if}

            </div>
            <div class="clear"></div>
        </div>
    </li>
    {/foreach}
</ul>
  