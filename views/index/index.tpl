<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>{$title}</title>
	<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
	<link rel="stylesheet" href="{$docroot}static/css/home.css" />
	<link rel="stylesheet" href="{$docroot}static/css/commodity.css" />
	<link rel="stylesheet" href="{$docroot}static/font/fonticon_new.css" />
	<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
	<script src="{$docroot}static/script/Wshop/jquery-2.1.1.min.js"></script>
	<script src='{$docroot}static/script/Wshop/swipe.js' type="text/javascript"></script>
	<script src="{$docroot}static/layer/layer.js"></script>
	<link  rel="stylesheet" herf=="{$docroot}static/css/echo.css">
	<script src="{$docroot}static/script/echo.min.js"></script>
</head>
	<style>body{ background-color: #f0f1ee; }</style>
<body>
<input type="hidden" value="{$catId}" id="cat" />
<input type="hidden" value="{$endTime}" id="endTime" />
<input type="hidden" value="{$docroot}" id="domain" />
	{if !$is_activity}
	<div class="toptab">
		<ul>
            {foreach $topcat as $key => $val}

                    {if $val.cat_id == $catId}
                        <li class="{if {$key} == 0}miaosha{/if}">
                            <span class="tab active" data-catid="{$val.cat_id}">{$val.cat_name}</span>
                        </li>
                    {else}
                        <li class="{if {$key} == 0}miaosha{/if}">
                            <span class="tab" data-catid="{$val.cat_id}">{$val.cat_name}</span>
                        </li>
                    {/if}
            {/foreach}
		</ul>
	</div>
	<div class="emptyheight" style="height: 44px;"></div>
    {/if}
	<div class="addWrap bannertop">
		<div class="swipe" id="mySwipe">
	        <div class="swipe-wrap">

                {foreach $banners as $key => $val}
	        	<div>
	        	<a href="{$val['banner_href']}">
	        		<img  class="img-responsive" src="{$docroot}uploads/banner/{$val['banner_image']}" alt=""></a>
	        	</div>
                {/foreach}
                </div>
			<ul id="position">
			{foreach $banners as $key => $val}
            <li class="cur"></li>
            {/foreach}
        </ul>
		</div>
	</div>
    <div  id="rightContainer"  class="pro_list" >

	</div>
	<div class="trademark">
		<img src="../../static/img/trademark.jpg" alt="">
		<p  style="text-align: center;color: #999999;font-size: 8px;margin-top: -10px;">沪ICP备14047489号-2</p>
		<p style="text-align: center;color: #999999;font-size: 8px;margin-top: -10px;">工信部网站（www.miitbeian.gov.cn）</p>
	</div>
	<div style="text-align: center">沪ICP备14047489号-2 工信部网站（www.miitbeian.gov.cn）</div>
	<div class="cart-list" style="display: none;">
		<img src="{$docroot}static/img/mouse-ground.png" class="cartlist-bg"/>
		<div class="list-header" >
			<span class="header-left">商品列表</span>
			<span class="header-right"> <i class="proicon icon-empty"></i>
				清空购物车
			</span>
		</div>
        <ul id="cart_list"><!--参照原来的-->

        </ul>
	</div>
	<!--购物车不为空 -->
	<div class="body-footer cartlist">
		<div class="shopping-cart">
			<span class="carticon">
			</span>
			<div class="price_total">
				<span class="pro_price">{$total|string_format:"%.2f"}元</span>
				<span class="freight">
					运费: <b>{$yun|string_format:"%.2f"}元</b>
				</span>
			</div>
		</div>
		<div id="buy" class="settlement">立即结算</div>
	</div>
	<!--购物车为空 -->
	<div class="body-footer empty" >
		<div class="shopping-cart empty">
			<span class="carticon">
			</span>
			<div class="price_total empty">
				<span class="pro_price">0.00元</span>
				<span class="freight">
					运费: <b>0.00元</b>
				</span>
			</div>
		</div>
		<div  class="settlement empty">立即结算</div>
	</div>
</body>
<script>
	$(function(){
		if($(".toptab ul li.miaosha .tab").hasClass('active')){
			$(".addWrap").show();
		}
		else{
			$(".addWrap").hide();
		}
		$(".toptab ul li").click(function(){ //选择分类
			$(".toptab ul li .tab").removeClass("active");
			$(this).find(".tab").addClass("active");
			if(!$(this).hasClass("miaosha")){
				$(".addWrap").hide();
			}
			else{
				$(".addWrap").show();
			}
		});
		$(window).on('scroll',function(){
		var sT = $(this).scrollTop()

		if(sT > 0){
		$(".toptab").css('boxShadow','0 5px 5px rgba(0,0,0,.1)')
		}else{
		$(".toptab").css('boxShadow','')
		}

	})
	});


</script>
<script type="text/javascript">
    var bullets = document.getElementById('position').getElementsByTagName('li');

    var banner = Swipe(document.getElementById('mySwipe'), {
                       auto: 3000,
                       continuous: true,
                       disableScroll:false,
                       callback: function(pos) {
                       var i = bullets.length;
                       while (i--) {
                       bullets[i].className = ' ';
                       }
                       bullets[pos].className = 'cur';
                       }
                       });

</script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: {$signPackage.timestamp},
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: 'CheersLife全球直采-海外直购，正品保障，100%营养健康', // 分享标题
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: 'CheersLife全球直采', // 分享标题
            desc: '营养师专注搜罗全球健康好货，海外直购，正品保障，100%营养健康，100%吃的放心', // 分享描述
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}&catId=', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
    });
</script>
<script data-main="{$docroot}static/script/Wshop/index.js?v={$cssversion}" src="{$docroot}static/script/require.min.js"></script>
</html>