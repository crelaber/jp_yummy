<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
<link rel="stylesheet" href="{$docroot}static/css/commodity.css" />
<link rel="stylesheet" href="{$docroot}static/css/bootstrap.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon_new.css" />

<script src="{$docroot}static/layer/layer.js"></script>
<script src="{$docroot}static/iscroll/iscroll.js"></script>
 <script>
                var myscroll;
				var obj=new Object();
				obj.vScrollbar=false;	
                function loaded(){
                           myscroll=new iScroll("wrapper",obj);
                 }
               window.addEventListener("DOMContentLoaded",loaded,false);
         </script>
<title>CheersLife健康商城</title>
</head>

<body>
    <input type="hidden" value="{$catId}" id="cat" />
    <input type="hidden" value="{$show_tip}" name="show_tip" />
    <div class="product-body">




<img src="{$docroot}static/img/bottom-mask.png" class="bot-mask" style="display:none;">

  <div class="body-header">

    <div class="left-cat" id="wrapper">
      <ul>
        {foreach $topcat as $key => $val}
        {if {$key} == 0}
        <li class="cat-a active" cat-data="cat-01"><span class="cat-num">{$val['count']}</span><a class="cat-btn" data-catid="{$val.cat_id}">{$val.cat_name}</a></li>
        {else}
        <li  class="cat-c" cat-data="cat-03"><span class="cat-num">{$val['count']}</span><a class="cat-btn" data-catid="{$val.cat_id}">{$val.cat_name}</a></li>
        {/if}
        {/foreach}
        
      </ul>
    </div>
    <div  id="rightContainer"  class="right-pro" >
    
      <div id="list-loading" style="display: block; padding-top:50px;"><img src="{$docroot}static/images/icon/loading.gif" width="30"></div>
    </div>
  </div>
  <div class="body-footer">
    <div class="shopping-cart"><span class="cart-icon"><i class="proicon icon-cart"></i></span><span class="number">{$count}</span><span class="cart-price">&yen;{$total|string_format:"%.2f"}</span><span class="send-price"></span></div>
    <div class="settlement" id="buy">结算</div>
  </div>
  <div class="cart-list" style="display:none;"> <img src="{$docroot}static/img/mouse-ground.png" class="cartlist-bg"/>
    <div class="list-header" ><span class="header-left">商品列表</span><span class="header-right"><i class="proicon icon-empty"></i>清空购物车</span></div>
    <ul id="cart_list">
    </ul>
  </div>
</div>

<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: {$signPackage.timestamp},
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: 'CheersLife健康商城-营养师为您精选', // 分享标题
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: 'CheersLife健康商城', // 分享标题
            desc: '营养师为您精选的营养健康产品', // 分享描述
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
    });
</script>
<script data-main="{$docroot}static/script/Wshop/index.js?v={$cssversion}" src="{$docroot}static/script/require.min.js"></script>
</body>
</html>
