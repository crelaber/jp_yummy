<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
		<meta name="format-detection" content="telephone=no" />
        <link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
        <link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
        <link rel="stylesheet" href="{$docroot}static/font/fonticon_new.css" />
		<link rel="stylesheet" href="{$docroot}static/css/commodity.css" />
		<link rel="stylesheet" href="{$docroot}static/css/bootstrap.css" />
        <script src="{$docroot}static/script/Wshop/jquery-2.1.1.min.js"></script>
        <title>Cheerslife健康轻食</title>
    </head>
    <style>
    .prompt-title img {
        width: 100%;
    }
    </style>
    <body  style="background-color:#C9B68C;">
        <div class="prompt-title">
            <img src="{$docroot}static/img/titleimg.png" />
        </div>
        <div class="store_con">
            <div class="stores_list">
                {foreach $stores as $val}
                <div class="store-item" data-val="{$val.id}" data-name="{$val.store_name}">
                    <span class="lj_text">{$val.store_name}<br>{$val.range_desc}</span>
                    <span class="select-btn"><i class="proicon icon-select"></i></span>
                </div>
                {/foreach}
            </div>
        </div>
    </body>
</html>
<script data-main="{$docroot}static/script/Wshop/setStore.js?v={$cssversion}" src="{$docroot}static/script/require.min.js"></script>