<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
	<meta content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
	<title>订单评价</title>
	<meta name="description" content="订单评价" />
	<link rel="stylesheet" href="{$docroot}static/css/comment.css">
	<script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>

	<script type="text/javascript"> 
	window.onload = function (){
	var oStar = document.getElementById("star");
	var commentTag = document.getElementById("commentTag");
	var aLi = oStar.getElementsByTagName("li");
	var oUl = oStar.getElementsByTagName("ul")[0];
	var oSpan = oStar.getElementsByTagName("span")[1];
	var oP = oStar.getElementsByTagName("p")[0];
	var i = iScore = iStar = 5;

	var tagMsg =['<span>态度恶劣</span><span>送餐速度慢</span><span>味道差</span><span>酱料有异味</span><span>份量太少</span>','<span>态度服务好</span><span>送餐速度快</span><span>味道好</span><span>食材新鲜</span><span>酱料美味</span><span>份量足</span>']
	commentTag.innerHTML=tagMsg[1];
	checkTag();
	for (i = 1; i <= aLi.length; i++){
		aLi[i - 1].index = i;
		
		//鼠标移过显示分数
		aLi[i - 1].onmouseover = function (){
			fnPoint(this.index);
			if(this.index>=1&&this.index<=3){
			commentTag.innerHTML=tagMsg[0];
		}else{
			commentTag.innerHTML=tagMsg[1];
		}
			checkTag();
			iStar = this.index;
			//浮动层显示
			oP.style.display = "block";
			//计算浮动层位置
			oP.style.left = oUl.offsetLeft + this.index * this.offsetWidth - 104 + "px";

		};
		
		//鼠标离开后恢复上次评分
		aLi[i - 1].onmouseout = function (){
			//关闭浮动层
			oP.style.display = "none";	
					
		};
		
		//点击后进行评分处理
		aLi[i - 1].onclick = function (){
			iStar = this.index;
			oP.style.display = "none";


		}
	}
	
	//评分处理
	function fnPoint(iArg){
		//分数赋值
		iScore = iArg || iStar;
		for (i = 0; i < aLi.length; i++) aLi[i].className = i < iScore ? "on" : "";	
	}
	function checkTag(){
		$("#commentTag span").click(function(){
			if(!$(this).hasClass('active')){
			$(this).addClass('active');
			}
			else{
			$(this).removeClass('active');	
			}
		});
	}
	$(".submitBtn").click(function(){
		var tagCon=$("#commentTag span.active");
		var tagL=$("#commentTag span.active").length;
		var tagArr=new Array();
		for(i=0;i<tagL;i++)
		{
		tagArr[i]=tagCon.eq(i).text();
		}
		tagRu=tagArr.join(",");
		var content = $('#comment').val();
		var url = "?/Order/addComment";
		$.post(url, {
				order_id:{$order_id},
				tag:tagRu,
			    score:iStar,
			    comment:content

		 }, commentResult);
		
		
	});
	function commentResult(result){
	   
	   if(result.ret_code == 1 ){
	   	    alert('非常感谢你的评价');
	   	    window.location.href = '?/Index/index';
	   	    
	   }else if(result.ret_code == 2){
	       alert('此订单你已评价过，请勿重复评价');
	   	    window.location.href = '?/Index/index';
	   }else{
	   	  alert('失败');
	   }
	
	}

};

</script></head>
<body>
	<div class="CommentLogo">
		<img src="{$docroot}static/images/cheerslifelogo.png"></div>
	<div id="star">

		<span></span>
		<ul>
			<span>整体评价</span>
			<li class="on">
				<a href="javascript:;">1</a>
			</li>
			<li class="on">
				<a href="javascript:;">2</a>
			</li>
			<li class="on">
				<a href="javascript:;">3</a>
			</li>
			<li class="on">
				<a href="javascript:;">4</a>
			</li>
			<li class="on">
				<a href="javascript:;">5</a>
			</li>
		</ul>

		<span></span>
		<p></p>
	</div>
	<!--star end-->
	<div id="commentTag"></div>
	<div class="commentArea">
		<textarea name="Comment" id="comment" placeholder="请写下您宝贵的建议" id="saladC" cols="30" rows="10"></textarea>
	</div>
	<div class="submitBtn">提交评论</div>
</body>
</html>