<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html;charset=utf-8" />
<title>订单中心</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
<script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
        <!-- 微信JSSDK -->
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="{$docroot}static/layer/layer.js"></script>
</head>
<body style='background: #eeeeee;'>
<div class="expressdetail-body">
 <div class="express-detail">
 <div class="order_state"><span class="stateTxt">{$orderdetail.statusX}</span><span class="orderIdTxt">订单号：{$orderdetail.serial_number}</span></div>
 <div class="order-pro-list order-list">
 <ul class="express">
     {foreach $orderProviderList as $key => $val}

     <li class="expressN">
         <a href="{if $val['express'] != '' && $val['express']['express_name'] != ''}{$docroot}?/Order/lookExpress/express_no={$val['express']['express_no']}&express_num={$val['express']['express_num']}{/if}">
         <div class="toptag">

             {if $val['express'] != '' && $val['express']['express_name'] != ''}
                 <span class="linktolog">查看物流</span>
               <span class="logisticsNum">{$val['express']['express_name']} {$val['express']['express_num']}</span>

           {else}
               <span class="logisticsNum">{$val['yun']['express_name']} {$val['mSupplier']['city']} 发货</span>

           {/if}

           <span class="logisticsdetail">{$val['yun_cost']} 元</span>
         </div>
        
         {foreach $val['product_list'] as $k_p => $v_p}
         <div class="order_list">
             <div class="proimgleft">
               <img src="/uploads/product_hpic/{$v_p['catimg']}" alt="" />
             </div>
             <div class="rightpro">
               <p class="pro-name">{$v_p['product_name']}</p>
               <p class="proprice">￥{$v_p['product_discount_price']}&times;{$v_p['product_count']}</p>
             </div>
             <div class="clear"></div>
             </div>
              </a>
         {/foreach}
       </li>
     {/foreach}
      </ul>
      <div class="order_price_detail">
        <span class="left">
          总计： <b>￥{$orderdetail.order_amount}</b>
        </span>
        <span class="center">
          优惠： <b>￥{round($orderdetail.order_amount-$orderdetail.pay_amount-$orderdetail.balance_amount,2)}</b>
        </span>
        <span class="right">
          实付：
          <b>￥{$orderdetail.pay_amount}</b>
        </span>
        <div class="clear"></div>
      </div>
        </div>
  <div class="send-detail">
 <div class="sendto sendhone"><span class="left">下单时间</span><span class="right">{$orderdetail.order_time}</span></div>
 <div class="sendto sendname"><span class="left">联系人</span><span class="right">{$address.user_name}</span></div>
 <div class="sendto sendhone"><span class="left">联系电话</span><span class="right">{$address.phone}</span></div>
     {if $address.identity != ''}
     <div class="sendto sendhone"><span class="left">身份证</span><span class="right">{$address.identity}</span></div>
     {/if}
     <div class="sendto sendhone"><span class="left">联系地址</span><span class="right">{$address.province}{$address.city}{$address.area}{$address.address}</span><div class="clear"></div></div>
 <div class="sendto sendhone"><span class="left">备注</span><span class="right">{$orderdetail.notes}</span><div class="clear"></div></div>

 </div>
 <!-- 未支付-->
  {if $orderdetail.status == "unpay"}
 <div class="payment-btn" onclick="javascript:Orders.reWePay({$orderdetail.order_id});">立即支付</div>
 </div>
 {/if}

    {if $group != ""}
    <div class="payment-btn" onclick="location = '{$docroot}?/Group/grouping/id={$group.id}';">查看拼团</div>
{/if}
 
 </div>
</div>
<!-- <div class="sendhb"></div> -->
  <script type="text/javascript">
            WeixinJSBridgeReady(ExpressDetailOnload);
        </script>
   <script type="text/javascript">

         var istuan = {$isTuan};
         var url = "{$base_url}?/Index/index/from_uid={$uinfo.uid}";
         var title = "CheersLife，我们只做最好的营养健康产品";
         var desc = "营养师为您挑选的营养健康产品";
         if(istuan == 1){
             url = "{$base_url}?/Group/grouping/id={$group.id}";
         }
            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: {$signPackage.timestamp},
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: ['chooseWXPay','onMenuShareTimeline', 'onMenuShareAppMessage']
            });


            wx.ready(function () {
                // 在这里调用 API
                wx.onMenuShareTimeline({
                    title: title, // 分享标题
                    link: url, // 分享链接
                    imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
                    success: function () {

                    }
                });
                wx.onMenuShareAppMessage({
                    title: title, // 分享标题
                    desc: desc, // 分享描述
                    link: url, // 分享链接
                    imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
                    success: function () {

                    }
                });
            });
   </script>
        <script data-main="{$docroot}static/script/Wshop/order.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script> 
</body>
</html>
