<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
	<title>物流信息</title>
</head>
<body style="background-color: #eeeeee;">
	<div class="logistics_infor">
		<div class="logisticsimg"><img src="{$express['url']}" alt=""></div>
		<div class="logisticstxt">
			<p><span class="stateleft">物流状态</span>&nbsp;&nbsp;&nbsp;<span class="stateright">
                    {if $state == 2}
                       在途中
                    {/if}
                    {if  $state == 3}
                       已签收
                    {/if}
                    {if $state == 4}
                       问题件
                    {/if}
                    </span></p>
			<p>承运来源：{$express['name']}</p>
			<p>运单编号：{$express_num}</p>
			<p>官方电话：<b>{$express['tel']}</b></p>
		</div>
	</div>
	<div class="logistics_progress">
		<ul>
            {foreach  $list as $key => $val}

            <li class="cur">
				<i></i>
				<p>{$val->AcceptStation}</p>
				<small>{$val->AcceptTime}</small>
			</li>
            {/foreach}

        </ul>
	</div>
</body>
</html>