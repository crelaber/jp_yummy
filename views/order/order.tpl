<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="format-detection" content="telephone=no" />  
    <meta name="format-detection" content="email=no" /> 
<link rel="stylesheet" type="text/css" href="{$docroot}static/city_select/mobile-select-area.css">
<link rel="stylesheet" type="text/css" href="{$docroot}static/city_select/dialog.css">
<script type="text/javascript" src="{$docroot}static/city_select/zepto.min.js"></script>
<script type="text/javascript" src="{$docroot}static/city_select/dialog.js"></script>
<script type="text/javascript" src="{$docroot}static/city_select/mobile-select-area.js"></script>
<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
<script language="javascript" src="{$docroot}static/layer/layer.js"></script>
<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon_new.css" />
<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
<link rel="stylesheet" href="{$docroot}static/css/commodity.css" />
<script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
<link rel="stylesheet" href="{$docroot}static/css/bootstrap.css" />
<!--<script language="javascript" src="{$docroot}static/script/fastclick.js"></script>-->
<title>CheersLife健康商城</title>
</head>

<body style="background-color:#eeeeee;">
<input type="hidden" id="addressId" value="{$address.id}" />
<input type="hidden" id="userCouponId" value="{$couponId}" />
<input type="hidden" id="orderCouponId" value="{$orderCoupons.id}" />
<input type="hidden" id="isbalance" value="{$isbalance}" />
<input type="hidden" id="fromUid" value="{$fromUid}" />
<input type="hidden" id="pid" value="{$pid}" />
<input type="hidden" id="isTuan" value="{$isTuan}" />
<input type="hidden" id="groupId" value="{$groupId}" />


<div class="order-detail-body"> 
  <div class="order-detail" >
  <div class="orderinfor">

  {if $address}
    <div class="order-address" id="update-address">
      <div class="addressleft"><i class="iconfont icon-local"></i></div>
      <div class="addresslist"><p>收货人：{$address.user_name}&nbsp;{$address.phone}</p><p>收货地址：{$address.province}{$address.city}{$address.area}{$address.address}</p><p>
          {if $address.identity != ''}
          <p>身份证号：{$address.identity}</p>
          {/if}
      <span class="modify-icon"><i class="proicon icon-right"></i></span></div>
      </div>
      {else}
      <div class="link_add" id="update-address" style="text-align:center;" > <span class="modify-icon"><i class="proicon icon-right"></i></span><span style="color: #CAB68C; font-size:18px;">点击添加地址</span></div>
      {/if}
      </div>
    <div class="order-list">
      <ul class="express">
          {foreach $provider_list as $key => $val}
       <li class="expressN">
         <div class="toptag">
           <span class="package">包裹 {$key+1}</span>
           <span class="expressmode">{$val['mSupplier'].city}&nbsp;&nbsp;&nbsp;发货</span>
         </div>
           {foreach $val['productList'] as $k => $v}

            <div class="order_list">
             <div class="proimgleft">
               <img src="/uploads/product_hpic/{$v['pinfo'].catimg}" alt="" width="40px" height="40px" />
             </div>
             <div class="rightpro">
               <p class="pro-name">{$v.product_name}</p>
               {if $v.is_tuan == 1}
                   <p class="proprice">￥{$v['group_price']|string_format:"%.2f"}&times;1</p>
               {/if}
                 {if $v.is_tuan == 2}
                     <p class="proprice">￥{$v['pinfo'].sale_prices|string_format:"%.2f"}&times;1</p>

                 {/if}
               {if $v.is_tuan == ""}
                   <p class="proprice">￥{$v['pinfo'].sale_prices|string_format:"%.2f"}&times;{$v.product_quantity}</p>

                {/if}
             </div>
             <div class="clear"></div>
             </div>
           {/foreach}

           <div class="bottag">
           <span class="botleft">{$val['yun'].express_name}</span>
           <span class="botright">快递费：￥{$val['yun_cost']}</span>
         </div>
       </li>
          {/foreach}
      </ul>
    </div>
  </div>
  <div class="order-detail1">

    
    {if $coupon}
     <div class="detail-modify discount" id="discount_money"><span class="detail-modify-left">可扣除金额</span><span class="modify-icon"><i class="proicon icon-right"></i></span><span class="detail-modify-right" style=" color:#C9B68C;">
        
        {if $coupon.discount_type == 1}
          {$coupon.discount_val/10}折
        {else}
             -￥{$coupon.discount_val/100}
        {/if}
    
   
       </span>
       </div>
    {else}
        {if $userCoupons}
    <div class="detail-modify discount" id="discount_money"><span class="detail-modify-left">可用优惠券</span><span class="modify-icon"><i class="proicon icon-right"></i></span><span class="detail-modify-right">

    
           {count($userCoupons)}张可用


   
       </span>
    </div>
    {*
    {else}
      <div class="detail-modify discount"  ><span class="detail-modify-left">可用优惠券</span><span class="modify-icon"></span><span class="detail-modify-right">
          无 
       </span>
    </div>
    *}
     {/if}
    {/if}
   

  <div class="detail-modify discount"><span class="detail-modify-left">订单优惠   {if $orderCoupons}   |  {$orderCoupons.coupon_name}{/if}</span><span class="modify-icon"></span><span class="detail-modify-right"> {if $orderCoupons}
     <b style=" font-weight:400; color:#C9B68C;">-￥{$orderCoupons.coupon_value}</b>
    {else}
          无
       {/if}

       </span></div>
    <div class="balance"><span class="detail-modify-left">余额</span>&nbsp;<span class="balance-num">￥{$userInfo.balance}</span></div>
  </div>
  

  <!-- <div class="detail-footer">
    <div class="price-all">共计：&yen;{$amount|string_format:"%.2f"}</div>
    <div class="settle-btn" id="pay">结算</div>
  </div> -->
    <div class="order_note"><input id="user_note" maxlength="100" placeholder="如您还有其他要求，请在此添加备注（100字内）" /></div>
    <div class="submit_order">
    <span class="allprice">总计：<b>￥{$totalamount|string_format:"%.2f"}</b></span>
    <span class="actualprice">实付：<b>￥{$pay_amount|string_format:"%.2f"}</b></span>
    <button class="settlement_order" id="pay">付款</button>
  </div>
</div>
     <script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>

        <script type="text/javascript">
            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: {$signPackage.timestamp},
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: ['chooseWXPay']
            });

        </script>
        <script data-main="{$docroot}static/script/Wshop/order.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script>     
</body>
</html>


