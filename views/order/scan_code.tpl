<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
        <title></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link href=".{$docroot}static/css/order_success.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
    </head>
    <style>

    </style>
    <body style='background: #ffffff;'>
        <div class="header_ss">
            <div class="order_ss">下单成功</div>
            <div class="order_note">可在个人中心中查看订单</div>
       </div>
        <div class="article_ss">
            <div class="toptt">联系客服</div>
            <div class="middleimg"><img src="../../static/images/service_code.jpg" alt=""></div>
            <div class="bottt">长按【识别二维码】联系客服</div>
        </div>
        <div class="footer_ss">
        <a href="{$docroot}">
        <div class="footbutton continue_sale">
            继续逛逛
        </div>
        </a>
         <a href="?/Uc/home">
         <div class="footbutton usercenter">
            个人中心
        </div> 
        </a>     
        </div>
    </body>

</html>