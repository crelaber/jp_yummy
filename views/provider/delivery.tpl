<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<title>发货</title>
	<link rel="stylesheet" href="../../static/css/mobile.css" />
	<link rel="stylesheet" href="../../static/font/fonticon_new.css" />
    <link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
    <script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
    <script data-main="{$docroot}static/script/Wshop/order.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script>
	<script src="{$docroot}static/layer/layer.js"></script>
</head>
<body style="background-color: #eeeeee;">
	<div class="delivery_infor">
        <input type="hidden" value="{$order_id}" id="orderId" />
        <input type="hidden" value="{$provider_id}" id="providerId" />
        <input type="hidden" value="{$url}" id="url" />

        <div class="delivery_select">
			<span class="left">配送方式</span>
			<span class="rightsec">
				<select name="express" id="express">
                    {foreach $expressList as $key => $val}
                       <option value="{$val['id']}" {if $val['id'] eq $yun['express_id']}selected{/if}>{$val['name']}</option>
                    {/foreach}
				</select>
			</span>
			<span class="rightarrow"> <i class="iconfont icon-open"></i>
				</span>
		</div>
		<div class="delivery_num">
			<span class="left">运单号</span>
            <input class="rightnum" id="num" type="text" placeholder="请填写快递单号"/>

		</div>
	</div>
	<div class="delivery_submit" id="send">发货</div>
</body>
<script type="application/javascript">



</script>
</html>