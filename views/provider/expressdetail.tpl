<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv=Content-Type content="text/html;charset=utf-8" />
<title>订单中心</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
<script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="static/script/main.js?v={$cssversion}"></script>
        <!-- 微信JSSDK -->
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="{$docroot}static/layer/layer.js"></script>
</head>
<body style='background: #eeeeee;'>
<div class="expressdetail-body">

 <div class="express-detail">
 <div class="order_state"><span class="orderIdTxt">订单号：{$order.serial_number}</span></div>
 <div class="order-pro-list order-list">
 <ul class="express">


     <li class="expressN">
         {foreach $order['products'] as $k_p => $v_p}

         <div class="order_list">
             <div class="proimgleft">
               <img src="/uploads/product_hpic/{$v_p['catimg']}" alt="" />
             </div>
             <div class="rightpro">
               <p class="pro-name">{$v_p['product_name']}</p>
               <p class="proprice">￥{$v_p['product_discount_price']}&times;{$v_p['product_count']}</p>
             </div>
             <div class="clear"></div>
             </div>
         {/foreach}
       </li>
      </ul>

        </div>
  
 <div class="send-detail">
 
 <div class="sendto sendhone"><span class="left">下单时间</span><span class="right">{$order.order_time}</span></div>
 <div class="sendto sendname"><span class="left">联系人</span><span class="right">{$address.user_name}</span></div>
 <div class="sendto sendhone"><span class="left">联系电话</span><span class="right">{$address.phone}</span></div>
     {if $address.identity != ''}
     <div class="sendto sendhone"><span class="left">身份证</span><span class="right">{$address.identity}</span></div>
     {/if}
     <div class="sendto sendhone"><span class="left">联系地址</span><span class="right">{$address.province}{$address.city}{$address.area}{$address.address}</span><div class="clear"></div></div>
    <div class="sendto sendhone"><span class="left">备注</span><span class="right">{$order.notes}</span></div>

 </div>

<a href="{$docroot}?/Order/sendProviderExpress/order_id={$order['order_id']}&provider_id={$provider_id}"> <div class="payment-btn">发货</div> </a>

 
 </div>
</div>

  <script type="text/javascript">
            WeixinJSBridgeReady(ExpressDetailOnload);
        </script>
   <script type="text/javascript">
            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: {$signPackage.timestamp},
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: ['chooseWXPay','onMenuShareTimeline', 'onMenuShareAppMessage']
            });


            wx.ready(function () {
                // 在这里调用 API
                wx.onMenuShareTimeline({
                    title: 'CheersLife，我们只做最好的营养健康产品', // 分享标题
                    link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
                    imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
                    success: function () {

                    }
                });
                wx.onMenuShareAppMessage({
                    title: 'CheersLife，我们只做最好的营养健康产品', // 分享标题
                    desc: '营养师为您挑选的营养健康产品', // 分享描述
                    link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
                    imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
                    success: function () {

                    }
                });
            });
   </script>
        <script data-main="{$docroot}static/script/Wshop/order.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script> 
</body>
</html>
