<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
    <title>发货中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
    <script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>


</head>
<body style='background: #eeeeee;'>

<div id="uc-orderlist" class="uc-order-list order-list">

    <ul class="express">


        {foreach $orders as $key => $val}
         {if $val['issend'] == 0}


      <a href="{$docroot}?/Order/providerOrderDetail/order_id={$val['order_id']}&provider_id={$val['provider_id']}">
      <li class="orderid_list">
      <div class="order_state"><span class="orderIdTxt">订单号：{$val['serial_number']}</span></div>
        <ul>
          <li class="expressN">
              {foreach $val['product_list'] as $k_p => $v_p}
            <div class="order_list">
              <div class="proimgleft">
                <img src="/uploads/product_hpic/{$v_p['catimg']}" alt="" />
              </div>
              <div class="rightpro">
                <p class="pro-name">{$v_p['product_name']}</p>
                <p class="proprice">￥{$v_p['product_discount_price']}&times;{$v_p['product_count']}</p>
              </div>
              <div class="clear"></div>
            </div>
              {/foreach}
          </li>

        </ul>
        <div class="order_price_detail">

        <div class="clear"></div>
      </div>
      </li>
      </a>
     {/if}
{/foreach}
   </ul>
</div>


{if $empty == 0}
<div class="noorder"><img src="{$docroot}static/img/no_order.png" alt=""></div>
{/if}


 <div class="send-detail">
<div id="list-loading" style="display: none;"><img src="{$docroot}static/images/icon/spinner-g-60.png" width="30"></div>

</body>

