<html>
    <head>
        <meta name="viewport" content="width=640"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport"/>
        <title>转盘抽奖</title>
        <link rel="stylesheet" href="{$docroot}static/font/turnplate/iconfont.css"/>
        <link rel="stylesheet" href="{$docroot}static/layer/layer.css"/>
        <link rel="stylesheet" href="{$docroot}static/css/wshop_turnplate.css"/>
        <script type="text/javascript" src="{$docroot}static/layer/layer.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/lib/jQueryRotate.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/turnplate/index.js"></script>
    </head>
    <body>
    <input type="hidden"  id="turnplate_angle" />
    <input type="hidden"  id="times"/>
    <input type="hidden"  id="order_id" value="{$order_id}"/>
    <div class="turn_title">
        <div class="3th">3000元</div>
        <img src="{$docroot}static/images/turnplate/noteimg.png"/>
    </div>
    <div class="rotate-con-pan">
        <img class="turnimg" src="{$docroot}static/images/turnplate/turntable.png"/>
        <div class="rotate-con-zhen"><img src="{$docroot}static/images/turnplate/pointer.png"/></div>
        <div class="trun_bot"><img src="{$docroot}static/images/turnplate/bot.png"/></div>
    </div>
    {*中奖结果*}
    <div class="draw_result"></div>
    <div class="winning_list">
        <div class="list_title"><img src="{$docroot}static/images/turnplate/namelist.png"/></div>
        <ul id="winner_list">
        </ul>
    </div>
</html>