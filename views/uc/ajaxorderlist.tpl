{foreach $orders as $key => $val}
    <!-- <li>
      <div class="orderlist" >
        <div class="orderCon" onclick="location = '{$docroot}?/Order/expressDetail/order_id={$orders[oi].order_id}';">
        <div class="order-number"><span class="order-num">订单号：{$orders[oi].serial_number}</span><span class="order-state"><b>&bull;</b>{$orders[oi].statusX}</span></div>
        <div class="pro-list">
         {section name=di loop=$orders[oi]['data']}
          <p><span class="title-pro">{$orders[oi]['data'][di].product_name}</span><span class="pro-price">&yen;{$orders[oi]['data'][di].product_discount_price}</span><span class="pro-num">×{$orders[oi]['data'][di].product_count}</span></p>
          {/section}
        </div>
        </div>
        <div class="order-pay"><span class="pay-left">总计：</span><span class="pay-num">&yen;{$orders[oi].pay_amount}</span>

        </div>
      </div>

    </li> -->
    <ul class="express">
      <li class="orderid_list">
      <a href="{$docroot}?/Order/expressDetail/order_id={$val['order_id']}">
      <div class="order_state"><span class="stateTxt">{$val['statusX']}</span><span class="orderIdTxt">订单号：{$val['serial_number']}</span></div>
        <ul>
          {foreach $val['orderProviderList'] as $k => $v}
          <li class="expressN">
            
              <div class="toptag">
                  {if $v['express'] != '' && $v['express']['express_name'] != ''}
                      <span class="logisticsNum">{$v['express']['express_name']} {$v['express']['express_num']}</span>

                  {else}
                      <span class="logisticsNum">{$v['yun']['express_name']} {$v['mSupplier']['city']} 发货</span>

                  {/if}
                  <span class="logisticsdetail">{$v['yun_cost']} 元</span>

              </div>


            
              {foreach $v['product_list'] as $k_p => $v_p}
            <div class="order_list">
            <div class="groupstate">
                {if $val['group'] != ''}

                    {if $val['group']['status'] == -1}
                        <img src="{$docroot}static/img/home/failicon.png" alt="">
                    {/if}

                    {if $val['group']['status'] == 0}
                        <img src="{$docroot}static/img/home/grouping.png" alt="">
                    {/if}

                    {if $val['group']['status'] == 1}
                        <img src="{$docroot}static/img/home/success.png" alt="">
                    {/if}
                {/if}

                {if $val['group'] == "" && $val['is_tuan'] == 1}

                <!--   <img src="{$docroot}static/img/home/dkt.jpg" alt="">-->

                {/if}

                <!-- 团成功--><!-- 团失败--><!--  --><!-- 拼团中--><!--  --></div>
              <div class="proimgleft">
                <img src="/uploads/product_hpic/{$v_p['catimg']}" alt="" />
              </div>
              <div class="rightpro">
                <p class="pro-name">{$v_p['product_name']}</p>
                <p class="proprice">￥{$v_p['product_discount_price']}&times;{$v_p['product_count']}</p>
              </div>
              <div class="clear"></div>
            </div>
              {/foreach}
          </li>
            {/foreach}
        </ul>
        <div class="order_price_detail">
        <span class="left">
          总计： <b>￥{$val['order_amount']}</b>
        </span>
        <span class="center">
          优惠： <b>￥{round($val['order_amount']-$val['pay_amount']-$val['balance_amount'],2)}</b>
        </span>
        <span class="right">
          实付：
          <b>￥{$val['pay_amount']}</b>
        </span>
        <div class="clear"></div>
      </div>
      </a>
      </li>
{/foreach}


