<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<link rel="stylesheet" type="text/css" href="{$docroot}static/city_select/mobile-select-area.css">
<link rel="stylesheet" type="text/css" href="{$docroot}static/city_select/dialog.css">
        <script type="text/javascript" src="{$docroot}static/city_select/zepto.min.js"></script>
        <script type="text/javascript" src="{$docroot}static/city_select/dialog.js"></script>
        <script type="text/javascript" src="{$docroot}static/city_select/mobile-select-area.js"></script>
 <link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
<link rel="stylesheet" href="{$docroot}static/css/bootstrap.css" />
<script data-main="{$docroot}static/script/Wshop/address.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script> 
<script src="{$docroot}static/layer/layer.js"></script>
<title>Cheerslife健康轻食</title>
</head>

<body style="background-color:#eeeeee;">
<script >
//var selectArea = new MobileSelectArea();
</script>
<input type="hidden" id="couponId" value="{$couponId}" />
<input type="hidden" id="time" value="{$time}" />
<input type="hidden" id="isbalance" value="{$isbalance}" />
<div class="fill-address-body">
<div class="detail-header"> <span class="backicon" id="back"><i class="proicon icon-back"></i></span> <span class="title-text">添加新地址</span> </div>
<div class="empty-header" style="height:51px;"></div>
<div class="fill-add-detail">
<div class="fill-name"><input placeholder="收货人姓名：" type="text" class="nametext" id="name" /></div>
<div class="fill-send-address">


</div>
<div class="send-add-detail"><input placeholder="地址：上海市-浦东新区-xx路xx号xx楼"  type="text" class="detailtext" id="address"/></div>
<div class="fill-phone"><input placeholder="手机号码："  value="{$phone}" type="text" class="phone-text" id="phone" /></div>
</div>
<div class="add-address"><div class="add_left" id="submit">确认添加</div></div>



     <script type="text/javascript">
	 $(document).ready(function(){
		 check_add();
		 })
		 
     function check_add(){
		 $("#sec_submit").click(function(){
            //$(".check_address").show();
            $('#address').val("{$store.store_address}");
        });
		$(".take_submit").click(function(){
			$(".check_address").hide();
			})
		$(".close_take").click(function(){
			$(".check_address").hide();
			})
		$(".check_address_top div").click(function(){
			 $(".check_address_top .select-btn").removeClass('active');
             $(this).children('.select-btn').addClass('active');
			 var _this=$(this);
				$("#address").val(_this.children('.select-btn').attr('data-val')); 
				$(".check_address").hide();
	
             
			 })
		 }
	 </script>
</body>
</html>
