<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport" />
  <title>个人中心</title>
  <link rel="stylesheet" href="../../static/css/yummy.css" />
  <link rel="stylesheet" href="../../static/font/fonticon.css" />
    <link rel="stylesheet" href="../../static/font/fonticon_new.css" />
</head>
<body style="background-color: #eeeeee;">
  <div class="head-sculpture">
  <div class="headercon">
   <div class="head-img">
      <img src="{$userinfo.client_head}/0" alt="用户头像">
    </div>
    <div class="inforright">
      <p class="personal-name">{$userinfo.client_nickname}</p>
      {if $userinfo.is_agent == '1'}
      <p class="identity"><span class="vipicon"></span>会员：VIP用户</p>
      {else}
      <p class="viplink"><a href="http://www.icheerslife.com/?/Gmess/view/id=3">点击链接 成为VIP会员</a></p>
      {/if}
    </div>
    </div>
   </div>

  {if $userinfo.is_agent == '1'}
  <a href="?/Uc/qa_code">
    <div class="QR-Code-block">
      <span class="QR-Code">我的二维码</span>
      <div class="QR-Code-icon">
        <span  class="QR-Code-icon-span">
          <img src="../../static/images/QR-Code-icon.png" alt=""></span >
        <span class="pullright"> <i class="iconfont icon-right"></i>
        </span>
      </div>
    </div>
  </a>
  {/if}

  <a href="?/Uc/present">
    <div class="reward-block">
      <p>历史累计奖励（元）</p>
      <div class="personal-money">
        <span class="money-num">{$userinfo.all_account}</span>
        <span class="pullright"> <i class="iconfont icon-right"></i>
        </span>
      </div>
      <div class="withdraw-cash">可提现金额{$userinfo.account}元&nbsp&nbsp&nbsp&nbsp&nbsp待入账金额:{$userinfo.will_account}</div>
    </div>
  </a>

<div class="footlink">
<ul>
    {if $userinfo.is_agent == '1'}
  <li>
    <div class="fan-block">
     <span class="openicon"><i class="iconfont icon-menuopen"></i></span>
      <span class="myfan">我的粉丝</span>
      <span class="fan-num">{$tfans}人</span>
    </div>
  <ul class="hidden">
    <li>
      <a href="?/Uc/fansList">
    <div class="fans_level">
      <span class="myfan">一级粉丝</span>
      <span class="fan-num">{$fans}人</span>
    </div>
  </a> 
    </li>

     <!--
    <li>
      <a href="?/Uc/sfansList">
    <div class="fans_level">
      <span class="myfan">二级粉丝</span>
      <span class="fan-num">{$sfans}人</span>
    </div>
  </a>
    </li>
     -->
  </ul>
  </li>


  {/if}
  <li>
  <a href="{$docroot}?/Uc/orderList/">
    <div class="fan-block couponlink">
      <span class="myfan">我的订单</span>
      <span class="pullright"> <i class="iconfont icon-right"></i>
        </span>
    </div>
    </a>
  </li>
  <li>
    <div class="fan-block couponlink">
     <span class="openicon"><i class="iconfont icon-menuopen"></i></span>
      <span class="myfan">我的优惠券</span>
      <span class="fan-num">{$couponCount}张</span>
    </div>
    <ul class="hidden">
        {section name=oi loop=$coupons}
    <li>
      <a href="{$docroot}?/Coupon/user_coupon/">
    <div class="fans_level">
      <span class="myfan">{$coupons[oi].coupon_name}</span>
     <!--  <span class="fan-num">2张</span> -->
    </div>
  </a> 
    </li>
        {/section}
  </ul>
  </li>
  </ul>
</div>

</body>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: {$signPackage.timestamp},
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: 'CheersLife全球直采-海外直购，正品保障，100%营养健康', // 分享标题
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: 'CheersLife全球直采', // 分享标题
            desc: '营养师专注搜罗全球健康好货，海外直购，正品保障，100%营养健康，100%吃的放心！', // 分享描述
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
    });
</script>
<script data-main="{$docroot}static/script/Wshop/home.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script> 
</html>