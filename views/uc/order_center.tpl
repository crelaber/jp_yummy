<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="../../static/css/yummy.css" />
	<link rel="stylesheet" href="../../static/css/mobile.css" />
	<link rel="stylesheet" href="../../static/css/commodity.css" />
	<link rel="stylesheet" href="../../static/font/fonticon.css" />
	<link rel="stylesheet" href="../../static/citypicker/dist/css/sm.css">
    <link rel="stylesheet" href="../../static/citypicker/dist/css/sm-extend.css">
    <link rel="stylesheet" href="../../static/citypicker/assets/css/demos.css">
    
    <script src="../../static/citypicker/assets/js/zepto.js"></script>
    <script src="../../static/citypicker/assets/js/config.js"></script>
    <script data-main="../../static/script/Wshop/order.js?v={$smarty.now}" src="../../static/script/require.min.js"></script> 
	<title>订单中心</title>
</head>
<body style="background-color: #eeeeee;">
	<div class="sendaddress">
		<div class="sendtop">
			<div class="sendto">送到这</div>
			<div class="sendtoadd">
				<span class="name_phone">王&nbsp;18217218243</span>
				<span class="sendtolocal">上海市松涛路556号</span>
			</div>
			<span class="addright"> <i class="iconfont icon-right"></i>
			</span>
		</div>
		<div class="sendbottm">
		<ul><li>
			<span class="protitle">商品1</span>
			<span class="proprice">￥35</span>
			<span class="pronum">×1</span>
		</li>
		<li>
			<span class="protitle">商品1</span>
			<span class="proprice">￥35</span>
			<span class="pronum">×1</span>
		</li>
		<li>
			<span class="protitle">商品1</span>
			<span class="proprice">￥35</span>
			<span class="pronum">×1</span>
		</li></ul>
		</div>
	</div>
	<div class="sendmode">
		<div class="sendway">
			<span class="leftPrompt">配送方式</span>
			<span class="fillinput">
				<input type="text" name="fillway" id="waytxt" value="EMS"></span>
			<span class="rightarrow"> <i class="iconfont icon-right"></i>
			</span>
		</div>
		<div class="sendprice"><span class="leftPrompt">运输费</span><span class="sendpriceN">100元</span></div>
		<div class="balance"><span class="leftPrompt">余额</span><span class="balanceN">100元</span><span class="yes-no"><img src="../../static/img/yes.png" /></span></div>
		<div class="sendNote"><p>1.国际满500包邮</p><p>2.国内满200包邮</p></div>
	</div>
	  <div class="body-footer">
    <div class="shopping-cart"><span class="cart-price countprice">共计：￥12</span></div>
    <div class="settlement" id="buy">结算</div>
  </div>
</body>
<script src="../../static/citypicker/dist/js/sm.js"></script>
    <script src="../../static/citypicker/dist/js/sm-extend.js"></script>
    <script src="../../static/citypicker/dist/js/sm-city-picker.js"></script>
    <script src="../../static/citypicker/assets/js/demos.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".sendway").picker({
  toolbarTemplate: '<header class="bar bar-nav">\
  <button class="button button-link pull-left close-picker">取消</button>\
  <button class="button button-link pull-right close-picker">确定</button>\
  <h1 class="title">配送方式</h1>\
  </header>',
  cols: [
    {
      textAlign: 'center',
      values: ['EMS','顺风','圆通']
    }
  ]
});
});
</script>
</html>