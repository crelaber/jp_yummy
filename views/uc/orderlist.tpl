<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv=Content-Type content="text/html;charset=utf-8" />
    <title>订单中心</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
    <script language="javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>


</head>
<body style='background: #eeeeee;'>

<div id="uc-orderlist" class="uc-order-list order-list">

</div>
<div id="list-loading" style="display: none;"><img src="{$docroot}static/images/icon/spinner-g-60.png" width="30"></div>

<div class="take-order" id="order" ><a class="order-sumbit"> 再去看看</a></div>
</body>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script type="text/javascript">
    wx.config({
        debug: false,
        appId: '{$signPackage.appId}',
        timestamp: {$signPackage.timestamp},
        nonceStr: '{$signPackage.nonceStr}',
        signature: '{$signPackage.signature}',
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
    });
    wx.ready(function () {
        // 在这里调用 API
        wx.onMenuShareTimeline({
            title: 'CheersLife，我们只做最好的营养健康产品', // 分享标题
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
        wx.onMenuShareAppMessage({
            title: 'CheersLife，我们只做最好的营养健康产品', // 分享标题
            desc: '营养师为您挑选的营养健康产品', // 分享描述
            link: '{$base_url}?/Index/index/from_uid={$uinfo.uid}', // 分享链接
            imgUrl: '{$base_url}/static/img/site_icon.jpg', // 分享图标
            success: function () {

            }
        });
    });
</script>

<script type="text/javascript">

    $('#order').click(function(){
        var url = "?/Index/index";
        window.location.href =url;
    });

</script>
<script data-main="{$docroot}static/script/Wshop/shop_orderlist.js" src="{$docroot}static/script/require.min.js"></script>
</html>