<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="{$docroot}static/css/yummy.css" />
	<title>提现页面</title>
</head>
<body style="background-color: #eeeeee;">
	<div class="present_body">
		<div class="presentimg"><img src="{$docroot}static/images/present.png" alt=""></div>
		<div class="presentmain">可提现金额</div>
		<div class="presentNum">{$uinfo.account}</div>
		<div class="presentBtn" id="doPresent">提现</div>
		<a href="?/Uc/present_history"><div class="historylink">提现历史</div></a>
	</div>
</body>
<script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="application/javascript">

    $(document).ready(function(){

        $('#doPresent').click(function(){

            var url = "{$base_url}?/Uc/do_present/uid={$uinfo.client_id}";
            $.post(url,function(data){
                var code = data.ret_code;
                var msg = data.ret_msg;
                if(code ==1){

                    $('.presentNum').html("0.00");

                }
                alert(msg);
            });
        });
    });

</script>
</html>