<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="../../static/css/yummy.css" />
	<link rel="stylesheet" href="../../static/css/mobile.css" />
	<title>提现历史</title>
</head>
<body style="background-color: #eeeeee;">

{if count($list) > 0}
	<div class="history_list">
		<ul>
            {section name=oi loop=$list}
			<li>
				<div class="history_detail">
					<span class="pricehistory">金额：{$list[oi].amount}元</span>
					<span class="presenttime">{$list[oi].add_time}</span>
					<span class="presentstate">

                         {if $list[oi].status == "0"}
                            处理中
                        {else if $list[oi].status == "1"}
                             处理成功
                        {else}
                             处理失败
                        {/if}
					</span>
					<div class="clear"></div>
				</div>
			</li>
	        {/section}
		</ul>
	</div>

{else }
    <div class="no_present"><img src="{$docroot}static/images/no_present.png" alt=""></div>
{/if}
	<!--没有提现样式-->
	<!-- <div class="no_present"><img src="{$docroot}static/images/no_present.png" alt=""></div> -->
</body>
</html>