<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <link rel="stylesheet" href="{$docroot}static/css/yummy.css" />
    <link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
    <title>我的二级粉丝</title>
</head>
<body style="background-color: #eeeeee;">

{if count($list) > 0}

    <div class="fansbody">
        <ul>

            {section name=oi loop=$list}
                <li>
                    <div class="fansdetail">
                        <div class="fans_infor">
						<span class="fansimg">
							<img src="{$list[oi].client_head}/0" alt=""></span>
                            <span class="fansname">{$list[oi].client_nickname}    |   {$list[oi]['from_user'].client_nickname} </span>
                            <span class="fanstime">{$list[oi].client_joindate}</span>
                        </div>
                        <div class="fansorder">
                            <span class="orderprice">订单金额：{$list[oi].orderMoney}</span>
                            <span class="distrprice">分销金额：{$list[oi].fansMoney}</span>
                            <span class="ordernum">订单量：{$list[oi].orderCount}</span>
                            <div class="clear"></div>
                        </div>
                    </div>
                </li>
            {/section}
        </ul>
    </div>
{else}
    <!--没有粉丝样式-->
    <div class="no_fans"><img src="{$docroot}static/images/no_fans.png" alt=""></div>

{/if}
</body>
</html>