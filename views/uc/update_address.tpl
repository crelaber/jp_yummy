<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
	<link rel="stylesheet" href="{$docroot}static/css/yummy.css" />
	<link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
	<link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
	<link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
	<script data-main="{$docroot}static/script/Wshop/address.js?v={$smarty.now}" src="{$docroot}static/script/require.min.js"></script>
	<script src="{$docroot}static/layer/layer.js"></script>
	<script src="{$docroot}static/script/jquery-1.7.2.min.js"></script>
	<script src="{$docroot}static/citypicker/Area.js" type="text/javascript"></script>
	<script src="{$docroot}static/citypicker/AreaData_min.js" type="text/javascript"></script>

	<title>修改地址</title>
	<script type="text/javascript">
		$(document).ready(function(){
			initComplexArea('provincetxt', 'citytxt', 'areatxt', area_array, sub_array, '0', '0', '0');
			var provice = '{$address.province}';			
			 var city= '{$address.city}';
			if(city.length<=0){
				city = '{$address.province}';
			}			
			 var area= '{$address.area}';
			$("#provincetxt option:contains("+provice+")").attr('selected', true);
			$("#provincetxt").change();
			$("#citytxt option:contains("+city+")").attr('selected', true);
			$("#citytxt").change();
			$("#areatxt option:contains("+area+")").attr('selected', true);
			$("#areatxt").change();

		})
</script>
</head>

<body style="background-color: #eeeeee;">
	<div class="filladdbody">
		<div class="address_sec">
			<div class="add_province">
				<span class="leftPrompt">省份</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillprovince" id="provincetxt" onChange="changeComplexProvince(this.value, sub_array, 'citytxt', 'areatxt');"></select>
				</span>
				<span class="rightarrow"> <i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_city">
				<span class="leftPrompt">城市</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillcity" id="citytxt" onChange="changeCity(this.value,'areatxt','areatxt');"></select>
				</span>
				<span class="rightarrow"> <i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_area">
				<span class="leftPrompt">区域</span>
				<span class="fillinput">
					<select  dir="rtl" name="fillarea" id="areatxt"></select>
				</span>
				<span class="rightarrow">
					<i class="iconfont icon-right"></i>
				</span>
			</div>
			<div class="add_detail">
				<span class="leftPrompt">地址</span>
				<span class="fillinput">
					<textarea  type="text" name="fillarea"  value="{$address.address}" id="fillarea" placeholder="请填写详细地址，不少于5个字">{$address.address}</textarea>
				</span>
			</div>
		</div>
		<div class="fill_infor">
			<div class="fillname">
				<span class="leftPrompt">姓名</span>
				<span class="fillinput">
					<input type="text" name="fillname" value="{$address.user_name}" id="fillname"></span>
			</div>
			<div class="fillphone">
				<span class="leftPrompt">手机号</span>
				<span class="fillinput">
					<input type="text" name="fillphone" value="{$address.phone}" id="fillphone"></span>
			</div>
			<div class="fillId">
				<span class="leftPrompt">身份证号</span>
				<span class="fillinput">
					<input type="text" placeholder="海外直邮和免税必填" name="fillid"  id="fillid"></span>
			</div>
		</div>
		<div class="fillIdnote">
			<div class="notetitle">cheerslife提醒您填写真实身份证信息</div>
			<ul>
				<li>1.请填写真实的姓名和身份证信息。</li>
				<li>2.海关需对海外购物查验身份信息，错误信息可能导致无法正常通关。</li>
				<li>3.身份证信息会加密报关，绝不外泄。</li>
			</ul>

		</div>
        <input  type="hidden" value="{$address.id}" id="id" />

		<div class="add-address" id="update">修改地址</div>
	</div>
	<!-- <div class="info">
	<div>
		<script class="resources library" src="{$docroot}static/citypicker/area.js" type="text/javascript"></script>

		<script type="text/javascript">_init_area();</script>
	</div>
	<div id="show"></div>
</div>
-->
<!--     <script type="text/javascript">
var Gid  = document.getElementById ;
var showArea = function(){
	Gid('show').innerHTML = "
<h3>
	省" + Gid('provincetxt').value + " - 市" + 	
	Gid('citytxt').value + " - 县/区" + 
	Gid('areatxt').value + "
</h3>
"
							}
Gid('areatxt').setAttribute('onchange','showArea()');
</script>
-->
</body>

</html>