<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
    <link rel="stylesheet" href="{$docroot}static/css/bootstrap.css" />
    <link rel="stylesheet" href="{$docroot}static/css/home.css" />
    <link rel="stylesheet" href="{$docroot}static/layer/layer.css" />
    <link rel="stylesheet" href="{$docroot}static/font/fonticon.css" />
    <link rel="stylesheet" href="{$docroot}static/font/fonticon_new.css" />
    <link rel="stylesheet" href="{$docroot}static/css/commodity.css" />
    <link rel="stylesheet" href="{$docroot}static/css/mobile.css" />
    <link href=".{$docroot}static/css/order_success.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
    <script src='{$docroot}static/script/Wshop/swipe.js' type="text/javascript"></script>
    <script src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
    <script src='{$docroot}static/script/Wshop/feedify.min.js' type="text/javascript"></script>
    <script src="{$docroot}static/layer/layer.js"></script>
    <title>CheersLife健康商城</title>
</head>
<style>body{ background-color: #f0f1ee; }</style>
<body>
    <input type="hidden" value="{$productid}" id="iproductId" />
    <input type="hidden" value="{$uid}" id="from_uid" />
    {if $productInfo.sale_tips }
    <div class="header_bar">
        <span class="close_bar"><img src="../../static/images/close.png" alt=""></span>
        <span class="recommender"><img src="../../static/img/01.jpg" alt=""></span>
        <span class="prompt">好友，<b>抽抽</b>推荐<br>关注公众号，享专属服务</span>
        <div class="bar_bt"><span>立即关注</span></div>
    </div>
    {/if}
    <div class="comm-detail">
<!--         <div class="commodity-img">
            <img src="{if $config.usecdn}{$config.imagesPrefix}product_hpic/{$images[0].image_path}_x500{else}{$config.productPicLink}{$images[0].image_path}{/if}"></div> -->
        <div class="addWrap">
            <div class="swipe" id="mySwipe">
                <div class="swipe-wrap">
                    {foreach from=$images item=img}
                        {if $img['image_id'] != ''}
                    <div>

                            <img  class="img-responsive" src="/uploads/product_hpic/{$img.image_path}" alt="">

                    </div>
                        {/if}
                    {/foreach}
                    </div>
                <ul id="position">

                    {foreach from=$images item=img}
                        {if $img['image_id'] != ''}
                <li class="cur"></li>
                        {/if}
                    {/foreach}
            </ul>
            </div>
        </div>    
        <div class="commodity-body" data-instock="1000000"  data-p="{$productInfo.product_id}" data-sp="{$specs[0].id}"  data-hash = "p{$productInfo.product_id}m{$specs[0].id}">
        <div class="feedify">
            <div class="feedify-item">
            <div class="proInfor detail_proinfor feedify-item-header" >
                {if $productInfo['product_cat'] != '119'}
                <div class="abroadbuy"><!--日本直邮等-->
                            <span class="spot_red"><img src="{$docroot}uploads/banner/{$cat.cat_image}" alt=""></span>
                            <span class="abroadcart">

                                {$productInfo.yun_attr}</span>
                            <span class="abroaddetail">{$productInfo.shop_attr}</span>
                        </div>
                {else}
                        <!--秒杀包邮-->
                <div class="panic_buying"  data-time="{$productInfo.kill_time}"> 
                            <span class="msicon"> <i class="proicon icon-msicon"></i>
                                秒杀包邮
                            </span>
                            <span class="timeend" >
                                {if $productInfo.is_kill == '1'}
                                     秒杀已结束
                                 {else}
                                     距离结束时间：
                                 {/if}
                                <i class="time_end"></i>
                            </span>
                    </div>
                {/if}
                <div class="prointroduction">{$productInfo.product_name}</div>
                <div class="proprice" data-p="{$productInfo.product_id}">
                {if $productInfo.sale_tips }
                    <span class="leftprice"> 
                        <b class="n">{$specs[0].sale_price|string_format:"%.2f"+20}</b> 
                        <b class="y">元/件（下单仅{$specs[0].sale_price|string_format:"%.2f"}元）</b>
                    </span>
                {else}
                    <span class="leftprice">
                        <b class="n">{$specs[0].sale_price|string_format:"%.2f"}</b>
                        <b class="y">元/件</b>
                        <s>￥{$specs[0].market_price|string_format:"%.2f"}</s>
                    </span>
                    {/if}
                    {if $productInfo['product_cat'] == '119'}
                        {if $productInfo['product_online'] != 0 && $productInfo['is_kill'] != 1}
                            <div class="add_pro msadd_pro">
                            {if $count == '0'}
                                <div class="hidden" style="display: none;">
                                <i class="proicon icon-minus"></i>
                                <b class="num">0</b>
                                 <i class="proicon icon-plus"></i>
                                </div>
                               
                                <div class="addcart {$cartCss} {if $productInfo.sale_tips }cartred{/if}">加入购物车</div>
                            {else}
                            <div class="hidden">
                                <i class="proicon icon-minus"></i>
                                <b class="num">{$count}</b>
                                <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss} {if $productInfo.sale_tips }cartred{/if}" style="display: none;">加入购物车</div>
                        {/if}
                        </div>
                        {/if}
                    {else}
                    {if $productInfo['product_online'] != 0}
                    <div class="add_pro">
                            {if $count == '0'}
                                <div class="hidden" style="display: none;">
                                <i class="proicon icon-minus"></i>
                                <b class="num">0</b>
                                <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss} {if $productInfo.sale_tips }cartred{/if}">加入购物车</div>
                            {else}
                            <div class="hidden">
                                <i class="proicon icon-minus"></i>
                                <b class="num">{$count}</b>
                                <i class="proicon icon-plus"></i>
                                </div>
                                <div class="addcart {$cartCss} {if $productInfo.sale_tips }cartred{/if}" style="display: none;">加入购物车</div>
                            {/if}
                        </div>
                    {/if}
                    {/if}
                        </div>

                <div class="clear"></div>
            </div>
            <div class="feedify-item-body">
            <div class="pro_introduction">
                <div class="c_code"><img src="{$docroot}static/img/cheerslife_c.jpg" alt=""></div>
                <div class="rightintro">
                <div class="introText"><span class="intronote">商品介绍：</span><span class="introcon">{$productInfo.product_subtitle}</span></div>
                <div class="introText"><span class="intronote">适宜人群：</span><span class="introcon">{$productInfo.people}</span></div>
                </div>
                <div class="clear"></div>
            </div>
             <div class="pro-information">
                <div class="inforhead">商品信息</div>
                <div class="infor_list">
                    <ul>
                        <li>
                            <span class="leftinfor">【品&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;牌】</span>
                            <span class="rightinfor">{$productInfo.brand_info}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【产&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;地】</span>
                            <span class="rightinfor">{$productInfo.place}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【储存方法】</span>
                            <span class="rightinfor">{$productInfo.save}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【规&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;格】</span>
                            <span class="rightinfor">{$productInfo.spec_attr}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【保&nbsp;&nbsp;质&nbsp;&nbsp;期】</span>
                            <span class="rightinfor">{$productInfo.is_valid_time}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【使用方法】</span>
                            <span class="rightinfor">{$productInfo.user_method}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【快递信息】</span>
                            <span class="rightinfor">{$productInfo.kd_attr}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【服务信息】</span>
                            <span class="rightinfor">{$productInfo.service_info}</span>
                        </li>
                        <li>
                            <span class="leftinfor">【温馨提示】</span>
                            <span class="rightinfor">{$productInfo.tips}</span>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="advantage"><img src="{$docroot}static/img/home/advantage.jpg" alt=""></div>
            <div class="commodity-infor" id="vpd-content"></div>
            </div>
            </div>
            
           
        </div>
        <!--购物车不为空 -->
            <div class="body-footer cartlist">
                <div class="shopping-cart">
                    <span class="carticon">
                    </span>
                    <div class="price_total">
                        <span class="pro_price">{$total|string_format:"%.2f"}元</span>
                        <span class="freight">
                            运费: <b>{$yun|string_format:"%.2f"}元</b>
                        </span>
                    </div>
                </div>
                <div id="buy" class="settlement">立即结算</div>
            </div>
            <!--购物车为空 -->
            <div class="body-footer empty" >
                <div class="shopping-cart empty">
                    <span class="carticon">
                    </span>
                    <div class="price_total empty">
                        <span class="pro_price">0.00元</span>
                        <span class="freight">
                            运费: <b>0.00元</b>
                        </span>
                    </div>
                </div>
                <div  class="settlement empty">立即结算</div>
            </div>                

        <div class="cart-list" style="display:none;">
            <img src="{$docroot}static/img/mouse-ground.png" class="cartlist-bg"/>
            <div class="list-header" >
                <span class="header-left">商品列表</span>
                <span class="header-right">
                    <i class="proicon icon-empty"></i>
                    清空购物车
                </span>
            </div>
            <ul id="cart_list"></ul>
        </div>
    </div>
    <a href="{$backUrl}"><div class="backtohome"><img src="{$docroot}static/images/backcircle.png" alt=""></div></a>
    <div class="to_top hidden"></div>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript">

            var name = "【精选】营养师为您挑选的{$productInfo.product_name}";
            var name2 = "【精选】CheersLife健康商城  营养师为您挑选的{$productInfo.product_name} ";
            var imgUrl = "{$base_url}{$config.productPicLink}{$images[0].image_path}";

            wx.config({
                debug: false,
                appId: '{$signPackage.appId}',
                timestamp: {$signPackage.timestamp},
                nonceStr: '{$signPackage.nonceStr}',
                signature: '{$signPackage.signature}',
                jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
            });
            wx.ready(function () {
                // 在这里调用 API


                wx.onMenuShareTimeline({
                    title: name2, // 分享标题
                    link: '{$base_url}?/vProduct/view/id={$productInfo.product_id}&showwxpaytitle=1&from_uid={$uinfo.uid}', // 分享链接
                    imgUrl: imgUrl, // 分享图标
                    success: function () {

                    }
                });
                wx.onMenuShareAppMessage({
                    title: 'CheersLife健康商城', // 分享标题
                    desc: name, // 分享描述
                    link: '{$base_url}?/vProduct/view/id={$productInfo.product_id}&showwxpaytitle=1&from_uid={$uinfo.uid}', // 分享链接
                    imgUrl: imgUrl, // 分享图标
                    success: function () {

                    }
                });
            });
        </script>
    <script type="text/javascript">
    var bullets = document.getElementById('position').getElementsByTagName('li');

    var banner = Swipe(document.getElementById('mySwipe'), {
                       auto: 3000,
                       continuous: true,
                       disableScroll:false,
                       callback: function(pos) {
                       var i = bullets.length;
                       while (i--) {
                       bullets[i].className = ' ';
                       }
                       bullets[pos].className = 'cur';
                       }
                       });

</script>
    <script data-main="{$docroot}static/script/Wshop/detail.js" src="{$docroot}static/script/require.min.js"></script>
     <script>
                    $(function() {
                  $('.feedify').feedify();
                });
     </script>
</body>
</html>