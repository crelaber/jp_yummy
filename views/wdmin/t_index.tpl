<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <title>供应商管理管理后台</title>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta name="format-detection" content="telephone=no" />
        <link href="{$docroot}favicon.ico" rel="Shortcut Icon" />
        <link href="static/css/wshop_admin_style.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="static/css/wshop_admin_index.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="static/script/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="static/css/animate.min.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="static/css/common.css?v={$cssversion}" type="text/css" rel="Stylesheet" />


        <script type="text/javascript" src="{$docroot}static/script/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="{$docroot}static/script/fancyBox/source/jquery.fancybox.pack.js"></script>

        <link href="{$docroot}static/css/wshop_admin_style.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="{$docroot}static/script/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="Stylesheet" />
        <link href="{$docroot}static/script/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="Stylesheet" />
        <link href="{$docroot}static/script/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="{$docroot}static/script/JqueryPagination/jpaginate.css" type="text/css" rel="Stylesheet" />
        <link href="{$docroot}static/script/DataTables/media/css/jquery.dataTables.min.css" type="text/css" rel="Stylesheet" />

        <i id="scriptTag">page_orders_t</i>



    </head>
    <body style="background-color: #ffffff" >
        <div id="topnav">
            <div class="in">
                <div class="left">
                    Welcome Back!{*<b>贺维Ivy</b>*}  今天是 {$today}{*，今天有<a href="{$docroot}?/Wdmin/logOut/">7</a>个订单待发货，<a href="{$docroot}?/Wdmin/logOut/">3</a>个新消息。*}
                </div>
                <div class="right clearfix">
                  <!--  {include file="./tnav.tpl"} -->
                </div>
            </div>
        </div>
        <div>
            <table class="dTable" style="margin-top:45px;">
                <thead>
                <tr>
                    <th class="hidden"></th>
                    <th class="od-exp-check"><input class="checkAll" type="checkbox" /></th>
                    <th>订单编号</th>
                    <th>收货人</th>
                    <th>收货电话</th>
                    <th>收货地址</th>
                    <th>订单金额</th>
                    <th>运费</th>
                    <th>商品名字</th>
                    <th>商品数量</th>
                    <th>商品价格</th>
                    <th>下单时间</th>
                    <th>订单状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                {section name=oi loop=$orderlist}

                    <tr id='order-exp-{$orderlist[oi].order_id}'>
                        <td class="hidden">{$orderlist[oi].order_id}</td>
                        <td class="od-exp-check"><input class='pd-exp-checks' type="checkbox" data-id='{$orderlist[oi].order_id}' /></td>
                        <td>{$orderlist[oi].serial_number}</td>
                        <td>{$orderlist[oi].address.user_name}</td>
                        <td>{$orderlist[oi].address.phone}</td>
                        <td>{$orderlist[oi].address.province}{$orderlist[oi].address.city}{$orderlist[oi].address.area}{$orderlist[oi].address.address}</td>
                        <td class="prices font12">{$orderlist[oi].order_amount}</td>
                        <td class="prices font12">{$orderlist[oi].order_yunfei}</td>
                        <td>{$orderlist[oi].product.product_name}</td>
                        <td>{$orderlist[oi].product_count} </td>
                        <td>{$orderlist[oi].product.sale_prices}</td>
                        <td>{$orderlist[oi].order_time}</td>
                        <td>{$orderlist[oi].statusX}</td>
                        <th class="gray font12">
                            <a data-orderid="{$orderlist[oi].order_id}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/addOrderNotes/id={$orderlist[oi].order_id}">发货</a>


                        </th>
                    </tr>
                {/section}

                </tbody>
            </table>
        </div>



    </body>


</html>