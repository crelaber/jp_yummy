{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/will_account.js</i>

<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th> 
            	<input type="checkbox" id="check_all"/>
            </th>
            <th>类型</th>
            <th>用户名</th>
            <th>订单号</th>
            <th>状态</th>
            <th>订单金额</th>

            <th>待入账金额</th>
            <th>时间</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">
        {section name=ls loop=$list}
            <tr>
                <input class="classId" type="hidden" id="id" value="{$list[ls].id}" />
                <td class="hidden">{$list[ls].cid}</td>
                <td>
                	 <input type="checkbox" name="check_list" data-id="{$list[ls].client_id}" data-openid="{$list[ls].client_wechat_openid}"/>
               	</td>
                <td style="color: red">

                    {if $list[ls].type == 0}
                        分销
                    {else}
                        CPS
                    {/if}
                </td>

                <td>{$list[ls].nickname}</td>
                <td>{$list[ls].order_num}</td>
                <td>{$list[ls].status}</td>
                <td>{$list[ls].amount}</td>
                <td>{$list[ls].cps_amount}</td>
                <td>{$list[ls].add_time}</td>
                <td>
                    <button class="us-edit" id="cps_account">入账</button>
                    <button class="us-edit" id="redraw_account">撤回</button>
                </td>
            </tr>
        {/section}
    </tbody>
</table>
<input type="hidden" id="client_level" value="{$client_level}">
{$pageup}{$pagedown}{$pageconfig}

<div id="page" style="margin-top:10px;float:right;"></div>

{include file='../__footer.tpl'} 