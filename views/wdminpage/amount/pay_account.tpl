{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/shop_product.js</i>

<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th>姓名</th>
            <th>提现金额</th>
            <th>状态</th>


            <th>操作</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">
        {section name=ls loop=$list}
            <tr>
                <input type="hidden" id="do_id" value="{$list[ls].id}" />
                <input type="hidden" id="do_amount" value="{$list[ls].amount}" />
                <td class="hidden">{$list[ls].cid}</td>

                <td>{$list[ls]['user'].client_name}</td>
                <td>{$list[ls].amount}</td>
                <td>
                    {if $list[ls].status == 0}
                       处理中
                     {else if $list[ls].status == 1}
                        处理成功
                    {/if}
                    </td>


                <td>
                    {if $list[ls].status == 0}

                        <button id="do_success">成功</button>
                    {/if}
                </td>
            </tr>
        {/section}
    </tbody>
</table>
<input type="hidden" id="client_level" value="{$client_level}">

<div id="page" style="margin-top:10px;float:right;"></div>
{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
    {literal}
        {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
            <tr>
                <td class="hidden">{{ d.list[i].id }}</td>
                <td>
                    <input type="checkbox" name="check_list" data-id="{{ d.list[i].cid}}" data-openid="{{ d.list[i].client_wechat_openid }}"/>
                </td>
                <td>
                    {{# if(d.list[i].client_head == ''){ }}
                        <img class='ccl-head' src='{$docroot}static/images/login/profle_1.png' />
                    {{# }else{ }}
                        <img class='ccl-head' src='{{ d.list[i].client_head }}/64' />
                    {{#  } }}
                </td>
                <td>{{ d.list[i].client_name }}</td>
                <td>{{ d.list[i].client_sex }}</td>
                <td>{{ d.list[i].client_province }} {{ d.list[i].client_city}}</td>
                <td>{{ d.list[i].client_credit }}</td>
                <td>
                    {{# if(d.list[i].order_count == 0){ }}
                        {{ d.list[i].order_count}}
                    {{# }else{ }}
                        <a href="?/WdminPage/customer_profile/id={{ d.list[i].cid}}">{{ d.list[i].order_count}}</a>
                    {{#  } }}
                </td>
                <td>
                    {{# if(d.list[i].levelname && ''!=d.list[i].levelname && null!=d.list[i].levelname){ }}
                        {{ d.list[i].levelname}}
                    {{#  } }}
                </td>
                <td>
                    <a class="us-edit" href="?/WdminPage/iframe_alter_customer/id={{ d.list[i].cid }}">取消代理</a>
                </td>
            </tr>
        {{# } }}
    {/literal}
</script>

{include file='../__footer.tpl'} 