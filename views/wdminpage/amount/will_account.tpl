{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/will_account.js</i>

<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th> 
            	<input type="checkbox" id="check_all"/>
            </th>
            <th>用户名</th>
            <th>历史提现金额</th>
            <th>可提现额度</th>
            <th>待入账金额</th>

            <th>操作</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">
        {section name=ls loop=$list}
            <tr>
                <input type="hidden" id="client_id" value="{$list[ls].client_id}" />
                <td class="hidden">{$list[ls].cid}</td>
                <td>
                	 <input type="checkbox" name="check_list" data-id="{$list[ls].client_id}" data-openid="{$list[ls].client_wechat_openid}"/>
               	</td>
                <td>{$list[ls].client_nickname}</td>
                <td>{$list[ls].all_account}</td>
                <td>{$list[ls].account}</td>
                <td>{$list[ls].will_account}</td>

                <td>
                    <button class="us-edit" id="do_account">入账</button>
                </td>
            </tr>
        {/section}
    </tbody>
</table>
<input type="hidden" id="client_level" value="{$client_level}">

<div id="page" style="margin-top:10px;float:right;"></div>
{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
    {literal}
        {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
            <tr>
                <td class="hidden">{{ d.list[i].id }}</td>
                <td>
                    <input type="checkbox" name="check_list" data-id="{{ d.list[i].cid}}" data-openid="{{ d.list[i].client_wechat_openid }}"/>
                </td>
                <td>
                    {{# if(d.list[i].client_head == ''){ }}
                        <img class='ccl-head' src='{$docroot}static/images/login/profle_1.png' />
                    {{# }else{ }}
                        <img class='ccl-head' src='{{ d.list[i].client_head }}/64' />
                    {{#  } }}
                </td>
                <td>{{ d.list[i].client_name }}</td>
                <td>{{ d.list[i].client_sex }}</td>
                <td>{{ d.list[i].client_province }} {{ d.list[i].client_city}}</td>
                <td>{{ d.list[i].client_credit }}</td>
                <td>
                    {{# if(d.list[i].order_count == 0){ }}
                        {{ d.list[i].order_count}}
                    {{# }else{ }}
                        <a href="?/WdminPage/customer_profile/id={{ d.list[i].cid}}">{{ d.list[i].order_count}}</a>
                    {{#  } }}
                </td>
                <td>
                    {{# if(d.list[i].levelname && ''!=d.list[i].levelname && null!=d.list[i].levelname){ }}
                        {{ d.list[i].levelname}}
                    {{#  } }}
                </td>
                <td>
                    <a class="us-edit" href="?/WdminPage/iframe_alter_customer/id={{ d.list[i].cid }}">取消代理</a>
                </td>
            </tr>
        {{# } }}
    {/literal}
</script>

{include file='../__footer.tpl'} 