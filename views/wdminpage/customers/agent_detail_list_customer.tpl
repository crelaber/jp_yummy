{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/customers/list_agent.js</i>
<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th> 
            	<input type="checkbox" id="check_all"/>
            </th>
            <th>头像</th>
            <th>姓名</th>
            <th>性别</th>
            <th>省市</th>
            {if $type == 2}<th>带入人</th>{/if}
            <th>操作</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">
        {section name=ls loop=$list}
            <tr>
                <td class="hidden">{$list[ls].cid}</td>
                <td>
                	 <input type="checkbox" name="check_list" data-id="{$list[ls].cid}" data-openid="{$list[ls].client_wechat_openid}"/>
               	</td>
                <td><img class='ccl-head' src='{if $list[ls].client_head eq ''}{$docroot}static/images/login/profle_1.png{else}{$list[ls].client_head}/64{/if}' /></td>
                <td>{$list[ls].client_name}</td>
                <td>{$list[ls].client_sex}</td>
                <td>{$list[ls].client_province} {$list[ls].client_city}</td>
                {if $type == 2}<td>{$list[ls]['from_user'].client_name}</td>{/if}
                <td>
                     <a class="us-edit" href="{$docroot}?/WdminPage/customer_profile/id={$list[ls].client_id}">查看详情</a>
                </td>
            </tr>
        {/section}
    </tbody>
</table>
<input type="hidden" id="client_level" value="{$client_level}">

<div id="page" style="margin-top:10px;float:right;"></div>
{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
    {literal}
        {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
            <tr>
                <td class="hidden">{{ d.list[i].id }}</td>
                <td>
                    <input type="checkbox" name="check_list" data-id="{{ d.list[i].cid}}" data-openid="{{ d.list[i].client_wechat_openid }}"/>
                </td>
                <td>
                    {{# if(d.list[i].client_head == ''){ }}
                        <img class='ccl-head' src='{$docroot}static/images/login/profle_1.png' />
                    {{# }else{ }}
                        <img class='ccl-head' src='{{ d.list[i].client_head }}/64' />
                    {{#  } }}
                </td>
                <td>{{ d.list[i].client_name }}</td>
                <td>{{ d.list[i].client_sex }}</td>
                <td>{{ d.list[i].client_province }} {{ d.list[i].client_city}}</td>
                <td>{{ d.list[i].client_credit }}</td>
                <td>
                    {{# if(d.list[i].order_count == 0){ }}
                        {{ d.list[i].order_count}}
                    {{# }else{ }}
                        <a href="?/WdminPage/customer_profile/id={{ d.list[i].cid}}">{{ d.list[i].order_count}}</a>
                    {{#  } }}
                </td>
                <td>
                    {{# if(d.list[i].levelname && ''!=d.list[i].levelname && null!=d.list[i].levelname){ }}
                        {{ d.list[i].levelname}}
                    {{#  } }}
                </td>
                <td>
                    <a class="us-edit" href="?/WdminPage/iframe_alter_customer/id={{ d.list[i].cid }}">取消代理</a>
                </td>
            </tr>
        {{# } }}
    {/literal}
</script>

{include file='../__footer.tpl'} 