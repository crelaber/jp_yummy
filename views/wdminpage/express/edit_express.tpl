{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/shop_product.js</i>
<form style="padding:15px 20px;padding-bottom: 70px;" id="settingFrom">
    <input type="hidden" value="{$info.id}" id="express_id" />
    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>快递名字</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="name" name="name" value="{$info.name}" placeholder="快递名字" autofocus />
        </div>

        <div class="fv2Left">
            <span>快递编号</span>
        </div>
        <div class="fv2Right">
          <input type="text" class="gs-input-query" id="num" name="num" value="{$info.express_no}" placeholder="快递编号" autofocus />
        </div>

        <div class="fv2Left">
            <span>电话</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="tel" name="tel" value="{$info.tel}" placeholder="官方电话" autofocus />
        </div>

        <div class="fv2Left">
            <span>图标 URL</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="url" name="url" value="{$info.url}" placeholder=" 图标 URL" autofocus />
        </div>
    </div>

</form>



<div class="fix_bottom" style="position: fixed">
    <a class="wd-btn primary" id='express_save' data-id='{$info.id}' >{if $info.id > 0}保存{else}添加{/if}</a>
    <a id="express_del_btn" class="wd-btn default">删除</a>
    <a onclick="history.go(-1)" class="wd-btn default">返回</a>

</div>

{include file='../__loading.tpl'} 

{include file='../__footer.tpl'} 