<input type="hidden" value="{$groupId}" id="groupId"/>
<div style="width:550px;">
    <div class="orderwpa-top">
        <span class="orderwpa-amount" data-amount='{$data.order_amount}'>&yen;{$data.order_amount}</span>
        <span class="orderwpa-serial">
            订单状态：<span class="orderstatus {$data.status}">{$data.statusX}</span>
            <br />

            下单时间：{$data.order_time}
            <br />
            订单编号：{$data.serial_number} 
            <br />
            微信支付金额：￥{$data.pay_amount}
            <br />
            余额支付金额：￥{$data.balance_amount}

            <br />
            微支付号：{$data.wepay_serial}
        </span>
    </div>
    <div class="clearfix">
        {section name=od loop=$data.products}
            <div class='orderwpa-pdlist'>
                <img width="60px" height="60px" src="/uploads/product_hpic/{$data.products[od].catimg}" />
                <div style="margin-left: 70px;height: 60px;line-height: 20px;">
                    <div style="height:42px;overflow:hidden;padding-right: 2px;">{$data.products[od].product_name}</div>
                    <div style="margin-top:3px;">
                        <i class="opprice">&yen;{$data.products[od].product_discount_price}</i> &times; 
                        <i id="order{$data.order_id}count" class="opcount">{$data.products[od].product_count}</i>
                    </div>
                </div>
            </div>
        {/section}
    </div>
    <div class="orderwpa-address clearfix">
        <p>姓名：{$data.address.user_name}</p>
        <p>电话：{$data.address.phone}</p>
        <p>地址：{$data.address.province}{$data.address.city}{$data.address.area}{$data.address.address}</p>
        <p>身份证：{$data.address.identity}</p>
        <p>备注：{$data.notes}</p>
    </div>
    <div  class="orderwpa-address clearfix">

        <p>  余额:<input type="text" class="gs-input" id='balance_amount' value="{$data.balance_amount}" /><p>
        <p>  微信支付:<input type="text" class="gs-input" id='pay_amount' value="{$data.pay_amount}" /><p>

        <a data-id='{$data.order_id}' class="olbtn" id='refundBtn' style='margin:0;height:28px;line-height:28px;' href='javascript:;' data-orderid="{$data.order_id}">确认退款</a>
    </div>
</div>