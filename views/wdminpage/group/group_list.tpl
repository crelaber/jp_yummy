{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/account_log.js</i>

<div class="button-set">

</div>
<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th width="60" height="60">图片</th>
            <th>名字</th>
            <th>团长</th>
            <th>头像</th>
            <th>结束时间</th>
            <th>团人数</th>
            <th>已参人数</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">
    {foreach from=$list item=group}
            <tr>
                <td class="hidden"></td>
                <td ><img width="60" height="60" src="/uploads/product_hpic/{$group['productInfo']['catimg']}" /></td>
                <td width="50">{$group['productInfo']['product_name']}</td>
                <td>{$group['user']['client_name']}</td>
                <td><img width="60" height="60" src="{$group['user']['client_head']}/0" /></td>
                <td>{$group['end_time']}</td>
                <td>{$group['num']}</td>
                <td>{$group['joinCount']}</td>
                <td style="color: red">
                    {if $group['status'] == -1}
                        拼团失败
                    {/if}

                    {if $group['status'] == 1}
                        拼团成功
                    {/if}

                    {if $group['status'] == 0}
                        拼团中
                    {/if}
                </td>
                <td><a href="?/WdminPage/join_list/groupId={$group['id']}">查看参团详情</a></td>

            </tr>
    {/foreach}
    </tbody>
</table>
{$pageup}{$pagedown}{$pageconfig}



{include file='../__footer.tpl'} 