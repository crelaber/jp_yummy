{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/orders/orders_group.js</i>

<div class="button-set">

</div>
<table cellpadding=0 cellspacing=0 class="dTable" style="margin-top:45px;">
    <thead >
        <tr >
            <th class='hidden'> </th>
            <th>拼团状态</th>
            <th>订单编号</th>
            <th>订单状态</th>
            <th>收货人</th>
            <th>收货电话</th>
            <th>订单金额</th>
            <th>下单时间</th>
            <th>供应商</th>
            <th>操作</th>

        </tr>
    </thead>
    <tbody></tbody>
    <tbody style="margin-top:-45px;">

    <tr>
        <td class="hidden"></td>
        <td style="color: red">
            {if $group['status'] == -1}
                拼团失败
            {/if}

            {if $group['status'] == 1}
                拼团成功
            {/if}

            {if $group['status'] == 0}
                拼团中
            {/if}
        </td>
        <td>{$group['order']['serial_number']}</td>
        <td {if $group['order']['status'] == 'refunded'}style="color: red" {/if}>
            <a data-orderid="{$group['order']['order_id']}" class="queryRefund fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/FancyPage/queryRefund/id={$group['order']['order_id']}&groupId={$group['id']}">{$group['order']['statusX']}</a>
        </td>
        <td style="color: red">{$group['address']['user_name']}[团长]</td>
        <td>{$group['address']['phone']}</td>
        <td>{$group['order']['order_amount']}</td>
        <td>{$group['order']['order_time']}</td>
        <td style="color: red">
            <a class="various fancybox.ajax"
               data-orderid="{$group['order']['order_id']}"
               data-fancybox-type="ajax"
               href="{$docroot}?/WdminAjax/loadProviderOrderDetail/id={$group['order']['order_id']}&provider_id={$group['mSupplier']['id']}">{$group['mSupplier']['name']}</a>
        </td>

        <td> <a data-orderid="{$group['order']['order_id']}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/addOrderNotes/id={$group['order']['order_id']}">修改备注</a>
            | <a data-orderid="{$group['order']['order_id']}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/updateOrderStatus/id={$group['order']['order_id']}">修改状态</a>
            {if $group['status'] == -1}
            {if $group['order']['status'] != 'refunded'}

                |

                <a data-orderid="{$group['order']['order_id']}" class="orderRefund fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/FancyPage/orderRefund/id={$group['order']['order_id']}&groupId={$group['id']}">退款</a>
               {/if}
            {/if}
        </td>

    </tr>

    {foreach from=$list item=group}
            <tr>
                <td class="hidden"></td>
                <td style="color: red">
                {if $group['status'] == -1}
                    拼团失败
                {/if}

                {if $group['status'] == 1}
                    拼团成功
                {/if}

                {if $group['status'] == 0}
                    拼团中
                {/if}
                </td>
                <td>{$group['order']['serial_number']}</td>
                <td {if $group['order']['status'] == 'refunded'}style="color: red" {/if}>

                    <a data-orderid="{$group['order']['order_id']}" class="queryRefund fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/FancyPage/orderRefund/id={$group['order']['order_id']}&groupId={$group['id']}">{$group['order']['statusX']}</a>

                </td>
                <td>{$group['address']['user_name']}</td>
                <td>{$group['address']['phone']}</td>
                <td>{$group['order']['order_amount']}</td>
                <td>{$group['order']['order_time']}</td>
                <td style="color: red">
                    <a class="various fancybox.ajax"
                       data-orderid="{$group['order']['order_id']}"
                       data-fancybox-type="ajax"
                       href="{$docroot}?/WdminAjax/loadProviderOrderDetail/id={$group['order']['order_id']}&provider_id={$group['mSupplier']['id']}">{$group['mSupplier']['name']}</a>
                </td>

                <td> <a data-orderid="{$group['order']['order_id']}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/addOrderNotes/id={$group['order']['order_id']}">修改备注</a>
                    | <a data-orderid="{$group['order']['order_id']}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/updateOrderStatus/id={$group['order']['order_id']}">修改状态</a>
                {if $group['status'] == -1}
                    {if $group['order']['status'] != 'refunded'}
                    |
                      <a data-orderid="{$group['order']['order_id']}" class="orderRefund fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/FancyPage/orderRefund/id={$group['order']['order_id']}&groupId={$group['id']}">退款</a>
                    {/if}

                {/if}
                </td>

            </tr>
    {/foreach}
    </tbody>
</table>



{include file='../__footer.tpl'} 