{include file='../__header.tpl'}
{if $ed}
    <input type="hidden" value="{$prizeInfo.id}" id="pid" />
{/if}
<input type="hidden" value="{$mod}" id="mod" />
<i id="scriptTag">/static/script/Wdmin/health_clock/edit_prize.js?v={$smarty.now}</i>
<form id="pd-baseinfo" class='pt58'>
    <div style="padding: 22px;" class="clearfix">
        <input id="pd-catimg" name="cover_img" type="hidden" value="{$prizeInfo.cover_img}" />
        {foreach from=$prizeInfo.images item=pdi}
            <input class="pd-images" type="hidden" value="{$pdi.image_path}" data-sort="{$pdi.image_sort}" />
        {/foreach}

        <div class="clearfix">
            <div id="alterProductLeft">
                <!-- 奖品名称 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品名称</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="prize_name" value="{if $ed}{$prizeInfo.prize_name}{/if}" id="pd-form-title" autofocus/>
                    </div>
                </div>
                <!-- 奖品名称 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品简介</span>
                    </div>
                    <div class="fv2Right">
                        <textarea name="prize_desc" cols="48" rows="10">{if $ed}{$prizeInfo.prize_desc}{/if}</textarea>
                    </div>
                </div>

                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品类型</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="radio" name="prize_type" value="1" {if $prizeInfo.prize_type == 1 }checked{/if}{if !$ed}checked{/if}/> 普通奖品
                        <input type="radio" name="prize_type" value="2" {if $prizeInfo.prize_type == 2 }checked{/if}/> 满X减Y优惠券
                        <input type="radio" name="prize_type" value="3" {if $prizeInfo.prize_type == 3 }checked{/if}/> 固定面额优惠券
                        <input type="radio" name="prize_type" value="4" {if $prizeInfo.prize_type == 4 }checked{/if}/> 购物卡
                    </div>
                </div>
                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix prize_type prize_type_2" id="prize_type_sub_item_2">
                    <div class="fv2Left">
                        <span>满减值</span>
                    </div>
                    <div class="fv2Right margin-top">
                        满<input type="number" name="satisfy_money" value="{if $ed}{$prizeInfo.satisfy_money}{/if}"}/>
                        减<input type="number" name="coupon_money" value="{if $ed}{$prizeInfo.coupon_money}{/if}"}/>（单位分）
                    </div>
                </div>
                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix prize_type prize_type_3" id="prize_type_sub_item_3">
                    <div class="fv2Left">
                        <span>优惠券面值</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="number" name="coupon_value" value="{if $ed}{$prizeInfo.coupon_value}{/if}"}/>（单位分）
                    </div>
                </div>
                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix prize_type prize_type_4" id="prize_type_sub_item_4">
                    <div class="fv2Left">
                        <span>购物卡面值</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="number" name="shop_card_value" value="{if $ed}{$prizeInfo.shop_card_value}{/if}"}/>（单位分）
                    </div>
                </div>

                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品适用于</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="radio" name="is_to_user" value="1" {if $prizeInfo.is_to_user == 1 }checked{/if}{if !$ed}checked{/if}/> 中英用户
                        <input type="radio" name="is_to_user" value="0" {if $prizeInfo.is_to_user == 0 }checked{/if}/> 中英客户
                    </div>
                </div>
                <!-- 库存设置 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>最大奖品数</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="number" name="origin_stock" value="{if $ed}{$prizeInfo.origin_stock}{/if}"}/>
                    </div>
                </div>
                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品库存</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <input type="number" name="stock" value="{if $ed}{$prizeInfo.stock}{/if}"}/>
                    </div>
                </div>
                <!-- 优惠券类型 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>奖品月份</span>
                    </div>
                    <div class="fv2Right margin-top">
                        <select name="belong_time">
                            <option value="01"  {if $prizeInfo.belong_month == '01' }selected{/if}>1月</option>
                            <option value="02"  {if $prizeInfo.belong_month == '02' }selected{/if}>2月</option>
                            <option value="03"  {if $prizeInfo.belong_month == '03' }selected{/if}>3月</option>
                            <option value="04"  {if $prizeInfo.belong_month == '04' }selected{/if}>4月</option>
                            <option value="05"  {if $prizeInfo.belong_month == '05' }selected{/if}>5月</option>
                            <option value="06"  {if $prizeInfo.belong_month == '06' }selected{/if}>6月</option>
                            <option value="07"  {if $prizeInfo.belong_month == '07' }selected{/if}>7月</option>
                            <option value="08"  {if $prizeInfo.belong_month == '08' }selected{/if}>8月</option>
                            <option value="09"  {if $prizeInfo.belong_month == '09' }selected{/if}>9月</option>
                            <option value="10"  {if $prizeInfo.belong_month == '10' }selected{/if}>10月</option>
                            <option value="11"  {if $prizeInfo.belong_month == '11' }selected{/if}>11月</option>
                            <option value="12"  {if $prizeInfo.belong_month == '12' }selected{/if}>12月</option>
                        </select>
                    </div>
                </div>

                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>品牌信息</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="prize_brand" id="brand_info" value="{if $ed}{$prizeInfo.prize_brand}{/if}"/>
                    </div>
                </div>

                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>产地</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="place" id="place" value="{if $ed}{$prizeInfo.place}{/if}"/>
                    </div>
                </div>

                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>温馨提示</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="tips" id="tips" value="{if $ed}{$prizeInfo.tips}{/if}"/>
                    </div>
                </div>

                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>规格</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="specification" id="spec_attr" value="{if $ed}{$prizeInfo.specification}{/if}"/>
                    </div>
                </div>

                <!-- 排序 -->
                <div class="fv2Field clearfix">
                    <div class="fv2Left">
                        <span>排序</span>
                    </div>
                    <div class="fv2Right">
                        <input type="text" class="gs-input" name="sort" id="sort" value="{if $ed}{$prizeInfo.sort}{/if}"/>
                        <div class='fv2Tip'>数字越大越靠前</div>
                    </div>
                </div>

            </div>

            <div id="alterProductRight">
                <div class="t1">奖品首图</div>
                <!-- 奖品大图 -->
                <a class="pd-image-sec" data-id="0" href="javascript:;"></a>
                <div class="t2">建议使用500&#215;500尺寸图片 <a id="catimgPv" href="/uploads/product_hpic/1433819046557657a6967c4.jpeg">预览</a></div>
            </div>
        </div>
        <div class="fv2Field clearfix" style="max-width:100%;">
            <div class="fv2Left">
                <span>奖品图集</span>
            </div>
            <div class="fv2Right">
                <div id="pd-ilist" class="clearfix">
                    <a class="pd-image-sec ps20" data-id="1" href="javascript:;"></a>
                    <a class="pd-image-sec ps20" data-id="2" href="javascript:;"></a>
                    <a class="pd-image-sec ps20" data-id="3" href="javascript:;"></a>
                    <a class="pd-image-sec ps20" data-id="4" href="javascript:;"></a>
                    <a class="pd-image-sec ps20" data-id="5" href="javascript:;" style="margin-right: 0;"></a>
                </div>
                <div class='fv2Tip'>请进行图片选择</div>
            </div>
        </div>
        <div class="fv2Field clearfix" style="max-width:100%;">
            <div class="fv2Left">
                <span>详细介绍</span>
            </div>
            <div class="fv2Right">
                <script style='width:100%;' id="ueditorp" type="text/plain" name="note">{if $ed}{$prizeInfo.note}{/if}</script>
            </div>
        </div>
    </div>
</form>
<div class="fix_top fixed">
    <div class='button-set'>
        <a onclick="location.href = $('#http_referer').val();" class="button gray">返回</a>
        <a class='button' id="save_product_btn"  href="javascript:;">保存</a>
    </div>
</div>

{include file='../__footer.tpl'} 