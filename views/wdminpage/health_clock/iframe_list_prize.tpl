{include file='../__header.tpl'}
<link href="/static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="/static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">/static/script/Wdmin/health_clock/list_prize.js</i>
<div id="DataTables_Table_0_filter" class="dataTables_filter clearfix">
    <div class="search-w-box"><input type="text" class="searchbox" placeholder="输入搜索内容" /></div>
    <div class="button-set">
        <a class="button" href="?/HealthClockManage/editPrize/mod=add">添加奖品</a>
    </div>
</div>
<table class="dTable" style="margin-top: 50px;">
    <thead>
    <tr>
        <th>ID</th>
        <th>奖品名称</th>
        <th>奖品类型</th>
        <th>面向用户类型</th>
        <th>奖品归属月份</th>
        <th style="color: red;">奖品库存</th>
        <th>产品品牌</th>
        <th>添加时间</th>
        <th>更新时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
    {literal}
        {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
        <tr id='order-exp-{{ d.list[i].id }}'>
            <td>{{ d.list[i].id }}</td>
            <td>{{ d.list[i].prize_name }}</td>
            <td>{{ d.list[i].prize_type }}</td>
            <td>{{ d.list[i].is_to_user }}</td>
            <td>{{ d.list[i].belong_time }}</td>
            <td  style="color: red;">{{ d.list[i].stock }}</td>
            <td>{{ d.list[i].prize_brand }}</td>
            <td>{{ d.list[i].add_time }}</td>
            <td>{{ d.list[i].update_time }}</td>
            <td>
                <a class="pd-altbtn" href="?/HealthClockManage/editPrize/mod=edit&id={{ d.list[i].id }}" data-product-id="{{ d.list[i].id }}">编辑</a>&nbsp;
                <a href="javascript:;" onclick="parent.parent.window.open('?/vHealthClock/prizeDetail/id={{ d.list[i].id }}&from=preview');">预览</a>
                <a class="pd-altbtn pd-switchonline {{# if(d.list[i].is_online == 1){ }}tip{{# } }}" href="javascript:;" data-prize-id="{{ d.list[i].id }}" data-prize-online="{{ d.list[i].is_online }}">{{# if(d.list[i].is_online == 1){ }}下架{{# } }}{{# if(d.list[i].is_online == 0){ }}上架{{# } }}</a>&nbsp;
                <a class="pd-altbtn pd-del-btn del" href="javascript:;" data-product-id="{{ d.list[i].id }}">删除</a>
            </td>

        </tr>
        {{# } }}
    {/literal}
</script>



{include file='../__footer.tpl'}
