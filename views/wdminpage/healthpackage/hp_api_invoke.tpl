{include file='../__header.tpl'}
<link href="{$docroot}static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="{$docroot}static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/healthpackage/hp_api_invoke.js</i>
<table class="dTable" style="margin-top: 0px;">
    <thead>
        <tr>
			<th>ID</th>
			<th>订单编号</th>
			<th>康康接口状态</th>
			<th>插入卡号状态</th>
			<th>中英回调接口状态</th>
			<th>发送短信状态</th>
			<th>发送模板通知状态</th>
			<th>调用时间</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
	{literal}
		{{# for(var i = 0, len = d.list.length; i < len; i++){ }}
		    <tr id='order-exp-{{ d.list[i].id }}'>
                <td>{{ d.list[i].id }}</td>
                <td>{{ d.list[i].order_id }}</td>
                <td>{{ d.list[i].invoke_kk_card_api }}</td>
                <td>{{ d.list[i].insert_card_log }}</td>
                <td>{{ d.list[i].invoke_zy_callback_api }}</td>
                <td>{{ d.list[i].send_sms_to_user }}</td>
                <td>{{ d.list[i].send_new_order_notify_msg }}</td>
                <td>{{ d.list[i].add_time }}</td>
                <td>

				</td>

		    </tr>
		{{# } }}
	{/literal}
</script>



{include file='../__footer.tpl'} 
