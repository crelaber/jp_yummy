{include file='../__header.tpl'}
<link href="{$docroot}static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="{$docroot}static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/healthpackage/hp_hospital.js</i>
<table class="dTable" style="margin-top: 0px;">
    <thead>
        <tr>
			<th>ID</th>
			<th>省份</th>
			<th>城市</th>
			<th>运营类型</th>
			<th>体检单位</th>
			<th>体检地址</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
	{literal}
		{{# for(var i = 0, len = d.list.length; i < len; i++){ }}
		    <tr id='order-exp-{{ d.list[i].id }}'>
                <td>{{ d.list[i].id }}</td>
                <td>{{ d.list[i].province }}</td>
                <td>{{ d.list[i].city }}</td>
                <td>{{ d.list[i].operation_type }}</td>
                <td>{{ d.list[i].name }}</td>
                <td>{{ d.list[i].address }}</td>
                <td>

				</td>

		    </tr>
		{{# } }}
	{/literal}
</script>



{include file='../__footer.tpl'} 
