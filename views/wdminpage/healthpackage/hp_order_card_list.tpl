{include file='../__header.tpl'}
<link href="{$docroot}static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="{$docroot}static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/healthpackage/hp_order_card.js</i>
<table class="dTable" style="margin-top: 0px;">
    <thead>
        <tr>
			<th>订单编号</th>
			<th>卡号</th>
			<th>卡密</th>
			<th>客户号</th>
			<th>客户姓名</th>
			<th>套餐编码</th>
            <th>套餐名称</th>
            <th>套餐有效期</th>
			<th>套餐添加时间</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
	{literal}
		{{# for(var i = 0, len = d.list.length; i < len; i++){ }}
		    <tr id='order-exp-{{ d.list[i].id }}'>
                <td>{{ d.list[i].order_id }}</td>
                <td>{{ d.list[i].card_no }}</td>
                <td>{{ d.list[i].card_secret }}</td>
                <td>{{ d.list[i].partner_uid }}</td>
                <td>{{ d.list[i].partner_user_name }}</td>
                <td>{{ d.list[i].package_code }}</td>
                <td>{{ d.list[i].package_type }}</td>
                <td  style="color: red">{{ d.list[i].valid_time }}</td>
                <td>{{ d.list[i].add_time }}</td>
                <td>
				</td>

		    </tr>
		{{# } }}
	{/literal}
</script>



{include file='../__footer.tpl'} 
