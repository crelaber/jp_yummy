{include file='../__header.tpl'}
<link href="{$docroot}static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="{$docroot}static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/healthpackage/hp_order.js</i>

<div style="text-align: right;margin-top:15px;margin-bottom:5px;">
	<span style="font-size: 18px;float:left;margin-left:10px;">订单总数：<b id="total"></b></span>
    <button class="wd-btn date_btn {if $day == -1}primary{/if}" href='javascript:;' id="yesterday" data-diff="-1" data-status="{$status}">前一天</button>
    <button class="wd-btn date_btn {if $day == 0}primary{/if}" href='javascript:;' id="today" data-diff="0" data-status="{$status}">今天</a>
    <button class="wd-btn date_btn {if $day == 1}primary{/if}" href='javascript:;' id="tomorrow" data-diff="1" data-status="{$status}">后一天</a>
    <button class="wd-btn date_btn {if $day == 7}primary{/if}" href='javascript:;' id="yesterday" data-diff="7" data-status="{$status}">本周</button>
    <button class="wd-btn date_btn {if $day == 30}primary{/if}" href='javascript:;' id="yesterday" data-diff="30" data-status="{$status}">本月</button>
    <button class="wd-btn date_btn {if $day == 366}primary{/if}" href='javascript:;' id="yesterday" data-diff="366" data-status="{$status}">所有</button>
</div>


<ul class="nav nav-tabs">
	<li style="width: 10%" class="active">
		<a data-status='SUCC'>已支付</a>
	</li>
	<li style="width: 10%" >
		<a data-status='UNPAY'>未支付</a>
	</li>
	<li style="width: 10%" >
		<a data-status='REFUND'>已退款</a>
	</li>
	<li style="width: 10%">
		<a data-status='all'>全部</a>
	</li>
</ul>

<input type="hidden" value="{$day}" id="day">
<input type="hidden" value="{$status}" id="status">
<table class="dTable" style="margin-top: 0px;">
    <thead>
        <tr>
			<th>套餐名称</th>
			<th>套餐编码</th>
			<th>微信交易号</th>
			<th>订单序列号</th>
			<th>客户编号</th>
			<th>客户姓名</th>
            <th>手机</th>
            <th>客户性别</th>
			<th>婚姻状态</th>
			<th>支付金额</th>
			<th>体检城市</th>
            <th>下单时间</th>
            <th>支付时间</th>
            <th>支付状态</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
	{literal}
		{{# for(var i = 0, len = d.list.length; i < len; i++){ }}
		    <tr id='order-exp-{{ d.list[i].id }}'>
                <td>{{ d.list[i].package_info.name }}</td>
                <td>{{ d.list[i].package_code }}</td>
                <td>{{ d.list[i].bill_no }}</td>
                <td>{{ d.list[i].out_trade_no }}</td>
                <td>{{ d.list[i].partner_uid }}</td>
                <td>{{ d.list[i].partner_user_name }}</td>
                <td>{{ d.list[i].phone }}</td>
                <td>{{ d.list[i].user_sex }}</td>
                <td>{{ d.list[i].user_married_status }}</td>
                <td  style="color: red">{{ d.list[i].total_amount }}</td>
                <td>{{ d.list[i].city }}</td>
                <td>{{ d.list[i].order_time }}</td>
                <td>{{ d.list[i].paid_time }}</td>
                <td style="color: red">{{ d.list[i].status }}</td>
                <td>

				</td>

		    </tr>
		{{# } }}
	{/literal}
</script>



{include file='../__footer.tpl'} 
