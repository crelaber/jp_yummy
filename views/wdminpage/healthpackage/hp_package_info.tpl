{include file='../__header.tpl'}
<link href="{$docroot}static/script/DataTables/media/css/bootstrap.min.css" type="text/css" rel="Stylesheet" />
<link href="{$docroot}static/script/layui/layer/skin/layer.css" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/healthpackage/hp_package_info.js</i>
<table class="dTable" style="margin-top: 0px;">
    <thead>
        <tr>
			<th>ID</th>
			<th>套餐名称</th>
			<th>套餐描述</th>
			<th>售价(元)</th>
			<th>市场价(元)</th>
			<th>有效期</th>
            <th>添加时间</th>
            <th>操作</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<div id="page" style="margin-top:10px;float:right;"></div>

{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
	{literal}
		{{# for(var i = 0, len = d.list.length; i < len; i++){ }}
		    <tr id='order-exp-{{ d.list[i].id }}'>
                <td>{{ d.list[i].id }}</td>
                <td>{{ d.list[i].name }}</td>
                <td>{{ d.list[i].name_desc }}</td>
                <td style="color: red">{{ d.list[i].sale_price }}</td>
                <td>{{ d.list[i].market_price }}</td>
                <td>{{ d.list[i].expired_time_desc }}</td>
                <td>{{ d.list[i].add_time }}</td>
                <td>

				</td>

		    </tr>
		{{# } }}
	{/literal}
</script>



{include file='../__footer.tpl'} 
