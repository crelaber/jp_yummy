{strip}{if $olistcount == 0}0{else}
        <table class="dTable" style="margin-top:45px;">
            <thead>
                <tr>
                    <th>订单编号</th>
                    <th>收货人</th>
                    <th>收货电话</th>
                    <th>订单金额</th>
                    <th>运费</th>
                    <th>商品信息</th>
                    <th>商品数量</th>
                    <th>供应商信息</th>
                    <th>下单时间</th>
                    <th>操作</th>

                </tr>
            </thead>
            <tbody>
                {section name=oi loop=$orderlist}
                    <tr id='order-exp-{$orderlist[oi].order_id}'>
                        <td>{$orderlist[oi].serial_number}</td>
                        <td>{$orderlist[oi].address.user_name}</td>
                        <td>{$orderlist[oi].address.phone}</td>
                        <td class="prices font12">&yen;{$orderlist[oi].order_amount}</td>
                        <td class="prices font12">&yen;{$orderlist[oi].order_yunfei}</td>
                        <td>
                            <a class="od-list-pdinfo fancybox.ajax" 
                               data-fancybox-type="ajax" 
                               href="{$docroot}?/WdminAjax/loadOrderDetail/id={$orderlist[oi].order_id}">点击查看</a>
                        </td>
                        <td>{$orderlist[oi].product_count}件</td>
                        <td>
                            {foreach $orderlist[oi].providerList as $key => $val}
                                <a class="od-list-pdinfo fancybox.ajax"
                                   data-orderid="{$orderlist[oi].order_id}"
                                   data-fancybox-type="ajax"
                                   href="{$docroot}?/WdminAjax/loadProviderOrderDetail/id={$orderlist[oi].order_id}&provider_id={$val['supplier']['id']}"> {$val['supplier']['name']} </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            {/foreach}
                        </td>
                        <td>{$orderlist[oi].order_time}</td>

                       <td> <a data-orderid="{$orderlist[oi].order_id}" class="notes fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminAjax/updateOrderStatus/id={$orderlist[oi].order_id}">修改状态</a>
                        </td>
                    </tr>
                {/section}
            </tbody>
        </table>
{/if}{/strip}