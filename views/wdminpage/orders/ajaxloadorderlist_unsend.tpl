
        <table class="dTable" style="margin-top:45px;">
            <thead>
                <tr>
                    <th>订单编号</th>
                    <th>收货人</th>
                    <th>身份证</th>
                    <th>收货电话</th>
                    <th>收货地址</th>
                    <th>订单金额</th>
                    <th>运费</th>
                    <th>商品名字</th>
                    <th>商品数量</th>
                    <th>商品价格</th>
                    <th>下单时间</th>
                    <th>供应商</th>
                </tr>
            </thead>
            <tbody>
                {section name=oi loop=$orderlist}
                    <tr id='order-exp-{$orderlist[oi].order_id}'>
                        <td class="hidden">{$orderlist[oi].detail_id}</td>
                        <td class="od-exp-check"><input class='pd-exp-checks' type="checkbox" data-id='{$orderlist[oi].detail_id}' /></td>
                        <td>{$orderlist[oi].serial_number}

                        {if $orderlist[oi]['is_tuan'] == 1}
                        {if $orderlist[oi].group_type == 1}
                            <span style="color: red">拼团成功</span>
                        {/if}
                            {if $orderlist[oi].group_type == -1}
                                <span style="color: red">拼团失败</span>
                            {/if}
                            {if $orderlist[oi].group_type == 0}
                                <span style="color: red">拼团中</span>
                            {/if}
                          {/if}
                        </td>
                        <td>{$orderlist[oi].address.user_name}</td>
                        <td>{$orderlist[oi].address.identity}</td>
                        <td>{$orderlist[oi].address.phone}</td>
                        <td>{$orderlist[oi].address.province}{$orderlist[oi].address.city}{$orderlist[oi].address.area}{$orderlist[oi].address.address}</td>
                        <td class="prices font12">{$orderlist[oi].order_amount}</td>
                        <td class="prices font12">{$orderlist[oi].order_yunfei}</td>
                        <td>{$orderlist[oi].product.product_name}</td>
                        <td>{$orderlist[oi].product_count} </td>
                        <td>{$orderlist[oi].product.sale_prices}</td>
                        <td>{$orderlist[oi].order_time}</td>
                        <td>{$orderlist[oi].supplier.name}</td>
                    </tr>
                {/section}
            </tbody>
        </table>
