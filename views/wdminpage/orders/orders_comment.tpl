{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/orders/orders_comment.js</i>
<table class='dTable'  style="margin-top:45px;">
    <thead>
        <tr>
            <th>姓名</th>
            <th>评论分数</th>
            <th>标签</th>
            <th>内容</th>
            <th>时间</th>
            <th>订单号</th>
        
        </tr>
    </thead>
    <tbody>
        {foreach from=$list item=cm}
            <tr>
                <td>{$cm.nick_name}</td>
                <td>{$cm.score}</td>
                <td>{$cm.tag}</td>
                <td>{$cm.content}</td>
                <td>{$cm.time}</td>
                <td><a class="od-list-pdinfo fancybox.ajax" 
                           data-fancybox-type="ajax" 
                           href="{$docroot}?/WdminAjax/loadOrderDetail/id={$cm.order_id}">{$cm.order_id}</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>
{include file='../__footer.tpl'} 