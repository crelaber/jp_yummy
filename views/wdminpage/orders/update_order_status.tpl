<div style="width:200px">

    <div class="gs-label">状态</div>
    <div class="gs-text">
        <input type="hidden" value="{$order_id}" id="order_id" autofocus/>
    </div>


    <div class="gs-text">
       <select id="status">
           <!--
           <option value="unpay">未支付</option>
           <option value="payed">已经支付</option>
           <option value="canceled">已取消</option>

           -->
           <option value="received">已完成</option>
           <option value="delivering">配送中</option>
           <option value="closed">已关闭</option>
           <option value="refunded">已退款</option>

       </select>
    </div>
    <div class="center" style="margin-top: 15px">
        <a id="status_btn" href="javascript:;" class="wd-btn primary">提交</a>
    </div>
</div>