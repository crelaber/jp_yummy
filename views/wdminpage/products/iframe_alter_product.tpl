{include file='../__header.tpl'}
{if $ed}
    <input type="hidden" value="{$pd.product_id}" id="pid" />
{/if}
<link href="{$docroot}static/less/jquery.datetimepicker.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
<input type="hidden" value="{$mod}" id="mod" />
<input type="hidden" value="{$smarty.server.HTTP_REFERER}" id="http_referer" />
<i id="scriptTag">page_iframe_alter_product</i>
<form id="pd-baseinfo" class='pt58'>
<div style="padding: 22px;" class="clearfix">

<input id="pd-catimg" name="catimg" type="hidden" value="{$pd.catimg}" />
<input id="pd-serial-val" type="hidden" value="{$pd.product_serial}" />
<input id="pd-form-cat" type="hidden" value="{$cat}" />
{foreach from=$pd.images item=pdi}
    <input class="pd-images" type="hidden" value="{$pdi.image_path}" data-sort="{$pdi.image_sort}" />
{/foreach}

<div class="clearfix">

<div id="alterProductLeft">

<!-- 商品名称 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品名称</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_name" value="{if $ed}{$pd.product_name}{/if}" id="pd-form-title" autofocus/>
    </div>
</div>

<!-- 商品简称 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品简称</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_subname" id="pd-form-subname" value="{if $ed}{$pd.product_subname}{/if}"/>
    </div>
</div>

<!-- 商品编号 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品编号</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_code" onclick="this.select();" value="{if $ed}{$pd.product_code}{/if}" id="pd-form-code" />
    </div>
</div>

<!-- 商品简介 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品简介</span>
    </div>
    <div class="fv2Right">
        <span class="frm_textarea_box" style="width: 375px;"><textarea class="js_desc frm_textarea" id="pd-form-desc" name="product_subtitle">{if $ed}{$pd.product_subtitle}{/if}</textarea></span>
    </div>
</div>

<!-- 商品分类 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品分类</span>
    </div>
    <div class="fv2Right">
        {strip}
            <select id="pd-catselect" style="color:#000" name="product_cat" >
                {if $categorys|count > 0}
                    {foreach from=$categorys item=cat1}
                        <option value="{$cat1.dataId}" {if $cat1.hasChildren}disabled{/if}>{$cat1.name}</option>
                        {foreach from=$cat1.children item=cat2}
                            <option value="{$cat2.dataId}" {if $cat2.hasChildren}disabled{/if}>-- {$cat2.name}</option>
                            {foreach from=$cat2.children item=cat3}
                                <option value="{$cat3.dataId}" {if $cat3.hasChildren}disabled{/if}>---- {$cat3.name}</option>
                                {foreach from=$cat3.children item=cat4}
                                    <option value="{$cat4.dataId}" {if $cat4.hasChildren}disabled{/if}>------ {$cat4.name}</option>
                                {/foreach}
                            {/foreach}
                        {/foreach}
                    {/foreach}
                {else}
                    <option value="0">未分类</option>
                {/if}
            </select>
        {/strip}
    </div>
</div>

<!-- 商品品牌 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品品牌</span>
    </div>
    <div class="fv2Right">
        <select id="pd-serial" name="product_brand">
            <option value="0" {if $pd.product_brand eq 0}selected{/if}>默认</option>
            {foreach from=$brands item=ser}
                <option value="{$ser.id}" {if $pd.product_brand eq $ser.id}selected{/if}>{$ser.brand_name}</option>
            {/foreach}
        </select>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>是否推荐(首页显示)</span>
    </div>
    <div class="fv2Right">
        <select id="is_recommed" name="is_recommed">
            {if $pd.is_recommed == '0'}
                <option value="0" selected="selected">否</option>
                <option value="1">是</option>
            {else}
                <option value="0">否</option>
                <option value="1" selected="selected">是</option>
            {/if}
        </select>
    </div>
</div>

<!-- 供货商 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>运费模板</span>
    </div>
    <div class="fv2Right">
        <select id="yun_no" name="yun_no">
            {foreach from=$yunlist item=yun}
                <option value="{$yun.yun_no}" {if $pd.yun_no eq $yun.yun_no}selected{/if}>{$yun.yun_name}</option>
            {/foreach}
        </select>
    </div>
</div>

<!--  -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>邮寄方式</span>
    </div>
    <div class="fv2Right">
        <select id="yun_fa" name="yun_fa">
            {foreach from=$faList item=fa}     <option value="{$fa.yun_fa}" {if $pd.yun_fa eq $fa.yun_fa}selected{/if}>         {if $fa.yun_fa == 1}         日本直邮         {else if $fa.yun_fa == 2}         保税直发         {else if $fa.yun_fa == 3}         国内邮寄         {/if}     </option> {/foreach}

        </select>
    </div>
</div>

<!-- 运费模板 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>供应商</span>
    </div>
    <div class="fv2Right">
        <select id="provider_id" name="provider_id">
            {foreach from=$supplierlist item=supplier}
                <option value="{$supplier.id}" {if $pd.provider_id eq $supplier.id}selected{/if}>{$supplier.name}</option>
            {/foreach}
        </select>
    </div>
</div>




<!-- 分销折扣 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>分销比例</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" onclick="this.select();" value="{if $ed}{$pd.distri_count}{else}0.00{/if}" id="distri_count" name="distri_count" />
    </div>
</div>

<!-- cps折扣 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>CPS比例</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" onclick="this.select();" value="{if $ed}{$pd.cps}{else}0.00{/if}" id="cps" name="cps" />
    </div>
</div>

<!-- 商品价格 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品价格</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" onclick="this.select();" value="{if $ed}{$pd.sale_prices}{else}0.00{/if}" id="pd-form-prices" />
    </div>
</div>

<!-- 商品品牌 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>市场参考价</span>
    </div>
    <div class="fv2Right">
        <input type="hidden" id="pd-form-discount" value="{if $ed}{$pd.discount}{else}100{/if}"/>
        <input type="text" class="gs-input" name="market_price" onclick="this.select();" value="{if $ed and $pd.market_price}{$pd.market_price}{else}0.00{/if}" id="market_price" />
    </div>
</div>

    <!-- 促销tips -->
    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>特殊促销提醒</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input" name="sale_tips"  value="{if $ed and $pd.sale_tips}{$pd.sale_tips}{/if}" id="sale_tips" />
        </div>
    </div>

<!-- 商品重量 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品重量</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_weight" id="pd-form-weight" value="{if $ed > 0}{$pd.product_weight}{else}0{/if}"/>
        <div class='fv2Tip'>订单运费计算必备参数，单位 kg</div>
    </div>
</div>

<!-- 积分奖励 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>积分奖励(点)</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_credit" id="pd-form-credit" value="{if $ed > 0}{$pd.product_credit}{else}0{/if}"/>
        <div class='fv2Tip'>用户购买商品之后，奖励的积分数量</div>
    </div>
</div>
<!-- 积分奖励 -->

<!-- 商品运费 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>商品固定运费</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="product_expfee" id="pd-expfee" value="{if $ed}{$pd.product_expfee}{/if}"/>
        <div class='fv2Tip'>订单将以这个金额计算运费，默认为0</div>
    </div>
</div>

<!-- 排序 -->
<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>排序</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="sort" id="sort" value="{if $ed}{$pd.sort}{/if}"/>
        <div class='fv2Tip'>数字越大越靠前</div>
    </div>
</div>


<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>人群</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="people" id="people" value="{if $ed}{$pd.people}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>品牌信息</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="brand_info" id="brand_info" value="{if $ed}{$pd.brand_info}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>产地</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="place" id="place" value="{if $ed}{$pd.place}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>存储方法</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="save" id="save" value="{if $ed}{$pd.save}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>使用方法</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="user_method" id="user_method" value="{if $ed}{$pd.user_method}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>保质期</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="is_valid_time" id="is_valid_time" value="{if $ed}{$pd.is_valid_time}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>服务信息</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="service_info" id="service_info" value="{if $ed}{$pd.service_info}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>温馨提示</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="tips" id="tips" value="{if $ed}{$pd.tips}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>规格</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input" name="spec_attr" id="spec_attr" value="{if $ed}{$pd.spec_attr}{/if}"/>
    </div>
</div>

<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>快递方式</span>
    </div>
    <div class="fv2Right">
        <select id="yun_attr" name="yun_attr">
            <option value="日本直邮" {if $pd.yun_attr eq '日本直邮'}selected{/if}>日本直邮</option>
            <option value="法国拼邮" {if $pd.yun_attr eq '法国拼邮'}selected{/if}>法国拼邮</option>
            <option value="国内现货" {if $pd.yun_attr eq '国内现货'}selected{/if}>国内现货</option>
            <option value="法国直邮" {if $pd.yun_attr eq '法国直邮'}selected{/if}>法国直邮</option>
            <option value="日本拼邮" {if $pd.yun_attr eq '日本拼邮'}selected{/if}>日本拼邮</option>
            <option value="保税直发" {if $pd.yun_attr eq '保税直发'}selected{/if}>保税直发</option>
            <option value="澳新拼邮" {if $pd.yun_attr eq '澳新拼邮'}selected{/if}>澳新拼邮</option>


        </select>
    </div>
    <div class='fv2Tip'>商品信息里面的快递</div>

</div>


<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>促销小提示</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input mt10" name="shop_attr" id="shop_attr" value="{$pd.shop_attr}"/>
        <div class='fv2Tip'> 7-10天到货 满300 包邮</div>
    </div>
</div>




<div class="fv2Field clearfix">
    <div class="fv2Left">
        <span>快递信息</span>
    </div>
    <div class="fv2Right">
        <input type="text" class="gs-input mt10" name="kd_attr" id="kd_attr" value="{$pd.kd_attr}"/>
        <div class='fv2Tip'>商品信息里面的快递</div>
    </div>
</div>


<div class="fv2Field clearfix">

    <span>商品设置</span>

    <div class="fv2Right">
        <select id="hd_active" name="type">
            <option value="0" {if $pd.type eq 0}selected{/if}>无</option>
            <option value="1" {if $pd.type eq 1}selected{/if}>秒杀</option>
            <option value="2" {if $pd.type eq 2}selected{/if}>拼团</option>

        </select>
        <div id="ms"  style="margin-top: 10px;{if $pd.type == '0'} display: none{/if}">


            <input type="text" class="gs-input mt10" name="start_time" id="start_time" value="{$pd.start_time}"/>
            <div class='fv2Tip'> 开始时间 例如：(2015/07/20 00:00:00)</div>

            <input type="text" class="gs-input mt10" name="kill_time" id="kill_time" value="{$pd.kill_time}"/>
            <div class='fv2Tip'> 结束时间 例如：(2015/07/30 00:00:00)</div>


        </div>





        <div id="tg"  style="margin-top: 10px;{if $pd.type != '2'} display: none{/if}">

            <input type="text" class="gs-input mt10" name="num" id="num" value="{$pd.num}"/>
            <div class='fv2Tip'>人数，单位：个</div>

            <input type="text" class="gs-input mt10" name="group_price" id="group_price" value="{$pd.group_price}"/>
            <div class='fv2Tip'>拼团产品价格，单位：元</div>


            <input type="text" class="gs-input mt10" name="is_active" id="is_active" value="{$pd.is_active}"/>
            <div class='fv2Tip'>0代表预热，1代表正式开始</div>

        </div>


        <div id="div_price">
            <!--
                        {if $pd.type == '2' || $pd.type == '1'}
                        <input type="text" class="gs-input mt10" name="price"  value="{$pd.price}" />
                        {/if}
                         <div class='fv2Tip'>活动价格，单位：元</div>
                            -->
        </div>



    </div>
</div>
</div>

<div id="alterProductRight">
    <div class="t1">商品首图</div>
    <!-- 商品大图 -->
    <a class="pd-image-sec" data-id="0" href="javascript:;"></a>
    <div class="t2">建议使用500&#215;500尺寸图片 <a id="catimgPv" href="/uploads/product_hpic/1433819046557657a6967c4.jpeg">预览</a></div>
</div>

</div>

<div class="fv2Field clearfix" style="max-width:100%;">
    <div class="fv2Left">
        <span>商品规格</span>
    </div>
    <div class="fv2Right">
        <div class="button-set l pt0">
            {*                    <a id='pd-spec-edit-btn' class="button fancybox.ajax" data-fancybox-type="ajax" href="{$docroot}?/WdminPage/ajax_alter_product_spec_detail/id={$pd.product_id}">选择</a>*}
            <!-- 规格增加按钮 -->
            <a class="button" id="pd-spec-add" href="javascript:;">添加</a>
            <!-- 规格管理按钮 -->
            <a class="button orange" onclick="$('#__specmanage', parent.parent.document).get(0).click();">规格管理</a>
        </div>
        <table id='pd-spec-frame' class="{if $pd.specs|count eq 0}hidden1{/if}">
            <thead>
            <tr><th style="width: 180px;">规格</th><th style="width: 180px;">规格</th><th>售价</th><th>市场价</th><th>库存</th><th style="width: 40px;">操作</th></tr>
            </thead>
            <tbody>
            <tr class="specselect hidden" data-id="#">
                <td>
                    <select class="spec1">
                        <option value="0">无规格</option>
                        {foreach from=$speclist item=spec}
                            {foreach from=$spec.dets item=dets}
                                <option value="{$dets.id}" data-spec="{$spec.id}" data-name="{$spec.spec_name}">{$spec.spec_name}({$dets.det_name})</option>
                            {/foreach}
                        {/foreach}
                    </select>
                </td>
                <td>
                    <select class="spec2">
                        <option value="0">无规格</option>
                        {foreach from=$speclist item=spec}
                            {foreach from=$spec.dets item=dets}
                                <option value="{$dets.id}" data-spec="{$spec.id}" data-name="{$spec.spec_name}">{$spec.spec_name}({$dets.det_name})</option>
                            {/foreach}
                        {/foreach}
                    </select>
                </td>
                <td><input type="text" onclick="this.select();" class="pd-spec-prices" value="{$specs.sale_price}"></td>
                <td><input type="text" onclick="this.select();" class="pd-spec-market" value="{$specs.market_price}"></td>
                <td><input type="text" onclick="this.select();" class="pd-spec-stock" value="{$specs.instock}"></td>
                <td><a class="btn-delete-spectr" href="javascript:;">删除</a></td>
            </tr>
            {foreach from=$pd.specs item=specs}
                <tr class="specselect" data-id="{$specs.id}">
                    <td>
                        <select class="spec1">
                            <option value="0">无规格</option>
                            {foreach from=$speclist item=spec}
                                {foreach from=$spec.dets item=dets}
                                    <option value="{$dets.id}" data-spec="{$spec.id}" data-name="{$spec.spec_name}" {if $specs.id1 eq $dets.id}selected{/if}>{$spec.spec_name}({$dets.det_name})</option>
                                {/foreach}
                            {/foreach}
                        </select>
                    </td>
                    <td>
                        <select class="spec2">
                            <option value="0">无规格</option>
                            {foreach from=$speclist item=spec}
                                {foreach from=$spec.dets item=dets}
                                    <option value="{$dets.id}" data-spec="{$spec.id}" data-name="{$spec.spec_name}" {if $specs.id2 eq $dets.id}selected{/if}>{$spec.spec_name}({$dets.det_name})</option>
                                {/foreach}
                            {/foreach}
                        </select>
                    </td>
                    <td><input type="text" onclick="this.select();" class="pd-spec-prices" value="{$specs.sale_price}"></td>
                    <td><input type="text" onclick="this.select();" class="pd-spec-market" value="{$specs.market_price}"></td>
                    <td><input type="text" onclick="this.select();" class="pd-spec-stock" value="{$specs.instock}"></td>
                    <td><a class="btn-delete-spectr" href="javascript:;">删除</a></td>
                </tr>
                {*                            <tr>
                <td>{$specs.name1}</td>
                <td>{$specs.name2}</td>
                <td><input type="text" onclick="this.select();" class="pd-spec-prices" data-id="{$specs.id1}-{$specs.id2}" value="{$specs.sale_price}"></td>
                <td><input type="text" onclick="this.select();" class="pd-spec-market" data-id="{$specs.id1}-{$specs.id2}" value="{$specs.market_price}"></td>
                <td>0</td>
                </tr>*}
            {/foreach}
            </tbody>
        </table>
        <div class='fv2Tip'>请点击添加 新增一项规格</div>
    </div>
</div>

<div class="fv2Field clearfix" style="max-width:100%;">
    <div class="fv2Left">
        <span>商品图集</span>
    </div>
    <div class="fv2Right">
        <div id="pd-ilist" class="clearfix">
            <a class="pd-image-sec ps20" data-id="1" href="javascript:;"></a>
            <a class="pd-image-sec ps20" data-id="2" href="javascript:;"></a>
            <a class="pd-image-sec ps20" data-id="3" href="javascript:;"></a>
            <a class="pd-image-sec ps20" data-id="4" href="javascript:;"></a>
            <a class="pd-image-sec ps20" data-id="5" href="javascript:;" style="margin-right: 0;"></a>
        </div>
        <div class='fv2Tip'>请进行图片选择</div>
    </div>
</div>

<div class="fv2Field clearfix" style="max-width:100%;">
    <div class="fv2Left">
        <span>详细介绍</span>
    </div>
    <div class="fv2Right">
        <script style='width:100%;' id="ueditorp" type="text/plain" name="product_desc">{if $ed}{$pd.product_desc}{/if}</script>
    </div>
</div>

</div>

</form>
<div class="fix_top fixed">
    <div class='button-set'>
        <a onclick="location.href = $('#http_referer').val();" class="button gray">返回</a>
        {if $mod ne 'add'}
            <a class="button red pd-del-btn" data-product-id="{$pd.product_id}">删除</a>
            <a id="fetch_product_btn" onclick="javscript:;" class="button orange fancybox.ajax" data-fancybox-type="ajax"
               href="{$docroot}?/WdminPage/ajax_fetch_data/">抓取数据</a>
        {/if}
        <a class='button' id="save_product_btn"  href="javascript:;">保存</a>
    </div>
</div>

{include file='../__footer.tpl'} 