{include file='../__header.tpl'}
<link href="{$docroot}static/less/jquery.datetimepicker.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
<i id="scriptTag">{$docroot}static/script/Wdmin/settings/alter_store.js</i>

<form style="padding:15px 20px;padding-bottom: 70px;" id="settingFrom">

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>店铺名称</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input" name="store_name" id="name" value="{$store.store_name}" placeholder="请输入店铺名称" autofocus/>
        </div>
    </div>

    <input type="hidden" name="id" value="{$store.id}" />
    <input type="hidden" name="store_province" value="{$store_province}" placeholder="上海市" />
    <input type="hidden" name="store_city" value="{$store_city}" placeholder="上海市" />
    <input type="hidden" name="store_area" value="{$store_area}" placeholder="浦东新区" />
    <input type="hidden" name="admin_openids" value="{$store.admin_openids}" />

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>店铺地址</span>
        </div>
        <div class="fv2Right">
            {$store.store_city}{$store.store_area}
            <input type="text" class="gs-input" name="store_address" id="address" value="{$store.store_address}" placeholder="请输入店铺地址" autofocus />
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>服务半径</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input" name="service_range" id="range" value="{$store.service_range}" placeholder="请设置服务半径（公里）" autofocus />公里
            <input type="text" class="gs-input" name="range_desc" value="{$store.range_desc}" placeholder="服务范围说明" autofocus/>
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>店铺管理人员</span>
        </div>
    </div>

    <div class="fv2Field clearfix" style="max-width: 100%;">
        <div class="fv2Right" id="admin_ids" style="margin-left: 0;">
        {foreach from=$admins item=usr}
            {if $usr.client_wechat_openid neq ''}
            <div class="usrItem" data-openid="{$usr.client_wechat_openid}"><b></b>
                <img src="{$usr.client_head}/132" />
                <span class="Elipsis">{$usr.client_nickname}</span>
            </div>
            {/if}
        {/foreach}
            <a class="usrItem add fancybox.ajax" id="add-admin" href="{$docroot}?/WdminAjax/ajax_customer_select/" data-fancybox-type="ajax">
                <i></i><span>点击添加</span>
            </a>
        </div>
    </div>


    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>位置地图</span>
        </div>
        <div class="fv2Right">
            <div class="clearfix">
                <div class="alter-cat-img">
                    <div id="loading" style="transition-duration: .2s;"></div>
                    <img id="catimage" src="{$map}" />
                </div>
            </div>
        </div>
    </div>

</form>

<div class="fix_bottom" style="position: fixed">
    <a class="wd-btn primary" id='saveBtn' data-id='{$store.id}' href="javascript:;">{if $store.id > 0}保存{else}添加{/if}</a>
    <a onclick="history.go(-1)" class="wd-btn default">返回</a>
</div>

<script id="t:usrlist" type="text/html">
    {literal}
    <%for(var i=0;i<list.length;i++){%>
    <div class="usrItem" data-openid="<%=list[i].openid%>"><b></b>
        <img src="<%=list[i].src%>" /><span class="Elipsis"><%=list[i].uname%></span>
    </div>
    <%}%>
    {/literal}
</script>

{include file='../__footer.tpl'}