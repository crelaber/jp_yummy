{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/settings/stores.js</i>
<div id="list">
    <div id="DataTables_Table_0_filter" class="dataTables_filter clearfix">
        <div class="button-set">
            <a class="button gray" href="javascript:;" onclick='location.reload()'>刷新</a>
            <a class="button blue" id='add_cate_product' href="?/WdminPage/alter_store/">添加店铺</a>
        </div>
    </div>
    <table class="dTable">
        <thead>
            <tr>
                <th style='width:100px'>店铺名称</th>
                <th>店铺地址</th>
                <th>店铺经度</th>
                <th>店铺纬度</th>
                <th>服务半径</th>
                <th class="center">操作</th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$stores item=store}
                <tr>
                    <td>{$store.store_name}</td>
                    <td>{$store.store_city}，{$store.store_area}，{$store.store_address}</td>
                    <td>{$store.store_longitude|string_format:"%.4f"}</td>
                    <td>{$store.store_latitude|string_format:"%.4f"}</td>
                    <td>{$store.service_range|string_format:"%.2f"}公里</td>
                    <td class="center">
                        <a class="lsBtn" href="?/WdminPage/alter_store/id={$store.id}">编辑</a>
                        <a class="lsBtn del store_del" data-id="{$store.id}" href="javascript:;">删除</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>
<div class="fix_bottom" style="position: fixed">
</div>
{include file='../__footer.tpl'} 