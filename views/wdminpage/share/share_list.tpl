{include file='../__header.tpl'}
<i id="scriptTag">page_share_list</i>

<div id="products-stock-list" style="margin-bottom: 54px;">

    <table class="dTable">
        <thead>
            <tr>
                <th class="hidden"></th>
                <th class="od-exp-check"><input class="checkAll" type="checkbox" /></th>
                <th>分享人</th>
                <th>分享时间</th>
                <th>领取人数</th>
                <th>是否过期</th>
                <th>分享类型</th>
                <th>分享金额(订单类型)</th>
                <th>分享人优惠金额</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<input type="hidden" id="month-select" value="" />

{*分页,id必须命名为page*}
<div id="page" style="float:right;"></div>
{*laytpl模版展示数据区域*}
<script id="data_render" type="text/html">
    {literal}
        {{# for(var i = 0, len = d.list.length; i < len; i++){ }}
        <tr id='stock-exp-{{ d.list[i].id}}'>
            <td class="hidden">{{ d.list[i].id}}</td>
            <td class="od-exp-check"><input class='pd-exp-checks' type="checkbox" data-id='{{ d.list[i].id }}'/></td>
            <td>{{ d.list[i].uinfo.client_name }}</td>
            <td>{{ d.list[i].add_time_format }}</td>
            <td>{{ d.list[i].share_count }}</td>
            <td>
                {{# if(d.list[i].is_valid == 1){ }}
                    <span style="color:red">过期</span>
                {{# }else{ }}
                    有效
                {{#  } }}
            </td>
            <td>
                {{# if(d.list[i].type == 0){ }}
                    用户中心
                {{# }else{ }}
                    订单类型
                {{#  } }}
            </td>
            <td>{{ d.list[i].share_money }}</td>
            <td>{{ d.list[i].coupon_value }}</td>

            <td>
                <a class="various fancybox.ajax" data-fancybox-type="ajax" href="?/WdminAjax/detailShare/id={{ d.list[i].id }}">详情</a>&nbsp;&nbsp;
            </td>
        </tr>
        {{# } }}
    {/literal}
</script>

{include file='../__footer.tpl'} 