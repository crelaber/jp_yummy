{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/stat/overview.js</i>
<div class="cleafix">

    <div id="ovw-left">

        <div id="dTb">
            <table class="ovw-table">
                <tr>
                    <td width="50%">
                        <span>{$Datas.newuser}<b>人</b></span>
                        <span>今日新增</span>
                    </td>
                    <td width="50%" class="clickable" onclick="$('#__uslist:eq(0)', parent.document).get(0).click();">
                        <span><i id="usersum">{$Datas.alluser}</i><b>人</b></span>
                        <span>会员总数</span>
                    </td>
                </tr>

            </table>
            <table class="ovw-table">
                <tr>
                    <td>
                        <span class="pricesx">&yen;{$Datas.saleyestoday}<b>元</b></span>
                        <span>昨日成交额</span>
                    </td>
                    <td>
                        <span class="pricesx">&yen;{$Datas.saletoday}<b>元</b></span>
                        <span>今日成交额</span>
                    </td>
                    <td>
                        <span class="pricesx">&yen;{$Datas.newYorders}<b>单</b></span>
                        <span>昨日订单数</span>
                    </td>
                    <td>
                        <span class="pricesx">&yen;{$Datas.newTorders}<b>单</b></span>
                        <span>今日订单数</span>
                    </td>
                </tr>


            </table>
        </div>

        <div class='stat-h-50'></div>

        <div class='stat-h-50'></div>
    </div>
</div>
{include file='../__footer.tpl'} 