{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/shop_product.js</i>
<form style="padding:15px 20px;padding-bottom: 70px;" id="settingFrom">
    <input type="hidden" value="{$info.id}" id="supplier_id" />
    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>供应商名字</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="name" name="amount" value="{$info.name}" placeholder="供应商名字" autofocus />
        </div>

        <br />
        <div class="fv2Left">
            <span>所在地</span>
        </div>
        <div class="fv2Right">
          <input type="text" class="gs-input-query" id="city" name="city" value="{$info.city}" placeholder="所在城市" autofocus />
        </div>

        <br />
        <div class="fv2Left">
            <span>管理员</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="admin_name" name="admin_name" value="{$info.admin_name}" placeholder=" 管理员名字 最好英文" autofocus />
        </div>
    </div>

</form>



<div class="fix_bottom" style="position: fixed">
    <a class="wd-btn primary" id='supplier_save' data-id='{$info.id}' >{if $info.id > 0}保存{else}添加{/if}</a>
    <a id="sup_del_btn" class="wd-btn default">删除</a>
    <a onclick="history.go(-1)" class="wd-btn default">返回</a>

</div>

{include file='../__loading.tpl'} 

{include file='../__footer.tpl'} 