{include file='../__header.tpl'}

<i id="scriptTag">{$docroot}static/script/Wdmin/amount/shop_product.js</i>
<form style="padding:15px 20px;padding-bottom: 70px;" id="settingFrom">
    <input type="hidden" value="{$yun_no}" id="yun_no" />
    <input type="hidden" value="{$info.id}" id="yun_id" />
    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>区域名字</span>
        </div>
        <div class="fv2Right">

            <select id="city_name" name="city_name">
                {foreach from=$citylist item=city}
                    <option value="{$city.cityname}" {if $info.city_name eq $city.cityname}selected{/if}>{$city.cityname}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>首重</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="first_w" name="first_w" value="{$info.first_w}" placeholder="" autofocus />
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>价格</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="first_p" name="first_p" value="{$info.first_p}" placeholder="" autofocus />
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>次重</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="second_w" name="second_w" value="{$info.second_w}" placeholder="" autofocus />
        </div>
    </div>

    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>价格</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="second_p" name="second_p" value="{$info.second_p}" placeholder="" autofocus />
        </div>
    </div>

</form>



<div class="fix_bottom" style="position: fixed">
    <a class="wd-btn primary" id='yun_cost_save' data-id='{$info.id}' >{if $info.id > 0}保存{else}添加{/if}</a>

    {if $info != ''}
        <a class="wd-btn primary" id='yun_cost_del' data-id='{$info.id}' >删除</a>

    {/if}
</div>



{include file='../__loading.tpl'} 

{include file='../__footer.tpl'} 