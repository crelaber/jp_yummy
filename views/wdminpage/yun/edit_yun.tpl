{include file='../__header.tpl'}
<i id="scriptTag">{$docroot}static/script/Wdmin/amount/shop_product.js</i>
<form style="padding:15px 20px;padding-bottom: 70px;" id="settingFrom">
    <input type="hidden" value="{$info.yun_no}" id="yun_id" />
    <div class="fv2Field clearfix">
        <div class="fv2Left">
            <span>运费模板名字</span>
        </div>
        <div class="fv2Right">
            <input type="text" class="gs-input-query" id="yun_name" name="amount" value="{$info.yun_name}" placeholder="运费模板名字" autofocus />
        </div>

        <div class="fv2Left">
            <span>快递名字</span>
        </div>

        <div class="fv2Right">
            <select id="express" name="express">
                {foreach from=$expressList item=express}
                    <option value="{$express.id}" {if $info.express_name eq $express.name}selected{/if}>{$express.name}</option>
                {/foreach}
            </select>
        </div>
    </div>

</form>



<div class="fix_bottom" style="position: fixed">
    <a class="wd-btn primary" id='yun_save' data-id='{$info.yun_no}' >{if $info.yun_no > 0}保存{else}添加{/if}</a>

    {if $info != ''}
        <a class="wd-btn primary" id='yun_del' data-id='{$info.id}' >删除</a>

    {/if}
</div>

{include file='../__loading.tpl'} 

{include file='../__footer.tpl'} 