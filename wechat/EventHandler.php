<?php

/**
 * 微信公众号事件推送处理
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

/**
 * @property Wechat $wc Wechat
 */
class EventHandler extends WechatHandler {

    public function run($postObj) {
        global $config;
        /**
         * 处理二维码扫描事件
         */
        date_default_timezone_set('PRC');
        error_log("=======Event==========".$postObj->Event);
        if ($postObj->Event == "subscribe" || $postObj->Event == "SCAN") {

            $id =   preg_replace("/qrscene\_/is", "", $postObj->EventKey);
            $qr = $this->wc->Db->getOneRow("SELECT * FROM `group_qr_code` WHERE `id` = $id;");
            if($qr){

                if($qr['type'] == 'group'){
                    //group
                    $productId = $qr['one_info'];
                    $productInfo = $this->wc->Db->getOneRow(sprintf("SELECT po.*,ps.sale_prices,ps.discount FROM `products_info` po LEFT JOIN `product_onsale` ps ON po.product_id = ps.product_id WHERE po.product_id = '%s';", $productId), false);
                    $title = $productInfo['product_name'];
                    $url =   $this->serverRoot."?/Group/grouping/pid=".$productId."&id=".$qr['qr_value'] ;
                    $picUrl = $this->serverRoot."/uploads/product_hpic/".$productInfo['catimg'];
                    $desc = $productInfo['product_name'].$productInfo['num']."人团 快来抢购吧!";
                    $this->wc->responseMsg($title,$url,$picUrl,$desc);
                }else{
                    //user

                }
            }
        }


     }

}
